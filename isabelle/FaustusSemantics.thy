theory FaustusSemantics
imports Main MList SList ListTools Semantics SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star"
begin

type_synonym Identifier = ValueId

datatype FaustusPubKey = ConstantPubKey PubKey
  | UsePubKey Identifier

fun less_eq_FaustusPubKey :: "FaustusPubKey \<Rightarrow> FaustusPubKey \<Rightarrow> bool" where
"less_eq_FaustusPubKey (ConstantPubKey a) (UsePubKey b) = True" |
"less_eq_FaustusPubKey (UsePubKey a) (ConstantPubKey b) = False" |
"less_eq_FaustusPubKey (ConstantPubKey a) (ConstantPubKey b) = (a \<le> b)" |
"less_eq_FaustusPubKey (UsePubKey a) (UsePubKey b) = (a \<le> b)"

fun less_FaustusPubKey :: "FaustusPubKey \<Rightarrow> FaustusPubKey \<Rightarrow> bool" where
"less_FaustusPubKey a b = (\<not> (less_eq_FaustusPubKey b a))"

instantiation "FaustusPubKey" :: "ord"
begin
definition "a \<le> b = less_eq_FaustusPubKey a b"
definition "a < b = less_FaustusPubKey a b"
instance
proof
qed
end

lemma linearFaustusPubKey : "x \<le> y \<or> y \<le> (x::FaustusPubKey)"
  by (cases x y rule: FaustusPubKey.exhaust[case_product FaustusPubKey.exhaust], auto simp add: less_eq_FaustusPubKey_def)

instantiation "FaustusPubKey" :: linorder
begin
instance
proof
  fix x y
  have "(x < y) = (x \<le> y \<and> \<not> y \<le> (x :: FaustusPubKey))"
    by (meson less_FaustusPubKey.elims less_FaustusPubKey.elims less_FaustusPubKey_def less_eq_FaustusPubKey_def linearFaustusPubKey)
  thus "(x < y) = (x \<le> y \<and> \<not> y \<le> x)" by simp
next
  fix x
  have "x \<le> (x :: FaustusPubKey)" by (meson linearFaustusPubKey)
  thus "x \<le> x" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> (z :: FaustusPubKey)"
    by (cases x y z rule: FaustusPubKey.exhaust[case_product FaustusPubKey.exhaust[case_product FaustusPubKey.exhaust]], auto simp add: less_eq_FaustusPubKey_def)
  thus "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FaustusPubKey)"
    by (cases x y rule: FaustusPubKey.exhaust[case_product FaustusPubKey.exhaust], auto simp add: less_eq_FaustusPubKey_def)
  thus "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FaustusPubKey)" by simp
next
  fix x y
  from linearFaustusPubKey have "x \<le> y \<or> y \<le> (x :: FaustusPubKey)" by simp
  thus "x \<le> y \<or> y \<le> x" by simp
qed
end

datatype FaustusPayee = Account FaustusPubKey
  | Party FaustusPubKey

type_synonym FaustusParty = FaustusPubKey

datatype FaustusChoiceId = FaustusChoiceId ChoiceName FaustusParty

(* BEGIN Proof of linorder for FaustusChoiceId *)
fun less_eq_FaustusChoId :: "FaustusChoiceId \<Rightarrow> FaustusChoiceId \<Rightarrow> bool" where
"less_eq_FaustusChoId (FaustusChoiceId a b) (FaustusChoiceId c d) =
   (if a < c then True
    else (if (a > c) then False else b \<le> d))"

fun less_FaustusChoId :: "FaustusChoiceId \<Rightarrow> FaustusChoiceId \<Rightarrow> bool" where
"less_FaustusChoId a b = (\<not> (less_eq_FaustusChoId b a))"

instantiation "FaustusChoiceId" :: "ord"
begin
definition "a \<le> b = less_eq_FaustusChoId a b"
definition "a < b = less_FaustusChoId a b"
instance
proof
qed
end

lemma linearFaustusChoiceId : "x \<le> y \<or> y \<le> (x::FaustusChoiceId)"
  by (cases x y rule: FaustusChoiceId.exhaust[case_product FaustusChoiceId.exhaust], auto simp add: less_eq_FaustusChoiceId_def)

instantiation "FaustusChoiceId" :: linorder
begin
instance
proof
  fix x y
  have "(x < y) = (x \<le> y \<and> \<not> y \<le> (x :: FaustusChoiceId))"
    by (meson less_FaustusChoId.elims less_FaustusChoId.elims less_FaustusChoiceId_def less_eq_FaustusChoiceId_def linearFaustusChoiceId)
  thus "(x < y) = (x \<le> y \<and> \<not> y \<le> x)" by simp
next
  fix x
  have "x \<le> (x :: FaustusChoiceId)" by (meson linearFaustusChoiceId)
  thus "x \<le> x" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> (z :: FaustusChoiceId)"
    by (cases x y z rule: FaustusChoiceId.exhaust[case_product FaustusChoiceId.exhaust[case_product FaustusChoiceId.exhaust]], auto simp add: less_eq_FaustusChoiceId_def split: if_split_asm)
  thus "x \<le> y \<Longrightarrow> y \<le> z \<Longrightarrow> x \<le> z" by simp
next
  fix x y z
  have "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FaustusChoiceId)"
    by (cases x y rule: FaustusChoiceId.exhaust[case_product FaustusChoiceId.exhaust], auto simp add: less_eq_FaustusChoiceId_def split: if_split_asm)
  thus "x \<le> y \<Longrightarrow> y \<le> x \<Longrightarrow> x = (y :: FaustusChoiceId)" by simp
next
  fix x y
  from linearFaustusChoiceId have "x \<le> y \<or> y \<le> (x :: FaustusChoiceId)" by simp
  thus "x \<le> y \<or> y \<le> x" by simp
qed
end
(* END Proof of linorder for FaustusChoiceId *)

datatype FaustusValue = AvailableMoney FaustusPubKey Token
               | Constant int
               | NegValue FaustusValue
               | AddValue FaustusValue FaustusValue
               | SubValue FaustusValue FaustusValue
               | MulValue FaustusValue FaustusValue
               | Scale int int FaustusValue
               | ChoiceValue FaustusChoiceId
               | SlotIntervalStart
               | SlotIntervalEnd
               | UseValue Identifier
               | Cond FaustusObservation FaustusValue FaustusValue
and FaustusObservation = AndObs FaustusObservation FaustusObservation
                     | OrObs FaustusObservation FaustusObservation
                     | NotObs FaustusObservation
                     | ChoseSomething FaustusChoiceId
                     | ValueGE FaustusValue FaustusValue
                     | ValueGT FaustusValue FaustusValue
                     | ValueLT FaustusValue FaustusValue
                     | ValueLE FaustusValue FaustusValue
                     | ValueEQ FaustusValue FaustusValue
                     | UseObservation Identifier
                     | TrueObs
                     | FalseObs

datatype FaustusParameter = ValueParameter Identifier
  | ObservationParameter Identifier
  | PubKeyParameter Identifier

datatype FaustusArgument = ValueArgument FaustusValue
  | ObservationArgument FaustusObservation
  | PubKeyArgument FaustusPubKey

datatype FaustusAction = Deposit FaustusPubKey FaustusPubKey Token FaustusValue
                | Choice FaustusChoiceId "Bound list"
                | Notify FaustusObservation

datatype FaustusContract = Close
             | Pay FaustusPubKey FaustusPayee Token FaustusValue FaustusContract
             | If FaustusObservation FaustusContract FaustusContract
             | When "FaustusCase list" Timeout FaustusContract
             | Let Identifier FaustusValue FaustusContract
             | LetObservation Identifier FaustusObservation FaustusContract
             | LetPubKey Identifier FaustusPubKey FaustusContract
             | Assert FaustusObservation FaustusContract
             | LetC Identifier "FaustusParameter list" FaustusContract FaustusContract
             | UseC Identifier "FaustusArgument list"
and FaustusCase = Case FaustusAction FaustusContract

fun caseToAction :: "FaustusCase \<Rightarrow> FaustusAction" where
"caseToAction (Case act cont) = act"

fun caseToContract :: "FaustusCase \<Rightarrow> FaustusContract" where
"caseToContract (Case act cont) = cont"

lemma caseToContract_Size_LT:
"\<And>cases tCont x.
       x \<in> set cases \<Longrightarrow>
       size (caseToContract x) < size_list size cases"
  apply auto
  by (metis FaustusCase.exhaust FaustusCase.size(2) add.commute add.left_neutral add_Suc caseToContract.simps lessI size_list_estimation)

fun faustusContractSize :: "FaustusContract \<Rightarrow> nat" and
  faustusCasesSize :: "FaustusCase list \<Rightarrow> nat" where
"faustusContractSize Close = 1"|
"faustusContractSize (Pay accId payee tok val cont) = 1 + faustusContractSize cont"|
"faustusContractSize (If obs trueCont falseCont) = 1 + faustusContractSize trueCont + faustusContractSize falseCont"|
"faustusContractSize (When cases t tCont) = 1 + faustusContractSize tCont + faustusCasesSize cases"|
"faustusContractSize (Let vid val cont) = 1 + faustusContractSize cont"|
"faustusContractSize (LetObservation obsId obs cont) = 1 + faustusContractSize cont"|
"faustusContractSize (Assert obs cont) = 1 + faustusContractSize cont"|
"faustusContractSize (LetC cid params boundCon cont) = 1 + size params + faustusContractSize boundCon + faustusContractSize cont"|
"faustusContractSize (UseC cid args) = 1 + size args"|
"faustusContractSize (LetPubKey pkId pk cont) = 1 + faustusContractSize cont"|
"faustusCasesSize Nil = 0"|
"faustusCasesSize ((Case act cont) # cs) = (faustusContractSize cont + 1) + faustusCasesSize cs"

lemma faustusContractSize_GT0:
"faustusContractSize con > 0"
  apply (cases "con")
  by auto

datatype AbstractionTypeInformation = ValueType
  | ObservationType
  | PubKeyType
  | ContractType "FaustusParameter list"

(* Stack of type information. Will allow static scoping type checking. *)
type_synonym TypeContext = "(Identifier \<times> AbstractionTypeInformation) list"

fun paramsToTypeContext :: "FaustusParameter list \<Rightarrow> TypeContext" where
"paramsToTypeContext [] = []" |
"paramsToTypeContext (ValueParameter i#rest) = (i, ValueType)#(paramsToTypeContext rest)" |
"paramsToTypeContext (ObservationParameter i#rest) = (i, ObservationType)#(paramsToTypeContext rest)" |
"paramsToTypeContext (PubKeyParameter i#rest) = (i, PubKeyType)#(paramsToTypeContext rest)"

datatype AbstractionInformation = ValueAbstraction Identifier
  | ObservationAbstraction Identifier
  | PubKeyAbstraction Identifier
  | ContractAbstraction "Identifier \<times> FaustusParameter list \<times> FaustusContract"

(* Stack of abstraction information. Will allow static scoping in execution and compilation. *)
type_synonym FaustusContext = "AbstractionInformation list"

fun paramsToFaustusContext :: "FaustusParameter list \<Rightarrow> FaustusContext" where
"paramsToFaustusContext [] = []" |
"paramsToFaustusContext (ValueParameter vid#rest) = (ValueAbstraction vid)#(paramsToFaustusContext rest)" |
"paramsToFaustusContext (ObservationParameter obsId#rest) = (ObservationAbstraction obsId)#(paramsToFaustusContext rest)" |
"paramsToFaustusContext (PubKeyParameter pkId#rest) = (PubKeyAbstraction pkId)#(paramsToFaustusContext rest)"

record FaustusState = accounts :: Accounts
  choices :: "(ChoiceId \<times> ChosenNum) list"
  boundValues :: "(Identifier \<times> int) list"
  boundPubKeys :: "(Identifier \<times> PubKey) list"
  minSlot :: Slot

fun valid_faustus_state :: "FaustusState \<Rightarrow> bool" where
"valid_faustus_state state = (valid_map (accounts state)
                     \<and> valid_map (choices state)
                     \<and> valid_map (boundValues state)
                     \<and> valid_map (boundPubKeys state))"

(* Type rules require the identifier is most recently associated with the correct type in the stack.*)
fun wellTypedPubKey :: "TypeContext \<Rightarrow> FaustusPubKey \<Rightarrow> bool" where
"wellTypedPubKey tyCtx (ConstantPubKey pk) = True" |
"wellTypedPubKey [] (UsePubKey pkId) = False" |
"wellTypedPubKey ((boundPkId, ty)#rest) (UsePubKey pkId) = ((boundPkId = pkId \<and> ty = PubKeyType) \<or> (boundPkId \<noteq> pkId \<and> wellTypedPubKey rest (UsePubKey pkId)))"

fun wellTypedPayee :: "TypeContext \<Rightarrow> FaustusPayee \<Rightarrow> bool" where
"wellTypedPayee tyCtx (Account pk) = wellTypedPubKey tyCtx pk" |
"wellTypedPayee tyCtx (Party pk) = wellTypedPubKey tyCtx pk"

fun wellTypedChoiceId :: "TypeContext \<Rightarrow> FaustusChoiceId \<Rightarrow> bool" where
"wellTypedChoiceId tyCtx (FaustusChoiceId cname pk) = wellTypedPubKey tyCtx pk"

fun wellTypedValue :: "TypeContext \<Rightarrow> FaustusValue \<Rightarrow> bool" and
  wellTypedObservation :: "TypeContext \<Rightarrow> FaustusObservation \<Rightarrow> bool" where
"wellTypedValue tyCtx (AvailableMoney pk token) = wellTypedPubKey tyCtx pk" |
"wellTypedValue tyCtx (Constant integer) = True" |
"wellTypedValue tyCtx (NegValue val) = wellTypedValue tyCtx val" |
"wellTypedValue tyCtx (AddValue lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedValue tyCtx (SubValue lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedValue tyCtx (MulValue lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedValue tyCtx (Scale n d rhs) = wellTypedValue tyCtx rhs" |
"wellTypedValue tyCtx (ChoiceValue choId) = (wellTypedChoiceId tyCtx choId)" |
"wellTypedValue tyCtx (SlotIntervalStart) = True" |
"wellTypedValue tyCtx (SlotIntervalEnd) = True" |
"wellTypedValue [] (UseValue valId) = False" |
"wellTypedValue ((boundValId, ty)#rest) (UseValue valId) = ((boundValId = valId \<and> ty = ValueType) \<or> (boundValId \<noteq> valId \<and> wellTypedValue rest (UseValue valId)))" |
"wellTypedValue tyCtx (Cond cond thn els) = (wellTypedObservation tyCtx cond \<and> wellTypedValue tyCtx thn \<and> wellTypedValue tyCtx els)" |
"wellTypedObservation tyCtx (AndObs lhs rhs) = ((wellTypedObservation tyCtx lhs) \<and> (wellTypedObservation tyCtx rhs))" |
"wellTypedObservation tyCtx (OrObs lhs rhs) = ((wellTypedObservation tyCtx lhs) \<and> (wellTypedObservation tyCtx rhs))" |
"wellTypedObservation tyCtx (NotObs subObs) = wellTypedObservation tyCtx subObs" |
"wellTypedObservation tyCtx (ChoseSomething choId) = (wellTypedChoiceId tyCtx choId)" |
"wellTypedObservation tyCtx (ValueGE lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedObservation tyCtx (ValueGT lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedObservation tyCtx (ValueLT lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedObservation tyCtx (ValueLE lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedObservation tyCtx (ValueEQ lhs rhs) = ((wellTypedValue tyCtx lhs) \<and> (wellTypedValue tyCtx rhs))" |
"wellTypedObservation [] (UseObservation obsId) = False" |
"wellTypedObservation ((boundObsId, ty)#rest) (UseObservation obsId) = ((boundObsId = obsId \<and> ty = ObservationType) \<or> (boundObsId \<noteq> obsId \<and> wellTypedObservation rest (UseObservation obsId)))" |
"wellTypedObservation tyCtx TrueObs = True" |
"wellTypedObservation tyCtx FalseObs = True"

fun wellTypedArgument :: "TypeContext \<Rightarrow> FaustusParameter \<Rightarrow> FaustusArgument \<Rightarrow> bool" where
"wellTypedArgument tyCtx (ValueParameter vid) (ValueArgument val) = wellTypedValue tyCtx val" |
"wellTypedArgument tyCtx (ObservationParameter obsId) (ObservationArgument obs) = wellTypedObservation tyCtx obs" |
"wellTypedArgument tyCtx (PubKeyParameter pkId) (PubKeyArgument pk) = wellTypedPubKey tyCtx pk" |
"wellTypedArgument tyCtx p a = False"

fun wellTypedArguments :: "TypeContext \<Rightarrow> FaustusParameter list \<Rightarrow> FaustusArgument list \<Rightarrow> bool" where
"wellTypedArguments tyCtx [] [] = True" |
"wellTypedArguments tyCtx p [] = False" |
"wellTypedArguments tyCtx [] a = False" |
"wellTypedArguments tyCtx (firstParam#restParams) (firstArg#restArgs) = (wellTypedArgument tyCtx firstParam firstArg \<and> wellTypedArguments tyCtx restParams restArgs)"

fun lookupContractIdParamTypeInformation :: "TypeContext \<Rightarrow> Identifier \<Rightarrow> (FaustusParameter list \<times> TypeContext) option" where
"lookupContractIdParamTypeInformation [] cid = None" |
"lookupContractIdParamTypeInformation ((i, ty)#restTy) cid = (if i = cid
  then case ty of
    ContractType params \<Rightarrow> Some (params, restTy)
    | _ \<Rightarrow> None
  else lookupContractIdParamTypeInformation restTy cid)"

fun wellTypedContract :: "TypeContext \<Rightarrow> FaustusContract \<Rightarrow> bool" and
  wellTypedCases :: "TypeContext \<Rightarrow> FaustusCase list \<Rightarrow> bool" where
"wellTypedContract tyCtx Close = True" |
"wellTypedContract tyCtx (Pay accId payee token val cont) =
  (wellTypedPubKey tyCtx accId \<and> wellTypedPayee tyCtx payee \<and> wellTypedValue tyCtx val \<and> wellTypedContract tyCtx cont)" |
"wellTypedContract tyCtx (If obs cont1 cont2) =
  (wellTypedObservation tyCtx obs \<and> wellTypedContract tyCtx cont1 \<and> wellTypedContract tyCtx cont2)" |
"wellTypedContract tyCtx (When cases t cont) = (wellTypedCases tyCtx cases \<and> wellTypedContract tyCtx cont)" |
"wellTypedContract tyCtx (Assert obs cont) = (wellTypedObservation tyCtx obs \<and> wellTypedContract tyCtx cont)" |
"wellTypedContract tyCtx (Let valId val cont) = (wellTypedValue tyCtx val \<and> wellTypedContract ((valId, ValueType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetObservation obsId obs cont) = (wellTypedObservation tyCtx obs \<and> wellTypedContract ((obsId, ObservationType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetPubKey pkId pk cont) = (wellTypedPubKey tyCtx pk \<and> wellTypedContract ((pkId, PubKeyType)#tyCtx) cont)" |
"wellTypedContract tyCtx (LetC cid params body cont) = (wellTypedContract ((paramsToTypeContext params)@tyCtx) body \<and> wellTypedContract ((cid, ContractType params)#tyCtx) cont)" |
"wellTypedContract tyCtx (UseC cid args) = (case lookupContractIdParamTypeInformation tyCtx cid of
  Some (params, innerTyCtx) \<Rightarrow> wellTypedArguments tyCtx params args
  | _ \<Rightarrow> False)" |
"wellTypedCases tyCtx [] = True" |
"wellTypedCases tyCtx ((Case (Deposit fromPk toPk token value) c)#rest) =
  ((wellTypedPubKey tyCtx fromPk) \<and> (wellTypedPubKey tyCtx toPk) \<and> (wellTypedValue tyCtx value) \<and> (wellTypedContract tyCtx c) \<and> (wellTypedCases tyCtx rest))" |
"wellTypedCases tyCtx ((Case (Choice choiceId bounds) c)#rest) = ((wellTypedChoiceId tyCtx choiceId) \<and> (wellTypedContract tyCtx c) \<and> (wellTypedCases tyCtx rest))" |
"wellTypedCases tyCtx ((Case (Notify observation) c)#rest) = ((wellTypedObservation tyCtx observation) \<and> (wellTypedContract tyCtx c) \<and> (wellTypedCases tyCtx rest))"


(* Execution context and type context must match through the course of a program.
  If the execution context is larger, then there is not enough type information to type the program.
  If the type context is larger, then there is not enough meaning information to run a well-typed program.*)
fun wellTypedFaustusContext :: "TypeContext \<Rightarrow> FaustusContext \<Rightarrow> bool" where
"wellTypedFaustusContext [] [] = True" |
"wellTypedFaustusContext tyCtx [] = False" |
"wellTypedFaustusContext [] ctx = False" |
"wellTypedFaustusContext ((tyValId, ValueType)#restTypes) ((ValueAbstraction absValId)#restAbstractions) = 
  (tyValId = absValId \<and> wellTypedFaustusContext restTypes restAbstractions)" |
"wellTypedFaustusContext ((tyPkId, PubKeyType)#restTypes) ((PubKeyAbstraction absPkId)#restAbstractions) = 
  (tyPkId = absPkId \<and> wellTypedFaustusContext restTypes restAbstractions)" |
"wellTypedFaustusContext ((tyObsId, ObservationType)#restTypes) ((ObservationAbstraction absObsId)#restAbstractions) = 
  (tyObsId = absObsId \<and> wellTypedFaustusContext restTypes restAbstractions)" |
"wellTypedFaustusContext ((tyCid, ContractType tyParams)#restTypes) ((ContractAbstraction (absCid, absParams, absBody))#restAbstractions) = 
  (tyCid = absCid \<and> tyParams = absParams \<and> wellTypedContract ((paramsToTypeContext absParams)@restTypes) absBody \<and> wellTypedFaustusContext restTypes restAbstractions)" |
"wellTypedFaustusContext (someType#restTypes) (someAbs#restAbs) = False"

lemma consValueWellTypedContext:
"wellTypedFaustusContext tyCtx ctx \<longleftrightarrow>
  wellTypedFaustusContext ((valId, ValueType)#tyCtx) (ValueAbstraction valId#ctx)"
  by simp

lemma consObservationWellTypedContext:
"wellTypedFaustusContext tyCtx ctx \<longleftrightarrow>
  wellTypedFaustusContext ((obsId, ObservationType)#tyCtx) (ObservationAbstraction obsId#ctx)"
  by simp

lemma consPubKeyWellTypedContext:
"wellTypedFaustusContext tyCtx ctx \<longleftrightarrow>
  wellTypedFaustusContext ((pkId, PubKeyType)#tyCtx) (PubKeyAbstraction pkId#ctx)"
  by simp

lemma consContractWellTypedContext:
"wellTypedFaustusContext tyCtx ctx \<and> wellTypedContract ((paramsToTypeContext params)@tyCtx) bodyCont \<longleftrightarrow>
  wellTypedFaustusContext ((cid, ContractType params)#tyCtx) (ContractAbstraction (cid, params, bodyCont)#ctx)"
  by auto

lemma paramsWellTypedContext:
"wellTypedFaustusContext (paramsToTypeContext params) (paramsToFaustusContext params)"
proof (induction params)
  case Nil
  then show ?case by auto
next
  case (Cons firstParam params)
  then show ?case by (cases firstParam, auto)
qed

lemma wellTypedPubKeyAppendTypes:
"wellTypedPubKey tyCtx pk \<Longrightarrow>
  wellTypedPubKey (tyCtx @ tyCtx2) pk"
proof (induction tyCtx)
  case Nil
  then show ?case by (cases pk, auto)
next
  case (Cons firstType tyCtx)
  then show ?case by (cases pk firstType rule: FaustusPubKey.exhaust[case_product prod.exhaust], auto)
qed

lemma wellTypedPayeeAppendTypes:
"wellTypedPayee tyCtx payee \<Longrightarrow>
  wellTypedPayee (tyCtx @ tyCtx2) payee"
proof (induction tyCtx)
  case Nil
  then show ?case
    by (metis wellTypedPayee.elims(2) wellTypedPayee.simps(1) wellTypedPayee.simps(2) wellTypedPubKeyAppendTypes)
next
  case (Cons firstType tyCtx)
  then show ?case by (metis FaustusPayee.exhaust wellTypedPayee.simps(1) wellTypedPayee.simps(2) wellTypedPubKeyAppendTypes)
qed

lemma wellTypedChoiceIdAppendTypes:
"wellTypedChoiceId tyCtx choiceId \<Longrightarrow>
  wellTypedChoiceId (tyCtx @ tyCtx2) choiceId"
  apply (induction tyCtx, auto)
   apply (cases choiceId, auto)
  using wellTypedPubKeyAppendTypes apply fastforce
  by (metis append_Cons wellTypedChoiceId.elims(3) wellTypedChoiceId.simps wellTypedPubKeyAppendTypes)

lemma wellTypedValueAppendTypes:
"(\<forall> tyCtx2 . wellTypedValue tyCtx val \<longrightarrow>
  wellTypedValue (tyCtx @ tyCtx2) val)"
"(\<forall> tyCtx2 . wellTypedObservation tyCtx obs \<longrightarrow>
  wellTypedObservation (tyCtx @ tyCtx2) obs)"
  by (induction rule: wellTypedValue_wellTypedObservation.induct, auto simp add: wellTypedChoiceIdAppendTypes wellTypedPubKeyAppendTypes)

lemma wellTypedArgumentAppendTypes:
"(\<forall> tyCtx2 . wellTypedArgument tyCtx param arg \<longrightarrow>
  wellTypedArgument (tyCtx @ tyCtx2) param arg)"
  by (induction rule: wellTypedArgument.induct, auto simp add: wellTypedValueAppendTypes wellTypedPubKeyAppendTypes)

lemma wellTypedArgumentsAppendTypes:
"(\<forall> tyCtx2 . wellTypedArguments tyCtx params args \<longrightarrow>
  wellTypedArguments (tyCtx @ tyCtx2) params args)"
  by (induction rule: wellTypedArguments.induct, auto simp add: wellTypedArgumentAppendTypes)

lemma lookupCidAppendTypes:
"(\<forall> tyCtx2 restTy. lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdParamTypeInformation (tyCtx@tyCtx2) cid = Some (params, restTy@tyCtx2))"
proof (induction rule: lookupContractIdParamTypeInformation.induct)
  case (1 cid)
  then show ?case by auto
next
  case (2 i ty restTy cid)
  then show ?case by (cases ty, auto)
qed

lemma wellTypedContractAppendTypes:
"(\<forall> tyCtx2 . wellTypedContract tyCtx cont \<longrightarrow>
  wellTypedContract (tyCtx @ tyCtx2) cont)"
"(\<forall> tyCtx2 . wellTypedCases tyCtx cases \<longrightarrow>
  wellTypedCases (tyCtx @ tyCtx2) cases)"
proof (induction rule: wellTypedContract_wellTypedCases.induct)
  case (1 tyCtx)
  then show ?case by auto
next
  case (2 tyCtx accId payee token val cont)
  then show ?case by (simp add: wellTypedPayeeAppendTypes wellTypedPubKeyAppendTypes wellTypedValueAppendTypes(1))
next
  case (3 tyCtx obs cont1 cont2)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
next
  case (4 tyCtx cases t cont)
  then show ?case by simp
next
  case (5 tyCtx obs cont)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
next
  case (6 tyCtx valId val cont)
  then show ?case by (simp add: wellTypedValueAppendTypes(1))
next
  case (7 tyCtx obsId obs cont)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
next
  case (8 tyCtx pkId pk cont)
  then show ?case by (simp add: wellTypedPubKeyAppendTypes)
next
  case (9 tyCtx cid params body cont)
  then show ?case by auto
next
  case (10 tyCtx cid args)
  then show ?case by (cases "lookupContractIdParamTypeInformation tyCtx cid", auto simp add: lookupCidAppendTypes wellTypedArgumentsAppendTypes)
next
  case (11 tyCtx)
  then show ?case by simp
next
  case (12 tyCtx fromPk toPk token val c rest)
  then show ?case by (simp add: wellTypedPubKeyAppendTypes wellTypedValueAppendTypes(1))
next
  case (13 tyCtx choiceId bounds c rest)
  then show ?case by (simp add: wellTypedChoiceIdAppendTypes)
next
  case (14 tyCtx observation c rest)
  then show ?case by (simp add: wellTypedValueAppendTypes(2))
qed

lemma concatWellTypedContexts:
"wellTypedFaustusContext tyCtx1 ctx1 \<Longrightarrow>
  wellTypedFaustusContext tyCtx2 ctx2 \<Longrightarrow>
  wellTypedFaustusContext (tyCtx2 @ tyCtx1) (ctx2 @ ctx1)"
  apply (induction tyCtx2 ctx2 rule: wellTypedFaustusContext.induct, auto)
  using wellTypedContractAppendTypes(1) by fastforce

(* If an identifier has a type, it should be in the state. *)
fun wellTypedState :: "TypeContext \<Rightarrow> FaustusState \<Rightarrow> bool" where
"wellTypedState [] s = True" |
"wellTypedState ((valId, ValueType)#restTypes) s = (member valId (boundValues s) \<and> wellTypedState restTypes s)" |
"wellTypedState ((obsId, ObservationType)#restTypes) s = (member obsId (boundValues s) \<and> wellTypedState restTypes s)" |
"wellTypedState ((pkId, PubKeyType)#restTypes) s = (member pkId (boundPubKeys s) \<and> wellTypedState restTypes s)" |
"wellTypedState ((cid, ContractType params)#restTypes) s = wellTypedState restTypes s"

lemma insertValueWellTypedStateIsWellTyped:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState tyCtx (s\<lparr>boundValues := MList.insert vid val (boundValues s)\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by auto
next
  case (Cons firstType tyCtx)
  then show ?case proof (cases firstType)
    case (Pair identifier ty)
    then show ?thesis using Cons apply (cases ty, auto)
       apply (metis MList.insert_lookup_Some insert_lookup_different)
      by (metis MList.insert_lookup_Some insert_lookup_different)
  qed
qed

lemma insertAppendValueWellTypedStateIsWellTyped:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState ((vid, ValueType)#tyCtx) (s\<lparr>boundValues := MList.insert vid val (boundValues s)\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by (simp add: MList.insert_lookup_Some)
next
  case (Cons firstTy tyCtx)
  then show ?case proof (cases firstTy)
    case (Pair i ty)
    then show ?thesis using Cons.IH Cons.prems insertValueWellTypedStateIsWellTyped wellTypedState.elims(2) by fastforce
  qed
qed

lemma insertAppendObservationWellTypedStateIsWellTyped:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState ((vid, ObservationType)#tyCtx) (s\<lparr>boundValues := MList.insert vid val (boundValues s)\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by (simp add: MList.insert_lookup_Some)
next
  case (Cons firstTy tyCtx)
  then show ?case proof (cases firstTy)
    case (Pair i ty)
    then show ?thesis using Cons.IH Cons.prems insertValueWellTypedStateIsWellTyped wellTypedState.elims(2) by fastforce
  qed
qed

lemma insertPubKeyWellTypedStateIsWellTyped:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState tyCtx (s\<lparr>boundPubKeys := MList.insert pkId pk (boundPubKeys s)\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by auto
next
case (Cons firstType tyCtx)
  then show ?case proof (cases firstType)
    case (Pair identifier ty)
    then show ?thesis using Cons apply (cases ty, auto)
      by (metis MList.insert_lookup_Some insert_lookup_different)
  qed
qed

lemma insertAppendPubKeyWellTypedStateIsWellTyped:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState ((vid, PubKeyType)#tyCtx) (s\<lparr>boundPubKeys := MList.insert vid val (boundPubKeys s)\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by (simp add: MList.insert_lookup_Some)
next
  case (Cons firstTy tyCtx)
  then show ?case proof (cases firstTy)
    case (Pair i ty)
    then show ?thesis using Cons.IH Cons.prems insertPubKeyWellTypedStateIsWellTyped wellTypedState.elims by fastforce
  qed
qed

lemma wellTypedStateAccountsArbitrary:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState tyCtx (s\<lparr>accounts := newAccounts\<rparr>)"
proof (induction tyCtx)
  case Nil
  then show ?case by auto
next
  case (Cons firstType tyCtx)
  then show ?case proof (cases firstType)
    case (Pair identifier ty)
    then show ?thesis using Cons by (cases ty, auto)
  qed
qed

lemma wellTypedStateCombineTypeContexts:
"wellTypedState tyCtx s \<Longrightarrow>
  wellTypedState tyCtx2 s \<Longrightarrow>
  wellTypedState (tyCtx @ tyCtx2) s"
proof (induction tyCtx)
  case Nil
  then show ?case by auto
next
  case (Cons firstType tyCtx)
  then show ?case proof (cases firstType)
    case (Pair identifier ty)
    then show ?thesis using Cons by (cases ty, auto)
  qed
qed

fun evalFaustusPubKey :: "FaustusState \<Rightarrow> FaustusPubKey \<Rightarrow> PubKey option" where
"evalFaustusPubKey state (ConstantPubKey pk) = Some pk"|
"evalFaustusPubKey state (UsePubKey pkId) = lookup pkId (boundPubKeys state)"

lemma evalFaustusPubKey_SamePubKeyDomainImpliesExecution:
"\<forall>evaluatedPubKey. \<exists>otherEvaluatedPubKey.
  (\<forall>pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) \<longrightarrow>
  evalFaustusPubKey s pk = Some evaluatedPubKey \<longrightarrow>
  evalFaustusPubKey otherState pk = Some otherEvaluatedPubKey"
  apply auto
  subgoal premises ps proof (cases pk)
    case (ConstantPubKey x1)
    then show ?thesis by auto
  next
    case (UsePubKey x2)
    then show ?thesis using ps by auto
  qed
  done

lemma wellTypedPubKeyEvaluates:
"wellTypedPubKey tyCtx pk \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusPubKey s pk \<noteq> None"
proof (induction rule: wellTypedPubKey.induct)
  case (1 tyCtx pk)
  then show ?case by auto
next
  case (2 pkId)
  then show ?case by auto
next
  case (3 boundPkId ty rest pkId)
  then show ?case apply auto
    by (cases ty, auto)
qed

fun evalFaustusPayee :: "FaustusState \<Rightarrow> FaustusPayee \<Rightarrow> Payee option" where
"evalFaustusPayee state (Account acc) = (case (evalFaustusPubKey state acc) of
  None \<Rightarrow> None
  | Some pk \<Rightarrow> Some (Semantics.Account pk))"|
"evalFaustusPayee state (Party p) = (case (evalFaustusPubKey state p) of
  None \<Rightarrow> None
  | Some pk \<Rightarrow> Some (Semantics.Party pk))"

lemma wellTypedPayeeEvaluates:
"wellTypedPayee tyCtx payee \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusPayee s payee \<noteq> None"
  by (metis (no_types, lifting) evalFaustusPayee.simps option.case_eq_if option.simps(3) wellTypedPayee.elims(2) wellTypedPubKeyEvaluates)

fun evalFaustusChoiceId :: "FaustusState \<Rightarrow> FaustusChoiceId \<Rightarrow> ChoiceId option" where
"evalFaustusChoiceId state (FaustusChoiceId cname pk) = (case evalFaustusPubKey state pk of
  None \<Rightarrow> None
  | Some pkResult \<Rightarrow> Some (ChoiceId cname pkResult))"

lemma wellTypedChoiceIdEvaluates:
"wellTypedChoiceId tyCtx choiceId \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusChoiceId s choiceId \<noteq> None"
  by (metis evalFaustusChoiceId.simps option.case_eq_if option.distinct(1) wellTypedChoiceId.elims(2) wellTypedPubKeyEvaluates)

fun evalFaustusValue :: "Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusValue \<Rightarrow> int option" and
  evalFaustusObservation :: "Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusObservation \<Rightarrow> bool option" where
"evalFaustusValue env state (AvailableMoney faustusPubKey token) =
  (case (evalFaustusPubKey state faustusPubKey) of
  None \<Rightarrow> None
  | Some accId \<Rightarrow> Some (findWithDefault 0 (accId, token) (accounts state)))" |
"evalFaustusValue env state (Constant integer) = Some integer" |
"evalFaustusValue env state (NegValue val) = (case (evalFaustusValue env state val) of
  None \<Rightarrow> None
  | Some res \<Rightarrow> Some (uminus res))" |
"evalFaustusValue env state (AddValue lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 + res2))" |
"evalFaustusValue env state (SubValue lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 - res2))" |
"evalFaustusValue env state (MulValue lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 * res2))" |
"evalFaustusValue env state (Scale n d rhs) = (case evalFaustusValue env state rhs of
  None \<Rightarrow> None
  | Some res \<Rightarrow> (let nn = res * n in
     let (q, r) = nn quotRem d in
     Some (if abs r * 2 < abs d then q else q + signum nn * signum d)))" |
"evalFaustusValue env state (ChoiceValue choId) = (case evalFaustusChoiceId state choId of
  None \<Rightarrow> None
  | Some evalChoId \<Rightarrow> Some (findWithDefault 0 evalChoId (choices state)))" |
"evalFaustusValue env state (SlotIntervalStart) = Some (fst (slotInterval env))" |
"evalFaustusValue env state (SlotIntervalEnd) = Some (snd (slotInterval env))" |
"evalFaustusValue env state (UseValue valId) =
    MList.lookup valId (boundValues state)" |
"evalFaustusValue env state (Cond cond thn els) =
  (case (evalFaustusObservation env state cond, evalFaustusValue env state thn, evalFaustusValue env state els) of
  (None, _, _) \<Rightarrow> None
  | (_, None, _) \<Rightarrow> None
  | (_, _, None) \<Rightarrow> None
  | (Some condRes, Some thenRes, Some elseRes) \<Rightarrow>
    Some (if condRes then thenRes else elseRes))" |
"evalFaustusObservation env state (AndObs lhs rhs) = (case (evalFaustusObservation env state lhs, evalFaustusObservation env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow>  None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 \<and> res2))" |
"evalFaustusObservation env state (OrObs lhs rhs) = (case (evalFaustusObservation env state lhs, evalFaustusObservation env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow>  None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 \<or> res2))" |
"evalFaustusObservation env state (NotObs subObs) = (case evalFaustusObservation env state subObs of
  None \<Rightarrow> None
  | Some res \<Rightarrow> Some (\<not>res))" |
"evalFaustusObservation env state (ChoseSomething choId) = (case evalFaustusChoiceId state choId of
  None \<Rightarrow> None
  | Some evalChoId \<Rightarrow> Some (member evalChoId (choices state)))" |
"evalFaustusObservation env state (ValueGE lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 \<ge> res2))" |
"evalFaustusObservation env state (ValueGT lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 > res2))" |
"evalFaustusObservation env state (ValueLT lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 < res2))" |
"evalFaustusObservation env state (ValueLE lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 \<le> res2))" |
"evalFaustusObservation env state (ValueEQ lhs rhs) = (case (evalFaustusValue env state lhs, evalFaustusValue env state rhs) of
  (None, _) \<Rightarrow> None
  | (_, None) \<Rightarrow> None
  | (Some res1, Some res2) \<Rightarrow> Some (res1 = res2))" |
"evalFaustusObservation env state (UseObservation obsId) = (case MList.lookup obsId (boundValues state) of
  None \<Rightarrow> None
  | Some v \<Rightarrow> Some (v \<noteq> 0))" |
"evalFaustusObservation env state TrueObs = Some True" |
"evalFaustusObservation env state FalseObs = Some False"

lemma evalFaustusValue_InsertDifferentValueStillExecutes:
"\<forall>evaluatedValue. \<exists>otherEvaluatedValue.
  (\<forall>valId . member valId (boundValues s) = member valId (boundValues otherState)) \<longrightarrow>
  (\<forall>pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) \<longrightarrow>
  evalFaustusValue e s val = Some evaluatedValue \<longrightarrow>
  evalFaustusValue e otherState val = Some otherEvaluatedValue"
"\<forall>evaluatedObservation. \<exists>otherEvaluatedObservation.
  (\<forall>valId . member valId (boundValues s) = member valId (boundValues otherState)) \<longrightarrow>
  (\<forall>pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) \<longrightarrow>
  evalFaustusObservation e s obs = Some evaluatedObservation \<longrightarrow>
  evalFaustusObservation e otherState obs = Some otherEvaluatedObservation"
proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
  case (1 env state faustusPubKey token)
  then show ?case apply auto
    apply (cases faustusPubKey, auto)
    by (split option.split, auto)
next
  case (2 env state integer)
  then show ?case by auto
next
  case (3 env state val)
  then show ?case by auto
next
  case (4 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (5 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (6 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (7 env state n d rhs)
  then show ?case apply auto
    apply (split option.split_asm, auto)
    by metis
next
  case (8 env state choId)
  then show ?case apply (auto split: option.split)
    by (smt FaustusPubKey.exhaust evalFaustusChoiceId.elims evalFaustusChoiceId.simps evalFaustusPubKey.simps(2) not_None_eq option.case_eq_if wellTypedChoiceId.simps wellTypedChoiceIdEvaluates wellTypedPubKey.simps(1) wellTypedState.simps(1))
next
  case (9 env state)
  then show ?case by auto
next
  case (10 env state)
  then show ?case by auto
next
  case (11 env state valId)
  then show ?case by auto
next
  case (12 env state cond thn els)
  then show ?case apply auto
        apply (metis (no_types, lifting) option.case_eq_if)
       apply (metis (no_types, lifting) option.case_eq_if)
      apply (metis (no_types, lifting) option.case_eq_if)
     apply (metis (no_types, lifting) option.case_eq_if)
    by (metis (no_types, lifting) option.case_eq_if)
next
  case (13 env state lhs rhs)
  then show ?case apply auto
    by (metis (no_types, lifting) option.case_eq_if)
next
  case (14 env state lhs rhs)
  then show ?case apply auto
    by (metis (no_types, lifting) option.case_eq_if)
next
  case (15 env state subObs)
  then show ?case by auto
next
  case (16 env state choId)
  then show ?case apply (auto split: option.split)
    by (smt FaustusPubKey.exhaust evalFaustusChoiceId.elims evalFaustusChoiceId.simps evalFaustusPubKey.simps(2) not_None_eq option.case_eq_if wellTypedChoiceId.simps wellTypedChoiceIdEvaluates wellTypedPubKey.simps(1) wellTypedState.simps(1))
next
  case (17 env state lhs rhs)
  then show ?case apply (simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    by auto
next
  case (18 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (19 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (20 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (21 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
next
  case (22 env state obsId)
  then show ?case apply auto
    by (split option.split, auto)
next
  case (23 env state)
  then show ?case by auto
next
  case (24 env state)
  then show ?case by auto
qed

lemma wellTypedValueObservationExecutes:
"wellTypedValue tyCtx val \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusValue env s val \<noteq> None"
"wellTypedObservation tyCtx obs \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusObservation env s obs \<noteq> None"
proof (induction rule: wellTypedValue_wellTypedObservation.induct)
  case (1 tyCtx pk token)
  then show ?case by (simp add: option.case_eq_if wellTypedPubKeyEvaluates)
next
  case (2 tyCtx integer)
  then show ?case by simp
next
  case (3 tyCtx val)
  then show ?case by auto
next
  case (4 tyCtx lhs rhs)
  then show ?case by auto
next
  case (5 tyCtx lhs rhs)
  then show ?case by auto
next
  case (6 tyCtx lhs rhs)
  then show ?case by auto
next
  case (7 tyCtx n d rhs)
  then show ?case apply auto
    by metis
next
  case (8 tyCtx choId)
  then show ?case by (auto simp add: wellTypedChoiceIdEvaluates split: option.split)
next
  case (9 tyCtx)
  then show ?case by auto
next
  case (10 tyCtx)
  then show ?case by auto
next
  case (11 valId)
  then show ?case by auto
next
  case (12 boundValId ty rest valId)
  then show ?case by (cases ty, auto)
next
  case (13 tyCtx cond thn els)
  then show ?case by auto
next
  case (14 tyCtx lhs rhs)
  then show ?case by auto
next
  case (15 tyCtx lhs rhs)
  then show ?case by auto
next
  case (16 tyCtx subObs)
  then show ?case by auto
next
  case (17 tyCtx choId)
  then show ?case by (auto simp add: wellTypedChoiceIdEvaluates split: option.split)
next
  case (18 tyCtx lhs rhs)
  then show ?case by auto
next
  case (19 tyCtx lhs rhs)
  then show ?case by auto
next
  case (20 tyCtx lhs rhs)
  then show ?case by auto
next
  case (21 tyCtx lhs rhs)
  then show ?case by auto
next
  case (22 tyCtx lhs rhs)
  then show ?case by auto
next
  case (23 obsId)
  then show ?case by auto
next
  case (24 boundObsId ty rest obsId)
  then show ?case by (cases ty, auto)
next
  case (25 tyCtx)
  then show ?case by auto
next
  case (26 tyCtx)
  then show ?case by auto
qed

(* Needs to pass modified state to maintain parity with compiled Marlowe contract. Evaluate sequentially left to right with previous results available to next arguments. *)
fun evalFaustusArguments :: "Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusParameter list \<Rightarrow> FaustusArgument list \<Rightarrow> FaustusState option" where
"evalFaustusArguments e s [] [] = Some s" |
"evalFaustusArguments e s ((ValueParameter vid)#restParams) ((ValueArgument val)#restArgs) = (case evalFaustusValue e s val of
  None \<Rightarrow> None
  | Some res \<Rightarrow> evalFaustusArguments e (s\<lparr>boundValues := MList.insert vid res (boundValues s)\<rparr>) restParams restArgs)" |
"evalFaustusArguments e s ((ObservationParameter obsId)#restParams) ((ObservationArgument obs)#restArgs) = (case evalFaustusObservation e s obs of
  None \<Rightarrow> None
  | Some res \<Rightarrow> evalFaustusArguments e (s\<lparr>boundValues := MList.insert obsId (if res then 1 else 0) (boundValues s)\<rparr>) restParams restArgs)" |
"evalFaustusArguments e s ((PubKeyParameter pkId)#restParams) ((PubKeyArgument pk)#restArgs) = (case evalFaustusPubKey s pk of
  None \<Rightarrow> None
  | Some res \<Rightarrow> evalFaustusArguments e (s\<lparr>boundPubKeys := MList.insert pkId res (boundPubKeys s)\<rparr>) restParams restArgs)" |
"evalFaustusArguments e s (firstParam#restParams) (firstArg#restArgs) = None" |
"evalFaustusArguments e s params [] = None" |
"evalFaustusArguments e s [] args = None"

lemma evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys:
"\<forall>evaluatedState otherState otherEvaluatedState .
  evalFaustusArguments e s params args = Some evaluatedState \<longrightarrow>
  evalFaustusArguments e (otherState\<lparr>boundPubKeys := boundPubKeys s\<rparr>) params args = Some otherEvaluatedState \<longrightarrow>
  boundPubKeys evaluatedState = boundPubKeys otherEvaluatedState"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusValue e s val", auto)
    apply (split option.split_asm, auto)
    by (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    apply (split option.split_asm, simp)
    apply (split option.split_asm, simp)
    apply (cases "the (evalFaustusObservation e s obs)", auto)
     apply (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
    by (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply (cases pk)
     apply simp
    apply auto
    apply (split option.split_asm, simp)
    apply (split option.split_asm)
     apply blast
    by (metis (no_types) FaustusState.surjective FaustusState.update_convs(4) option.inject)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsExecute:
"wellTypedArguments tyCtx params args \<Longrightarrow>
  wellTypedState tyCtx s \<Longrightarrow>
  evalFaustusArguments e s params args \<noteq> None"
proof (induction arbitrary: s rule: wellTypedArguments.induct)
  case (1 tyCtx)
  then show ?case by auto
next
  case (2 tyCtx v va)
  then show ?case by auto
next
  case (3 tyCtx v va)
  then show ?case by auto
next
  case (4 tyCtx firstParam restParams firstArg restArgs)
  then show ?case apply auto
    subgoal premises ps proof (cases firstParam firstArg rule: FaustusParameter.exhaust[case_product FaustusArgument.exhaust])
      case (ValueParameter_ValueArgument valId val)
      then show ?thesis using ps apply auto
        by (cases "evalFaustusValue e s val", auto simp add: wellTypedValueObservationExecutes(1) insertValueWellTypedStateIsWellTyped ps)
    next
      case (ValueParameter_ObservationArgument x1 x2)
      then show ?thesis using ps by auto
    next
      case (ValueParameter_PubKeyArgument x1 x3)
      then show ?thesis using ps by auto
    next
      case (ObservationParameter_ValueArgument x2 x1)
      then show ?thesis using ps by auto
    next
      case (ObservationParameter_ObservationArgument obsId obs)
      then show ?thesis using ps apply auto
        apply (cases "evalFaustusObservation e s obs", auto)
        using wellTypedValueObservationExecutes(2) insertValueWellTypedStateIsWellTyped by auto
    next
      case (ObservationParameter_PubKeyArgument x2 x3)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_ValueArgument x3 x1)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_ObservationArgument x3 x2)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_PubKeyArgument pkId pk)
      then show ?thesis using ps apply auto
        apply (cases "evalFaustusPubKey s pk", auto)
        using wellTypedPubKeyEvaluates insertPubKeyWellTypedStateIsWellTyped by auto
    qed
    done
qed

lemma wellTypedArgumentsOnlyAddIdsToState:
"(wellTypedState tyCtx s \<longrightarrow>
  wellTypedArguments tyCtx params args \<longrightarrow> 
  Some newS = evalFaustusArguments e s params args \<longrightarrow>
  wellTypedState tyCtx newS)"
proof (induction arbitrary: newS rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusValue e s val", auto)
    by (cases "evalFaustusArguments e s restParams restArgs", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    by (cases "evalFaustusObservation e s obs", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
    by (cases "evalFaustusPubKey s pk", auto simp add: insertPubKeyWellTypedStateIsWellTyped)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsLookupPreviousBoundValueIds:
"(\<forall>newS vid boundVal. \<exists>otherBoundVal. Some newS = evalFaustusArguments e s params args \<longrightarrow>
  MList.lookup vid (boundValues s) = Some boundVal \<longrightarrow>
  MList.lookup vid (boundValues newS) = Some otherBoundVal)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply (cases "evalFaustusValue e s val", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusObservation e s obs", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case by (cases "evalFaustusPubKey s pk", auto)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsLookupPreviousBoundPubKeyIds:
"(\<forall>newS pkId boundVal. \<exists>otherBoundVal. Some newS = evalFaustusArguments e s params args \<longrightarrow>
  MList.lookup pkId (boundPubKeys s) = Some boundVal \<longrightarrow>
  MList.lookup pkId (boundPubKeys newS) = Some otherBoundVal)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case by (cases "evalFaustusValue e s val", auto)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case by (cases "evalFaustusObservation e s obs", auto)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply (cases "evalFaustusPubKey s pk", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed


lemma wellTypedArgumentsAddParamsToState:
"(\<forall> newS . Some newS = evalFaustusArguments e s params args \<longrightarrow> wellTypedState (paramsToTypeContext params) newS)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusValue e s val", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundValueIds apply fastforce
    by (cases "evalFaustusValue e s val", auto)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusObservation e s obs", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundValueIds apply fastforce
    by (cases "evalFaustusObservation e s obs", auto)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusPubKey s pk", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundPubKeyIds apply fastforce
    by (cases "evalFaustusPubKey s pk", auto)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsResultWellTypedState:
"wellTypedArguments restTyCtx params args \<Longrightarrow>
    wellTypedState restTyCtx s \<Longrightarrow>
    Some newS = evalFaustusArguments e s params args \<Longrightarrow> wellTypedState (paramsToTypeContext params @ restTyCtx) newS"
  by (metis wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedStateCombineTypeContexts)

fun lookupContractIdAbsInformation :: "FaustusContext \<Rightarrow> Identifier \<Rightarrow> (FaustusParameter list \<times> FaustusContract \<times> FaustusContext) option" where
"lookupContractIdAbsInformation [] cid = None" |
"lookupContractIdAbsInformation (ContractAbstraction (boundCid, params, bodyCont)#restAbs) cid = (if boundCid = cid
  then Some (params, bodyCont, restAbs)
  else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ValueAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ObservationAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (PubKeyAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)"

lemma lookupContractTypeWellTypedContext:
"(\<forall>cid params bodyCont restCtx. \<exists>restTy . wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy))"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractAbstractionWellTypedContext:
"(\<forall>cid params restTy. \<exists>bodyCont restCtx . wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx))"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractRestIsWellTyped:
"(\<forall>cid params restTy bodyCont restCtx . wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  wellTypedFaustusContext restTy restCtx)"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractFoundContractIsWellTyped:
"(\<forall>cid params restTy bodyCont restCtx . wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) \<longrightarrow>
  wellTypedContract (paramsToTypeContext params @ restTy) bodyCont)"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractStateStaysWellTyped:
"(\<forall>cid params restTy restCtx .
  wellTypedState tyCtx s \<longrightarrow>
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) \<longrightarrow>
  wellTypedState restTy s)"
  by (induction tyCtx s rule: wellTypedState.induct, auto)

type_synonym FaustusConfiguration = "FaustusContract * FaustusContext * FaustusState * Environment * (ReduceWarning list) * (Payment list)"

inductive
  small_step_reduce :: "FaustusConfiguration \<Rightarrow> FaustusConfiguration \<Rightarrow> bool" (infix "\<rightarrow>\<^sub>f" 55)
where
CloseRefund:  "refundOne (accounts s) = Some ((party, token, money), newAccount) \<Longrightarrow> 
  (Close, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f 
  (Close, ctx, (s\<lparr>accounts := newAccount\<rparr>), env, warns @ [ReduceNoWarning], payments @ [Payment party token money])" |
PayNonPositive: "\<lbrakk>evalFaustusValue env s val = Some res; res \<le> 0; Some fromAcc = (evalFaustusPubKey s accId); Some toPayee = (evalFaustusPayee s payee)\<rbrakk> \<Longrightarrow>
  (Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s, env, warns @ [ReduceNonPositivePay fromAcc toPayee token res], payments)" |
PayPositivePartialWithPayment: "\<lbrakk>evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment\<rbrakk> \<Longrightarrow>
  (Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  ((cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments @ [somePayment]))" |
PayPositivePartialWithoutPayment: "\<lbrakk>evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceNoPayment\<rbrakk> \<Longrightarrow>
  (Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  ((cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments))" |
PayPositiveFullWithPayment: "\<lbrakk>evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res \<le> moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment\<rbrakk> \<Longrightarrow>
  (Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReduceNoWarning], payments @ [somePayment])" |
PayPositiveFullWithoutPayment: "\<lbrakk>evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res \<le> moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceNoPayment\<rbrakk> \<Longrightarrow>
  (Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s\<lparr>accounts := finalAccs\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
IfTrue: "evalFaustusObservation env s obs = Some True \<Longrightarrow> 
  (If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont1, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
IfFalse: "evalFaustusObservation env s obs = Some False \<Longrightarrow> 
  (If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont2, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
WhenTimeout: "\<lbrakk>slotInterval env = (startSlot, endSlot);
  endSlot \<ge> timeout;
  startSlot \<ge> timeout\<rbrakk> \<Longrightarrow>
  (When cases timeout cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
LetShadow: "\<lbrakk>lookup valId (boundValues s) = Some oldVal; evalFaustusValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceShadowing valId oldVal res], payments)" |
LetNoShadow: "\<lbrakk>lookup valId (boundValues s) = None; evalFaustusValue env s val = Some res\<rbrakk> \<Longrightarrow>
  (Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ValueAbstraction valId#ctx, s\<lparr> boundValues := MList.insert valId res (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
LetObservation: "\<lbrakk>evalFaustusObservation env s obs = Some res\<rbrakk> \<Longrightarrow>
  (LetObservation obsId obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ObservationAbstraction obsId#ctx, s\<lparr>boundValues := MList.insert obsId (if res then 1 else 0) (boundValues s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
LetPubKey: "\<lbrakk>evalFaustusPubKey s pk = Some res\<rbrakk> \<Longrightarrow>
  (LetPubKey pkId pk cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, PubKeyAbstraction pkId#ctx, s\<lparr> boundPubKeys := MList.insert pkId res (boundPubKeys s)\<rparr>, env, warns @ [ReduceNoWarning], payments)" |
AssertTrue: "evalFaustusObservation env s obs = Some True \<Longrightarrow>
  (Assert obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
AssertFalse: "evalFaustusObservation env s obs = Some False \<Longrightarrow>
  (Assert obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ctx, s, env, warns @ [ReduceAssertionFailed], payments)" |
LetC: "(LetC cid params boundCon cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (cont, ContractAbstraction (cid, params, boundCon)#ctx, s, env, warns @ [ReduceNoWarning], payments)" |
UseCFound: "\<lbrakk>lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  Some newState = evalFaustusArguments env s params args\<rbrakk> \<Longrightarrow>
  (UseC cid args, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f
  (bodyCont, (paramsToFaustusContext params)@innerCtx, newState, env, warns @ [ReduceNoWarning], payments)"

abbreviation
  small_step_reduces :: "FaustusConfiguration \<Rightarrow> FaustusConfiguration \<Rightarrow> bool" (infix "\<rightarrow>\<^sub>f*" 55)
where "x \<rightarrow>\<^sub>f* y == star small_step_reduce x y"

thm small_step_reduce.induct
lemmas small_step_reduce_induct = small_step_reduce.induct[split_format(complete)]
declare small_step_reduce.intros[simp,intro]

inductive_cases CloseE[elim!]: "(Close, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm CloseE
inductive_cases PayE[elim!]: "(Pay accId payee token val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm PayE
inductive_cases IfE[elim!]: "(If obs cont1 cont2, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm IfE
inductive_cases WhenE[elim!]: "(When cases timeout cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm WhenE
inductive_cases LetE[elim!]: "(Let valId val cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm LetE
inductive_cases LetObservationE[elim!]: "(LetObservation obsId obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm LetObservationE
inductive_cases LetPubKeyE[elim!]: "(LetPubKey pkId pk cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm LetPubKeyE
inductive_cases AssertE[elim!]: "(Assert obs cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm AssertE
inductive_cases LetCE[elim!]: "(LetC cid boundCon params cont, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm LetCE
inductive_cases UseCE[elim!]: "(UseC cid args, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f ct"
thm UseCE

lemma deterministic:
  "cs \<rightarrow>\<^sub>f cs' \<Longrightarrow> cs \<rightarrow>\<^sub>f cs'' \<Longrightarrow> cs'' = cs'"
  apply(induction arbitrary: cs'' rule: small_step_reduce.induct)
                   apply auto
                      apply (metis option.inject)
                      apply (metis option.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis ReduceEffect.inject option.inject prod.inject)
                      apply (metis ReduceEffect.distinct(1) option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.inject prod.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis ReduceEffect.distinct(1) option.inject prod.inject)
                     apply (metis leD option.simps(5))
                    apply (metis leD option.simps(5))
                   apply (metis leD option.simps(5))
                  apply (metis option.inject prod.inject)
                 apply (metis option.inject prod.inject)
                apply (metis leD option.simps(5))
               apply (metis leD option.simps(5))
              apply (metis leD option.simps(5))
             apply (metis leD option.simps(5))
            apply (metis ReduceEffect.inject option.inject prod.inject)
           apply (metis Pair_inject ReduceEffect.distinct(1) option.inject)
          apply (metis leD option.simps(5))
         apply (metis leD option.simps(5))
        apply (metis option.inject prod.inject)
       apply (metis option.inject prod.inject)
      apply (metis leD option.simps(5))
     apply (metis leD option.simps(5))
    apply (metis leD option.simps(5))
   apply (metis Pair_inject ReduceEffect.distinct(1) option.inject)
  by (metis option.inject)

lemma smallStepWarningsAreArbitrary:
"(c, bc, s, e, w, p) \<rightarrow>\<^sub>f (c', bc', s', e', w', p') \<Longrightarrow>
  (\<forall>w'' . \<exists>w''' . (c, bc, s, e, w'', p) \<rightarrow>\<^sub>f (c', bc', s', e', w''', p'))"
proof (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  then show ?case by auto
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  then show ?case by auto
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  then show ?case by fastforce
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  then show ?case by auto
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  then show ?case by auto
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  then show ?case by auto
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  then show ?case by auto
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  then show ?case by auto
next
  case (LetObservation env s obs res obsId cont ctx warns payments)
  then show ?case by fastforce
next
  case (LetPubKey s pk res pkId cont ctx env warns payments)
  then show ?case by auto
next
  case (AssertTrue env s obs cont ctx warns payments)
  then show ?case by auto
next
  case (AssertFalse env s obs cont ctx warns payments)
  then show ?case by auto
next
  case (LetC cid params boundCon cont ctx s env warns payments)
  then show ?case by auto
next
  case (UseCFound ctx cid params bodyCont innerCtx newState env s args warns payments)
  then show ?case by auto
qed

lemma smallStepStarWarningsAreArbitrary:
  "(c, bc, s, e, w, p) \<rightarrow>\<^sub>f* (c', bc', s', e', w'', p') \<Longrightarrow>
    (\<forall>w'' . \<exists>w''' . (c, bc, s, e, w'', p) \<rightarrow>\<^sub>f* (c', bc', s', e', w''', p'))"
  apply (induction rule: star.induct[of "small_step_reduce", split_format(complete)], auto)
  by (meson smallStepWarningsAreArbitrary star.step)

definition "final cs \<longleftrightarrow> \<not>(\<exists>cs'. cs \<rightarrow>\<^sub>f cs')"

lemma wellTypedPayContinues:
  assumes "wellTypedContract tyCtx (Pay accId payee token amt cont) \<and>
  wellTypedFaustusContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newS newW newP .(Pay accId payee token amt cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f (cont, ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFaustusValue e s amt = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationExecutes(1))
  show ?thesis proof (cases "res \<le> 0")
    case True
    then show ?thesis using assms apply auto
      using \<open>evalFaustusValue e s amt = Some res\<close> wellTypedPayeeEvaluates wellTypedPubKeyEvaluates by fastforce
  next
    case False
    then show ?thesis proof -
      obtain fromAcc where "evalFaustusPubKey s accId = Some fromAcc" using assms apply auto
        by (meson option.exhaust_sel wellTypedPubKeyEvaluates)
      moreover obtain toPayee where "evalFaustusPayee s payee = Some toPayee" using assms apply auto
        by (meson option.exhaust wellTypedPayeeEvaluates)
      ultimately show ?thesis proof (cases "res > moneyInAccount fromAcc token (accounts s)")
        case True
        then show ?thesis using assms apply auto
          by (metis FaustusSemantics.small_step_reduce.PayNonPositive FaustusSemantics.small_step_reduce.PayPositiveFullWithPayment FaustusSemantics.small_step_reduce.PayPositiveFullWithoutPayment FaustusSemantics.small_step_reduce.PayPositivePartialWithPayment FaustusSemantics.small_step_reduce.PayPositivePartialWithoutPayment \<open>\<And>thesis. (\<And>res. evalFaustusValue e s amt = Some res \<Longrightarrow> thesis) \<Longrightarrow> thesis\<close> \<open>evalFaustusPayee s payee = Some toPayee\<close> \<open>evalFaustusPubKey s accId = Some fromAcc\<close> giveMoney.elims not_less)
      next
        case False
        then show ?thesis using assms apply auto
          by (metis False FaustusSemantics.small_step_reduce.PayNonPositive FaustusSemantics.small_step_reduce.PayPositiveFullWithPayment FaustusSemantics.small_step_reduce.PayPositiveFullWithoutPayment \<open>evalFaustusPayee s payee = Some toPayee\<close> \<open>evalFaustusPubKey s accId = Some fromAcc\<close> \<open>evalFaustusValue e s amt = Some res\<close> giveMoney.elims not_less)
      qed
    qed
  qed
qed

lemma wellTypedAssertContinues:
  assumes "wellTypedContract tyCtx (Assert obs cont) \<and>
  wellTypedFaustusContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newW newP .(Assert obs cont, ctx, s, e, w, p) \<rightarrow>\<^sub>f (cont, ctx, s, e, newW, newP))"
proof -
  obtain res where "evalFaustusObservation e s obs = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationExecutes(2))
  show ?thesis proof (cases res)
    case True
    then show ?thesis using assms \<open>evalFaustusObservation e s obs = Some res\<close> by force
  next
    case False
    then show ?thesis using assms \<open>evalFaustusObservation e s obs = Some res\<close> by force
  qed
qed

lemma wellTypedUseCContinues:
  assumes "wellTypedContract tyCtx (UseC cid args) \<and>
  wellTypedFaustusContext tyCtx ctx \<and>
  wellTypedState tyCtx s"
  shows "(\<exists> newCont newCtx newS newW .(UseC cid args, ctx, s, e, w, p) \<rightarrow>\<^sub>f (newCont, newCtx, newS, e, newW, p))"
  using assms apply (cases "lookupContractIdParamTypeInformation tyCtx cid", auto)
  by (metis UseCFound lookupContractAbstractionWellTypedContext option.exhaust_sel wellTypedArgumentsExecute)

(* A well typed final configuration only has Close or When contracts. *)
lemma finalD: "(final (contract, ctx, s, e, w, p) \<and> (wellTypedContract tyCtx contract) \<and> wellTypedFaustusContext tyCtx ctx \<and> wellTypedState tyCtx s) \<Longrightarrow> contract = Close \<or> (\<exists> cases timeout continue . contract = When cases timeout continue)"
  apply (auto simp add: final_def)
  apply (cases contract, auto)
         apply (meson wellTypedContract.simps(2) wellTypedPayContinues)
        apply (smt FaustusSemantics.small_step_reduce.IfFalse FaustusSemantics.small_step_reduce.intros(7) option.exhaust_sel wellTypedValueObservationExecutes(2))
       apply (metis FaustusSemantics.small_step_reduce.LetNoShadow FaustusSemantics.small_step_reduce.LetShadow not_Some_eq wellTypedValueObservationExecutes(1))
      apply (meson FaustusSemantics.small_step_reduce.intros(12) option.exhaust_sel wellTypedValueObservationExecutes(2))
     apply (meson FaustusSemantics.small_step_reduce.intros(13) option.exhaust_sel wellTypedPubKeyEvaluates)
    apply (meson wellTypedAssertContinues wellTypedContract.simps)
   apply blast
  by (metis wellTypedContract.simps(10) wellTypedUseCContinues)

lemma insertGivesSameDomainAsConsCompared:
"set (map fst m) \<subseteq> set c \<Longrightarrow> set (map fst (MList.insert newKey (newVal) (m))) \<subseteq> set ((newKey)#c)"
  by (induction m rule: MList.insert.induct, auto)

lemma typePreservationUseC:
  assumes"(UseC cid args, ContractAbstraction (cid, params, newContract) # innerCtx, s, e, w, p) \<rightarrow>\<^sub>f (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract ((cid, ContractType params) # restTyCtx) (UseC cid args) \<and>
    wellTypedFaustusContext ((cid, ContractType params) # restTyCtx) (ContractAbstraction (cid, params, newContract) # innerCtx) \<and>
    wellTypedState ((cid, ContractType params) # restTyCtx) s"
  shows "wellTypedContract (paramsToTypeContext params@restTyCtx) newContract \<and>
    wellTypedFaustusContext (paramsToTypeContext params@restTyCtx) newCtx \<and>
    wellTypedState (paramsToTypeContext params@restTyCtx) newS"
  using assms apply auto
  using concatWellTypedContexts paramsWellTypedContext apply blast
  using wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedState.simps(5) wellTypedStateCombineTypeContexts by blast

(* A single step of reduction maintains type correctness. *)
lemma typePreservation:
  assumes "(contract, ctx, s, e, w, p) \<rightarrow>\<^sub>f (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract tyCtx contract \<and>
    wellTypedFaustusContext tyCtx ctx \<and>
    wellTypedState tyCtx s"
  shows "(\<exists>newTyCtx . wellTypedContract newTyCtx newContract \<and>
    wellTypedFaustusContext newTyCtx newCtx \<and>
    wellTypedState newTyCtx newS)"
  using assms apply (cases contract, auto)
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  subgoal premises ps proof -
    obtain vid val where "contract = Let vid val newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((vid, ValueType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain vid val where "contract = Let vid val newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((vid, ValueType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain obsId obs where "contract = LetObservation obsId obs newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((obsId, ObservationType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((obsId, ObservationType)#tyCtx) newS" using calculation ps assms apply simp
      apply (meson calculation ps assms insertValueWellTypedStateIsWellTyped MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain obsId obs where "contract = LetObservation obsId obs newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((obsId, ObservationType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((obsId, ObservationType)#tyCtx) newS" using calculation ps assms apply simp
      apply (meson calculation ps assms insertValueWellTypedStateIsWellTyped MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain pkId pk where "contract = LetPubKey pkId pk newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((pkId, PubKeyType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((pkId, PubKeyType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertPubKeyWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain cid args where "contract = UseC cid args" using ps by auto
    moreover obtain params restTy where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy)" using ps assms calculation by (cases "lookupContractIdParamTypeInformation tyCtx cid", auto)
    moreover obtain  innerCtx where "lookupContractIdAbsInformation ctx cid = Some (params, newContract, innerCtx)" using ps assms calculation by (metis FaustusContract.inject(9) Pair_inject lookupContractTypeWellTypedContext option.inject)
    moreover have "wellTypedFaustusContext restTy innerCtx" using calculation(2) calculation(3) lookupContractRestIsWellTyped ps(5) by blast
    moreover have "wellTypedFaustusContext (paramsToTypeContext params @ restTy) ((paramsToFaustusContext params) @ innerCtx)" by (simp add: calculation(4) concatWellTypedContexts paramsWellTypedContext)
    ultimately show ?thesis using ps assms apply auto
      by (meson lookupContractFoundContractIsWellTyped lookupContractStateStaysWellTyped wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedStateCombineTypeContexts)
  qed
  done

(* Many steps of reduction maintains type correctness. *)
lemma typePreservationManySteps:
assumes"(contract, ctx, s, e, w, p) \<rightarrow>\<^sub>f* (newContract, newCtx, newS, newE, newW, newP) \<and>
    wellTypedContract tyCtx contract \<and>
    wellTypedFaustusContext tyCtx ctx \<and>
    wellTypedState tyCtx s"
shows "(\<exists>newTyCtx . wellTypedContract newTyCtx newContract \<and>
    wellTypedFaustusContext newTyCtx newCtx \<and>
    wellTypedState newTyCtx newS)"
  using assms apply auto
  subgoal proof (induction arbitrary: tyCtx rule:star.induct[of small_step_reduce, split_format(complete)])
    case (refl)
    then show ?case by auto
  next
    case (step)
    then show ?case using typePreservation by blast
  qed
  done

(* A well typed contract will be able to continue unless in a final state. *)
lemma contractProgress:
  assumes "wellTypedContract tyCtx contract \<and>
    wellTypedFaustusContext tyCtx ctx \<and>
    wellTypedState tyCtx s \<and>
    \<not>(final (contract, ctx, s, e, w, p))"
  shows "\<exists>cs'. (contract, ctx, s, e, w, p) \<rightarrow>\<^sub>f cs'"
  using assms by (auto simp add: final_def)

(* A well typed program can get to a final state. *)
lemma typeSoundness:
  assumes "(contract, ctx, s, env, warns, payments) \<rightarrow>\<^sub>f* (newContract, newCtx, newS, newEnv, newWarns, newPayments) \<and>
            \<not>(final (newContract, newCtx, newS, newEnv, newWarns, newPayments)) \<and>
            wellTypedContract tyCtx contract \<and>
            wellTypedFaustusContext tyCtx ctx \<and>
            wellTypedState tyCtx s"
  shows "\<exists>cs''. (newContract, newCtx, newS, newEnv, newWarns, newPayments) \<rightarrow>\<^sub>f cs''"
  using assms by (auto simp add: final_def)

fun compileFaustusPubKey :: "FaustusPubKey \<Rightarrow> FaustusState \<Rightarrow> PubKey option" where
"compileFaustusPubKey (UsePubKey pkId) fs = MList.lookup pkId (boundPubKeys fs)" |
"compileFaustusPubKey (ConstantPubKey pk) s = Some pk"

fun compileFaustusPayee :: "FaustusPayee \<Rightarrow> FaustusState \<Rightarrow> Payee option" where
"compileFaustusPayee (Account pk) fs = (case compileFaustusPubKey pk fs of
  Some mPk \<Rightarrow> Some (Semantics.Account mPk)
  | None \<Rightarrow> None)" |
"compileFaustusPayee (Party pk) fs = (case compileFaustusPubKey pk fs of
  Some mPk \<Rightarrow> Some (Semantics.Party mPk)
  | None \<Rightarrow> None)"

fun compileFaustusChoiceId :: "FaustusChoiceId \<Rightarrow> FaustusState \<Rightarrow> ChoiceId option" where
"compileFaustusChoiceId (FaustusChoiceId cname pk) fs = (case compileFaustusPubKey pk fs of
  Some mPk \<Rightarrow> Some (ChoiceId cname mPk)
  | None \<Rightarrow> None)"

(* Convert observation abstractions to values. 0 is false and 1 is true. *)
fun compileFaustusValue :: "FaustusValue \<Rightarrow> FaustusState \<Rightarrow> Value option" and
  compileFaustusObservation :: "FaustusObservation \<Rightarrow> FaustusState \<Rightarrow> Observation option" where
"compileFaustusValue (AvailableMoney faustusPubKey token) s = (case compileFaustusPubKey faustusPubKey s of
  Some mPk \<Rightarrow> Some (Semantics.AvailableMoney mPk token)
  | None \<Rightarrow> None)" |
"compileFaustusValue (Constant v) s = Some (Semantics.Constant v)" |
"compileFaustusValue (NegValue v) s = (case compileFaustusValue v s of
  Some mv \<Rightarrow> Some (Semantics.NegValue mv)
  | None \<Rightarrow> None)" |
"compileFaustusValue (AddValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.AddValue compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusValue (SubValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.SubValue compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusValue (MulValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.MulValue compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusValue (Scale i1 i2 val) s = (case (compileFaustusValue val s) of
  Some compVal \<Rightarrow> Some (Semantics.Scale i1 i2 compVal)
  | None \<Rightarrow> None)" |
"compileFaustusValue (ChoiceValue choiceId) s = (case compileFaustusChoiceId choiceId s of
  Some compChoiceId \<Rightarrow> Some (Semantics.ChoiceValue compChoiceId)
  | None \<Rightarrow> None)" |
"compileFaustusValue SlotIntervalStart s = Some Semantics.SlotIntervalStart" |
"compileFaustusValue SlotIntervalEnd s = Some Semantics.SlotIntervalEnd" |
"compileFaustusValue (UseValue valId) s = Some (Semantics.UseValue valId)" |
"compileFaustusValue (Cond obs trueVal falseVal) s = (case (compileFaustusObservation obs s, compileFaustusValue trueVal s, compileFaustusValue falseVal s) of
  (Some compObs, Some compTrueVal, Some compFalseVal) \<Rightarrow> Some (Semantics.Cond compObs compTrueVal compFalseVal)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (AndObs lhs rhs) s = (case (compileFaustusObservation lhs s, compileFaustusObservation rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.AndObs compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (OrObs lhs rhs) s = (case (compileFaustusObservation lhs s, compileFaustusObservation rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.OrObs compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (NotObs obs) s = (case compileFaustusObservation obs s of
  Some compObs \<Rightarrow> Some (Semantics.NotObs compObs)
  | None \<Rightarrow> None)" |
"compileFaustusObservation (ValueGE lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.ValueGE compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (ValueGT lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.ValueGT compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (ValueLT lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.ValueLT compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (ValueLE lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.ValueLE compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (ValueEQ lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) \<Rightarrow> Some (Semantics.ValueEQ compLhs compRhs)
  | _ \<Rightarrow> None)" |
"compileFaustusObservation (UseObservation obsId) s = Some (Semantics.NotObs (Semantics.ValueEQ (Semantics.UseValue obsId) (Semantics.Constant 0)))" |
"compileFaustusObservation (ChoseSomething choiceId) s = (case compileFaustusChoiceId choiceId s of
  Some compChoiceId \<Rightarrow> Some (Semantics.ChoseSomething compChoiceId)
  | None \<Rightarrow> None)" |
"compileFaustusObservation TrueObs s = Some Semantics.TrueObs" |
"compileFaustusObservation FalseObs s = Some Semantics.FalseObs"

fun compileAction :: "FaustusAction \<Rightarrow> FaustusState \<Rightarrow> Semantics.Action option" where
"compileAction (Deposit fromPk toPk token value) s = (case (compileFaustusPubKey fromPk s, compileFaustusPubKey toPk s, compileFaustusValue value s) of
  (Some compiledFromPk, Some compiledToPk, Some compiledValue) \<Rightarrow> Some (Semantics.Deposit compiledFromPk compiledToPk token compiledValue)
  | _ \<Rightarrow> None)" |
"compileAction (Choice choiceId bounds) s = (case compileFaustusChoiceId choiceId s of
  Some compChoiceId \<Rightarrow> Some (Semantics.Choice compChoiceId bounds)
  | None \<Rightarrow> None)" |
"compileAction (Notify observation) s = (case compileFaustusObservation observation s of
  Some compiledObservation \<Rightarrow> Some (Semantics.Notify compiledObservation)
  | None \<Rightarrow> None)"

fun argumentsToCompilerState :: "FaustusParameter list \<Rightarrow> FaustusArgument list \<Rightarrow> FaustusState \<Rightarrow> FaustusState option" where
"argumentsToCompilerState [] [] fs = Some fs" |
"argumentsToCompilerState params [] fs = None" |
"argumentsToCompilerState [] args fs = None" |
"argumentsToCompilerState (PubKeyParameter pkId#restParams) (PubKeyArgument pk#restArgs) fs = 
  (case compileFaustusPubKey pk fs of
    Some pkCompiled \<Rightarrow> argumentsToCompilerState restParams restArgs (fs\<lparr>boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys fs)\<rparr>) 
    | None \<Rightarrow> None)" |
"argumentsToCompilerState (ValueParameter valId#restParams) (ValueArgument val#restArgs) ctx = argumentsToCompilerState restParams restArgs (ctx\<lparr>boundValues := MList.insert valId 0 (boundValues ctx)\<rparr>)" |
"argumentsToCompilerState (ObservationParameter obsId#restParams) (ObservationArgument obs#restArgs) ctx = argumentsToCompilerState restParams restArgs (ctx\<lparr>boundValues := MList.insert obsId 0 (boundValues ctx)\<rparr>)" |
"argumentsToCompilerState (firstParam#restParams) (firstArg#restArgs) ctx = None"

(* Creates Marlowe contract that executes parameters sequentially. *)
fun compileArguments :: "FaustusParameter list \<Rightarrow> FaustusArgument list \<Rightarrow> FaustusState \<Rightarrow> Contract \<Rightarrow> Contract option" where
"compileArguments [] [] fs cont = Some cont" |
"compileArguments params [] fs cont = None" |
"compileArguments [] args fs cont = None" |
"compileArguments (ValueParameter valId#restParams) (ValueArgument val#restArgs) fs cont = 
  (case compileFaustusValue val fs of
  Some valCompiled \<Rightarrow> (case compileArguments restParams restArgs (fs\<lparr>boundValues := MList.insert valId 0 (boundValues fs)\<rparr>) cont of
    Some compiledRest \<Rightarrow> Some (Semantics.Let valId valCompiled compiledRest)
    | None \<Rightarrow> None)
  | None \<Rightarrow> None)" |
"compileArguments (ObservationParameter obsId#restParams) (ObservationArgument obs#restArgs) fs cont = 
  (case compileFaustusObservation obs fs of
  Some obsCompiled \<Rightarrow> (case compileArguments restParams restArgs (fs\<lparr>boundValues := MList.insert obsId 0 (boundValues fs)\<rparr>) cont of
    Some compiledRest \<Rightarrow> Some (Semantics.Let obsId (Semantics.Cond obsCompiled (Semantics.Constant 1) (Semantics.Constant 0)) compiledRest)
    | None \<Rightarrow> None)
  | None \<Rightarrow> None)" |
"compileArguments (PubKeyParameter pkId#restParams) (PubKeyArgument pk#restArgs) fs cont = 
  (case compileFaustusPubKey pk fs of
  Some pkCompiled \<Rightarrow> compileArguments restParams restArgs (fs\<lparr>boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys fs)\<rparr>) cont
  | None \<Rightarrow> None)" |
"compileArguments (firstParam#restParams) (firstArg#restArgs) ctx cont = None"

fun abstractionInformationSize :: "AbstractionInformation \<Rightarrow> nat" where
"abstractionInformationSize (PubKeyAbstraction pkId) = 0" |
"abstractionInformationSize (ValueAbstraction valId) = 0" |
"abstractionInformationSize (ObservationAbstraction obsId) = 0" |
"abstractionInformationSize (ContractAbstraction (cid, params, bodyCont)) = size params + faustusContractSize bodyCont"

lemma abstractionInformationSize_GE0:
"(abstractionInformationSize a) \<ge> 0"
  by (cases a, auto simp add: faustusContractSize_GT0)

lemma paramsToFaustusContextNoSize:
"paramsToFaustusContext params = newCtx \<Longrightarrow>
  fold (+) (map (abstractionInformationSize) newCtx) 0 = 0"
  by (induction arbitrary: newCtx rule: paramsToFaustusContext.induct, auto)

lemma abstractionInformationSizeFold0:
"fold (+) (map (abstractionInformationSize \<circ> snd) ctx) 0 = 0 \<Longrightarrow>
 fold (+) (map (abstractionInformationSize \<circ> snd) (ctx@ctx2)) 0 = fold (+) (map (abstractionInformationSize \<circ> snd) ctx2) 0"
  by auto

lemma lookupContractCompilerContextSmaller:
"lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) \<Longrightarrow>
  1 + fold (+) (map (abstractionInformationSize) ctx) 0 > faustusContractSize bodyCont + fold (+) (map (abstractionInformationSize) (innerCtx)) 0"
  apply (induction rule: lookupContractIdAbsInformation.induct, auto)
     apply (smt add.commute add.right_neutral fold_plus_sum_list_rev less_add_Suc1 less_trans nat_add_left_cancel_less not_less_eq not_less_iff_gr_or_eq option.inject prod.inject)
    apply (meson option.distinct(1))
   apply (meson option.distinct(1))
  by (meson option.distinct(1))

function compile :: "FaustusContract \<Rightarrow> FaustusContext \<Rightarrow> FaustusState \<Rightarrow> Contract option"
  and compileCases :: "FaustusCase list \<Rightarrow> FaustusContext \<Rightarrow> FaustusState \<Rightarrow> Semantics.Case list option" where
"compile Close ctx state = Some Semantics.Close"|
"compile (Pay accId payee tok val cont) ctx state = (let innerCompilation = compile cont ctx state in
  let compiledAccIdOption = compileFaustusPubKey accId state in
  let compiledPayeeOption = compileFaustusPayee payee state in
  let compiledValueOption = compileFaustusValue val state in
  case (innerCompilation, compiledAccIdOption, compiledPayeeOption, compiledValueOption) of
    (Some innerCompiled, Some compiledAccId, Some compiledPayee, Some compiledValue) \<Rightarrow> Some (Semantics.Pay compiledAccId compiledPayee tok compiledValue innerCompiled)
  | _ \<Rightarrow> None)"|
"compile (If obs trueCont falseCont) ctx state = (let trueCompilation = compile trueCont ctx state in
  let falseCompilation = compile falseCont ctx state in
  let obsCompilation = compileFaustusObservation obs state in
  case (trueCompilation, falseCompilation, obsCompilation) of
    (Some trueCompiled, Some falseCompiled, Some obsCompiled) \<Rightarrow> Some (Semantics.If obsCompiled trueCompiled falseCompiled)
  | _ \<Rightarrow> None)"|
"compile (When cases t tCont) ctx state = (
    let compiledT = compile tCont ctx state in
    let newCases = compileCases cases ctx state in
    case (compiledT, newCases) of
      (Some compiledTCont, Some compiledCases) \<Rightarrow> Some (Semantics.When compiledCases t compiledTCont)
    | _ \<Rightarrow> None)"|
"compile (Let valId val cont) ctx state = (let innerCompilation = compile cont ((ValueAbstraction valId)#ctx) (state\<lparr>boundValues := MList.insert valId 0 (boundValues state)\<rparr>) in
  let valCompilation = compileFaustusValue val state in
  case (innerCompilation, valCompilation) of
    (Some innerCompiled, Some valCompiled) \<Rightarrow> Some (Semantics.Let valId valCompiled innerCompiled)
  | _ \<Rightarrow> None)"|
"compile (LetObservation obsId obs cont) ctx state = (let innerCompilation = compile cont ((ObservationAbstraction obsId)#ctx) (state\<lparr>boundValues := MList.insert obsId 0 (boundValues state)\<rparr>) in
  let obsCompilation = compileFaustusObservation obs state in
  case (innerCompilation, obsCompilation) of
    (Some innerCompiled, Some obsCompiled) \<Rightarrow> Some (Semantics.Let obsId (Semantics.Cond obsCompiled (Semantics.Constant 1) (Semantics.Constant 0)) innerCompiled)
  | _ \<Rightarrow> None)"|
"compile (LetPubKey pkId pk cont) ctx state = (case compileFaustusPubKey pk state of
  Some pkCompiled \<Rightarrow> compile cont ((PubKeyAbstraction pkId)#ctx) (state\<lparr>boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys state)\<rparr>)
  | None \<Rightarrow> None)"|
"compile (Assert obs cont) ctx state = (let innerCompilation = compile cont ctx state in
  let obsCompilation = compileFaustusObservation obs state in
  case (innerCompilation, obsCompilation) of
    (Some innerCompiled, Some obsCompiled) \<Rightarrow> Some (Semantics.Assert obsCompiled innerCompiled)
  | _ \<Rightarrow> None)"|
"compile (LetC cid params boundCon cont) ctx state = compile cont ((ContractAbstraction (cid, params, boundCon))#ctx) state" |
"compile (UseC cid args) ctx state = (case lookupContractIdAbsInformation ctx cid of
  Some (params, bodyCont, innerCtx) \<Rightarrow> (case argumentsToCompilerState params args state of
    Some newState \<Rightarrow> (case compile bodyCont ((paramsToFaustusContext params)@innerCtx) newState of
      Some bodyCompiled \<Rightarrow> compileArguments params args state bodyCompiled
      | None \<Rightarrow> None)
    | None \<Rightarrow> None)
  | None \<Rightarrow> None)" |
"compileCases ((Case a c) # rest) ctx state = 
  (case (compile c ctx state, compileCases rest ctx state, compileAction a state) of 
    (Some compiledCaseCont, Some compiledCases, Some compiledAction) \<Rightarrow> Some ((Semantics.Case compiledAction compiledCaseCont) # compiledCases)
    | _ \<Rightarrow> None)" |
"compileCases [] ctx state = Some []"
  by pat_completeness auto

termination
  apply (relation "measures [(\<lambda>x::((FaustusContract \<times> FaustusContext \<times> FaustusState) + (FaustusCase list \<times> FaustusContext \<times> FaustusState)) . case x of 
    Inl (contract, ctx, state) \<Rightarrow> (faustusContractSize contract) + (fold (+) (map (abstractionInformationSize) ctx) 0)
    | Inr (cases, ctx, state) \<Rightarrow> (faustusCasesSize cases) + (fold (+) (map (abstractionInformationSize) ctx) 0)),
    (\<lambda>x::((FaustusContract \<times> FaustusContext \<times> FaustusState) + (FaustusCase list \<times> FaustusContext \<times> FaustusState)) . case x of
    Inl (contract, ctx, state) \<Rightarrow> length ctx
    | Inr (cases, ctx, state) \<Rightarrow> length ctx)]")
               apply (auto)[1]
              apply (auto)[1]
             apply (auto)[1]
            apply (auto)[1]
           apply (auto)[1]
          apply (auto)[1]
         apply (auto)[1]
        apply (auto)[1]
       apply (auto)[1]
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
     apply (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]
    apply (auto)[1]
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
   apply (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]
  by (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]

lemma compileCorrectness_Close:
"compile Close ctx state = Some (Semantics.Close)"
  by auto

lemma compileCorrectness_Pay:
"compile (Pay accId payee tok val cont) ctx state = Some (mCont) \<Longrightarrow> 
  \<exists>innerMCont compiledAccId compiledPayee compiledVal .compile cont ctx state = Some (innerMCont) \<and>
  compileFaustusPubKey accId state = Some compiledAccId \<and>
  compileFaustusPayee payee state = Some compiledPayee \<and>
  compileFaustusValue val state = Some compiledVal \<and>
  mCont = (Semantics.Pay compiledAccId compiledPayee tok compiledVal innerMCont)"
  apply auto
  apply (cases "compile cont ctx state", auto)
  apply (cases "compileFaustusPubKey accId state", auto)
  apply (cases "compileFaustusPayee payee state", auto)
  by (cases "compileFaustusValue val state", auto)

lemma compileCorrectness_If:
"compile (If obs trueCont falseCont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>trueMCont falseMCont compiledObs. compile trueCont ctx state = Some (trueMCont) \<and>
  compile falseCont ctx state = Some (falseMCont) \<and> 
  compileFaustusObservation obs state = Some compiledObs \<and>
  (mCont = Semantics.If compiledObs trueMCont falseMCont)"
  apply auto
  apply (cases "compile trueCont ctx state", auto)
  apply (cases "compile falseCont ctx state", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_When:
"compile (When cases t tCont) ctx state = Some (mCont) \<Longrightarrow>
\<exists>mcs mTCont. compile tCont ctx state = Some (mTCont) \<and>
  mCont = (Semantics.When mcs t mTCont)"
  apply auto
  apply (cases "compile tCont ctx state", auto)
  by(cases "compileCases cases ctx state", auto)

lemma compileCorrectness_Let:
"compile (Let vid val cont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>innerMCont valId compiledVal . compile cont ((ValueAbstraction vid)#ctx) (state\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues state)\<rparr>) = Some (innerMCont) \<and>
  compileFaustusValue val state = Some compiledVal \<and>
  mCont = (Semantics.Let valId compiledVal innerMCont)"
  apply auto
  apply (cases "compile cont ((ValueAbstraction vid)#ctx) (state\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues state)\<rparr>)", auto)
  by (cases "compileFaustusValue val state", auto)

lemma compileCorrectness_LetObservation:
"compile (LetObservation obsId obs cont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>innerMCont valId compiledObs . compile cont ((ObservationAbstraction obsId)#ctx) (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>) = Some (innerMCont) \<and>
  compileFaustusObservation obs state = Some compiledObs \<and>
  mCont = (Semantics.Let valId (Semantics.Cond compiledObs (Semantics.Constant 1) (Semantics.Constant 0)) innerMCont)"
  apply auto
  apply (cases "compile cont ((ObservationAbstraction obsId)#ctx) (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_LetPubKey:
"compile (LetPubKey pkId pk cont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>innerMCont valId compiledPk . compile cont ((PubKeyAbstraction pkId)#ctx) (state\<lparr>boundPubKeys := MList.insert pkId compiledPk (boundPubKeys state)\<rparr>) = Some (innerMCont) \<and>
  compileFaustusPubKey pk state = Some compiledPk \<and>
  mCont = innerMCont"
  apply auto
  by (cases "compileFaustusPubKey pk state", auto)

lemma compileCorrectness_Assert:
"compile (Assert obs cont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>innerMCont compiledObs. compile cont ctx state = Some (innerMCont) \<and>
  compileFaustusObservation obs state = Some compiledObs \<and>
  mCont = (Semantics.Assert compiledObs innerMCont)"
  apply auto
  apply (cases "compile cont ctx state", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_LetC:
"compile (LetC cid params boundCon cont) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>innerMCont . compile cont ((ContractAbstraction (cid, params, boundCon))#ctx) state = Some (innerMCont) \<and>
  mCont = innerMCont"
  by auto

lemma compileCorrectness_UseC:
"compile (UseC cid args) ctx state = Some (mCont) \<Longrightarrow>
  \<exists>params bodyCont innerCtx newState bodyCompiled innerMCont . lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) \<and>
  argumentsToCompilerState params args state = Some newState \<and>
  compile bodyCont ((paramsToFaustusContext params)@innerCtx) newState = Some bodyCompiled \<and>
  compileArguments params args state bodyCompiled = Some (innerMCont) \<and>
  mCont = innerMCont"
  by (auto split: option.split_asm)

lemma wellTypedPubKeyCompiles:
"(\<exists>compiledPk. wellTypedPubKey tyCtx pk \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  compileFaustusPubKey pk state = Some (compiledPk))"
proof (induction rule: wellTypedState.induct)
  case (1 s)
  then show ?case by (cases pk, auto)
next
  case (2 valId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (3 obsId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (4 pkId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (5 cid params restTypes s)
  then show ?case by (cases pk, auto)
qed

lemma wellTypedPayeeCompiles:
"wellTypedPayee tyCtx payee \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  (\<exists>compiledPayee . compileFaustusPayee payee state = Some (compiledPayee))"
  apply (cases payee, auto)
  using wellTypedPubKeyCompiles apply force
  using wellTypedPubKeyCompiles by fastforce

lemma wellTypedChoiceIdCompiles:
"wellTypedChoiceId tyCtx choiceId \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  (\<exists>compiledChoiceId . compileFaustusChoiceId choiceId state = Some (compiledChoiceId))"
  apply (cases choiceId, auto split: option.split)
  using wellTypedPubKeyCompiles by fastforce

lemma wellTypedValueObservationCompiles:
"wellTypedValue tyCtx val \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  (\<exists>compiledVal . compileFaustusValue val state = Some compiledVal)"
"wellTypedObservation tyCtx obs \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  (\<exists>compiledObs . compileFaustusObservation obs state = Some compiledObs)"
   apply (induction rule: compileFaustusValue_compileFaustusObservation.induct, auto split: option.split)
  using wellTypedPubKeyCompiles apply fastforce
  using wellTypedChoiceIdCompiles apply fastforce
  using wellTypedChoiceIdCompiles by fastforce

lemma wellTypedArgumentsCompilerContext:
"(\<forall>tyCtx . \<exists>newState . wellTypedState tyCtx state \<longrightarrow>
  wellTypedArguments tyCtx params args \<longrightarrow>
  argumentsToCompilerState params args state = Some newState)"
  apply (induction rule: argumentsToCompilerState.induct, auto)
    apply (metis (mono_tags, lifting) FaustusState.fold_congs(4) insertPubKeyWellTypedStateIsWellTyped option.simps(5) wellTypedPubKeyCompiles)
  using insertValueWellTypedStateIsWellTyped apply blast
  using insertValueWellTypedStateIsWellTyped by blast

lemma argumentsCompilerContextLookupPreviousBoundValueIds:
"(\<forall>newS vid boundVal. \<exists>otherBoundVal. Some newS = argumentsToCompilerState params args state \<longrightarrow>
  MList.lookup vid (boundValues state) = Some boundVal \<longrightarrow>
  MList.lookup vid (boundValues newS) = Some otherBoundVal)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case by (cases "compileFaustusPubKey pk fs", auto)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply auto
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply auto
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsCompilerContextLookupPreviousBoundPubKeysIds:
"(\<forall>newS pkId boundVal. \<exists>otherBoundVal. Some newS = argumentsToCompilerState params args state \<longrightarrow>
  MList.lookup pkId (boundPubKeys state) = Some boundVal \<longrightarrow>
  MList.lookup pkId (boundPubKeys newS) = Some otherBoundVal)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case by auto
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case by auto
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedArgumentsCompiles:
"(\<forall>tyCtx innerCont ctx. \<exists>mCont. wellTypedArguments tyCtx params args \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  compileArguments params args state innerCont = Some mCont)"
  apply (induction rule: compileArguments.induct, auto)
  using wellTypedValueObservationCompiles(1) apply (split option.split, auto)
  using insertValueWellTypedStateIsWellTyped apply (split option.split, auto)
  using wellTypedValueObservationCompiles(2) apply (split option.split, auto)
  apply (split option.split, auto)
  apply (split option.split, auto)
  using wellTypedPubKeyCompiles apply fastforce
  by (simp add: insertPubKeyWellTypedStateIsWellTyped)

lemma wellTypedArgumentsAddsParamsToCompilerState:
"(\<forall>newState . argumentsToCompilerState params args state = Some newState \<longrightarrow> wellTypedState (paramsToTypeContext params) newState)"
proof (induction rule: argumentsToCompilerState.induct)
case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  moreover have "\<forall>a . \<exists>pk . lookup pkId (boundPubKeys (fs\<lparr>boundPubKeys := MList.insert pkId a (boundPubKeys fs)\<rparr>)) = Some pk" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (metis \<open>\<forall>a. \<exists>pk. lookup pkId (boundPubKeys (fs\<lparr>boundPubKeys := MList.insert pkId a (boundPubKeys fs)\<rparr>)) = Some pk\<close> argumentsCompilerContextLookupPreviousBoundPubKeysIds)
next
  case (5 valId restParams val restArgs ctx)
  moreover have "\<exists>y. lookup valId (FaustusState.boundValues (ctx\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues ctx)\<rparr>)) = Some y" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply auto
    by (metis \<open>\<exists>y. lookup valId (FaustusState.boundValues (ctx\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues ctx)\<rparr>)) = Some y\<close> argumentsCompilerContextLookupPreviousBoundValueIds)
next
  case (6 obsId restParams obs restArgs ctx)
  moreover have "\<exists>y. lookup obsId (FaustusState.boundValues (ctx\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues ctx)\<rparr>)) = Some y" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply auto
    by (metis \<open>\<exists>y. lookup obsId (FaustusState.boundValues (ctx\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues ctx)\<rparr>)) = Some y\<close> argumentsCompilerContextLookupPreviousBoundValueIds)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedArgumentsOnlyAddIdsToCompilerState:
"(wellTypedState tyCtx s \<longrightarrow>
  wellTypedArguments tyCtx params args \<longrightarrow> 
  Some newS = argumentsToCompilerState params args s \<longrightarrow>
  wellTypedState tyCtx newS)"
proof (induction arbitrary: newS rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    by (cases "compileFaustusPubKey pk fs", auto simp add: insertPubKeyWellTypedStateIsWellTyped)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: MList.insert_lookup_Some)
    by (auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: MList.insert_lookup_Some)
    by (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedCompiles_UseC_Inductive:
  assumes "lookupContractIdParamTypeInformation tyCtx cid = Some (x, restTy) \<and>
  argumentsToCompilerState params args state = Some argCompilerState \<and>
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restC) \<and>
  (\<forall>tyCtx.
        wellTypedContract tyCtx bodyCont \<longrightarrow>
        wellTypedState tyCtx argCompilerState \<longrightarrow>
        wellTypedFaustusContext tyCtx (paramsToFaustusContext params @ restC) \<longrightarrow>
        (\<exists>mCont. compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState = Some mCont)) \<and>
  wellTypedState tyCtx state \<and>
  wellTypedFaustusContext tyCtx ctx \<and>
  wellTypedArguments tyCtx x args"
  shows"\<exists>mCont.
    (case compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState of None \<Rightarrow> None | Some x \<Rightarrow> compileArguments params args state x) =
    Some mCont"
proof -
  have "x = params" using lookupContractAbstractionWellTypedContext assms by fastforce
  moreover have "wellTypedContract (paramsToTypeContext params @ restTy) bodyCont" using calculation lookupContractFoundContractIsWellTyped assms by auto
  moreover have "wellTypedState (paramsToTypeContext params @ restTy) argCompilerState" by (metis assms calculation(1) lookupContractStateStaysWellTyped wellTypedArgumentsAddsParamsToCompilerState wellTypedArgumentsOnlyAddIdsToCompilerState wellTypedStateCombineTypeContexts)
  moreover have "wellTypedFaustusContext (paramsToTypeContext params @ restTy) (paramsToFaustusContext params @ restC)" using lookupContractRestIsWellTyped assms calculation concatWellTypedContexts paramsWellTypedContext by blast
  moreover obtain bodyMCont where "compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState = Some bodyMCont" using calculation assms by auto
  ultimately show ?thesis using assms wellTypedArgumentsCompiles wellTypedArgumentsOnlyAddIdsToCompilerState by auto
qed

(* A well typed contract/cases will compile
  to a Marlowe contract/cases *)
lemma wellTypedCompiles:
"(\<forall>tyCtx . (wellTypedContract tyCtx contract \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  (\<exists>mCont . compile contract ctx state = Some mCont)))"
"(\<forall>tyCtx .(wellTypedCases tyCtx cases \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  (\<exists>mCases. compileCases cases ctx state = Some mCases)))"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx)
  then show ?case using wellTypedPayeeCompiles wellTypedPubKeyCompiles wellTypedValueObservationCompiles(1) by fastforce
next
  case (3 obs trueCont falseCont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) by fastforce
next
  case (4 cases t tCont ctx)
  then show ?case by fastforce
next
  case (5 vid val cont ctx state)
  then show ?case apply auto
    using wellTypedValueObservationCompiles insertValueWellTypedStateIsWellTyped insertAppendValueWellTypedStateIsWellTyped by fastforce
next
  case (6 obsId obs cont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) insertAppendValueWellTypedStateIsWellTyped by fastforce
next
  case (7 pkId pk cont ctx)
  then show ?case using wellTypedPubKeyCompiles insertAppendPubKeyWellTypedStateIsWellTyped by fastforce
next
  case (8 obs cont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) by fastforce
next
  case (9 cid params boundCon cont ctx)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof(cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps apply auto
        by (smt case_optionE case_prodE lookupContractAbstractionWellTypedContext option.distinct(1))
    next
      case (Some compilerCidInfo)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases compilerCidInfo)
          case (fields params bodyCont restC)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using lookupContractTypeWellTypedContext ps wellTypedArgumentsCompilerContext by fastforce
            next
              case (Some argCompilerState)
              moreover obtain tyCtx where "(case lookupContractIdParamTypeInformation tyCtx cid of None \<Rightarrow> False | Some (params, innerTyCtx) \<Rightarrow> wellTypedArguments tyCtx params args) \<and> wellTypedState tyCtx state \<and> wellTypedFaustusContext tyCtx ctx" using ps by auto
              ultimately show ?thesis using ps apply auto
                subgoal premises ps proof (cases "lookupContractIdParamTypeInformation tyCtx cid")
                  case None
                  then show ?thesis using ps by auto
                next
                  case (Some contractIdParamTypeInfo)
                  then show ?thesis using ps wellTypedCompiles_UseC_Inductive by auto
                qed
                done
            qed
            done
        qed
        done
    qed
    done
next
  case (11 act cont rest ctx)
  then show ?case apply auto
    subgoal premises ps proof (cases act)
      case (Deposit x11 x12 x13 x14)
      then show ?thesis using ps apply auto
        by (smt case_prod_conv compileAction.simps(1) option.case_eq_if option.distinct(1) wellTypedPubKeyCompiles wellTypedValueObservationCompiles(1))
    next
      case (Choice x21 x22)
      then show ?thesis using ps wellTypedChoiceIdCompiles by fastforce
    next
      case (Notify obs)
      then show ?thesis using ps apply auto
        by (smt case_prod_conv compileAction.simps(3) option.case_eq_if option.distinct(1) wellTypedPubKeyCompiles wellTypedValueObservationCompiles(2))
    qed
    done
next
  case (12 ctx)
  then show ?case by auto
qed

lemma compilePubKeyAccountsArbitrary:
"compileFaustusPubKey pk state = Some compiledPk \<longrightarrow>
  compileFaustusPubKey pk (state\<lparr>accounts := newAccs\<rparr>) = Some compiledPk"
  by (cases pk, auto)

lemma compilePayeeAccountsArbitrary:
"compileFaustusPayee pk state = Some compiledPk \<longrightarrow>
  compileFaustusPayee pk (state\<lparr>accounts := newAccs\<rparr>) = Some compiledPk"
proof (cases pk)
  case (Account acc)
  then show ?thesis by (cases acc, auto)
next
  case (Party p)
  then show ?thesis by (cases p, auto)
qed

lemma compileChoiceIdAccountsArbitrary:
"compileFaustusChoiceId choiceId state = Some compiledChoiceId \<longrightarrow>
  compileFaustusChoiceId choiceId (state\<lparr>accounts := newAccs\<rparr>) = Some compiledChoiceId"
  by (cases choiceId, auto split: option.split simp add: compilePubKeyAccountsArbitrary)

lemma compileValueAccountsArbitrary:
"(\<forall>compiledVal newAccs. compileFaustusValue val state = Some compiledVal \<longrightarrow>
  compileFaustusValue val (state\<lparr>accounts := newAccs\<rparr>) = Some compiledVal)"
"(\<forall>compiledObs newAccs. compileFaustusObservation obs state = Some compiledObs \<longrightarrow>
  compileFaustusObservation obs (state\<lparr>accounts := newAccs\<rparr>) = Some compiledObs)"
proof (induction rule: compileFaustusValue_compileFaustusObservation.induct)
  case (1 faustusPubKey token s)
  then show ?case using compilePubKeyAccountsArbitrary by (cases "compileFaustusPubKey faustusPubKey s" "compileFaustusPubKey faustusPubKey (s\<lparr>FaustusState.accounts := newAccs\<rparr>)" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (2 v s)
  then show ?case by auto
next
  case (3 v s)
  then show ?case by (cases "compileFaustusValue v s", auto)
next
  case (4 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (6 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (7 i1 i2 val s)
  then show ?case by (cases "compileFaustusValue val s", auto)
next
  case (8 choiceId s)
  then show ?case by (auto split: option.split simp add: compileChoiceIdAccountsArbitrary)
next
  case (9 s)
  then show ?case by auto
next
  case (10 s)
  then show ?case by auto
next
  case (11 valId s)
  then show ?case by auto
next
  case (12 obs trueVal falseVal s)
  then show ?case by (cases "compileFaustusObservation obs s" "compileFaustusValue trueVal s" "compileFaustusValue falseVal s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
next
  case (13 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (14 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (15 obs s)
  then show ?case by (cases "compileFaustusObservation obs s", auto)
next
  case (16 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (17 lhs rhs s)
then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (18 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (19 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (20 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (21 obsId s)
  then show ?case by auto
next
  case (22 choiceId s)
  then show ?case by (auto split: option.split simp add: compileChoiceIdAccountsArbitrary)
next
  case (23 s)
  then show ?case by auto
next
  case (24 s)
  then show ?case by auto
qed

lemma stateModificationOrderArbitrary:
"state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state), FaustusState.accounts := newAccs\<rparr> =
  state\<lparr>FaustusState.accounts := newAccs, FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>"
"state\<lparr>FaustusState.accounts := newAccs, boundPubKeys := MList.insert pkId a (boundPubKeys state)\<rparr> =
  state\<lparr>boundPubKeys := MList.insert pkId a (boundPubKeys state), FaustusState.accounts := newAccs\<rparr>"
"state\<lparr>FaustusState.boundValues := newVals, boundPubKeys := newPubKeys\<rparr> =
  state\<lparr>boundPubKeys := newPubKeys, FaustusState.boundValues := newVals\<rparr>"
  by auto

lemma argumentsToContextAccountsArbitrary:
"(\<forall>newAccs newState . argumentsToCompilerState params args state = Some newState \<longrightarrow>
  argumentsToCompilerState params args (state\<lparr>FaustusState.accounts := newAccs\<rparr>) = Some (newState\<lparr>FaustusState.accounts := newAccs\<rparr>))"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma compileArgumentsAccountsArbitrary:
"(\<forall>newAccs compiledContract . compileArguments params args argCtx bodyCont = Some compiledContract \<longrightarrow>
  compileArguments params args (argCtx\<lparr>accounts := newAccs\<rparr>) bodyCont = Some compiledContract)"
proof (induction rule: compileArguments.induct)
  case (1 fs cont)
  then show ?case by auto
next
  case (2 v va fs cont)
  then show ?case by auto
next
  case (3 v va fs cont)
  then show ?case by auto
next
  case (4 valId restParams val restArgs fs cont)
  then show ?case apply (cases "compileFaustusValue val fs", auto)
    apply (cases "compileArguments restParams restArgs (fs\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues fs)\<rparr>) cont", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (5 obsId restParams obs restArgs fs cont)
  then show ?case apply (cases "compileFaustusObservation obs fs", auto)
    apply (cases "compileArguments restParams restArgs (fs\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues fs)\<rparr>) cont", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (6 pkId restParams pk restArgs fs cont)
  then show ?case by (cases "compileFaustusPubKey pk fs", auto simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case ("7_1")
  then show ?case by auto
next
  case ("7_2")
  then show ?case by auto
next
  case ("7_3")
  then show ?case by auto
next
  case ("7_4")
  then show ?case by auto
next
  case ("7_5")
  then show ?case by auto
next
  case ("7_6")
  then show ?case by auto
next
  case ("7_7")
  then show ?case by auto
next
  case ("7_8")
  then show ?case by auto
qed

lemma compileActionAccountsArbitrary:
"\<forall>newAccs compiledAct . compileAction act state = Some compiledAct \<longrightarrow>
  compileAction act (state\<lparr>accounts := newAccs\<rparr>) = Some compiledAct"
proof (cases act)
  case (Deposit x11 x12 x13 x14)
  then show ?thesis apply (cases "compileFaustusPubKey x11 state", auto)
    apply (cases "compileFaustusPubKey x12 state", auto)
    apply (cases "compileFaustusValue x14 state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary compilePubKeyAccountsArbitrary)
next
  case (Choice x21 x22)
  then show ?thesis by (auto split: option.split simp add: compileChoiceIdAccountsArbitrary)
next
  case (Notify x3)
  then show ?thesis apply (cases "compileFaustusObservation x3 state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary compilePubKeyAccountsArbitrary)
qed

lemma compileAccountsArbitrary:
"(\<forall>newAccs compiledContract . compile contract ctx state = Some compiledContract \<longrightarrow>
  compile contract ctx (state\<lparr>accounts := newAccs\<rparr>) = Some compiledContract)"
"(\<forall>newAccs compiledCases . compileCases cases ctx state = Some compiledCases \<longrightarrow>
  compileCases cases ctx (state\<lparr>accounts := newAccs\<rparr>) = Some compiledCases)"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx state)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusPubKey accId state", auto)
    apply (cases "compileFaustusPayee payee state", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: compilePayeeAccountsArbitrary compilePubKeyAccountsArbitrary compileValueAccountsArbitrary(1))
next
  case (3 obs trueCont falseCont ctx state)
  then show ?case apply (cases "compile trueCont ctx state" "compile falseCont ctx state" rule: option.exhaust[case_product option.exhaust], auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: compileValueAccountsArbitrary(2))
next
  case (4 cases t tCont ctx state)
  then show ?case by (cases "compile tCont ctx state" "compileCases cases ctx state" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 valId val cont ctx state)
  then show ?case apply (cases "compile cont (ValueAbstraction valId # ctx) (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>)", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (6 obsId obs cont ctx state)
  then show ?case apply (cases "compile cont (ObservationAbstraction obsId # ctx) (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (7 pkId pk cont ctx state)
  then show ?case apply (cases "compileFaustusPubKey pk state", auto)
    by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (8 obs cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (9 cid params boundCon cont ctx state)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof (cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps by auto
    next
      case (Some paramsBodyInnerCtx)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases paramsBodyInnerCtx)
          case (fields params bodyCont innerCtx)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using ps by auto
            next
              case (Some argCtx)
              then show ?thesis using ps apply auto
                apply (cases "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx", auto)
                by (simp add: argumentsToContextAccountsArbitrary compileArgumentsAccountsArbitrary)
            qed
            done
        qed
        done
    qed
    done
next
  case (11 a c rest ctx state)
  then show ?case apply auto
    apply (cases "compile c ctx state", auto)
    apply (cases "compileCases rest ctx state", auto)
    apply (cases "compileAction a state", auto)
    by (simp add: stateModificationOrderArbitrary compileActionAccountsArbitrary)
next
  case (12 ctx state)
  then show ?case by auto
qed

lemma compilePubKeyOnlyBoundPubKeysMatter:
"\<forall>compiledPk . compileFaustusPubKey pk state = Some compiledPk \<longrightarrow>
  compileFaustusPubKey pk (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledPk"
  by (cases pk, auto)

lemma compilePayeeOnlyBoundPubKeysMatter:
"\<forall>compiledPk . compileFaustusPayee payee state = Some compiledPk \<longrightarrow>
  compileFaustusPayee payee (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledPk"
proof (cases payee)
  case (Account x1)
  then show ?thesis by (cases x1, auto)
next
  case (Party x2)
  then show ?thesis by (cases x2, auto)
qed

lemma compileChoiceIdOnlyBoundPubKeysMatter:
"\<forall>compiledChoiceId . compileFaustusChoiceId choiceId state = Some compiledChoiceId \<longrightarrow>
  compileFaustusChoiceId choiceId (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledChoiceId"
  by (cases choiceId, auto split: option.split simp add: compilePubKeyOnlyBoundPubKeysMatter)

lemma compileValueOnlyBoundPubKeysMatter:
"\<forall>compiledVal newVals newState. compileFaustusValue val state = Some compiledVal \<longrightarrow>
  compileFaustusValue val (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledVal"
"\<forall>compiledObs newVals newState. compileFaustusObservation obs state = Some compiledObs \<longrightarrow>
  compileFaustusObservation obs (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledObs"
proof (induction rule: compileFaustusValue_compileFaustusObservation.induct)
  case (1 faustusPubKey token s)
  then show ?case apply auto
    by (cases "compileFaustusPubKey faustusPubKey s", auto simp add: compilePubKeyOnlyBoundPubKeysMatter)
next
  case (2 v s)
  then show ?case by auto
next
  case (3 v s)
  then show ?case by (cases "compileFaustusValue v s", auto)
next
  case (4 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (6 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (7 i1 i2 val s)
  then show ?case by (cases "compileFaustusValue val s", auto)
next
  case (8 choiceId s)
  then show ?case by (auto split: option.split simp add: compileChoiceIdOnlyBoundPubKeysMatter)
next
  case (9 s)
  then show ?case by auto
next
  case (10 s)
  then show ?case by auto
next
  case (11 valId s)
  then show ?case by auto
next
  case (12 obs trueVal falseVal s)
  then show ?case by (cases "compileFaustusObservation obs s" "compileFaustusValue trueVal s" "compileFaustusValue falseVal s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
next
  case (13 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (14 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (15 obs s)
  then show ?case by (cases "compileFaustusObservation obs s", auto)
next
  case (16 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (17 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (18 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (19 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (20 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (21 obsId s)
  then show ?case by auto
next
  case (22 choiceId s)
  then show ?case by (auto split: option.split simp add: compileChoiceIdOnlyBoundPubKeysMatter)
next
  case (23 s)
  then show ?case by auto
next
  case (24 s)
  then show ?case by auto
qed

lemma argumentsToContextOnlyBoundPubKeysMatter:
"(\<forall>newVals argState newState . \<exists>modifiedState . argumentsToCompilerState params args state = Some argState \<longrightarrow>
  argumentsToCompilerState params args (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some modifiedState)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs", auto)
    by (split option.split, auto simp add: stateModificationOrderArbitrary compilePubKeyOnlyBoundPubKeysMatter option.case_eq_if)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsToContextBoundPubKeysDeterministic:
"(\<forall>newVals argState newState modifiedArgState . argumentsToCompilerState params args state = Some argState \<longrightarrow>
  argumentsToCompilerState params args (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) = Some modifiedArgState \<longrightarrow>
  (boundPubKeys argState = boundPubKeys modifiedArgState))"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs")
     apply simp
    apply (split option.split_asm)
    apply simp
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter)
next
  case (5 valId restParams val restArgs fs)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case (6 obsId restParams obs restArgs fs)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsToContextBoundPubKeysSameAsEvaluateArguments:
"(\<forall>argState evalState newState modifiedArgState tyCtx . wellTypedState tyCtx state \<longrightarrow>
  wellTypedArguments tyCtx params args \<longrightarrow>
  argumentsToCompilerState params args state = Some argState \<longrightarrow>
  evalFaustusArguments env state params args = Some evalState \<longrightarrow>
  boundPubKeys argState = boundPubKeys evalState)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs", auto)
    apply (cases "evalFaustusPubKey fs pk", auto)
    by (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) insertPubKeyWellTypedStateIsWellTyped option.inject)
next
  case (5 valId restParams val restArgs fs)
  then show ?case apply auto
    subgoal premises ps proof (cases "evalFaustusValue env fs val")
      case None
      then show ?thesis using ps by auto
    next
      case (Some a)
      moreover obtain evaluatedArgs where "evalFaustusArguments env (fs\<lparr>FaustusState.boundValues := MList.insert valId a (FaustusState.boundValues fs)\<rparr>) restParams restArgs = Some evaluatedArgs" using ps calculation by auto
      moreover obtain tyCtx where "wellTypedState tyCtx fs \<and> wellTypedArguments tyCtx restParams restArgs" using ps by auto
      moreover have "wellTypedState tyCtx (fs\<lparr>boundValues := MList.insert valId 0 (boundValues fs)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
      moreover obtain otherEvaluatedArgs where "(evalFaustusArguments env (fs\<lparr>boundValues:=MList.insert valId 0 (boundValues fs)\<rparr>) restParams restArgs = Some otherEvaluatedArgs)" using ps calculation wellTypedArgumentsExecute by fastforce
      moreover have "(fs\<lparr>boundValues:=MList.insert valId 0 (boundValues fs)\<rparr>) = (fs\<lparr>boundValues:=MList.insert valId 0 (boundValues fs)\<rparr>)\<lparr>boundPubKeys := boundPubKeys (fs\<lparr>FaustusState.boundValues := MList.insert valId a (FaustusState.boundValues fs)\<rparr>)\<rparr>" using ps by auto
      moreover have "boundPubKeys otherEvaluatedArgs = boundPubKeys evaluatedArgs" using ps calculation evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys apply auto
        by (metis calculation(6))
      ultimately show ?thesis using ps by auto
    qed
    done
next
  case (6 obsId restParams obs restArgs fs)
  then show ?case apply auto
    subgoal premises ps proof (cases "evalFaustusObservation env fs obs")
      case None
      then show ?thesis using ps by auto
    next
      case (Some a)
      moreover obtain evaluatedArgs where "evalFaustusArguments env (fs\<lparr>FaustusState.boundValues := MList.insert obsId (if a then 1 else 0) (FaustusState.boundValues fs)\<rparr>) restParams restArgs = Some evaluatedArgs" using ps calculation by auto
      moreover obtain tyCtx where "wellTypedState tyCtx fs \<and> wellTypedArguments tyCtx restParams restArgs" using ps by auto
      moreover have "wellTypedState tyCtx (fs\<lparr>boundValues := MList.insert obsId 0 (boundValues fs)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
      moreover obtain otherEvaluatedArgs where "(evalFaustusArguments env (fs\<lparr>boundValues:=MList.insert obsId 0 (boundValues fs)\<rparr>) restParams restArgs = Some otherEvaluatedArgs)" using ps calculation wellTypedArgumentsExecute by fastforce
      moreover have "(fs\<lparr>boundValues:=MList.insert obsId 0 (boundValues fs)\<rparr>) = (fs\<lparr>boundValues:=MList.insert obsId 0 (boundValues fs)\<rparr>)\<lparr>boundPubKeys := boundPubKeys (fs\<lparr>FaustusState.boundValues := MList.insert obsId (if a then 1 else 0) (FaustusState.boundValues fs)\<rparr>)\<rparr>" using ps by auto
      moreover have "boundPubKeys otherEvaluatedArgs = boundPubKeys evaluatedArgs" using ps calculation evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys apply auto
        by (metis calculation(6))
      ultimately show ?thesis using ps by auto
    qed
    done
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma compileArgumentsOnlyBoundPubKeysMatter:
"(\<forall>newState compiledContract . compileArguments params args state bodyCont = Some compiledContract \<longrightarrow>
  compileArguments params args (newState\<lparr>boundPubKeys := (boundPubKeys state)\<rparr>) bodyCont = Some compiledContract)"
proof (induction rule: compileArguments.induct)
  case (1 fs cont)
  then show ?case by auto
next
  case (2 v va fs cont)
  then show ?case by auto
next
  case (3 v va fs cont)
  then show ?case by auto
next
  case (4 valId restParams val restArgs fs cont)
  then show ?case apply (cases "compileFaustusValue val fs", auto)
    apply (split option.split_asm, auto)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
     apply (metis stateModificationOrderArbitrary(3))
    by (metis option.sel stateModificationOrderArbitrary(3))
next
  case (5 obsId restParams obs restArgs fs cont)
  then show ?case apply (cases "compileFaustusObservation obs fs", auto)
    apply (split option.split_asm, auto)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
     apply (metis stateModificationOrderArbitrary(3))
    by (metis option.sel stateModificationOrderArbitrary(3))
next
  case (6 pkId restParams pk restArgs fs cont)
  then show ?case apply auto
    apply (split option.split_asm)
    apply simp
    apply (split option.split)
    using compilePubKeyOnlyBoundPubKeysMatter by fastforce
next
  case ("7_1")
  then show ?case by auto
next
  case ("7_2")
  then show ?case by auto
next
  case ("7_3")
  then show ?case by auto
next
  case ("7_4")
  then show ?case by auto
next
  case ("7_5")
  then show ?case by auto
next
  case ("7_6")
  then show ?case by auto
next
  case ("7_7")
  then show ?case by auto
next
  case ("7_8")
  then show ?case by auto
qed

lemma compileActionOnlyBoundPubKeysMatter:
"\<forall>compiledAction newState . compileAction action state = Some compiledAction \<longrightarrow>
  compileAction action (newState\<lparr>FaustusState.boundPubKeys := (boundPubKeys state)\<rparr>) = Some compiledAction"
proof (induction rule: compileAction.induct)
  case (1 fromPk toPk token "value" s)
  then show ?case apply (cases "compileFaustusPubKey fromPk s" "compileFaustusPubKey toPk s" "compileFaustusValue value s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter)
next
  case (2 choiceId bounds s)
  then show ?case by (auto split: option.split simp add: compileChoiceIdOnlyBoundPubKeysMatter)
next
  case (3 observation s)
  then show ?case apply (cases "compileFaustusObservation observation s", auto)
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter)
qed

lemma compileContractOnlyBoundPubKeysMatter_UseC_Inductive:
"argumentsToCompilerState params args state = Some argCtx \<Longrightarrow>
          paramsBodyInnerCtx = (params, bodyCont, innerCtx) \<Longrightarrow>
          lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) \<Longrightarrow>
          (argumentsToCompilerState params args state = Some argCtx \<Longrightarrow>
              \<forall>marloweContract.
                 compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some marloweContract \<longrightarrow>
                 (\<forall>newState. compile bodyCont (paramsToFaustusContext params @ innerCtx) (newState\<lparr>boundPubKeys := boundPubKeys argCtx\<rparr>) = Some marloweContract)) \<Longrightarrow>
          compileArguments params args state compiledBodyCont = Some arbMarloweContract \<Longrightarrow>
          compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some compiledBodyCont \<Longrightarrow>
          argumentsToCompilerState params args (arbNewState\<lparr>boundPubKeys := boundPubKeys state\<rparr>) = Some x2 \<Longrightarrow>
          (case compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 of None \<Rightarrow> None
           | Some x \<Rightarrow> compileArguments params args (arbNewState\<lparr>boundPubKeys := boundPubKeys state\<rparr>) x) =
          Some arbMarloweContract"
  apply auto
  subgoal premises ps proof -
    have "boundPubKeys x2 = boundPubKeys argCtx" using ps argumentsToContextBoundPubKeysDeterministic by presburger
    moreover have "x2 = x2\<lparr>boundPubKeys := boundPubKeys argCtx\<rparr>" using ps calculation by auto
    moreover have "compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 = compile bodyCont (paramsToFaustusContext params @ innerCtx) (x2\<lparr>boundPubKeys := boundPubKeys argCtx\<rparr>)" using ps calculation by (metis calculation(2))
    moreover have "compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 = Some compiledBodyCont" using ps by (simp add: calculation(3))
    moreover have "compileArguments params args (arbNewState\<lparr>boundPubKeys := boundPubKeys state\<rparr>) compiledBodyCont = Some arbMarloweContract" by (metis ps(5) compileArgumentsOnlyBoundPubKeysMatter)
    ultimately show ?thesis by (metis option.simps(5))
  qed
  done

lemma compileContractOnlyBoundPubKeysMatter:
"\<forall>marloweContract newState . compile faustusContract ctx state = Some marloweContract \<longrightarrow>
  compile faustusContract ctx (newState\<lparr>FaustusState.boundPubKeys := (boundPubKeys state)\<rparr>) = Some marloweContract"
"\<forall>marloweCases newState . compileCases faustusCases ctx state = Some marloweCases \<longrightarrow>
  compileCases faustusCases ctx (newState\<lparr>FaustusState.boundPubKeys := (boundPubKeys state)\<rparr>) = Some marloweCases"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx state)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusPubKey accId state", auto)
    apply (cases "compileFaustusPayee payee state", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: compilePayeeOnlyBoundPubKeysMatter compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter(1))
next
  case (3 obs trueCont falseCont ctx state)
  then show ?case apply (cases "compile trueCont ctx state" "compile falseCont ctx state" rule: option.exhaust[case_product option.exhaust], auto)
    by (cases "compileFaustusObservation obs state", auto simp add: compileValueOnlyBoundPubKeysMatter(2))
next
  case (4 cases t tCont ctx state)
  then show ?case by (cases "compile tCont ctx state" "compileCases cases ctx state" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 valId val cont ctx state)
  then show ?case apply (cases "compile cont (ValueAbstraction valId # ctx) (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>)", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (metis option.simps(5) stateModificationOrderArbitrary(3) compileValueOnlyBoundPubKeysMatter(1))
next
  case (6 obsId obs cont ctx state)
  then show ?case apply (cases "compile cont (ObservationAbstraction obsId # ctx) (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (metis option.simps(5) stateModificationOrderArbitrary(3) compileValueOnlyBoundPubKeysMatter(2))
next
  case (7 pkId pk cont ctx state)
  then show ?case apply (simp (no_asm_simp) only: compile.simps)
    subgoal premises ps proof (cases "compileFaustusPubKey pk state")
      case None
      then show ?thesis by auto
    next
      case (Some pkCompiled)
      then show ?thesis using ps apply (cases "compile cont (PubKeyAbstraction pkId # ctx) (state\<lparr>boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys state)\<rparr>)", simp (no_asm_simp))
        using compilePubKeyOnlyBoundPubKeysMatter stateModificationOrderArbitrary by fastforce
    qed
    done
next
  case (8 obs cont ctx state)
  then show ?case apply auto
    apply (cases "compile cont ctx state", auto)
    by (metis compileValueOnlyBoundPubKeysMatter(2) option.discI option.disc_eq_case(1) option.split_sel)
next
  case (9 cid params boundCon cont ctx state)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof (cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps by auto
    next
      case (Some paramsBodyInnerCtx)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases paramsBodyInnerCtx)
          case (fields params bodyCont innerCtx)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using ps by auto
            next
              case (Some argCtx)
              moreover obtain compiledBodyCont where "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some compiledBodyCont" using calculation ps by fastforce
              ultimately show ?thesis using ps apply auto
                apply (cases "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx", auto)
                apply (split option.split, auto)
                using argumentsToContextOnlyBoundPubKeysMatter argumentsToContextBoundPubKeysDeterministic apply (auto)[1]
                using compileContractOnlyBoundPubKeysMatter_UseC_Inductive by fastforce
            qed
            done
        qed
        done
    qed
    done
next
  case (11 a c rest ctx state)
  then show ?case apply (cases "compile c ctx state" "compileCases rest ctx state" "compileAction a state" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    by (simp add: compileActionOnlyBoundPubKeysMatter)
next
  case (12 ctx state)
  then show ?case by auto
qed

(* Problem: What to do about shadowed ids of different types. Should we say ill typed in well typed?*)
fun faustusStateToMarloweState :: "FaustusState \<Rightarrow> State" where
"faustusStateToMarloweState fs = \<lparr>State.accounts = accounts fs,
  State.choices = choices fs,
  State.boundValues = boundValues fs,
  State.minSlot = minSlot fs\<rparr>"

lemma faustusStateToMarloweStatePreservationOfBoundValues:
"boundValues state = State.boundValues (faustusStateToMarloweState state)"
  by auto

lemma preservationOfPubKeyEvaluation:
"(\<forall>tyCtx env res. wellTypedPubKey tyCtx faustusPubKey \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalFaustusPubKey faustusState faustusPubKey = compileFaustusPubKey faustusPubKey faustusState)"
  by (cases faustusPubKey, auto)

lemma preservationOfPayeeEvaluation:
"(\<forall>tyCtx env res. wellTypedPayee tyCtx faustusPayee \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalFaustusPayee faustusState faustusPayee = compileFaustusPayee faustusPayee faustusState)"
  by (cases faustusPayee, auto split: option.split simp add: preservationOfPubKeyEvaluation)

lemma preservationOfChoiceIdEvaluation:
"(\<forall>tyCtx env res. wellTypedChoiceId tyCtx faustusChoiceId \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalFaustusChoiceId faustusState faustusChoiceId = compileFaustusChoiceId faustusChoiceId faustusState)"
  by (cases faustusChoiceId, auto split: option.split simp add: preservationOfPubKeyEvaluation)

lemma preservationOfValueEvaluation:
"(\<forall>tyCtx res. wellTypedValue tyCtx faustusValue \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalFaustusValue env faustusState faustusValue = Some res \<longrightarrow>
  evalValue env (faustusStateToMarloweState faustusState) (the (compileFaustusValue faustusValue faustusState)) = res)"
"(\<forall>tyCtx res. wellTypedObservation tyCtx faustusObservation \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalFaustusObservation env faustusState faustusObservation = Some res \<longrightarrow>
  evalObservation env (faustusStateToMarloweState faustusState) (the (compileFaustusObservation faustusObservation faustusState)) = res)"
proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
  case (1 env state faustusPubKey token)
  then show ?case apply auto
    by (smt wellTypedPubKeyCompiles wellTypedPubKeyEvaluates FaustusPubKey.exhaust State.select_convs(1) compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) evalValue.simps(1) findWithDefault.simps option.simps(5) option.the_def)
next
  case (2 env state integer)
  then show ?case by auto
next
  case (3 env state val)
  then show ?case apply auto
    by (metis (no_types, lifting) evalValue.simps(3) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(1))
next
  case (4 env state lhs rhs)
  then show ?case apply auto
    by (smt evalValue.simps(4) not_None_eq option.case_eq_if option.exhaust_sel option.inject wellTypedValueObservationCompiles(1) wellTypedValueObservationExecutes(1))
next
  case (5 env state lhs rhs)
  then show ?case apply auto
  by (smt evalValue.simps(5) is_none_code(1) is_none_code(2) option.case_eq_if option.sel option.split_sel wellTypedValueObservationCompiles(1))
next
  case (6 env state lhs rhs)
  then show ?case apply auto
    by (smt evalValue.simps(6) is_none_code(2) option.case_eq_if option.discI option.sel option.split_sel wellTypedValueObservationCompiles(1))
next
  case (7 env state n d rhs)
  then show ?case apply auto
    subgoal premises ps proof -
      obtain evalRhs where "evalFaustusValue env state rhs = Some evalRhs" using ps wellTypedValueObservationExecutes by fastforce
      moreover obtain compiledRhs where "compileFaustusValue rhs state = Some compiledRhs" using ps wellTypedValueObservationCompiles by fastforce
      ultimately show ?thesis using ps apply auto
        apply (cases "evalRhs * n < 0" "d < 0" rule: bool.exhaust[case_product bool.exhaust], auto)
           apply (cases "\<bar>(evalRhs * n) - (evalRhs * n) div d * d\<bar> * 2 < - d", auto)
          apply (cases "\<bar>(evalRhs * n) + - (evalRhs * n) div d * d\<bar> * 2 < d", auto)
         apply (cases "\<bar>(evalRhs * n) + (evalRhs * n) div - d * d\<bar> * 2 < - d", auto)
         apply (cases "0 < (evalRhs * n)", auto)
        apply (cases "\<bar>(evalRhs * n) - (evalRhs * n) div d * d\<bar> * 2 < d", auto)
        by (cases "0 < (evalRhs * n)", auto)
    qed
    done
next
  case (8 env state choId)
  then show ?case by (auto split: option.split simp add: preservationOfChoiceIdEvaluation)
next
  case (9 env state)
  then show ?case by auto
next
  case (10 env state)
  then show ?case by auto
next
  case (11 env state valId)
  then show ?case by auto
next
  case (12 env state cond thn els)
  then show ?case apply auto
    by (smt evalValue.simps(12) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (13 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(1) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(1) is_none_code(2) option.case_eq_if option.sel option.split_sel)
next
  case (14 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(2) option.case_eq_if option.collapse option.discI option.sel)
    by (smt evalObservation.simps(2) option.case_eq_if option.distinct(1) option.exhaust option.sel option.simps(5) wellTypedValueObservationCompiles(2))
next
  case (15 env state subObs)
  then show ?case apply auto
     apply (metis (no_types, lifting) evalObservation.simps(3) is_none_code(2) is_none_simps(1) option.case_eq_if option.collapse option.sel wellTypedValueObservationCompiles(2))
    by (metis evalObservation.simps(3) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(2))
next
  case (16 env state choId)
  then show ?case by (auto split: option.split simp add: preservationOfChoiceIdEvaluation)
next
  case (17 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(5) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(5) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (18 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(6) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(6) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (19 env state lhs rhs)
  then show ?case apply auto
    apply (smt evalObservation.simps(7) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(7) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (20 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(8) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(8) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (21 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(9) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(9) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (22 env state obsId)
  then show ?case apply auto
     apply (smt Collect_empty_eq None_eq_map_option_iff Option.is_none_def all_not_in_conv bind.bind_lunit bind_eq_None_conv bind_eq_Some_conv case_map_option case_optionE combine_options_def combine_options_simps(3) iszero_0 iszero_def map_option_eq_Some mem_Collect_eq not_None_eq not_Some_eq not_iszero_1 odd_nonzero option.case_distrib option.case_eq_if option.discI option.disc_eq_case(2) option.exhaust option.exhaust_sel option.inject option.map_ident option.map_sel option.sel option.simps(15) option.simps(4) option.simps(5) option.split_sel_asm option.the_def ospec split_option_all split_option_ex zero_neq_one)
    by (smt option.case_eq_if option.exhaust_sel option.simps(5) option.the_def)
next
  case (23 env state)
  then show ?case by auto
next
  case (24 env state)
  then show ?case by auto
qed

lemma preservationOfValueEvaluation_MarloweImpliesFaustus:
"(\<forall>tyCtx res. wellTypedValue tyCtx faustusValue \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalValue env (faustusStateToMarloweState faustusState) (the (compileFaustusValue faustusValue faustusState)) = res \<longrightarrow>
  evalFaustusValue env faustusState faustusValue = Some res)"
"(\<forall>tyCtx res. wellTypedObservation tyCtx faustusObservation \<longrightarrow>
  wellTypedState tyCtx faustusState \<longrightarrow>
  evalObservation env (faustusStateToMarloweState faustusState) (the (compileFaustusObservation faustusObservation faustusState)) = res \<longrightarrow>
  evalFaustusObservation env faustusState faustusObservation = Some res)"
proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
  case (1 env state faustusPubKey token)
  then show ?case apply auto
    apply (cases "evalFaustusPubKey state faustusPubKey", auto)
     apply (simp add: wellTypedPubKeyEvaluates)
    apply (cases "compileFaustusPubKey faustusPubKey state", auto)
    using wellTypedPubKeyCompiles apply fastforce
    by (metis FaustusPubKey.exhaust compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) option.simps(5))
next
  case (2 env state integer)
  then show ?case by auto
next
  case (3 env state val)
  then show ?case apply auto
    apply (cases "compileFaustusValue val state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (4 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (5 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (6 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (7 env state n d rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    by metis
next
  case (8 env state choId)
  then show ?case apply (auto split: option.split simp add: preservationOfChoiceIdEvaluation)
    using wellTypedChoiceIdCompiles by fastforce
next
  case (9 env state)
  then show ?case by auto
next
  case (10 env state)
  then show ?case by auto
next
  case (11 env state valId)
  then show ?case apply auto
    apply (cases "lookup valId (FaustusState.boundValues state)", auto)
    using wellTypedValueObservationExecutes(1) by fastforce
next
  case (12 env state cond thn els)
  then show ?case apply auto
     apply (smt evalValue.simps(12) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalValue.simps(12) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (13 env state lhs rhs)
  then show ?case apply auto
      apply (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
     apply (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (14 env state lhs rhs)
  then show ?case apply auto
      apply (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
     apply (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (15 env state subObs)
  then show ?case apply auto
     apply (smt evalObservation.simps(3) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(3) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (16 env state choId)
  then show ?case apply (auto split: option.split simp add: preservationOfChoiceIdEvaluation)
    using wellTypedChoiceIdCompiles by fastforce
next
  case (17 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(5) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(5) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (18 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(6) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(6) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (19 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(7) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(7) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (20 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(8) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(8) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (21 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(9) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(9) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (22 env state obsId)
  then show ?case apply auto
    apply (cases "lookup obsId (FaustusState.boundValues state)", auto)
    using wellTypedValueObservationExecutes(2) by fastforce
next
  case (23 env state)
  then show ?case by auto
next
  case (24 env state)
  then show ?case by auto
qed

lemma preservationOfArgumentEvaluation_ValueParameter_Induct:
"(\<And>arbEvalVal. evaluatedVal = arbEvalVal \<Longrightarrow>
               \<forall>newState compiledBody compiledFullContract warnings payments tyCtx.
                  wellTypedArguments tyCtx restParams restArgs \<longrightarrow>
                  wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)\<rparr>) \<longrightarrow>
                  evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)\<rparr>) restParams restArgs =
                  Some newState \<longrightarrow>
                  compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)\<rparr>) compiledBody =
                  Some compiledFullContract \<longrightarrow>
                  (\<exists>newMWarnings.
                      (compiledFullContract,
                       \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s,
                          boundValues = MList.insert vid arbEvalVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>,
                       e, warnings, payments) \<rightarrow>*
                      (compiledBody,
                       \<lparr>State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState,
                          boundValues = FaustusState.boundValues newState, minSlot = FaustusState.minSlot newState\<rparr>,
                       e, newMWarnings, payments))) \<Longrightarrow>
       wellTypedValue tyCtx val \<Longrightarrow>
       wellTypedArguments tyCtx restParams restArgs \<Longrightarrow>
       wellTypedState tyCtx s \<Longrightarrow>
       evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr>) restParams restArgs = Some newState \<Longrightarrow>
       (case compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)\<rparr>) compiledBody of
        None \<Rightarrow> None | Some compiledRest \<Rightarrow> Some (Contract.Let vid compiledVal compiledRest)) =
       Some compiledFullContract \<Longrightarrow>
       evalFaustusValue e s val = Some evaluatedVal \<Longrightarrow>
       compileFaustusValue val s = Some compiledVal \<Longrightarrow>
       \<exists>newMWarnings.
          (compiledFullContract,
           \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = FaustusState.boundValues s,
              minSlot = FaustusState.minSlot s\<rparr>,
           e, warnings, payments) \<rightarrow>*
          (compiledBody,
           \<lparr>State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState, boundValues = FaustusState.boundValues newState,
              minSlot = FaustusState.minSlot newState\<rparr>,
           e, newMWarnings, payments)"
  subgoal premises ps proof-
    have "evalValue e (faustusStateToMarloweState s) compiledVal = evaluatedVal" using ps preservationOfValueEvaluation(1) by fastforce
    moreover obtain tyCtx where "wellTypedValue tyCtx val \<and> wellTypedArguments tyCtx restParams restArgs \<and> wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover have "wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr>) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles apply auto
      by blast
    moreover have "s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr> = (s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr>)\<lparr>boundPubKeys:=(boundPubKeys (s\<lparr>FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)\<rparr>))\<rparr>" by auto
    moreover have "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation apply auto
      by (metis calculation(7) compileArgumentsOnlyBoundPubKeysMatter)
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (Semantics.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, (faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert vid evaluatedVal (State.boundValues (faustusStateToMarloweState s))\<rparr>, e, newMWarnings, payments))" using ps calculation apply (cases "lookup vid (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply blast
      using SmallStep.LetShadow by blast
    moreover have "(faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert vid evaluatedVal (State.boundValues (faustusStateToMarloweState s))\<rparr> = \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>" by auto
    moreover have "(\<exists>newMWarnings. (Semantics.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt \<open>\<exists>newMWarnings. (Contract.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments)\<close> \<open>\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)\<close> faustusStateToMarloweState.simps star.step)
  qed
  done

lemma preservationOfArgumentEvaluation_ObservationParameter_Induct:
"(\<And>arbEvalObs. evaluatedObs = arbEvalObs \<Longrightarrow>
               \<forall>newState compiledBody compiledFullContract warnings payments tyCtx.
                  wellTypedArguments tyCtx restParams restArgs \<longrightarrow>
                  wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)\<rparr>) \<longrightarrow>
                  evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)\<rparr>)
                   restParams restArgs =
                  Some newState \<longrightarrow>
                  compileArguments restParams restArgs
                   (s\<lparr>FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)\<rparr>) compiledBody =
                  Some compiledFullContract \<longrightarrow>
                  (\<exists>newMWarnings.
                      (compiledFullContract,
                       \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s,
                          boundValues = MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>,
                       e, warnings, payments) \<rightarrow>*
                      (compiledBody,
                       \<lparr>State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState,
                          boundValues = FaustusState.boundValues newState, minSlot = FaustusState.minSlot newState\<rparr>,
                       e, newMWarnings, payments))) \<Longrightarrow>
       evalFaustusObservation e s obs = Some evaluatedObs \<Longrightarrow>
       compileFaustusObservation obs s = Some compiledObs \<Longrightarrow>
       wellTypedObservation tyCtx obs \<Longrightarrow>
       wellTypedArguments tyCtx restParams restArgs \<Longrightarrow>
       wellTypedState tyCtx s \<Longrightarrow>
       evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert obsId (if evaluatedObs then 1 else 0) (FaustusState.boundValues s)\<rparr>) restParams
        restArgs =
       Some newState \<Longrightarrow>
       (case compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>) compiledBody of
        None \<Rightarrow> None | Some compiledRest \<Rightarrow> Some (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest)) =
       Some compiledFullContract \<Longrightarrow>
       \<exists>newMWarnings.
          (compiledFullContract,
           \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = FaustusState.boundValues s,
              minSlot = FaustusState.minSlot s\<rparr>,
           e, warnings, payments) \<rightarrow>*
          (compiledBody,
           \<lparr>State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState, boundValues = FaustusState.boundValues newState,
              minSlot = FaustusState.minSlot newState\<rparr>,
           e, newMWarnings, payments)"
  apply (cases evaluatedObs, auto)
  subgoal premises ps proof-
    have "evaluatedObs = True" using ps by auto
    moreover have "evalObservation e (faustusStateToMarloweState s) compiledObs = True" using ps preservationOfValueEvaluation(2) by fastforce
    moreover have "evalValue e (faustusStateToMarloweState s) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) = 1" using ps calculation by auto
    moreover obtain tyCtx where "wellTypedObservation tyCtx obs \<and> wellTypedArguments tyCtx restParams restArgs \<and> wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover have "wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr>) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles by fastforce
    moreover have "s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr> = (s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr>)\<lparr>boundPubKeys:=(boundPubKeys (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>))\<rparr>" by auto
    moreover have "s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr> = (s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr>)\<lparr>boundPubKeys:=(boundPubKeys (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>))\<rparr>" by auto
    moreover have "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation apply auto
      by (metis calculation(9) compileArgumentsOnlyBoundPubKeysMatter)
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, (faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState s))\<rparr>, e, newMWarnings, payments))" using calculation(3) apply (cases "lookup obsId (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply metis
      using SmallStep.LetShadow by metis
    moreover have "(faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState s))\<rparr> = \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>" by auto
    moreover have "(\<exists>newMWarnings. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt \<open>\<exists>newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments)\<close> \<open>\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)\<close> faustusStateToMarloweState.simps star.step)
  qed
  subgoal premises ps proof-
    have "evaluatedObs = False" using ps by auto
    moreover have "evalObservation e (faustusStateToMarloweState s) compiledObs = False" using ps preservationOfValueEvaluation(2) by fastforce
    moreover have "evalValue e (faustusStateToMarloweState s) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) = 0" using ps calculation by auto
    moreover obtain tyCtx where "wellTypedObservation tyCtx obs \<and> wellTypedArguments tyCtx restParams restArgs \<and> wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles by fastforce
    moreover have "s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr> = (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>)\<lparr>boundPubKeys:=(boundPubKeys (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>))\<rparr>" by auto
    moreover have "s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr> = (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>)\<lparr>boundPubKeys:=(boundPubKeys (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>))\<rparr>" by auto
    moreover have "compileArguments restParams restArgs (s\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)\<rparr>) compiledBody = Some compiledRest" using ps calculation by auto
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(\<forall>warnings payments. \<exists>newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, (faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState s))\<rparr>, e, newMWarnings, payments))" using calculation(3) apply (cases "lookup obsId (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply metis
      using SmallStep.LetShadow by metis
    moreover have "(faustusStateToMarloweState s)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState s))\<rparr> = \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>" by auto
    moreover have "(\<exists>newMWarnings. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt \<open>\<exists>newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) \<rightarrow> (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, newMWarnings, payments)\<close> \<open>\<forall>warnings payments. \<exists>newMWarnings. (compiledRest, \<lparr>State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s\<rparr>, e, warnings, payments) \<rightarrow>* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)\<close> faustusStateToMarloweState.simps star.step)
  qed
  done

(* EvalFaustusArguments will give same state as executing compiled Marlowe to body. *)
lemma preservationOfArgumentEvaluation:
"(\<forall>newState compiledBody compiledFullContract warnings payments tyCtx argCtx. wellTypedArguments tyCtx params args \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  evalFaustusArguments env state params args = Some newState \<longrightarrow>
  compileArguments params args state compiledBody = Some compiledFullContract \<longrightarrow>
  (\<exists>newMWarnings . (compiledFullContract, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow>*
  (compiledBody, faustusStateToMarloweState newState, env, newMWarnings, payments)))"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply (cases "evalFaustusValue e s val" "compileFaustusValue val s" rule: option.exhaust[case_product option.exhaust], auto)
    using preservationOfArgumentEvaluation_ValueParameter_Induct by presburger
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply (cases "evalFaustusObservation e s obs" "compileFaustusObservation obs s" rule: option.exhaust[case_product option.exhaust], auto split del: if_split)
    using preservationOfArgumentEvaluation_ObservationParameter_Induct by presburger
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusPubKey s pk", auto)
    apply (cases "compileFaustusPubKey pk s", auto)
    by (simp add: insertPubKeyWellTypedStateIsWellTyped preservationOfPubKeyEvaluation)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

(* Zero or more well typed Faustus steps correspond
  to zero or more Marlowe steps. *)
lemma preservationOfSemantics_FaustusStepImpMarloweSteps:
"f \<rightarrow>\<^sub>f f' \<Longrightarrow>
  f = (faustusContract, ctx, state, env, warnings, payments) \<Longrightarrow>
  f' = (newFaustusContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  wellTypedContract tyCtx faustusContract \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  compile faustusContract ctx state = Some marloweContract \<Longrightarrow>
  compile newFaustusContract newCtx newState = Some newMarloweContract \<Longrightarrow>
  (\<exists>newMWarnings . (marloweContract, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow>* (newMarloweContract, faustusStateToMarloweState newState, newEnv, newMWarnings, newPayments))"
proof (induction rule: small_step_reduce.induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  moreover have "refundOne (State.accounts (faustusStateToMarloweState state)) = Some ((party, token, money), newAccount)" using calculation by auto
  moreover have "\<lparr>State.accounts = newAccount, choices = FaustusState.choices state, boundValues = FaustusState.boundValues state, minSlot = FaustusState.minSlot state\<rparr> = (faustusStateToMarloweState state)\<lparr>State.accounts := newAccount\<rparr>" using calculation by auto
  moreover have "(Contract.Close, faustusStateToMarloweState state, newEnv, warnings, payments) \<rightarrow> (Contract.Close, (faustusStateToMarloweState state)\<lparr>State.accounts := newAccount\<rparr>, newEnv, warnings @ [ReduceNoWarning], payments @ [Payment party token money])" using calculation SmallStep.CloseRefund by blast
  ultimately show ?case by fastforce
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  moreover obtain compiledAccId where "compileFaustusPubKey accId newState = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee newState = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val newState = Some compiledFaustusValue" using calculation by fastforce
  ultimately show ?case apply auto
    using preservationOfValueEvaluation(1) by fastforce
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > 0" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token 0 (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyToPay" using calculation apply (auto split: option.split)
      apply (metis compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.elims option.distinct(1) option.sel)
     apply (metis compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.elims option.distinct(1) option.sel)
    by (metis calculation(14) calculation(19) calculation(3) option.inject preservationOfPubKeyEvaluation)
  moreover have "toPayee = compiledPayee" using calculation wellTypedPubKeyEvaluates apply auto
    apply (cases payee, simp)
     apply (metis calculation(4) evalFaustusPayee.simps(1) option.inject preservationOfPubKeyEvaluation)
    by (metis compileFaustusPayee.simps(2) compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPayee.simps(2) evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) option.inject wellTypedPayee.simps(2) wellTypedPubKey.elims(2))
  moreover have "giveMoney compiledPayee token moneyToPay newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(15) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow> (newCompiledCont, (faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>, newEnv, warnings @ [ReducePartialPay compiledAccId compiledPayee token moneyToPay (evalValue env (faustusStateToMarloweState state) compiledFaustusValue)], payments @ [somePayment])" using calculation by blast
  moreover have "((faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > 0" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token 0 (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyToPay" using calculation apply (auto split: option.split)
      apply (metis compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.elims option.distinct(1) option.sel)
     apply (metis compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.elims option.distinct(1) option.sel)
    by (metis calculation(14) calculation(19) calculation(3) option.inject preservationOfPubKeyEvaluation)
  moreover have "toPayee = compiledPayee" using calculation preservationOfPayeeEvaluation by auto
  moreover have "giveMoney compiledPayee token moneyToPay newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(15) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow> (newCompiledCont, (faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>, newEnv, warnings @ [ReducePartialPay compiledAccId compiledPayee token moneyToPay (evalValue env (faustusStateToMarloweState state) compiledFaustusValue)], payments)" using calculation by blast
  moreover have "((faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue = res" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue \<le> moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "updateMoneyInAccount compiledAccId token newBalance (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyInAcc" using calculation apply simp
    by (metis compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.elims option.sel)
  moreover have "giveMoney compiledPayee token res newAccs = (payment, finalAccs)" using calculation preservationOfPayeeEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation by auto
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow> (newCompiledCont, (faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>, newEnv, warnings @ [ReduceNoWarning], payments @ [somePayment])" using calculation by blast
  moreover have "((faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue = res" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue \<le> moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "updateMoneyInAccount compiledAccId token newBalance (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by auto
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyInAcc" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "toPayee = compiledPayee" using calculation preservationOfPayeeEvaluation wellTypedPubKeyEvaluates by auto
  moreover have "giveMoney compiledPayee token res newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(16) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow> (newCompiledCont, (faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>, newEnv, warnings @ [ReduceNoWarning], payments)" using calculation by blast
  moreover have "((faustusStateToMarloweState state)\<lparr>State.accounts := finalAccs\<rparr>) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  then show ?case apply (cases "compile cont2 newCtx newState", auto)
    using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles(2) wellTypedCompiles by fastforce
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles(2) wellTypedCompiles by fastforce
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  then show ?case apply auto
    by (cases "compileCases cases newCtx newState", auto)
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  moreover have "MList.lookup valId (State.boundValues (faustusStateToMarloweState state)) = Some oldVal" using calculation by auto
  moreover obtain compiledVal where "compileFaustusValue val state = Some compiledVal" using calculation wellTypedValueObservationCompiles(1) by fastforce
  moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using calculation wellTypedCompiles by fastforce
  moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(12) calculation(13))
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (boundValues state)\<rparr>" using calculation by auto
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
  moreover have "(evalValue env (faustusStateToMarloweState state) compiledVal) = res" using calculation preservationOfValueEvaluation by fastforce
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow>
    (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ [ReduceShadowing valId oldVal (res)], payments)" using calculation by blast
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow>
    (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceShadowing valId oldVal (res)], payments)" using calculation by auto
  ultimately show ?case apply auto
    by (metis (no_types, lifting) State.update_convs(3) \<open>(Contract.Let valId compiledVal compiledCont, faustusStateToMarloweState state, env, warns, payments) \<rightarrow> (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceShadowing valId oldVal res], payments)\<close> \<open>faustusStateToMarloweState newState = faustusStateToMarloweState state \<lparr>State.boundValues := MList.insert valId res (State.boundValues (faustusStateToMarloweState state))\<rparr>\<close> faustusStateToMarloweState.simps faustusStateToMarloweStatePreservationOfBoundValues star_step1)
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  moreover have "MList.lookup valId (State.boundValues (faustusStateToMarloweState state)) = None" using calculation by auto
  moreover obtain compiledVal where "compileFaustusValue val state = Some compiledVal" using calculation wellTypedValueObservationCompiles(1) by fastforce
  moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using calculation wellTypedCompiles by fastforce
  moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(12) calculation(13))
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (boundValues state)\<rparr>" using calculation by auto
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
  moreover have "(evalValue env (faustusStateToMarloweState state) compiledVal) = res" using calculation preservationOfValueEvaluation by fastforce
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow>
    (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ [ReduceNoWarning], payments)" using calculation by blast
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow>
    (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceNoWarning], payments)" using calculation by auto
  ultimately show ?case apply auto
    by (metis (no_types, lifting) State.update_convs(3) \<open>(Contract.Let valId compiledVal compiledCont, faustusStateToMarloweState state, env, warns, payments) \<rightarrow> (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceNoWarning], payments)\<close> \<open>faustusStateToMarloweState newState = faustusStateToMarloweState state \<lparr>State.boundValues := MList.insert valId res (State.boundValues (faustusStateToMarloweState state))\<rparr>\<close> faustusStateToMarloweState.simps faustusStateToMarloweStatePreservationOfBoundValues star_step1)
next
  case (LetObservation env s obs res obsId cont ctx warns payments)
  then show ?case apply auto
    subgoal premises ps proof (cases "MList.lookup obsId (State.boundValues (faustusStateToMarloweState state))")
      case None
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(4) calculation(3))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (boundValues state)\<rparr>" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using calculation preservationOfValueEvaluation by fastforce
      moreover have "(Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ [ReduceNoWarning], payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetNoShadow)
      moreover have "(Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceNoWarning], payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) \<open>(Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, faustusStateToMarloweState state, env, warns, payments) \<rightarrow> (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceNoWarning], payments)\<close> faustusStateToMarloweState.simps option.inject option.simps(5) ps(4) ps(5) ps(7) ps(9) ps(10) ps(11) ps(12) ps(13) ps(14) ps(15) ps(17) star_step1)
    next
      case (Some x2)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(3) calculation(4))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (boundValues state)\<rparr>" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using calculation preservationOfValueEvaluation by fastforce
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetShadow)
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) LetObservation.prems(7) \<open>\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)\<close> faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) ps(9) star_step1)
    qed
    subgoal premises ps proof (cases "MList.lookup obsId (State.boundValues (faustusStateToMarloweState state))")
      case (None)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(4) calculation(3))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (boundValues state)\<rparr>" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using calculation preservationOfValueEvaluation by fastforce
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetNoShadow)
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply (auto)
        by (metis (no_types, lifting) LetObservation.prems(7) \<open>\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)\<close> faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) star_step1)
    next
      case (Some x2)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState\<lparr>boundPubKeys := boundPubKeys (state\<lparr>FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)\<rparr>)\<rparr>" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(3) calculation(4))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (boundValues state)\<rparr>" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))\<rparr>" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using calculation preservationOfValueEvaluation by fastforce
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState state)\<lparr>State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))\<rparr>, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetShadow)
      moreover have "\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) LetObservation.prems(7) \<open>\<exists>newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) \<rightarrow> (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)\<close> faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) star_step1)
    qed
    done
next
  case (LetPubKey s pk res pkId cont ctx env warns payments)
  then show ?case using star.refl preservationOfPubKeyEvaluation by fastforce
next
  case (AssertTrue env s obs cont ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles by fastforce
next
  case (AssertFalse env s obs cont ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles by fastforce
next
  case (LetC cid params boundCon cont ctx s env warns payments)
  then show ?case by fastforce
next
  case (UseCFound ctx cid params bodyCont innerCtx newState env s args warns payments)
  moreover obtain innerTyCtx where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, innerTyCtx)" using calculation lookupContractTypeWellTypedContext by fastforce
  moreover have "wellTypedArguments tyCtx params args" using calculation by auto
  moreover obtain argState where "argumentsToCompilerState params args state = Some argState" using calculation wellTypedArgumentsCompiles by fastforce
  moreover have "boundPubKeys argState = boundPubKeys newState" using calculation argumentsToContextBoundPubKeysSameAsEvaluateArguments by auto
  moreover have "newState\<lparr>boundPubKeys := boundPubKeys argState\<rparr> = newState" using calculation by auto
  moreover obtain innerTyCtx where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, innerTyCtx)" using calculation lookupContractTypeWellTypedContext by blast
  moreover obtain compiledBody where "compile newFaustusContract newCtx argState = Some compiledBody" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "newState = newState\<lparr>boundPubKeys := (boundPubKeys argState)\<rparr>" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledBody" using calculation by (metis compileContractOnlyBoundPubKeysMatter(1))
  moreover have "compileArguments params args state compiledBody = Some marloweContract" using calculation by auto
  moreover have "\<exists>newMWarnings . (marloweContract, faustusStateToMarloweState state, env, warnings, payments) \<rightarrow>*
    (compiledBody, faustusStateToMarloweState newState, env, newMWarnings, payments)" using calculation preservationOfArgumentEvaluation by auto
  ultimately show ?case by auto
qed

(* If a well typed Faustus contract executes to a certain state in any number of steps,
  then the compiled Marlowe contract will execute to the same state.*)
lemma preservationOfSemantics_FaustusStepsImpMarloweSteps:
"(faustusContract, ctx, state, env, warnings, payments) \<rightarrow>\<^sub>f* (newFaustusContract, newCtx, newState, newEnv, newWarnings, newPayments) \<Longrightarrow>
  wellTypedContract tyCtx faustusContract \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  (\<exists>newMWarnings . (the (compile faustusContract ctx state), faustusStateToMarloweState state, env, warnings, payments) \<rightarrow>* (the (compile newFaustusContract newCtx newState), faustusStateToMarloweState newState, newEnv, newMWarnings, newPayments))"
proof (induction arbitrary: tyCtx rule: star.induct[of small_step_reduce, split_format(complete)])
  case (refl fc ctx a a a b)
  then show ?case by auto
next
  case (step fCont ctx s e w p newFCont newCtx newS newE newW newP finalFCont finalCtx finalS finalE finalW finalP)
  moreover obtain newTyCtx where "wellTypedContract newTyCtx newFCont \<and> wellTypedFaustusContext newTyCtx newCtx \<and> wellTypedState newTyCtx newS" using typePreservation step by blast
  moreover obtain firstMarloweContract where "compile fCont ctx s = Some firstMarloweContract" using step wellTypedCompiles by blast
  moreover obtain secondMarloweContract finalMarloweContract fmw where "compile newFCont newCtx newS = Some secondMarloweContract" and
    "(secondMarloweContract, faustusStateToMarloweState newS, newE, newW, newP) \<rightarrow>* (finalMarloweContract, faustusStateToMarloweState finalS, finalE, fmw, finalP)" using step calculation wellTypedCompiles by (metis option.sel)
  moreover obtain secondMarloweWarnings where "(firstMarloweContract, faustusStateToMarloweState s, e, w, p) \<rightarrow>* (secondMarloweContract, faustusStateToMarloweState newS, newE, secondMarloweWarnings, newP)" using step calculation preservationOfSemantics_FaustusStepImpMarloweSteps by presburger
  moreover obtain finalMarloweWarnings where "(secondMarloweContract, faustusStateToMarloweState newS, newE, secondMarloweWarnings, newP) \<rightarrow>* (finalMarloweContract, faustusStateToMarloweState finalS, finalE, finalMarloweWarnings, finalP)" using step calculation SmallStep.smallStepStarWarningsAreArbitrary by blast
  ultimately show ?case using step apply auto
    by (meson SmallStep.smallStepStarWarningsAreArbitrary star_trans)
qed

lemma preservationOfSemantics_HaltedFaustusImpliesHaltedMarlowe:
"final (faustusContract, ctx, state, env, warnings, payments) \<Longrightarrow>
  wellTypedContract tyCtx faustusContract \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  finalMarlowe (the (compile faustusContract ctx state), faustusStateToMarloweState state, env, warnings, payments)"
  subgoal premises ps proof (cases faustusContract)
    case Close
    then show ?thesis using ps apply (cases "refundOne (accounts state)", auto simp add: finalMarlowe_def final_def)
      by blast
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (If x31 x32 x33)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (When x41 timeout x43)
    then show ?thesis using ps apply (auto simp add: finalMarlowe_def final_def)
      apply (cases "snd (slotInterval env) \<ge> timeout" "fst (slotInterval env) \<ge> timeout" rule: bool.exhaust[case_product bool.exhaust], auto)
      using surjective_pairing apply blast
        apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
        apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) apply fastforce
       apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
       apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) apply fastforce
      apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
      apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) by fastforce
  next
    case (Let x51 x52 x53)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetObservation x61 x62 x63)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetPubKey x71 x72 x73)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (Assert x81 x82)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetC x91 x92 x93 x94)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (UseC x101 x102)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  qed
  done

datatype FaustusReduceStepResult = Reduced FaustusContext ReduceWarning ReduceEffect FaustusState FaustusContract
                          | NotReduced
                          | AmbiguousSlotIntervalReductionError
                          | UnboundIdentifierError
                          | ArgumentEvaluationError

fun reduceContractStep :: "FaustusContext \<Rightarrow> Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusContract \<Rightarrow> FaustusReduceStepResult" where
"reduceContractStep ctx _ state Close =
  (case refundOne (accounts state) of
     Some ((party, token, money), newAccount) \<Rightarrow>
       let newState = state \<lparr> accounts := newAccount \<rparr> in
       Reduced ctx ReduceNoWarning (ReduceWithPayment (Payment party token money)) newState Close
   | None \<Rightarrow> NotReduced)" |
"reduceContractStep ctx env state (Pay faustusAccId faustusPayee token val cont) =
  (case (evalFaustusPubKey state faustusAccId, evalFaustusPayee state faustusPayee, evalFaustusValue env state val) of 
    (None, _, _) \<Rightarrow> UnboundIdentifierError
    | (_, None, _) \<Rightarrow> UnboundIdentifierError
    | (_, _, None) \<Rightarrow> UnboundIdentifierError
    | (Some accId, Some payee, Some moneyToPay) \<Rightarrow>
     (if moneyToPay \<le> 0
       then (let warning = ReduceNonPositivePay accId payee token moneyToPay in
             Reduced ctx warning ReduceNoPayment state cont)
       else (let balance = moneyInAccount accId token (accounts state) in
            (let paidMoney = min balance moneyToPay in
             let newBalance = balance - paidMoney in
             let newAccs = updateMoneyInAccount accId token newBalance (accounts state) in
             let warning = (if paidMoney < moneyToPay
                            then ReducePartialPay accId payee token paidMoney moneyToPay
                            else ReduceNoWarning) in
             let (payment, finalAccs) = giveMoney payee token paidMoney newAccs in
             Reduced ctx warning payment (state \<lparr> accounts := finalAccs \<rparr>) cont))))" |
"reduceContractStep ctx env state (If obs cont1 cont2) =
  (case evalFaustusObservation env state obs of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let cont = (if evaluatedObservation then cont1 else cont2) in
       Reduced ctx ReduceNoWarning ReduceNoPayment state cont))" |
"reduceContractStep ctx env state (When _ timeout cont) =
  (let (startSlot, endSlot) = slotInterval env in
   if endSlot < timeout
   then NotReduced
   else (if timeout \<le> startSlot
         then Reduced ctx ReduceNoWarning ReduceNoPayment state cont
         else AmbiguousSlotIntervalReductionError))" |
"reduceContractStep ctx env state (Let valId val cont) =
  (case evalFaustusValue env state val of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedValue \<Rightarrow> (let boundVals = boundValues state in
      let newState = state \<lparr> boundValues := MList.insert valId evaluatedValue boundVals\<rparr> in
      let warn = case lookup valId boundVals of
                  Some oldVal \<Rightarrow> ReduceShadowing valId oldVal evaluatedValue
                | None \<Rightarrow> ReduceNoWarning in
      Reduced (ValueAbstraction valId#ctx) warn ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetObservation obsId obs cont) =
  (case evalFaustusObservation env state obs of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let boundVals = boundValues state in
      let newState = state \<lparr> boundValues := MList.insert obsId (if evaluatedObservation then 1 else 0) boundVals \<rparr> in
      Reduced (ObservationAbstraction obsId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetPubKey pkId pk cont) =
  (case evalFaustusPubKey state pk of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedPubKey \<Rightarrow> (let boundPubKeys = boundPubKeys state in
      let newState = state \<lparr> boundPubKeys := MList.insert pkId evaluatedPubKey boundPubKeys \<rparr> in
      Reduced (PubKeyAbstraction pkId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (Assert obs cont) =
  (case evalFaustusObservation env state obs of
    None \<Rightarrow> UnboundIdentifierError
    | Some evaluatedObservation \<Rightarrow> (let warning = if evaluatedObservation
                 then ReduceNoWarning
                 else ReduceAssertionFailed
   in Reduced ctx warning ReduceNoPayment state cont))" |
"reduceContractStep ctx env state (LetC cid params boundCon cont) = Reduced (ContractAbstraction (cid, params, boundCon)#ctx) ReduceNoWarning ReduceNoPayment state cont" |
"reduceContractStep ctx env state (UseC cid args) = (
  case lookupContractIdAbsInformation ctx cid of
    None \<Rightarrow> UnboundIdentifierError
    | Some (params, bodyCont, innerCtx) \<Rightarrow> (case evalFaustusArguments env state params args of
      None \<Rightarrow> ArgumentEvaluationError
      | Some newState \<Rightarrow> Reduced ((paramsToFaustusContext params)@innerCtx) ReduceNoWarning ReduceNoPayment newState bodyCont))"

(* If reduce step function reduces a contract, then a small step reduction is possible. *)
lemma reduceStepIsSmallStepReduction:
assumes "reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract"
shows "\<exists> appendPayment .(contract, ctx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, newState, env, initialWarnings @ [warning], initialPayments @ appendPayment)"
proof (cases contract paymentReduceResult rule: FaustusContract.exhaust[case_product ReduceEffect.exhaust])
  case Close_ReduceNoPayment
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (Close_ReduceWithPayment reducePayment)
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (Pay_ReduceNoPayment accId payee token val cont)
  then show ?thesis using assms apply auto
    apply (cases "evalFaustusPubKey state accId" "evalFaustusPayee state payee" "evalFaustusValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
     apply (metis FaustusSemantics.small_step_reduce.PayNonPositive append.right_neutral)
    apply (split option.split_asm, auto simp add: min_def)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments)\<close> append_Nil2)
    qed
    apply (split if_split_asm, auto)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments)\<close> append_Nil2)
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments)\<close> append_Nil2)
    qed
    apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token evaluatedValue (MList.insert (evaluatedPubKey, token) (availableMoney - evaluatedValue) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments)\<close> append_Nil2)
    qed
    done
next
  case (Pay_ReduceWithPayment accId payee token val cont payment)
  then show ?thesis using assms apply auto
    apply (cases "evalFaustusPubKey state accId" "evalFaustusPayee state payee" "evalFaustusValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
    apply (split option.split_asm, auto simp add: min_def)
     apply (auto split: prod.split_asm)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments @ [payment])\<close>)
    qed
    apply (split if_split_asm, auto)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])\<close>)
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])\<close>)
    qed
    apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token evaluatedValue (MList.insert (evaluatedPubKey, token) (availableMoney - evaluatedValue) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state\<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis \<open>(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) \<rightarrow>\<^sub>f (continueContract, newCtx, state \<lparr>FaustusState.accounts := finalAccs\<rparr>, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])\<close>)
    qed
    done
next
  case (If_ReduceNoPayment obs x32 x33)
  then show ?thesis using assms apply (auto split: option.split_asm)
     apply (metis IfTrue append_Nil2)
    by (metis IfFalse append.right_neutral)
next
  case (If_ReduceWithPayment x31 x32 x33 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (When_ReduceNoPayment x41 timeout x43)
  then show ?thesis using assms apply auto
    subgoal premises ps proof (cases "slotInterval env")
      case (Pair ss es)
      then show ?thesis using ps apply auto
        apply (cases "es < timeout", auto)
        apply (cases "timeout \<le> ss", auto)
        by (smt WhenTimeout append_Nil2)
    qed
    done
next
  case (When_ReduceWithPayment x41 x42 x43 x2)
  then show ?thesis using assms by (auto split: if_split_asm prod.split_asm)
next
  case (Let_ReduceNoPayment vid x52 x53)
  then show ?thesis using assms apply (auto split: option.split_asm)
    apply (cases "lookup vid (boundValues state)", auto)
     apply (metis LetNoShadow append_Nil2)
    by (metis (no_types, lifting) LetShadow append_Nil2)
next
  case (Let_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (cases "lookup vid (boundValues state)", auto)
next
  case (LetObservation_ReduceNoPayment obsId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetObservation append_Nil2)
next
  case (LetObservation_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetPubKey_ReduceNoPayment pkId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetPubKey append_Nil2)
next
  case (LetPubKey_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (Assert_ReduceNoPayment x61 x62)
  then show ?thesis using assms apply (auto split: option.split_asm)
     apply (metis AssertTrue append.right_neutral)
    by (metis AssertFalse append_Nil2)
next
  case (Assert_ReduceWithPayment x61 x62 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetC_ReduceNoPayment cid x72 x73)
  then show ?thesis using assms apply auto
    by (metis LetC append.right_neutral)
next
  case (LetC_ReduceWithPayment cid x72 x73 x2)
  then show ?thesis using assms by auto
next
  case (UseC_ReduceNoPayment cid)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (metis UseCFound append_Nil2)
next
  case (UseC_ReduceWithPayment cid x2)
  then show ?thesis using assms by (auto split: option.split_asm)
qed

(* If a small step reduction is possible, then the reduce step function will reduce the contract *)
lemma smallStepReductionImpReduceStep:
assumes "cs = (contract, ctx, state, env, initialWarnings, initialPayments) \<and>
    cs' = (continueContract, newCtx, newState, newEnv, newWarnings, newPayments) \<and>
    cs \<rightarrow>\<^sub>f cs'"
shows "\<exists>warning paymentReduceResult . reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract"
proof (cases contract)
  case Close
  then show ?thesis using assms by auto
next
  case (Pay accId payee token val cont)
  then show ?thesis using assms apply auto
        apply (auto split: option.split)
              apply (auto simp add: min.strict_order_iff min.absorb_iff2)
        apply metis
       apply metis
      apply metis
     apply metis
    by metis
next
  case (If x31 x32 x33)
  then show ?thesis using assms by auto
next
  case (When cases timeout cont)
  then show ?thesis using assms by auto
next
  case (Let x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetObservation x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetPubKey x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (Assert x61 x62)
  then show ?thesis using assms by auto
next
  case (LetC x71 x72 x73)
  then show ?thesis using assms by auto
next
  case (UseC x8)
  then show ?thesis using assms by (auto split: option.split)
qed

lemma wellTypedContractNeverGetsUnboundIdentifierError:
"wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reduceContractStep ctx env state contract \<noteq> UnboundIdentifierError"
  apply (cases contract, auto)
          apply (cases "refundOne (accounts state)", auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm simp add: wellTypedPubKeyEvaluates wellTypedValueObservationExecutes wellTypedPayeeEvaluates min_def)
   apply (meson FaustusReduceStepResult.distinct(5))
  using lookupContractAbstractionWellTypedContext by fastforce

lemma wellTypedContractNeverGetsArgumentEvaluationError:
"wellTypedContract tyCtx contract \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  reduceContractStep ctx env state contract \<noteq> ArgumentEvaluationError"
  apply (cases contract, auto)
          apply (cases "refundOne (accounts state)", auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm simp add: wellTypedPubKeyEvaluates wellTypedValueObservationExecutes wellTypedPayeeEvaluates min_def)
   apply (meson FaustusReduceStepResult.distinct)
  by (metis lookupContractTypeWellTypedContext old.prod.inject option.inject wellTypedArgumentsExecute)


datatype FaustusReduceResult = ContractQuiescent "ReduceWarning list" "Payment list"
                                          FaustusState FaustusContract
                                          | RRAmbiguousSlotIntervalError
                                          | RRUnboundIdentifierError
                                          | RRArgumentEvaluationError

function (sequential) reductionLoop :: "FaustusContext \<Rightarrow> Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusContract \<Rightarrow> ReduceWarning list \<Rightarrow>
                                        Payment list \<Rightarrow> FaustusReduceResult" where
"reductionLoop ctx env state contract warnings payments =
  (case reduceContractStep ctx env state contract of
     Reduced newCtx warning effect newState ncontract \<Rightarrow>
       let newWarnings = (if warning = ReduceNoWarning
                          then warnings
                          else warning # warnings) in
       let newPayments = (case effect of
                            ReduceWithPayment payment \<Rightarrow> payment # payments
                          | ReduceNoPayment \<Rightarrow> payments) in
       reductionLoop newCtx env newState ncontract newWarnings newPayments
   | AmbiguousSlotIntervalReductionError \<Rightarrow> RRAmbiguousSlotIntervalError
   | NotReduced \<Rightarrow> ContractQuiescent (rev warnings) (rev payments) state contract
   | UnboundIdentifierError \<Rightarrow> RRUnboundIdentifierError
   | ArgumentEvaluationError \<Rightarrow> RRArgumentEvaluationError)"
  by pat_completeness auto

lemma reductionLoop_termination_aux1:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract \<Longrightarrow>
       \<not> faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
       faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 \<le> fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps by (cases "lookup x51 (boundValues state)", auto split: option.split_asm)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux2:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
       \<not> faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       \<not> length newCtx < length ctx \<Longrightarrow> length newCtx = length ctx"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 \<le> fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux3:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
       \<not> faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
       \<not> length newCtx < length ctx \<Longrightarrow>
       (\<And>ctx env state contract newCtx x12 x13 x14 newContract.
           FaustusSemantics.reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract \<Longrightarrow>
           \<not> faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
           faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
           faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)) \<Longrightarrow>
       (\<And>ctx env state contract newCtx x12 x13 x14 x15.
           FaustusSemantics.reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 \<Longrightarrow>
           \<not> faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) \<Longrightarrow>
           \<not> length newCtx < length ctx \<Longrightarrow> length newCtx = length ctx) \<Longrightarrow>
       length (accounts x14) < length (accounts state)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps apply auto
      apply (induction "accounts state", auto)
      by (cases "refundOne (accounts state)", auto simp add: refundOneShortens)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
    case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 \<le> fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

termination reductionLoop
  apply (relation "measures [(\<lambda>(ctx, env, state, contract, _) . ((faustusContractSize contract)) + (fold (+) (map (abstractionInformationSize) ctx) 0)), 
    (\<lambda>(ctx, _, state, contract, _) . (length ctx)),
    (\<lambda>(ctx, _, state, contract, _) . (length (accounts state)))]", auto)
  using reductionLoop_termination_aux1
    reductionLoop_termination_aux2 
    reductionLoop_termination_aux3 by auto

fun reduceContractUntilQuiescent :: "FaustusContext \<Rightarrow> Environment \<Rightarrow> FaustusState \<Rightarrow> FaustusContract \<Rightarrow> FaustusReduceResult" where
"reduceContractUntilQuiescent ctx env state contract = reductionLoop ctx env state contract [] []"

datatype ApplyResult = Applied ApplyWarning FaustusState FaustusContract
                     | ApplyNoMatchError

fun applyCases :: "Environment \<Rightarrow> FaustusState \<Rightarrow> Input \<Rightarrow> FaustusCase list \<Rightarrow> ApplyResult" where
"applyCases env state (IDeposit accId1 party1 tok1 amount)
            (Case (Deposit accId2 party2 tok2 val) cont # rest) =
  (if (Some accId1 = evalFaustusPubKey state accId2 \<and> Some party1 = evalFaustusPubKey state party2 \<and> tok1 = tok2
        \<and> Some amount = evalFaustusValue env state val)
   then let warning = if amount > 0
                      then ApplyNoWarning
                      else ApplyNonPositiveDeposit party1 accId1 tok2 amount in
        let newState = state \<lparr> accounts := addMoneyToAccount accId1 tok1 amount (accounts state) \<rparr> in
        Applied warning newState cont
   else applyCases env state (IDeposit accId1 party1 tok1 amount) rest)" |
"applyCases env state (IChoice choId1 choice)
            (Case (Choice choId2 bounds) cont # rest) =
  (if (Some choId1 = evalFaustusChoiceId state choId2 \<and> inBounds choice bounds)
   then let newState = state \<lparr> choices := MList.insert choId1 choice (choices state) \<rparr> in
        Applied ApplyNoWarning newState cont
   else applyCases env state (IChoice choId1 choice) rest)" |
"applyCases env state INotify (Case (Notify obs) cont # rest) =
  (if evalFaustusObservation env state obs = Some True
   then Applied ApplyNoWarning state cont
   else applyCases env state INotify rest)" |
"applyCases env state (IDeposit accId1 party1 tok1 amount) (Cons _ rest) =
  applyCases env state (IDeposit accId1 party1 tok1 amount) rest" |
"applyCases env state (IChoice choId1 choice) (Cons _ rest) =
  applyCases env state (IChoice choId1 choice) rest" |
"applyCases env state INotify (Cons _ rest) =
  applyCases env state INotify rest" |
"applyCases env state acc Nil = ApplyNoMatchError"

lemma preservationOfApplyCases_FaustusErrorImpliesMarloweError:
"applyCases env state inp cases = ApplyNoMatchError \<Longrightarrow>
  wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  compileCases cases ctx state = Some marloweCases \<Longrightarrow>
  Semantics.applyCases env (faustusStateToMarloweState state) inp marloweCases = Semantics.ApplyNoMatchError"
proof (induction cases arbitrary: marloweCases)
  case Nil
  then show ?case by auto
next
  case (Cons firstCase cases)
  then show ?case apply auto
    subgoal premises ps proof (cases firstCase inp rule: FaustusCase.exhaust[case_product Input.exhaust])
      case (Case_IDeposit firstAct firstCont x11 x12 x13 x14)
      then show ?thesis using ps apply (cases firstAct, auto split: if_split_asm option.split_asm)
          apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
         apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
        by (simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(1))
    next
      case (Case_IChoice firstAct firstCont x21 x22)
      then show ?thesis using ps by (cases firstAct, auto split: if_split_asm option.split_asm simp add: preservationOfChoiceIdEvaluation)
    next
      case (Case_INotify firstAct x2)
      then show ?thesis using ps by (cases firstAct, auto split: if_split_asm option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(2))
    qed
    done
qed

lemma preservationOfApplyCases_FaustusAppliedImpliesMarloweApplied:
"applyCases env state inp cases = Applied warn newState newCont \<Longrightarrow>
  wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  compileCases cases ctx state = Some marloweCases \<Longrightarrow>
  Semantics.applyCases env (faustusStateToMarloweState state) inp marloweCases = Semantics.Applied warn (faustusStateToMarloweState newState) (the (compile newCont ctx newState))"
proof (induction cases arbitrary: marloweCases warn newState newCont)
  case Nil
  then show ?case by auto
next
  case (Cons firstCase cases)
  then show ?case apply auto
    subgoal premises ps proof (cases firstCase inp rule: FaustusCase.exhaust[case_product Input.exhaust])
      case (Case_IDeposit firstAct firstCont inPk1 inPk2 inToken inAmt)
      then show ?thesis using ps apply (auto split: option.split_asm)
        subgoal premises ps proof (cases firstAct)
          case (Deposit depPk1 depPk2 depToken depValue)
          then show ?thesis using ps apply (auto split: if_split_asm)
                  apply (split option.split_asm, simp)
                  apply (split option.split_asm, simp)
                  apply (split option.split_asm, simp)
                  apply (split option.split_asm, simp)
                  apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
                  apply (metis faustusStateToMarloweState.simps option.sel preservationOfValueEvaluation(1))
                 apply (split option.split_asm, simp)
                 apply (split option.split_asm, simp)
                  apply (split option.split_asm, simp)
                  apply (split option.split_asm, simp)
                  apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
                  apply (metis faustusStateToMarloweState.simps option.sel preservationOfValueEvaluation(1))
                 apply (split option.split_asm, simp)
                 apply (split option.split_asm, simp)
                 apply (split option.split_asm, simp)
                 apply (split option.split, simp)
                 apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
                 apply (metis faustusStateToMarloweState.simps option.sel preservationOfValueEvaluation(1))
                apply (split option.split_asm, simp)
                apply (split option.split_asm, simp)
                apply (split option.split_asm, simp)
                apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
                apply (metis faustusStateToMarloweState.simps option.sel preservationOfValueEvaluation(1))
               apply (split option.split_asm, simp)
               apply (split option.split_asm, simp)
               apply (split option.split_asm, simp)
               apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
              apply (split option.split_asm, simp)
              apply (split option.split_asm, simp)
              apply (split option.split_asm, simp)
              apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
             apply (split option.split_asm, simp)
             apply (split option.split_asm, simp)
             apply (split option.split_asm, simp)
             apply (auto simp add: compileAccountsArbitrary(1) preservationOfPubKeyEvaluation)[1]
            apply (split option.split_asm, simp)
            apply (split option.split_asm, simp)
            apply (split option.split_asm, simp)
            by (auto split: option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(1))
        next
          case (Choice choiceId x22)
          then show ?thesis using ps by (auto split: option.split_asm)
        next
          case (Notify x3)
          then show ?thesis using ps by (auto split: option.split_asm)
        qed
        done
    next
      case (Case_IChoice firstAct firstCont inChoice inNum)
      then show ?thesis using ps apply (auto split: option.split_asm)
        subgoal premises ps proof (cases firstAct)
          case (Deposit depPk1 depPk2 depToken depValue)
          then show ?thesis using ps by (auto split: option.split_asm)
        next
          case (Choice choiceId choiceBound)
          then show ?thesis using ps apply (auto split: if_split_asm option.split_asm simp add: preservationOfChoiceIdEvaluation)
            by (metis compileContractOnlyBoundPubKeysMatter(1) FaustusState.surjective FaustusState.update_convs(2) FaustusState.update_convs(4) option.sel)
        next
          case (Notify x3)
          then show ?thesis using ps by (auto split: option.split_asm)
        qed
        done
    next
      case (Case_INotify firstAct firstCont)
      then show ?thesis using ps apply (auto split: option.split_asm)
        subgoal premises ps proof (cases firstAct)
          case (Deposit depPk1 depPk2 depToken depValue)
          then show ?thesis using ps by (auto split: option.split_asm)
        next
          case (Choice choiceId choiceBound)
          then show ?thesis using ps by (auto split: option.split_asm)
        next
          case (Notify x3)
          then show ?thesis using ps by (auto split: if_split_asm option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(2))
        qed
        done
    qed
    done
qed

lemma preservationOfApplyCases_MarloweErrorImpliesFaustusError:
"wellTypedCases tyCtx cases \<Longrightarrow>
  wellTypedState tyCtx state \<Longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<Longrightarrow>
  Semantics.applyCases env (faustusStateToMarloweState state) inp (the (compileCases cases ctx state)) = Semantics.ApplyNoMatchError \<Longrightarrow>
  applyCases env state inp cases = ApplyNoMatchError"
proof (induction cases)
  case Nil
  then show ?case by auto
next
  case (Cons firstCase cases)
  then show ?case apply auto
    subgoal premises ps proof (cases firstCase inp rule: FaustusCase.exhaust[case_product Input.exhaust])
      case (Case_IDeposit firstAct firstCont x11 x12 x13 x14)
      then show ?thesis using ps apply (cases firstAct, auto split: if_split_asm option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(1))
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
              apply (metis compileFaustusPubKey.elims compileFaustusPubKey.simps(2) evalFaustusPubKey.elims evalFaustusPubKey.simps(2) option.inject wellTypedPubKey.simps(1) wellTypedPubKey.simps(2))
             apply (metis compileFaustusPubKey.elims compileFaustusPubKey.simps(2) evalFaustusPubKey.elims evalFaustusPubKey.simps(2) option.inject wellTypedPubKey.simps(1) wellTypedPubKey.simps(2))
        using wellTypedCompiles(1) apply fastforce
        using wellTypedChoiceIdCompiles apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(2) by fastforce
    next
      case (Case_IChoice firstAct firstCont x21 x22)
      then show ?thesis using ps apply (cases firstAct, auto split: if_split_asm option.split_asm simp add: preservationOfChoiceIdEvaluation)
        using wellTypedCompiles(1) apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedChoiceIdCompiles apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(2) by fastforce
    next
      case (Case_INotify firstAct x2)
      then show ?thesis using ps apply (cases firstAct, auto split: if_split_asm option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(2))
        using wellTypedCompiles(1) apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedPubKeyCompiles apply fastforce
        using wellTypedValueObservationCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedChoiceIdCompiles apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedValueObservationCompiles(2) apply fastforce
        using wellTypedValueObservationCompiles(2) by fastforce
    qed
    done
qed

lemma preservationOfApplyCases_MarloweAppliedImpliesFaustusApplied:
"(\<exists>newState newCont. wellTypedCases tyCtx cases \<longrightarrow>
  wellTypedState tyCtx state \<longrightarrow>
  wellTypedFaustusContext tyCtx ctx \<longrightarrow>
  Semantics.applyCases env (faustusStateToMarloweState state) inp (the (compileCases cases ctx state)) = Semantics.Applied warn newMarloweState newMarloweCont \<longrightarrow>
  (applyCases env state inp cases = Applied warn newState newCont \<and>
  faustusStateToMarloweState newState = newMarloweState \<and>
  (the (compile newCont ctx newState)) = newMarloweCont))"
proof (induction cases)
  case Nil
  then show ?case by auto
next
  case (Cons firstCase cases)
  then show ?case apply auto
    apply (smt FaustusSemantics.ApplyResult.exhaust Semantics.ApplyResult.distinct(1) Semantics.ApplyResult.inject faustusStateToMarloweState.simps option.sel preservationOfApplyCases_FaustusAppliedImpliesMarloweApplied preservationOfApplyCases_FaustusErrorImpliesMarloweError wellTypedCompiles(2))
    subgoal premises ps proof (cases firstCase inp rule: FaustusCase.exhaust[case_product Input.exhaust])
      case (Case_IDeposit firstAct firstCont x11 x12 x13 x14)
      then show ?thesis using ps apply (auto split: option.split)
        apply (cases firstAct, auto simp only: wellTypedCases.simps)
        apply (split option.split_asm)
        using wellTypedCompiles(1) apply fastforce
          apply (split option.split_asm)
        using wellTypedCompiles(1) apply fastforce
          apply (split option.split_asm)
           apply (metis (no_types, lifting) case_prod_conv compileCases.simps(1) option.distinct(1) option.simps(4) option.simps(5) ps(1) wellTypedCompiles(2))
        apply (cases firstAct, auto)
                            apply (auto split: option.split_asm if_split_asm)
                            apply (simp_all add: compileAccountsArbitrary(1) preservationOfValueEvaluation_MarloweImpliesFaustus(1))
                       apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                      apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                     apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                    apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                   apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                  apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                 apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
                apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
               apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
              apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
             apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
            apply (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2))
        using wellTypedCompiles(1) apply fastforce
        using wellTypedChoiceIdCompiles apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedValueObservationCompiles(2) by fastforce
    next
      case (Case_IChoice firstAct firstCont x21 x22)
      then show ?thesis using ps apply (auto split: option.split)
        apply (auto split: option.split_asm)
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        apply (cases firstAct, auto split: option.split_asm simp add: preservationOfChoiceIdEvaluation)
        by (metis FaustusState.surjective FaustusState.update_convs(2) FaustusState.update_convs(4) compileContractOnlyBoundPubKeysMatter(1) option.sel)
    next
      case (Case_INotify firstAct x2)
      then show ?thesis using ps apply (auto split: option.split)
        apply (auto split: option.split_asm)
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        by (cases firstAct, auto split: option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(2))
    qed
    subgoal premises ps proof (cases firstCase inp rule: FaustusCase.exhaust[case_product Input.exhaust])
      case (Case_IDeposit firstAct firstCont x11 x12 x13 x14)
      then show ?thesis using ps apply (auto split: option.split)
        apply (auto split: option.split_asm)
           apply (cases firstAct, auto simp only: wellTypedCases.simps)
            apply (metis option.discI wellTypedCompiles(1))
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(1) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        by (smt FaustusSemantics.ApplyResult.exhaust Semantics.ApplyResult.distinct(1) Semantics.ApplyResult.inject State.ext_inject faustusStateToMarloweState.simps option.sel preservationOfApplyCases_FaustusAppliedImpliesMarloweApplied preservationOfApplyCases_FaustusErrorImpliesMarloweError ps(4) wellTypedCompiles(2))
    next
      case (Case_IChoice firstAct firstCont x21 x22)
      then show ?thesis using ps apply (auto split: option.split)
        apply (auto split: option.split_asm)
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        by (smt FaustusSemantics.ApplyResult.exhaust Semantics.ApplyResult.distinct(1) Semantics.ApplyResult.inject State.ext_inject faustusStateToMarloweState.simps option.sel preservationOfApplyCases_FaustusAppliedImpliesMarloweApplied preservationOfApplyCases_FaustusErrorImpliesMarloweError ps(4) wellTypedCompiles(2))
    next
      case (Case_INotify firstAct x2)
      then show ?thesis using ps apply (auto split: option.split)
        apply (auto split: option.split_asm)
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        using wellTypedCompiles(2) apply fastforce
        by (cases firstAct, auto split: option.split_asm simp add: preservationOfValueEvaluation_MarloweImpliesFaustus(2))
    qed
    done
qed

end
