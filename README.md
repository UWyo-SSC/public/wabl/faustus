<p align="center">
  <img width="266" height="185" src="Faustus.jpg">
</p>

# Faustus (V1)

This repository contains Faustus, a financial smart contract language compiles to the Haskell DSL Marlowe. 

Version 2 of Faustus is the current version and can be found at [https://gitlab.com/UWyo-SSC/public/wabl/faustus-v2](https://gitlab.com/UWyo-SSC/public/wabl/faustus-v2)

