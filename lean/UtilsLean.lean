import .lovelib

namespace Utils

/-
This file only had the flip function in it in Isabelle.
default lean contains flip already. Here is a copy of the file already included in lean
-/
universes u v w

def flipCopy {α : Sort u} {β : Sort v} {φ : Sort w} (f : α → β → φ) : β → α → φ :=
λ b a, f a b

#check flip
#check flipCopy

end Utils