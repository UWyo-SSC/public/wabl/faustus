import data.real.basic
import .lovelib
import .MListLean
import .SListLean
import .ListToolsLean
--lexorder



namespace Semantics




@[reducible]def slot := int
#check slot
#reduce slot 


@[reducible]def pubkey := int

@[reducible]def ada := int
@[reducible]def currencySymbol := int
@[reducible]def tokenName := int

@[reducible]def party := pubkey
@[reducible]def choiceName := int
@[reducible]def numAccount := int
@[reducible]def timeout := slot

#reduce timeout
@[reducible]def money := ada
@[reducible]def chosenNum := int

@[reducible]def accountId := party
#check int.has_lt
#check has_lt currencySymbol
#check has_le tokenName

structure token :=
(currencySymbol : currencySymbol)
(tokenName : tokenName)
#check token.currencySymbol
#check token.tokenName
#reduce token

@[simp]def less_eq_tok : token → token → Prop:=
  λ a b, if (a.currencySymbol < b.currencySymbol) then true else (if (a.currencySymbol > b.currencySymbol)
    then false else a.tokenName ≤ b.tokenName)
#check less_eq_tok

@[simp]def less_tok : token → token → Prop :=
  λ a b, ¬ (less_eq_tok b a)
#check less_tok 

@[ext] theorem tok_eq :∀ {a b : token}, a.currencySymbol = b.currencySymbol → a.tokenName = b.tokenName → a = b:= 
  begin
    rintros ⟨a1, a2⟩ ⟨b1, b2⟩ ⟨rfl⟩ ⟨rfl⟩,
    refl,
  end

#check le_of_lt
instance : preorder token :=
{ le := less_eq_tok,
  lt := less_tok,
  le_refl := 
  begin
    intro a,
    simp,
    right,
    split,
    repeat{refl,},
  end,
  le_trans := 
  begin
    intros a b c hab hbc,
    simp,
    simp at hab,
    simp at hbc,
    cases hab with hab1 hab2,
    cases hbc with hbc1 hbc2,
    left,
    transitivity,
    exact hab1,
    exact hbc1,
    left,
    apply lt_of_lt_of_le,
    exact hab1,
    cases hbc2 with hbc21 hbc22,
    exact hbc21,
    cases hbc with hbc1 hbc2,
    left,
    cases hab2 with hab21 hab22,
    apply lt_of_le_of_lt,
    exact hab21,
    exact hbc1,
    right,
    cases hab2 with hab21 hab22,
    cases hbc2 with hbc21 hbc22,
    split,
    {
      transitivity,
      exact hab21,
      exact hbc21,
    },
    {
      transitivity,
      exact hab22,
      exact hbc22,
    },
  end,
  lt_iff_le_not_le := 
  begin
    intros a b,
    simp,
    push_neg,
    split,
    {
      intro hlt,
      cases hlt with hlt1 hlt2,
      have ht := decidable.lt_trichotomy a.currencySymbol b.currencySymbol,
      cases ht with hl ht,
      {
        split,
        {
          left,
          exact hl,
        },
        {
          split,
          {
            apply le_of_lt,
            exact hl,
          },
          {
            exact hlt2,
          },
        },
      },
      {
        cases ht with he hg,
        {
          split,
          {
            right,
            split,
            {
              exact hlt1,
            },
            {
              apply le_of_lt,
              apply hlt2,
              rw he,
            },
          },
          {
            split,
            {
              exact hlt1,
            },
            {
              exact hlt2,
            },
          },
        },
        {
          have hng : ¬ b.currencySymbol < a.currencySymbol,
          {
            push_neg,
            exact hlt1,
          },
          contradiction,
        },
      },
    },
    {
      intro hand,
      cases hand with h1 h2,
      cases h2 with h21 h22,
      split,
      {
        exact h21,
      },
      {
        exact h22,
      },
    },
  end
}
#check token.preorder.le

#check eq_or_lt_of_le
#check linear_order ℝ


/-# Proof token is of linear_order-/

noncomputable instance : linear_order token :=
{ le := token.preorder.le,
  lt := token.preorder.lt,
  le_refl := token.preorder.le_refl,
  le_trans := token.preorder.le_trans,
  lt_iff_le_not_le := token.preorder.lt_iff_le_not_le,
  le_antisymm := 
  begin
    intros a b hab hba,
    have ha : a ≤ b = less_eq_tok a b, from rfl,
    rw ha at hab,
    simp at hab,
    have hb : b ≤ a = less_eq_tok b a, from rfl,
    rw hb at hba,
    simp at hba,
    cases hab with hab1 hab2,
    cases hba with hba1 hba2,
    have h := asymm hab1,
    contradiction,
    cases hba2 with hba21 hba22,
    ext,
    have hor := eq_or_lt_of_le hba21,
    have hnl := asymm hab1,
    cases hor with h1 h2,
    symmetry,
    exact h1,
    contradiction,
    have hn : ¬ a.currencySymbol < b.currencySymbol,
    {
      push_neg,
      exact hba21,
    },
    contradiction,
    cases hab2 with hab21 hab22,
    ext,
    cases hba with hba1 hba2,
    have hor := eq_or_lt_of_le hab21,
    cases hor with h1 h2,
    exact h1,
    have hnl := asymm h2,
    contradiction,
    cases hba2 with hba21 hba22,
    apply le_antisymm,
    exact hab21,
    exact hba21,
    cases hba with hba1 hba2,
    have hn : ¬ b.currencySymbol < a.currencySymbol,
    {
      push_neg,
      exact hab21,
    },
    contradiction,
    cases hba2 with hba21 hba22,
    apply le_antisymm,
    exact hab22,
    exact hba22,
  end,
  le_total := 
  begin
    intros a b,
    have ha : a ≤ b = less_eq_tok a b, from rfl,
    have hb : b ≤ a = less_eq_tok b a, from rfl,
    rw [ha, hb],
    rw less_eq_tok,
    simp,
    by_contradiction,
    push_neg at h,
    cases h with h1 h2,
    cases h1 with h11 h12,
    cases h2 with h21 h22,
    specialize h22 h11,
    specialize h12 h21,
    have hn := asymm h22,
    apply hn,
    exact h12,
  end,
  decidable_le := classical.dec_rel has_le.le,
  decidable_eq := classical.dec_eq token,
  decidable_lt := classical.dec_rel has_lt.lt 
}


/-# ChoiceID section for linear_order-/
structure choiceID :=
(cn : choiceName)
(p : party)
#check choiceID

@[simp] def less_eq_choID : choiceID → choiceID → Prop :=
  λ a b, if a.cn < b.cn then true else (if a.cn > b.cn then false else a.p ≤ b.p)
#check less_eq_choID

@[simp] def less_choID : choiceID → choiceID → Prop :=
  λ a b, ¬ less_eq_choID b a
#check less_choID

@[ext] lemma choID_eq : ∀ {a b : choiceID}, a.cn = b.cn → a.p = b.p → a = b:= 
  begin
    rintros ⟨a1, a2⟩ ⟨b1, b2⟩ ⟨rfl⟩ ⟨rfl⟩,
    refl,
  end
#check choID_eq

--instance renaming less_eq_choID
instance : has_le choiceID := ⟨less_eq_choID⟩
instance : has_lt choiceID := ⟨less_choID⟩

noncomputable instance: linear_order choiceID :=
{ le := less_eq_choID,
  lt := less_choID,
  le_refl := 
  begin
    intro a,
    simp,
    right,
    split,
    repeat{refl,},
  end,
  le_trans := 
  begin
    intros a b c hab hbc,
    simp,
    simp at hab,
    simp at hbc,
    cases hab with hab1 hab2,
    cases hbc with hbc1 hbc2,
    left,
    transitivity,
    exact hab1,
    exact hbc1,
    left,
    apply lt_of_lt_of_le,
    exact hab1,
    cases hbc2 with hbc21 hbc22,
    exact hbc21,
    cases hbc with hbc1 hbc2,
    left,
    cases hab2 with hab21 hab22,
    apply lt_of_le_of_lt,
    exact hab21,
    exact hbc1,
    right,
    cases hab2 with hab21 hab22,
    cases hbc2 with hbc21 hbc22,
    split,
    {
      transitivity,
      exact hab21,
      exact hbc21,
    },
    {
      transitivity,
      exact hab22,
      exact hbc22,
    },
  end,
  lt_iff_le_not_le := 
  begin
    intros a b,
    simp,
    push_neg,
    split,
    {
      intro hlt,
      cases hlt with hlt1 hlt2,
      have ht := decidable.lt_trichotomy a.cn b.cn,
      cases ht with hl ht,
      {
        split,
        {
          left,
          exact hl,
        },
        {
          split,
          {
            apply le_of_lt,
            exact hl,
          },
          {
            exact hlt2,
          },
        },
      },
      {
        cases ht with he hg,
        {
          split,
          {
            right,
            split,
            {
              exact hlt1,
            },
            {
              apply le_of_lt,
              apply hlt2,
              rw he,
            },
          },
          {
            split,
            {
              exact hlt1,
            },
            {
              exact hlt2,
            },
          },
        },
        {
          have hng : ¬ b.cn < a.cn,
          {
            push_neg,
            exact hlt1,
          },
          contradiction,
        },
      },
    },
    {
      intro hand,
      cases hand with h1 h2,
      cases h2 with h21 h22,
      split,
      {
        exact h21,
      },
      {
        exact h22,
      },
    },
  end,
  le_antisymm := 
  begin
    intros a b hab hba,
    have ha : a ≤ b = less_eq_choID a b, from rfl,
    rw ha at hab,
    simp at hab,
    have hb : b ≤ a = less_eq_choID b a, from rfl,
    rw hb at hba,
    simp at hba,
    cases hab with hab1 hab2,
    cases hba with hba1 hba2,
    have h := asymm hab1,
    contradiction,
    cases hba2 with hba21 hba22,
    ext,
    have hor := eq_or_lt_of_le hba21,
    have hnl := asymm hab1,
    cases hor with h1 h2,
    symmetry,
    exact h1,
    contradiction,
    have hn : ¬ a.cn < b.cn,
    {
      push_neg,
      exact hba21,
    },
    contradiction,
    cases hab2 with hab21 hab22,
    ext,
    cases hba with hba1 hba2,
    have hor := eq_or_lt_of_le hab21,
    cases hor with h1 h2,
    exact h1,
    have hnl := asymm h2,
    contradiction,
    cases hba2 with hba21 hba22,
    apply le_antisymm,
    exact hab21,
    exact hba21,
    cases hba with hba1 hba2,
    have hn : ¬ b.cn < a.cn,
    {
      push_neg,
      exact hab21,
    },
    contradiction,
    cases hba2 with hba21 hba22,
    apply le_antisymm,
    exact hab22,
    exact hba22,
  end,
  le_total := 
  begin
    intros a b,
    have ha : a ≤ b = less_eq_choID a b, from rfl,
    have hb : b ≤ a = less_eq_choID b a, from rfl,
    rw [ha, hb],
    simp,
    by_contradiction,
    push_neg at h,
    cases h with h1 h2,
    cases h1 with h11 h12,
    cases h2 with h21 h22,
    specialize h22 h11,
    specialize h12 h21,
    have hn := asymm h22,
    apply hn,
    exact h12,
  end,
  decidable_le := classical.dec_rel has_le.le,
  decidable_eq := classical.dec_eq choiceID,
  decidable_lt := classical.dec_rel has_lt.lt 
}

-- OracleID was commented out in Isabelle
/-# Begin Section on linear_order for oracleID-/
structure oracleID :=
(pk : pubkey)
#check oracleID

@[simp]def less_eq_oraID : oracleID → oracleID → Prop :=
  λ a b, a.pk ≤ b.pk
#check less_eq_oraID

@[simp]def less_oraID : oracleID → oracleID → Prop :=
  λ a b, a.pk < b.pk
#check less_eq_oraID

@[ext] lemma oraID_eq : ∀ {a b : oracleID}, a.pk = b.pk → a = b:= 
  begin
    rintros ⟨a1, a2⟩ ⟨b1, b2⟩ ⟨rfl⟩,
    repeat{refl,},
  end
#check oraID_eq

--instance renaming less_eq_choID
instance : has_le oracleID := ⟨less_eq_oraID⟩
instance : has_lt oracleID := ⟨less_oraID⟩

noncomputable instance : linear_order oracleID :=
{ le := less_eq_oraID,
  lt := less_oraID,
  le_refl :=
  begin
    intro a,
    simp,
  end,
  le_trans := 
  begin
    intros a b c hab hbc,
    simp * at *,
    transitivity,
    exact hab,
    exact hbc,
  end,
  lt_iff_le_not_le := 
  begin
    intros a b,
    simp,
    split,
    {
      intro hlt,
      split,
      {
        exact le_of_lt hlt,
      },
      {
        exact hlt,
      },
    },
    {
      intro hand,
      cases hand with h1 h2,
      exact h2,
    },
  end,
  le_antisymm := 
  begin
    intros a b hab hba,
    ext,
    apply le_antisymm,
    exact hab,
    exact hba,
  end,
  le_total := 
  begin
    intros a b,
    have ha : a ≤ b = less_eq_oraID a b, from rfl,
    have hb : b ≤ a = less_eq_oraID b a, from rfl,
    rw [ha, hb],    
    by_contradiction,
    push_neg at h,
    cases h with h1 h2,
    simp at h1,
    apply h2,
    simp,
    exact le_of_lt h1,
  end,
  decidable_le := classical.dec_rel has_le.le,
  decidable_eq := classical.dec_eq oracleID,
  decidable_lt := classical.dec_rel has_lt.lt 
}


/-# Begin section on ValueID linear_order-/

structure valueID :=
(int : int)
#check valueID

@[simp]def less_eq_valID : valueID → valueID → Prop :=
  λ a b, a.int ≤ b.int
#check less_eq_oraID

@[simp]def less_valID : valueID → valueID → Prop :=
  λ a b, a.int < b.int
#check less_eq_oraID

@[ext] lemma valID_eq : ∀ {a b : valueID}, a.int = b.int → a = b:= 
  begin
    rintros ⟨a1, a2⟩ ⟨b1, b2⟩ ⟨rfl⟩,
    repeat{refl,},
  end
#check oraID_eq

--instance renaming less_eq_choID
instance : has_le valueID := ⟨less_eq_valID⟩
instance : has_lt valueID := ⟨less_valID⟩

noncomputable instance : linear_order valueID :=
{ le := less_eq_valID,
  lt := less_valID,
  le_refl :=
  begin
    intro a,
    simp,
  end,
  le_trans := 
  begin
    intros a b c hab hbc,
    simp * at *,
    transitivity,
    exact hab,
    exact hbc,
  end,
  lt_iff_le_not_le := 
  begin
    intros a b,
    simp,
    split,
    {
      intro hlt,
      split,
      {
        exact le_of_lt hlt,
      },
      {
        exact hlt,
      },
    },
    {
      intro hand,
      cases hand with h1 h2,
      exact h2,
    },
  end,
  le_antisymm := 
  begin
    intros a b hab hba,
    ext,
    apply le_antisymm,
    exact hab,
    exact hba,
  end,
  le_total := 
  begin
    intros a b,
    have ha : a ≤ b = less_eq_valID a b, from rfl,
    have hb : b ≤ a = less_eq_valID b a, from rfl,
    rw [ha, hb],    
    by_contradiction,
    push_neg at h,
    cases h with h1 h2,
    simp at h1,
    apply h2,
    simp,
    exact le_of_lt h1,
  end,
  decidable_le := classical.dec_rel has_le.le,
  decidable_eq := classical.dec_eq valueID,
  decidable_lt := classical.dec_rel has_lt.lt 
}



/-# linear_order of accountID-/

instance : linear_order accountId :=
int.linear_order

instance : linear_order (accountId × token) :=
{ le := _,
  lt := _,
  le_refl := _,
  le_trans := _,
  lt_iff_le_not_le := _,
  le_antisymm := _,
  le_total := _,
  decidable_le := _,
  decidable_eq := _,
  decidable_lt := _ } 
/-# End linear_order sections-/

mutual
inductive value, observation
with value : Type 
| availableMoney (ai : accountId) (t : token): value
| Constant (z : int) : value
| negValue (v : value) : value
| addValue (v w: value) : value
| subValue (v w: value) : value
| mulValue (v w : value) : value
| scale (x y : int ) (v : value) : value
| choiceValue (ci : choiceID) : value
| slotIntervalStart : value
| slotIntervalEnd : value 
| useValue (vi : valueID) : value 
| cond (o : observation) (v w : value) : value
  with observation : Type
  | andObs (o p : observation) : observation
  | orObs (o p : observation) : observation
  | notObs (o : observation) : observation
  | choseSomething (ci : choiceID) : observation
  | trueObs : observation
  | falseObs : observation
  | valueGE (v w : value)
  | valueGT (v w : value)
  | valueLT (v w : value)
  | valueLE (v w : value) 
  | valueEQ (v w : value)
#check value
#check observation


@[reducible] def slotInterval := slot × slot
@[reducible] def bound := int × int


--tests for inbounds
def anyf (y : chosenNum) : bound → Prop :=
  λ x, x.fst ≤ y ∧ y ≤ x.snd
  #check anyf

def inBounds : chosenNum → list bound → Prop :=
begin
  intros chn b,
  apply ListTools.any,
  exact anyf chn,
  exact anyf chn,
  exact b,
end

-- removes list for some reason, will return later
def newinBounds : chosenNum → list bound → Prop :=
  λ num, ListTools.any ((λ (x : bound), x.fst ≤ num ∧ num ≤ x.snd))
#check newinBounds


variables (num : chosenNum)
#check ListTools.any ((λ (x : bound), x.fst ≤ num ∧ num ≤ x.snd))
#check ((λ (x : bound), x.fst ≤ num ∧ num ≤ x.snd))

inductive action : Type
| deposit (ai : accountId) (p : party) (t : token) (v : value)
| choice (ci : choiceID) (l : list bound) 
| notify (o : observation)

inductive payee : Type
| account (ai : accountId) 
| party (p : party)

mutual
inductive contract, case 
with  contract : Type
| close
| pay (ai : accountId) (p : payee) (t : token) (v : value) (c : contract)
| If (o : observation) (c1 c2 : contract)
| when (l : list case) (t : timeout) (c : contract)
| Let (vi : valueID) (v : value) (c : contract)
| assert (o : observation) (c : contract)
  with case : Type
  | case (a : action) (c : contract)

#check contract
#check case

@[reducible]def accounts := list ((accountId × token) × money)


#check list.sorted


structure state :=
(acc : accounts)
(choices : list (choiceID × chosenNum))
(boundValues : list (valueID × int)) 
(minSlot : slot)
#check state

def valid_state : state → Prop :=
  λ S, (MList.valid_map (S.acc) ∧ MList.valid_map S.choices ∧ MList.valid_map S.boundValues)
#check valid_state

structure environment :=
(slotInterval : slotInterval) 
#check environment.mk
#check state.mk

inductive input : Type
| IDeposit (ai : accountId) (p : party) (t : token) (m : money)
| IChoice (ci : choiceID) (cn : chosenNum) 
| INotify

inductive intervalError : Type
| invalidInterval (si : slotInterval)
| intervalInPastError (s : slot) (si : slotInterval)

inductive intervalResult : Type
| intervalTrimmed (e : environment) (s : state)
| intervalError (ie : intervalError)

--⦇ ⦈ is .mk of structures
def fixInterval : slotInterval → state → intervalResult :=
  λ inter s, 
    (let curMinSlot := s.minSlot in 
      (let newLow := max inter.fst curMinSlot in 
        ( let curInterval := (newLow, inter.snd) in 
          (let env := environment.mk curInterval in
            (let newState := state.mk (s.acc) (s.choices) (s.boundValues) (newLow) in
              if inter.snd < inter.fst 
              then intervalResult.intervalError (intervalError.invalidInterval inter)
              else intervalResult.intervalTrimmed env newState
            )))))
#check fixInterval

def signum : int → int :=
  λ x, (if 0 < x then 1 else (if x = 0 then 0 else -1))
#check division_def

-- I might want to check about the keeping = instead of ∧, or possibly using ↔
-- integer division might be used here instead
def quot : int → int → int :=
  --λ x y, (if ((x < 0) ↔ (y < 0)) then (x / y) else -((abs x) / (abs y)))
  λ x y, (if ((x < 0) ↔(y < 0)) then (int.div x y) else -(int.div (abs x) (abs y)))
--infix quot
#check quot

def rem : int → int → int :=
  λ x y, x - (quot x y) * y

def quotRem : int → int → int × int :=
  λ x y, (quot x y, rem x y)

noncomputable mutual 
def evalValue, evalObservation
with evalValue : environment → state → value → int
  | env s (value.availableMoney accId token) := MList.findWithDefault 0 (accId, token) (s.acc)
  | env s (value.Constant integer) := integer
  | env s (value.negValue val) := -(evalValue env s val)
  | env s (value.addValue lhs rhs) := evalValue env s lhs + evalValue env s rhs
  | env s (value.subValue lhs rhs) := evalValue env s lhs - evalValue env s rhs
  | env s (value.mulValue lhs rhs) := evalValue env s lhs * evalValue env s rhs
  | env s (value.scale n d rhs) := 
    (let nn := evalValue env s rhs * n in 
      (let (q, r) := quotRem nn d in 
        (if (abs r*2) < (abs d) then q else q + signum nn * signum d)))
  -- here is a rec_fn_macro error
  | env s (value.choiceValue choId) := MList.findWithDefault 0 choId (s.choices) 
  | env s (value.slotIntervalStart) := (env.slotInterval).fst
  | env s (value.slotIntervalEnd) := (env.slotInterval).snd
  --and here
  | env s (value.useValue valId) := MList.findWithDefault 0 valId (s.boundValues)
  --and here
  | env s (value.cond cond thn els) := (if (evalObservation env s cond = true) 
                                          then (evalValue env s thn) 
                                          else (evalValue env s els))
with evalObservation : environment → state → observation → Prop
  | env s (observation.andObs lhs rhs) := (evalObservation env s lhs ∧ evalObservation env s rhs)
  | env s (observation.orObs lhs rhs) := (evalObservation env s lhs ∨ evalObservation env s rhs)
  | env s (observation.notObs subObs) := (¬ evalObservation env s subObs)
  | env s (observation.choseSomething choId) := (MList.member choId (s.choices))
  | env s (observation.valueGE lhs rhs) := (evalValue env s lhs ≥ evalValue env s rhs)
  | env s (observation.valueGT lhs rhs) := (evalValue env s lhs > evalValue env s rhs)
  | env s (observation.valueLT lhs rhs) := (evalValue env s lhs < evalValue env s rhs)
  | env s (observation.valueLE lhs rhs) := (evalValue env s lhs ≤ evalValue env s rhs)
  | env s (observation.valueEQ lhs rhs) := (evalValue env s lhs = evalValue env s rhs)
  | env s (observation.trueObs) := true
  | env s (observation.falseObs) := false
#check evalValue
#check evalObservation


--
lemma evalDoubleNegValue {env : environment} {sta : state} {x : value}: 
  evalValue env sta (value.negValue (value.negValue x)) = evalValue env sta x :=
  begin
    repeat{rw evalValue,},
    exact neg_neg _,
  end

lemma evalNegValue {env : environment}{sta : state} {x : value} :
  evalValue env sta (value.addValue x (value.negValue x)) = 0 :=
  begin
    repeat{rw evalValue,},
    exact add_right_neg _,
  end

lemma evalMulValue {env : environment} {sta : state} {x : value} :
  evalValue env sta (value.mulValue x (value.Constant 0)) = 0 :=
  begin 
    repeat{rw evalValue,},
    exact mul_zero _,
  end

lemma evalSubValue {env : environment}{sta : state}{x y : value}:
  evalValue env sta (value.subValue (value.addValue x y) y) = evalValue env sta x :=
  begin
    repeat{rw evalValue,},
    exact add_sub_cancel _ _,
  end

lemma evalScaleByZeroIsZero {env : environment} {sta : state} {x : value} :
  evalValue env sta (value.scale 0 1 x) = 0 :=
  begin
    rw evalValue,
    simp,
    rw quotRem,
    simp,
    rw quot,
    rw rem,
    simp,
    rw quot,
    simp,
    rw evalValue,
    split_ifs,
    repeat{refl,},
  end
#check evalScaleByZeroIsZero

lemma evalScaleByOneIsX {env : environment}{sta : state}{x : value} :
  evalValue env sta (value.scale 1 1 x) = evalValue env sta x :=
  begin
    rw evalValue,
    simp,
    rw quotRem,
    rw evalValue._match_1,
    split_ifs,
    {
      rw quot,
      simp,
      split_ifs,
      {
        have h : (evalValue env sta x).div 1 = (evalValue env sta x)/1, by refl,
        rw h,
        simp,
      },
      {
        sorry,
      },
    },
    {
      rw quot,
      simp,
      split_ifs,
      {
        have h : (evalValue env sta x).div 1 = (evalValue env sta x)/1, by refl,
        rw h,
        simp,
        sorry,
      },
      sorry,
    },

  end

lemma evalScaleMultiplies {env : environment}{sta : state}{a b c x: int}:
  evalValue env sta (value.scale a b (value.Constant (x*c))) = evalValue env sta (value.scale (x*a) b (value.Constant c)) :=
  begin
    repeat {rw evalValue,},
    rw mul_assoc,
    rw mul_comm,
    rw mul_comm x a,
    rw ← mul_assoc,
  end


lemma quotMultiplyEquivalence {a b c : int} :
  c ≠ 0 → quot (c*a) (c * b) = quot a b :=
  begin 
    intro hne,
    have h :c * quot a (c * b) = quot a b,
    {
      rw quot,
      simp,
      have he := int.div_self hne,
      split_ifs,
      {
        --is true
        sorry,
      },
      {
        have ha := decidable.lt_trichotomy a 0,
        cases ha with hl ha,
        have habs := abs_of_neg hl,
        rw habs,
        cases h,
        sorry,
        sorry,
      },
      {
        sorry,
      },
      {
        sorry,
      },
    },
    rw ← h,
    rw quot,
    simp,
    sorry,
    /-
    rw quot,
    simp,
    have h : c*a/(c*b) = c/c*a/b, 
    {
      sorry,
    },
    rw h,
    rw int.div_self hne,
    rw one_mul,
    /-
    have habsc : (abs (c * a)) / (abs (c * b)) = abs ((c * a) / (c * b)),
    {
      sorry,
    },
    have habs : (abs a / (abs b)) = abs (a /b),
    {

      sorry,
    },
    rw habs,
    rw habsc,
    -/
    split_ifs,
    {
      refl,
    },
    {
      cases h_1,
      push_neg at h_2,
      have ht := decidable.lt_trichotomy a 0,
      cases ht with hl ht,
      {
        have habsa := abs_of_neg hl,
        rw habsa,
        specialize h_2 hl,
        have habsb := abs_of_nonneg h_2,
        rw habsb,
        sorry,
      },
      {
        cases ht with he hg,
        {
          have habsa : abs a = 0,
          {
            rw he,
            simp,
          },
          rw habsa,
          rw he,
          simp,
        },
        {
          have hclt : c < 0,
          {
            sorry,
          },
          have hbpos : 0 < b,
          {
            sorry,
          },
          have habsb := abs_of_pos hbpos,
          have habsa := abs_of_pos hg,
          rw habsa,
          rw habsb,
          sorry,
        },
      },
    },
    {
      cases h_2,
      push_neg at h_1,
      have ht := decidable.lt_trichotomy c 0,
      cases ht with hl ht,
      {
        have hcag := mul_pos_of_neg_of_neg hl h_2_left,
        have hcbg := mul_pos_of_neg_of_neg hl h_2_right,
        have habsca := abs_of_pos hcag,
        have habscb := abs_of_pos hcbg,
        rw habsca,
        rw habscb,
        rw h,
        
        sorry,
      },
      {
        sorry,
      },
    },
    {

      sorry,
    },
    -/
  end

lemma remMultiplyEquivalence {a b c : int} : 
  c ≠ 0 → rem (c*a) (c*b) = c* (rem a b) :=
  begin
    intro hne,
    have h := quotMultiplyEquivalence hne,
    rw rem,
    simp,
    rw h,
    ring,
  end

lemma signEqualityPreservation {a b c : int} :
  a ≠ 0 → b ≠ 0 → c ≠ 0 → ((c*a < 0) = (c * b < 0)) = ((a < 0) = (b < 0)) :=
  begin
    simp,
    intros hnea hneb hnec,
    split,
    {
      intro hc,
      cases hc,
      split,
      {
        intro hal,
        have ht := decidable.lt_trichotomy c 0,
        cases ht with hl ht,
        {
          sorry,

        },
        {

          sorry,
        },
      },
      {

        sorry,
      },
    },
    {
      intro hiff,
      cases hiff,
      sorry,
    },
  end

#check int.add_mul_div_left
#check div_mul_div
#check div_eq_mul_inv
#check int.div_eq_of_eq_mul_left

lemma divMultiply {a b c : int} : c ≠ 0 → int.div (c * a)(c * b) = int.div a b :=
  begin
    intro hne,

    have h : int.div (c * a)  (c * b) = int.div c c * int.div a b,
    {
      sorry,
    },
    rw h,
    sorry,
    --rw int.div_self hne,
    --simp,
  end
--Isabelle more lemmas on integers (420 -489 in isabelle)
/-lemma divAbsMultiply : "(c :: int) ≠ 0 ⟹ ¦c * a¦ div ¦c * b¦ = ¦a¦ div ¦b¦"
  by (simp add: abs_mult)

lemma addMultiply : "¦(c :: int) * a + x * (c * b)¦ = ¦c * (a + x * b)¦"
  by (simp add: distrib_left mult.left_commute)

lemma addAbsMultiply : "¦(c :: int) * a + x * (c * b)¦ = ¦c * (a + x * b)¦"
  by (simp add: distrib_left mult.left_commute)

lemma subMultiply : "¦(c :: int) * a - x * (c * b)¦ = ¦c * (a - x * b)¦"
  by (simp add: mult.left_commute right_diff_distrib')

lemma ltMultiply : "(c :: int) ≠ 0 ⟹ (¦x¦ * 2 < ¦b¦) = (¦c * x¦ * 2 < ¦c * b¦)"
  by (simp add: abs_mult)

lemma remMultiplySmalle_aux : "(a :: int) ≠ 0 ⟹ (b :: int) ≠ 0 ⟹ (c :: int) ≠ 0 ⟹ (¦a - a div b * b¦ * 2 < ¦b¦) = (¦c * a - c * a div (c * b) * (c * b)¦ * 2 < ¦c * b¦)"
  apply (subst divMultiply)
  apply simp
  apply (subst subMultiply)
  using ltMultiply by blast

lemma remMultiplySmalle_aux2 : "(a :: int) ≠ 0 ⟹ (b :: int) ≠ 0 ⟹ (c :: int) ≠ 0 ⟹ (¦a + ¦a¦ div ¦b¦ * b¦ * 2 < ¦b¦) = (¦c * a + ¦c * a¦ div ¦c * b¦ * (c * b)¦ * 2 < ¦c * b¦)"
  apply (subst divAbsMultiply)
  apply simp
  apply (subst addMultiply)
  using ltMultiply by blast

lemma remMultiplySmaller : "c ≠ 0 ⟹ (¦a rem b¦ * 2 < ¦b¦) = (¦(c * a) rem (c * b)¦ * 2 < ¦c * b¦)"
  apply (cases "b = 0")
  apply simp
  apply (cases "a = 0")
  apply (simp add: mult_less_0_iff)
  apply (simp only:quot.simps rem.simps)
  apply (subst signEqualityPreservation[of a b c])
  apply simp
  apply simp
  apply simp
  apply (cases "(a < 0) = (b < 0)")
  apply (simp only:if_True refl)
  using remMultiplySmalle_aux apply blast
  apply (simp only:if_False refl)
  by (metis (no_types, hide_lams) diff_minus_eq_add mult_minus_left remMultiplySmalle_aux2)

lemma evalScaleMultiplyFractByConstant :
  "c ≠ 0 ⟹ evalValue env sta (Scale (c * a) (c * b) x) = evalValue env sta (Scale a b x)"
  apply (simp only:evalValue.simps Let_def)
  apply (cases "evalValue env sta x * (c * a) quotRem (c * b)")
  apply (cases "evalValue env sta x * a quotRem b")
  subgoal for cq cr q r
    apply (auto split:prod.splits simp only:Let_def)
    apply (cases "¦cr¦ * 2 < ¦c * b¦")
    apply (simp only:if_True)
    apply (cases "¦r¦ * 2 < ¦b¦")
    apply (simp only:if_True)
    apply (metis mult.left_commute prod.sel(1) quotMultiplyEquivalence quotRem.simps)
    apply (simp only:if_False)
    apply (metis mult.left_commute quotRem.simps remMultiplySmaller snd_conv)
    apply (cases "¦r¦ * 2 < ¦b¦")
    apply (simp only:if_False if_True)
    apply (metis mult.left_commute quotRem.simps remMultiplySmaller snd_conv)
    apply (simp only:if_True if_False)
    apply (simp only:quotRem.simps)
    by (smt Pair_inject mult.left_commute mult_cancel_left1 mult_eq_0_iff mult_neg_neg mult_pos_pos quotMultiplyEquivalence signum.simps zero_less_mult_pos zero_less_mult_pos2 zmult_eq_1_iff)
  done
-/
def refundOne : accounts → option ((party × token × money) × accounts) 
| (((accId, tok), money) :: rest) := (if 0 < money then some ((accId, tok, money), rest) else refundOne rest)
| list.nil := none


lemma refundOneShortens {acc nacc: accounts} { c : party × token × money} :
  refundOne acc = some (c, nacc) → list.length nacc < list.length acc :=
  begin
    intro href,
    induction acc,
    {
      simp,
      rw refundOne at href,
      simp at href,
      exact href,
    },
    {
      simp,
      transitivity,
      {
        apply acc_ih,
        cases acc_hd,
        cases acc_hd_fst,
        rw refundOne at href,
        split_ifs at href,
        {
          simp at href,
          cases href,
          
          sorry,
        },
        {
          exact href,
        },
      },
      {
        simp,
      },
    },
  end

inductive payment : Type
| payment (p : party) (t : token) (m : money)

inductive reduceEffect 
| reduceNoPayment
| reduceWithPayment (p : payment)

def moneyInAccount : accountId → token → accounts → money :=
  λ accId token accountsV, MList.findWithDefault 0 (accId, token) accountsV

def updateMoneyInAccount : accountId → token → money → accounts → accounts :=
  λ accId token money accountsV,(
  if money ≤ 0
  then MList.delete_map (accId, token) accountsV
  else MList.insert_map (accId, token) money accountsV)

def addMoneyToAccount : accountId → token → money → accounts → accounts :=
  λ accId token money accountsV, 
  (let balance := moneyInAccount accId token accountsV in (
    let newBalance := balance + money in (
      if money ≤ 0
      then accountsV
      else updateMoneyInAccount accId token newBalance accountsV
    )))

def giveMoney : payee → token → money → accounts → (reduceEffect × accounts)
| (payee.party party) token money accountsV := (reduceEffect.reduceWithPayment (payment.payment party token money), accountsV)
| (payee.account accId) token money accountsV := 
  (let newAccs := addMoneyToAccount accId token money accountsV in (reduceEffect.reduceNoPayment, newAccs))


lemma giveMoneyIncOne {p : payee}{t : token}{m : money}{a : accounts}{e : reduceEffect}{na : accounts} : 
  giveMoney p t m a = (e, na) → list.length na ≤ list.length a + 1 :=
  begin
    cases p,
    rw giveMoney,
    simp,
    intros re addm,
    rw addMoneyToAccount at addm,
    simp at addm,
    have h : m ≤ 0,
    {
      cases m,
      split_ifs at addm,
      exact h,
      rw moneyInAccount at addm,
      simp at addm,
      sorry,
      split_ifs at addm,
      exact h,
      push_neg at h,
      rw updateMoneyInAccount at addm,
      simp at addm,
      split_ifs at addm,
      have hle : -[1+m] ≤ 0,
      {

        sorry,
      },
      sorry,
      rw moneyInAccount at h_1,
      simp at h_1,
      rw MList.findWithDefault at h_1,
      sorry,
    },
    split_ifs at addm,
    rw addm,
    simp,
    intro hgm,
    rw giveMoney at hgm,
    simp at hgm,
    cases hgm with hgm1 hgm2,
    rw hgm2,
    simp,
  end

/-# REFUND SECTION #-/

inductive reduceWarning : Type
| reduceNoWarning
| reduceNonPositivePay (ai : accountId) (p : payee) (t : token) (m : money)
| reducePartialPay (ai : accountId) (p : payee) (t : token) (m1 m2 : money)
| reduceShadowing (vi : valueID) (x y : int)
| reduceAssertionFailed

inductive reduceStepResult : Type
| reduced (r : reduceWarning) (re : reduceEffect) (s : state) (c : contract)
| notReduced
| ambiguousSlotIntervalReductionError

noncomputable def reduceContractStep : environment → state → contract → reduceStepResult 
| _ s (contract.close) := (match refundOne (s.acc) with 
                          | some ((party, token, money), newAccount) := 
                            let newState := state.mk (newAccount) (s.choices) (s.boundValues) (s.minSlot) in
                              (reduceStepResult.reduced reduceWarning.reduceNoWarning (reduceEffect.reduceWithPayment (payment.payment party token money)) newState (contract.close))
                          | none := reduceStepResult.notReduced
                          end
                          )
| env s (contract.pay accId payee token val cont) := (let moneyToPay := evalValue env s val in 
                                                      if moneyToPay ≤ 0
                                                      then (let warning := reduceWarning.reduceNonPositivePay accId payee token moneyToPay in
                                                        reduceStepResult.reduced warning reduceEffect.reduceNoPayment s cont)
                                                      else (let balance := moneyInAccount accId token (s.acc) in 
                                                        let paidMoney := min balance moneyToPay in 
                                                          let newBalance := balance - paidMoney in 
                                                            let newAccs := updateMoneyInAccount accId token newBalance (s.acc) in
                                                              let warning := (if paidMoney < moneyToPay
                                                                              then reduceWarning.reducePartialPay accId payee token paidMoney moneyToPay
                                                                              else reduceWarning.reduceNoWarning) in
                                                                let (payment, finalAccs) := giveMoney payee token paidMoney newAccs in 
                                                                  reduceStepResult.reduced warning payment (state.mk (finalAccs) (s.choices) (s.boundValues) (s.minSlot)) cont
                                                            )
                                                      )
| env s (contract.If obs cont1 cont2) := (let cont := (if (evalObservation env s obs = true) then cont1 else cont2) in 
                                          reduceStepResult.reduced reduceWarning.reduceNoWarning reduceEffect.reduceNoPayment s cont)
| env s (contract.when _ timeout cont) := (let (startSlot, endSlot) := env.slotInterval in 
                                            if endSlot < timeout
                                            then reduceStepResult.notReduced
                                            else  (if timeout ≤ startSlot
                                                   then reduceStepResult.reduced reduceWarning.reduceNoWarning reduceEffect.reduceNoPayment s cont
                                                   else reduceStepResult.ambiguousSlotIntervalReductionError))
| env s (contract.Let valId val cont) := (let evaluatedValue := evalValue env s val in 
                                            let boundVals := s.boundValues in 
                                              let newState := state.mk (s.acc) (s.choices) (MList.insert_map valId evaluatedValue boundVals) (s.minSlot) in
                                                let warn := match (MList.lookup valId boundVals) with
                                                            | some oldVal := reduceWarning.reduceShadowing valId oldVal evaluatedValue
                                                            | none := reduceWarning.reduceNoWarning
                                                            end in
                                                    reduceStepResult.reduced warn reduceEffect.reduceNoPayment newState cont) 
| env s (contract.assert obs cont) := (let warning := if evalObservation env s obs = true
                                                      then reduceWarning.reduceNoWarning
                                                      else reduceWarning.reduceAssertionFailed in
                                            reduceStepResult.reduced warning reduceEffect.reduceNoPayment s cont)
#check reduceContractStep

inductive reduceResult : Type
| contractQuiescent (rw : list (reduceWarning)) (lp : list payment) (s : state) (c : contract)
| RRAmbiguousSlotError

def evalBound : state → contract → nat := 
  λ sta cont, list.length (sta.acc) + 2 * (sizeof cont)
#check evalBound

lemma reduceContractStepReducesSize_Refund_aux {sta : state}{ party : party} { money :token × money} {newAccount : accounts}: 
  refundOne (sta.acc) = some ((party, money), newAccount) →
  list.length ((state.mk (newAccount)(sta.choices)(sta.boundValues) (sta.minSlot)).acc) < list.length (sta.acc) :=
  begin
    intro href,
    apply refundOneShortens,
    simp,
    exact href,
  end

lemma reduceContractStepReducesSize_Refund_aux2 {sta newsta: state} {c newc: contract} {party : party} {token : token} {money : money} 
{newAccount : accounts} {twarning : reduceWarning} {teffect : reduceEffect}:
  reduceStepResult.reduced reduceWarning.reduceNoWarning (reduceEffect.reduceWithPayment (payment.payment party token money))
  (state.mk (newAccount)(sta.choices)(sta.boundValues) (sta.minSlot)) contract.close = reduceStepResult.reduced twarning teffect newsta newc →
  c = contract.close →
  refundOne (sta.acc) = some ((party, token, money), newAccount) →
  list.length (newsta.acc) + 2 * sizeof newc < list.length (sta.acc) :=
  begin
    intros hreduce hc href,
    simp at hreduce,
    cases hreduce,
    cases hreduce_right,
    cases hreduce_right_right,
    rw ← hreduce_right_right_right,
    have h := contract.close.sizeof_spec,
    have hdef : contract.close.sizeof = sizeof contract.close, by refl,
    rw ← hdef,
    rw h,
    simp,


    have hred := reduceContractStepReducesSize_Refund_aux href,
    simp at hred,
    transitivity,
    swap,
    {
      exact hred,
    },
    {
      sorry,
    },
  end

lemma reduceContractStepReducesSize_Refund {newAccount: accounts}{sta newsta: state} {c newc: contract} {party : party} {token : token} {money : money} 
  {twarning : reduceWarning} {teffect : reduceEffect}{a : (Semantics.party × Semantics.token × Semantics.money) × accounts }: 
  --match a with 
    | 
    reduceStepResult.reduced reduceWarning.reduceNoWarning (reduceEffect.reduceWithPayment (payment.payment party token money))
    (state.mk (newAccount)(sta.choices)(sta.boundValues) (sta.minSlot)) contract.close = reduceStepResult.reduced twarning teffect newsta newc →
    c = contract.close →
    refundOne (sta.acc) = some a →
    list.length (newsta.acc) + 2 * sizeof newc < list.length (sta.acc) :=
    begin
    intros hreduce hc href,
    end
#check list.sorted

fun evalValue :: "Environment ⇒ State ⇒ Value ⇒ int" and evalObservation :: "Environment ⇒ State ⇒ Observation ⇒ bool" where
  "evalValue env state (AvailableMoney accId token) =
    findWithDefault 0 (accId, token) (accounts state)" |
  "evalValue env state (Constant integer) = integer" |
  "evalValue env state (NegValue val) = uminus (evalValue env state val)" |
  "evalValue env state (AddValue lhs rhs) =
    evalValue env state lhs + evalValue env state rhs" |
  "evalValue env state (SubValue lhs rhs) =
    evalValue env state lhs - evalValue env state rhs" |
  "evalValue env state (MulValue lhs rhs) =
    evalValue env state lhs * evalValue env state rhs" |
  "evalValue env state (Scale n d rhs) = 
    (let nn = evalValue env state rhs * n in
     let (q, r) = nn quotRem d in
     if abs r * 2 < abs d then q else q + signum nn * signum d)" |
  "evalValue env state (ChoiceValue choId) =
    findWithDefault 0 choId (choices state)" |
  "evalValue env state (SlotIntervalStart) = fst (slotInterval env)" |
  "evalValue env state (SlotIntervalEnd) = snd (slotInterval env)" |
  "evalValue env state (UseValue valId) =
    findWithDefault 0 valId (boundValues state)" |
  "evalValue env state (Cond cond thn els) =
    (if evalObservation env state cond then evalValue env state thn else evalValue env state els)" |
  "evalObservation env state (AndObs lhs rhs) =
    (evalObservation env state lhs ∧ evalObservation env state rhs)" |
  "evalObservation env state (OrObs lhs rhs) =
    (evalObservation env state lhs ∨ evalObservation env state rhs)" |
  "evalObservation env state (NotObs subObs) =
    (¬ evalObservation env state subObs)" |
  "evalObservation env state (ChoseSomething choId) =
    (member choId (choices state))" |
  "evalObservation env state (ValueGE lhs rhs) =
    (evalValue env state lhs ≥ evalValue env state rhs)" |
  "evalObservation env state (ValueGT lhs rhs) =
    (evalValue env state lhs > evalValue env state rhs)" |
  "evalObservation env state (ValueLT lhs rhs) =
    (evalValue env state lhs < evalValue env state rhs)" |
  "evalObservation env state (ValueLE lhs rhs) =
    (evalValue env state lhs ≤ evalValue env state rhs)" |
  "evalObservation env state (ValueEQ lhs rhs) =
    (evalValue env state lhs = evalValue env state rhs)" |
  "evalObservation env state TrueObs = True" |
  "evalObservation env state FalseObs = False"

lemma evalDoubleNegValue :
  "evalValue env sta (NegValue (NegValue x)) = evalValue env sta x"
  by auto

lemma evalNegValue :
  "evalValue env sta (AddValue x (NegValue x)) = 0"
  by auto

lemma evalMulValue :
  "evalValue env sta (MulValue x (Constant 0)) = 0"
  by auto

lemma evalSubValue :
  "evalValue env sta (SubValue (AddValue x y) y) = evalValue env sta x"
  by auto

lemma evalScaleByZeroIsZero :
  "evalValue env sta (Scale 0 1 x) = 0"
  by auto

lemma evalScaleByOneIsX : "evalValue env sta (Scale 1 1 x) = evalValue env sta x"
  apply simp
  by (smt div_by_1)

lemma evalScaleMultiplies :
  "evalValue env sta (Scale a b (Constant (x * c))) = evalValue env sta (Scale (x * a) b (Constant c))"
  apply (simp only:evalValue.simps)
  by (simp add: mult.commute mult.left_commute)

lemma quotMultiplyEquivalence : "c ≠ 0 ⟹ (c * a) quot (c * b) = a quot b"
  apply auto
  apply (simp_all add: mult_less_0_iff)
  apply (metis div_mult_mult1 less_irrefl mult_minus_right)
  apply (smt div_minus_minus mult_minus_right nonzero_mult_div_cancel_left zdiv_zmult2_eq)
  apply (metis div_minus_right div_mult_mult1 mult_minus_right)
  by (metis div_mult_mult1 less_irrefl mult_minus_right)

lemma remMultiplyEquivalence : "c ≠ 0 ⟹ (c * a) rem (c * b) = c * (a rem b)"
  proof -
  assume "c ≠ 0"
  then have "⋀i ia. c * i quot (c * ia) = i quot ia"
    using quotMultiplyEquivalence by presburger
  then show ?thesis
    by (simp add: right_diff_distrib')
  qed


lemma signEqualityPreservation : "(a :: int) ≠ 0 ⟹ (b :: int) ≠ 0 ⟹ (c :: int) ≠ 0 ⟹ ((c * a < 0) = (c * b < 0)) = ((a < 0) = (b < 0))"
  by (smt mult_neg_pos mult_nonneg_nonneg mult_nonpos_nonpos mult_pos_neg)


fun refundOne :: "Accounts ⇒
                  ((Party × Token × Money) × Accounts) option" where
  "refundOne (((accId, tok), money)#rest) =
    (if money > 0 then Some ((accId, tok, money), rest) else refundOne rest)" |
  "refundOne [] = None"

lemma refundOneShortens : "refundOne acc = Some (c, nacc) ⟹
                           length nacc < length acc"
  apply (induction acc)
  apply simp
  by (metis Pair_inject length_Cons less_Suc_eq list.distinct(1)
            list.inject option.inject refundOne.elims)

datatype Payment = Payment Party Token Money

datatype ReduceEffect = ReduceNoPayment
                      | ReduceWithPayment Payment

fun moneyInAccount :: "AccountId ⇒ Token ⇒ Accounts ⇒ Money" where
  "moneyInAccount accId token accountsV = findWithDefault 0 (accId, token) accountsV"

fun updateMoneyInAccount :: "AccountId ⇒ Token ⇒ Money ⇒
                             Accounts ⇒
                             Accounts" where
  "updateMoneyInAccount accId token money accountsV =
    (if money ≤ 0
      then MList.delete (accId, token) accountsV
      else MList.insert (accId, token) money accountsV)"

fun addMoneyToAccount :: "AccountId ⇒ Token ⇒ Money ⇒
                          Accounts ⇒
                          Accounts" where
  "addMoneyToAccount accId token money accountsV =
    (let balance = moneyInAccount accId token accountsV in
    let newBalance = balance + money in
    if money ≤ 0
    then accountsV
    else updateMoneyInAccount accId token newBalance accountsV)"

fun giveMoney :: "Payee ⇒ Token ⇒ Money ⇒ Accounts ⇒
                  (ReduceEffect × Accounts)" where
  "giveMoney (Party party) token money accountsV =
    (ReduceWithPayment (Payment party token money), accountsV)" |
  "giveMoney (Account accId) token money accountsV =
    (let newAccs = addMoneyToAccount accId token money accountsV in
      (ReduceNoPayment, newAccs))"

lemma giveMoneyIncOne : "giveMoney p t m a = (e, na) ⟹ length na ≤ length a + 1"
  apply (cases p)
  apply (cases "m ≤ 0")
  apply auto
  by (smt Suc_eq_plus1 delete_length insert_length le_Suc_eq)

(* REDUCE *)
datatype ReduceWarning = ReduceNoWarning
                       | ReduceNonPositivePay AccountId Payee Token Money
                       | ReducePartialPay AccountId Payee Token Money Money
                       | ReduceShadowing ValueId int int
                       | ReduceAssertionFailed

datatype ReduceStepResult = Reduced ReduceWarning ReduceEffect State Contract
                          | NotReduced
                          | AmbiguousSlotIntervalReductionError

fun reduceContractStep :: "Environment ⇒ State ⇒ Contract ⇒ ReduceStepResult" where
"reduceContractStep _ state Close =
  (case refundOne (accounts state) of
     Some ((party, token, money), newAccount) ⇒
       let newState = state ⦇ accounts := newAccount ⦈ in
       Reduced ReduceNoWarning (ReduceWithPayment (Payment party token money)) newState Close
   | None ⇒ NotReduced)" |
"reduceContractStep env state (Pay accId payee token val cont) =
  (let moneyToPay = evalValue env state val in
   if moneyToPay ≤ 0
   then (let warning = ReduceNonPositivePay accId payee token moneyToPay in
         Reduced warning ReduceNoPayment state cont)
   else (let balance = moneyInAccount accId token (accounts state) in
        (let paidMoney = min balance moneyToPay in
         let newBalance = balance - paidMoney in
         let newAccs = updateMoneyInAccount accId token newBalance (accounts state) in
         let warning = (if paidMoney < moneyToPay
                        then ReducePartialPay accId payee token paidMoney moneyToPay
                        else ReduceNoWarning) in
         let (payment, finalAccs) = giveMoney payee token paidMoney newAccs in
         Reduced warning payment (state ⦇ accounts := finalAccs ⦈) cont)))" |
"reduceContractStep env state (If obs cont1 cont2) =
  (let cont = (if evalObservation env state obs
               then cont1
               else cont2) in
   Reduced ReduceNoWarning ReduceNoPayment state cont)" |
"reduceContractStep env state (When _ timeout cont) =
  (let (startSlot, endSlot) = slotInterval env in
   if endSlot < timeout
   then NotReduced
   else (if timeout ≤ startSlot
         then Reduced ReduceNoWarning ReduceNoPayment state cont
         else AmbiguousSlotIntervalReductionError))" |
"reduceContractStep env state (Let valId val cont) =
  (let evaluatedValue = evalValue env state val in
   let boundVals = boundValues state in
   let newState = state ⦇ boundValues := MList.insert valId evaluatedValue boundVals ⦈ in
   let warn = case lookup valId boundVals of
                Some oldVal ⇒ ReduceShadowing valId oldVal evaluatedValue
              | None ⇒ ReduceNoWarning in
   Reduced warn ReduceNoPayment newState cont)" |
"reduceContractStep env state (Assert obs cont) =
  (let warning = if evalObservation env state obs
                 then ReduceNoWarning
                 else ReduceAssertionFailed
   in Reduced warning ReduceNoPayment state cont)"

datatype ReduceResult = ContractQuiescent "ReduceWarning list" "Payment list"
                                          State Contract
                      | RRAmbiguousSlotIntervalError

fun evalBound :: "State ⇒ Contract ⇒ nat" where
  "evalBound sta cont = length (accounts sta) + 2 * (size cont)"

lemma reduceContractStepReducesSize_Refund_aux :
  "refundOne (accounts sta) = Some ((party, money), newAccount) ⟹
   length (accounts (sta⦇accounts := newAccount⦈)) < length (accounts sta)"
  by (simp add: refundOneShortens)

lemma reduceContractStepReducesSize_Refund_aux2 :
  "Reduced ReduceNoWarning (ReduceWithPayment (Payment party token money))
          (sta⦇accounts := newAccount⦈) Close =
   Reduced twa tef nsta nc ⟹
   c = Close ⟹
   refundOne (accounts sta) = Some ((party, token, money), newAccount) ⟹
   length (accounts nsta) + 2 * size nc < length (accounts sta)"
  apply simp
  using reduceContractStepReducesSize_Refund_aux by blast

lemma reduceContractStepReducesSize_Refund :
  "(case a of
          ((party, token, money), newAccount) ⇒
            Reduced ReduceNoWarning (ReduceWithPayment (Payment party token money))
             (sta⦇accounts := newAccount⦈) Close) =
         Reduced twa tef nsta nc ⟹
         c = Close ⟹
         refundOne (accounts sta) = Some a ⟹
         length (accounts nsta) + 2 * size nc < length (accounts sta)"
  apply (cases a)
  apply simp
  using reduceContractStepReducesSize_Refund_aux2 by fastforce

lemma zeroMinIfGT : "x > 0 ⟹ min 0 x = (0 :: int)"
  by simp

lemma reduceContractStepReducesSize_Pay_aux :
  "length z ≤ length x ⟹
   giveMoney x22 tok a z = (tef, y) ⟹
   length y < Suc (Suc (length x))"
  using giveMoneyIncOne by fastforce

lemma reduceContractStepReducesSize_Pay_aux2 :
  "giveMoney dst tok a (MList.delete (src, tok) x) = (tef, y) ⟹
   length y < Suc (Suc (length x))"
  using delete_length reduceContractStepReducesSize_Pay_aux by blast

lemma reduceContractStepReducesSize_Pay_aux3 :
  "sta⦇accounts := b⦈ = nsta ⟹
   giveMoney dst tok a (MList.delete (src, tok) (accounts sta)) = (tef, b) ⟹
   length (accounts nsta) < Suc (Suc (length (accounts sta)))"
  using reduceContractStepReducesSize_Pay_aux2 by fastforce

lemma reduceContractStepReducesSize_Pay_aux4 :
  "lookup (k, tok) x = Some w ⟹
   giveMoney dst tok a (MList.insert (k, tok) v x) = (tef, y) ⟹
   length y < Suc (Suc (length x))"
  by (metis One_nat_def add.right_neutral add_Suc_right giveMoneyIncOne insert_existing_length le_imp_less_Suc)

lemma reduceContractStepReducesSize_Pay_aux5 :
"sta⦇accounts := ba⦈ = nsta ⟹
 lookup (src, tok) (accounts sta) = Some a ⟹
 giveMoney dst tok (evalValue env sta am) (MList.insert (src, tok) (a - evalValue env sta am) (accounts sta)) = (tef, ba) ⟹
 length (accounts nsta) < Suc (Suc (length (accounts sta)))"
  using reduceContractStepReducesSize_Pay_aux4 by fastforce

lemma reduceContractStepReducesSize_Pay_aux6 :
  "reduceContractStep env sta c = Reduced twa tef nsta nc ⟹
   c = Pay src dst tok am y ⟹
   evalValue env sta am > 0 ⟹
   lookup (src, tok) (accounts sta) = Some a ⟹
   evalBound nsta nc < evalBound sta c"
  apply (cases "a < evalValue env sta am")
  apply (simp add:min_absorb1)
  apply (cases "giveMoney dst tok a (MList.delete (src, tok) (accounts sta))")
  using reduceContractStepReducesSize_Pay_aux3 apply fastforce
  apply (cases "a = evalValue env sta am")
  apply (cases "giveMoney dst tok (evalValue env sta am) (MList.delete (src, tok) (accounts sta))")
  apply (simp add:min_absorb2)
  using reduceContractStepReducesSize_Pay_aux3 apply blast
  apply (cases "giveMoney dst tok (evalValue env sta am) (MList.insert (src, tok) (a - evalValue env sta am) (accounts sta))")
  apply (simp add:min_absorb2)
  using reduceContractStepReducesSize_Pay_aux5 by blast

lemma reduceContractStepReducesSize_Pay :
  "reduceContractStep env sta c = Reduced twa tef nsta nc ⟹
   c = Pay src dst tok am y ⟹ evalBound nsta nc < evalBound sta c"
  apply (cases "evalValue env sta am ≤ 0")
  apply auto[1]
  apply (cases "lookup (src, tok) (accounts sta)")
  apply (cases "evalValue env sta am > 0")
  apply (cases "giveMoney dst tok 0 (MList.delete (src, tok) (accounts sta))")
  apply (simp add:zeroMinIfGT)
  using reduceContractStepReducesSize_Pay_aux3 apply blast
  apply simp
  using reduceContractStepReducesSize_Pay_aux6 by auto

lemma reduceContractStepReducesSize_When :
  "reduceContractStep env sta c = Reduced twa tef nsta nc ⟹
   c = When cases timeout cont ⟹
   slotInterval env = (startSlot, endSlot) ⟹
   evalBound nsta nc < evalBound sta c"
  apply simp
  apply (cases "endSlot < timeout")
  apply simp
  apply (cases "timeout ≤ startSlot")
  by simp_all

lemma reduceContractStepReducesSize_Let_aux :
  "Reduced (ReduceShadowing vId a (evalValue env sta val)) ReduceNoPayment
         (sta⦇boundValues := MList.insert vId (evalValue env sta val) (boundValues sta)⦈) cont =
    Reduced twa tef nsta nc ⟹
   c = Contract.Let vId val cont ⟹
   lookup vId (boundValues sta) = Some a ⟹
   evalBound nsta nc < evalBound sta c"
  by auto

lemma reduceContractStepReducesSize_Let :
  "reduceContractStep env sta c = Reduced twa tef nsta nc ⟹
   c = Contract.Let vId val cont ⟹ evalBound nsta nc < evalBound sta c"
  apply (cases "lookup vId (boundValues sta)")
  apply auto[1]
  by (metis ReduceStepResult.inject reduceContractStep.simps(5) reduceContractStepReducesSize_Let_aux)

lemma reduceContractStepReducesSize :
  "reduceContractStep env sta c = Reduced twa tef nsta nc ⟹
     (evalBound nsta nc) < (evalBound sta c)"
  apply (cases c)
  apply (cases "refundOne (accounts sta)")
  apply simp
  apply simp
  apply (simp add:reduceContractStepReducesSize_Refund)
  using reduceContractStepReducesSize_Pay apply blast
  apply auto[1]
  apply (meson eq_fst_iff reduceContractStepReducesSize_When)
  using reduceContractStepReducesSize_Let apply blast
  by simp

function (sequential) reductionLoop :: "Environment ⇒ State ⇒ Contract ⇒ ReduceWarning list ⇒
                                        Payment list ⇒ ReduceResult" where
"reductionLoop env state contract warnings payments =
  (case reduceContractStep env state contract of
     Reduced warning effect newState ncontract ⇒
       let newWarnings = (if warning = ReduceNoWarning
                          then warnings
                          else warning # warnings) in
       let newPayments = (case effect of
                            ReduceWithPayment payment ⇒ payment # payments
                          | ReduceNoPayment ⇒ payments) in
       reductionLoop env newState ncontract newWarnings newPayments
   | AmbiguousSlotIntervalReductionError ⇒ RRAmbiguousSlotIntervalError
   | NotReduced ⇒ ContractQuiescent (rev warnings) (rev payments) state contract)"
  by pat_completeness auto
termination reductionLoop
  apply (relation "measure (λ(_, (state, (contract, _))) . evalBound state contract)")
  apply blast
  using reduceContractStepReducesSize by auto

fun reduceContractUntilQuiescent :: "Environment ⇒ State ⇒ Contract ⇒ ReduceResult" where
"reduceContractUntilQuiescent env state contract = reductionLoop env state contract [] []"

datatype ApplyWarning = ApplyNoWarning
                      | ApplyNonPositiveDeposit Party AccountId Token int

datatype ApplyResult = Applied ApplyWarning State Contract
                     | ApplyNoMatchError

fun applyCases :: "Environment ⇒ State ⇒ Input ⇒ Case list ⇒ ApplyResult" where
"applyCases env state (IDeposit accId1 party1 tok1 amount)
            (Cons (Case (Deposit accId2 party2 tok2 val) cont) rest) =
  (if (accId1 = accId2 ∧ party1 = party2 ∧ tok1 = tok2
        ∧ amount = evalValue env state val)
   then let warning = if amount > 0
                      then ApplyNoWarning
                      else ApplyNonPositiveDeposit party1 accId2 tok2 amount in
        let newState = state ⦇ accounts := addMoneyToAccount accId1 tok1 amount (accounts state) ⦈ in
        Applied warning newState cont
   else applyCases env state (IDeposit accId1 party1 tok1 amount) rest)" |
"applyCases env state (IChoice choId1 choice)
            (Cons (Case (Choice choId2 bounds) cont) rest) =
  (if (choId1 = choId2 ∧ inBounds choice bounds)
   then let newState = state ⦇ choices := MList.insert choId1 choice (choices state) ⦈ in
        Applied ApplyNoWarning newState cont
   else applyCases env state (IChoice choId1 choice) rest)" |
"applyCases env state INotify (Cons (Case (Notify obs) cont) rest) =
  (if evalObservation env state obs
   then Applied ApplyNoWarning state cont
   else applyCases env state INotify rest)" |
"applyCases env state (IDeposit accId1 party1 tok1 amount) (Cons _ rest) =
  applyCases env state (IDeposit accId1 party1 tok1 amount) rest" |
"applyCases env state (IChoice choId1 choice) (Cons _ rest) =
  applyCases env state (IChoice choId1 choice) rest" |
"applyCases env state INotify (Cons _ rest) =
  applyCases env state INotify rest" |
"applyCases env state acc Nil = ApplyNoMatchError"

fun applyInput :: "Environment ⇒ State ⇒ Input ⇒ Contract ⇒ ApplyResult" where
"applyInput env state input (When cases t cont) = applyCases env state input cases" |
"applyInput env state input c = ApplyNoMatchError"

datatype TransactionWarning = TransactionNonPositiveDeposit Party AccountId Token int
                            | TransactionNonPositivePay AccountId Payee Token int
                            | TransactionPartialPay AccountId Payee Token Money Money
                            | TransactionShadowing ValueId int int
                            | TransactionAssertionFailed

fun convertReduceWarnings :: "ReduceWarning list ⇒ TransactionWarning list" where
"convertReduceWarnings Nil = Nil" |
"convertReduceWarnings (Cons ReduceNoWarning rest) =
   convertReduceWarnings rest" |
"convertReduceWarnings (Cons (ReduceNonPositivePay accId payee tok amount) rest) =
   Cons (TransactionNonPositivePay accId payee tok amount)
        (convertReduceWarnings rest)" |
"convertReduceWarnings (Cons (ReducePartialPay accId payee tok paid expected) rest) =
   Cons (TransactionPartialPay accId payee tok paid expected)
        (convertReduceWarnings rest)" |
"convertReduceWarnings (Cons (ReduceShadowing valId oldVal newVal) rest) =
   Cons (TransactionShadowing valId oldVal newVal)
        (convertReduceWarnings rest)" |
"convertReduceWarnings (Cons ReduceAssertionFailed rest) =
   Cons TransactionAssertionFailed (convertReduceWarnings rest)"

fun convertApplyWarning :: "ApplyWarning ⇒ TransactionWarning list" where
"convertApplyWarning ApplyNoWarning = Nil" |
"convertApplyWarning (ApplyNonPositiveDeposit party accId tok amount) =
   Cons (TransactionNonPositiveDeposit party accId tok amount) Nil"

datatype ApplyAllResult = ApplyAllSuccess "TransactionWarning list" "Payment list"
                                     State Contract
                        | ApplyAllNoMatchError
                        | ApplyAllAmbiguousSlotIntervalError

fun applyAllLoop :: "Environment ⇒ State ⇒ Contract ⇒ Input list ⇒
                    TransactionWarning list ⇒ Payment list ⇒
                    ApplyAllResult" where
"applyAllLoop env state contract inputs warnings payments =
   (case reduceContractUntilQuiescent env state contract of
      RRAmbiguousSlotIntervalError ⇒ ApplyAllAmbiguousSlotIntervalError
    | ContractQuiescent reduceWarns pays curState cont ⇒
       (case inputs of
          Nil ⇒ ApplyAllSuccess (warnings @ (convertReduceWarnings reduceWarns))
                                 (payments @ pays) curState cont
        | Cons input rest ⇒
           (case applyInput env curState input cont of
              Applied applyWarn newState cont ⇒
                  applyAllLoop env newState cont rest
                               (warnings @ (convertReduceWarnings reduceWarns)
                                         @ (convertApplyWarning applyWarn))
                               (payments @ pays)
            | ApplyNoMatchError ⇒ ApplyAllNoMatchError)))"

fun applyAllInputs :: "Environment ⇒ State ⇒ Contract ⇒ Input list ⇒
                 ApplyAllResult" where
"applyAllInputs env state contract inputs = applyAllLoop env state contract inputs Nil Nil"

type_synonym TransactionSignatures = "Party list"

datatype TransactionError = TEAmbiguousSlotIntervalError
                          | TEApplyNoMatchError
                          | TEIntervalError IntervalError
                          | TEUselessTransaction

record TransactionOutputRecord = txOutWarnings :: "TransactionWarning list"
                                 txOutPayments :: "Payment list"
                                 txOutState :: State
                                 txOutContract :: Contract

datatype TransactionOutput = TransactionOutput TransactionOutputRecord
                           | TransactionError TransactionError

record Transaction = interval :: SlotInterval
                     inputs :: "Input list"

fun computeTransaction :: "Transaction ⇒ State ⇒ Contract ⇒ TransactionOutput" where
"computeTransaction tx state contract =
  (let inps = inputs tx in
   case fixInterval (interval tx) state of
     IntervalTrimmed env fixSta ⇒
       (case applyAllInputs env fixSta contract inps of
          ApplyAllSuccess warnings payments newState cont ⇒
            if ((contract = cont) ∧ ((contract ≠ Close) ∨ (accounts state = [])))
            then TransactionError TEUselessTransaction
            else TransactionOutput ⦇ txOutWarnings = warnings
                                   , txOutPayments = payments
                                   , txOutState = newState
                                   , txOutContract = cont ⦈
        | ApplyAllNoMatchError ⇒ TransactionError TEApplyNoMatchError
        | ApplyAllAmbiguousSlotIntervalError ⇒ TransactionError TEAmbiguousSlotIntervalError)
     | IntervalError error ⇒ TransactionError (TEIntervalError error))"

fun playTraceAux :: "TransactionOutputRecord ⇒ Transaction list ⇒ TransactionOutput" where
"playTraceAux res Nil = TransactionOutput res" |
"playTraceAux ⦇ txOutWarnings = warnings
              , txOutPayments = payments
              , txOutState = state
              , txOutContract = cont ⦈ (Cons h t) =
   (let transRes = computeTransaction h state cont in
    case transRes of
      TransactionOutput transResRec ⇒ playTraceAux (transResRec ⦇ txOutPayments := payments @ (txOutPayments transResRec)
                                                                 , txOutWarnings := warnings @ (txOutWarnings transResRec) ⦈) t
    | TransactionError _ ⇒ transRes)"

fun emptyState :: "Slot ⇒ State" where
"emptyState sl = ⦇ accounts = MList.empty
                 , choices = MList.empty
                 , boundValues = MList.empty
                 , minSlot = sl ⦈"

fun playTrace :: "Slot ⇒ Contract ⇒ Transaction list ⇒ TransactionOutput" where
"playTrace sl c t = playTraceAux ⦇ txOutWarnings = Nil
                                 , txOutPayments = Nil
                                 , txOutState = emptyState sl
                                 , txOutContract = c ⦈ t"

(* Extra functions *)

type_synonym TransactionOutcomes = "(Party × Money) list"

definition "emptyOutcome = (MList.empty :: TransactionOutcomes)"

lemma emptyOutcomeValid : "valid_map emptyOutcome"
  using MList.valid_empty emptyOutcome_def by auto

fun isEmptyOutcome :: "TransactionOutcomes ⇒ bool" where
"isEmptyOutcome trOut = all (λ (x, y) ⇒ y = 0) trOut"

fun addOutcome :: "Party ⇒ Money ⇒ TransactionOutcomes ⇒ TransactionOutcomes" where
"addOutcome party diffValue trOut =
   (let newValue = case MList.lookup party trOut of
                     Some value ⇒ value + diffValue
                   | None ⇒ diffValue in
    MList.insert party newValue trOut)"

fun combineOutcomes :: "TransactionOutcomes ⇒ TransactionOutcomes ⇒ TransactionOutcomes" where
"combineOutcomes x y = MList.unionWith plus x y"

fun getPartiesFromReduceEffect :: "ReduceEffect list ⇒ (Party × Token × Money) list" where
"getPartiesFromReduceEffect (Cons (ReduceWithPayment (Payment p tok m)) t) =
   Cons (p, tok, -m) (getPartiesFromReduceEffect t)" |
"getPartiesFromReduceEffect (Cons x t) = getPartiesFromReduceEffect t" |
"getPartiesFromReduceEffect Nil = Nil"

fun getPartiesFromInput :: "Input list ⇒ (Party × Token × Money) list" where
"getPartiesFromInput (Cons (IDeposit _ p tok m) t) =
   Cons (p, tok, m) (getPartiesFromInput t)" |
"getPartiesFromInput (Cons x t) = getPartiesFromInput t" |
"getPartiesFromInput Nil = Nil"

fun getOutcomes :: "ReduceEffect list ⇒ Input list ⇒ TransactionOutcomes" where
"getOutcomes eff inp =
   foldl (λ acc (p, t, m) . addOutcome p m acc) emptyOutcome
         ((getPartiesFromReduceEffect eff) @ (getPartiesFromInput inp))"

fun addSig :: "Party list ⇒ Input ⇒ Party list" where
"addSig acc (IDeposit _ p _ _) = SList.insert p acc" |
"addSig acc (IChoice (ChoiceId _ p) _) = SList.insert p acc" |
"addSig acc INotify = acc"

fun getSignatures :: "Input list ⇒ TransactionSignatures" where
"getSignatures l = foldl addSig SList.empty l"

fun isQuiescent :: "Contract ⇒ State ⇒ bool" where
"isQuiescent Close state = (accounts state = [])" |
"isQuiescent (When _ _ _) _ = True" |
"isQuiescent _ _ = False"

fun maxTimeContract :: "Contract ⇒ int"
and maxTimeCase :: "Case ⇒ int" where
"maxTimeContract Close = 0" |
"maxTimeContract (Pay _ _ _ _ contract) = maxTimeContract contract" |
"maxTimeContract (If _ contractTrue contractFalse) = max (maxTimeContract contractTrue) (maxTimeContract contractFalse)" |
"maxTimeContract (When Nil timeout contract) = max timeout (maxTimeContract contract)" |
"maxTimeContract (When (Cons head tail) timeout contract) = max (maxTimeCase head) (maxTimeContract (When tail timeout contract))" |
"maxTimeContract (Let _ _ contract) = maxTimeContract contract" |
"maxTimeContract (Assert _ contract) = maxTimeContract contract" |
"maxTimeCase (Case _ contract) = maxTimeContract contract"

end Semantics
