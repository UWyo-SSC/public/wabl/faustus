import .lovelib
import .MListLean
import .SListLean
import .SemanticsLean
--SmallStep "HOL-Library.Product_Lexorder" "HOL.Product_Type" "HOL-IMP.Star"


namespace FaustusSemantics


@[reducible]def identifier := Semantics.valueID

inductive faustusPubKey 
| constantPubKey (pk : Semantics.pubkey) 
| usePubKey (i : identifier) 

inductive faustusPayee 
| account (fpk : faustusPubKey) 
| party (fpk : faustusPubKey) 

mutual inductive faustusValue, faustusObservation
with faustusValue : Type
| availableMoney (fpk : faustusPubKey) (t : Semantics.token) 
| Constant (i : int) 
| negValue (fv : faustusValue) 
| addValue (fv1 fv2 : faustusValue) 
| subValue (fv1 fv2 : faustusValue) 
| mulValue (fv1 fv2 : faustusValue) 
| scale (k l : int) (fv : faustusValue) 
| choiceValue (ci : Semantics.choiceID)
| slotIntervalStart
| slotIntervalEnd 
| useValue (i : identifier) 
| cond (fo : faustusObservation) (fv1 fv2 : faustusValue) 
  with faustusObservation : Type
  | andObs (fo1 fo2 : faustusObservation) 
  | orObs (fo1 fo2 : faustusObservation) 
  | notObs (fo1 : faustusObservation) 
  | choseSomething (ci : Semantics.choiceID) 
  | useObservation (i : identifier)
  | trueObs : faustusObservation
  | falseObs : faustusObservation
  | valueGE (fv1 fv2 : faustusValue)
  | valueGT (fv1 fv2 : faustusValue)
  | valueLT (fv1 fv2 : faustusValue) 
  | valueLE (fv1 fv2 : faustusValue)
  | valueEQ (fv1 fv2 : faustusValue)

inductive faustusParameter
| valueParameter (i : identifier) 
| observationParameter (i : identifier) 
| pubKeyParameter (i : identifier) 

inductive faustusArgument
| valueArgument (fv : faustusValue) 
| observationArgument (fo : faustusObservation) 
| pubKeyArgument (fpk : faustusPubKey) 

inductive faustusAction
| deposit (fpk1 fpk2 : faustusPubKey) (t : Semantics.token) (fv : faustusValue) 
| choice (cid : Semantics.choiceID) (l : list Semantics.bound) 
| notify (fo : faustusObservation)


mutual inductive faustusContract, faustusCase
with faustusContract : Type
| close
| pay (fpk : faustusPubKey) (fp : faustusPayee) (t : Semantics.token) (fv : faustusValue) (fc : faustusContract)
| If (fo : faustusObservation) (fc1 fc2 : faustusContract) 
| when (l : list faustusCase) (t : Semantics.timeout) (fc : faustusContract)
| Let (i : identifier) (fv : faustusValue) (fc : faustusContract) 
| letObservation (i : identifier) (fo : faustusObservation) (fc : faustusContract) 
| letPubKey (i : identifier) (fpk : faustusPubKey) (fc : faustusContract)
| assert (fo : faustusObservation) (fc : faustusContract)
| letC (i : identifier) (l : list (faustusParameter)) (fc1 fc2 : faustusContract)
| useC (i : identifier) (l : list (faustusArgument))
with faustusCase : Type
  | case (fa : faustusAction) (fc : faustusContract)
#check faustusContract

--def faustusCase := faustusAction × faustusContract
#check faustusCase



def caseToAction : faustusCase → faustusAction :=
  λ fCase, 
  faustusCase.cases_on (λ (fCase : faustusCase), faustusAction) fCase (λ (fa : faustusAction) (fc : faustusContract), fa)
#check caseToAction

def caseToContract : faustusCase → faustusContract := 
  λ fCase,
  faustusCase.cases_on (λ (fCase : faustusCase), faustusContract) fCase (λ (fa : faustusAction) (fc : faustusContract), fc)
#check caseToContract

#check list.sizeof
#check list.length
#check list.sizeof_slice_lt
#check faustusCase.sizeof
#check has_sizeof.cases_on
#eval sizeof Type
#check list.length
#eval sizeof (list.length [1, 2])
#check list.length_tail


lemma caseToContract_Size_LT {cases : list faustusCase} {tCont : faustusContract} {x : faustusCase}:
  x ∈ cases → (caseToContract x).sizeof < cases.sizeof :=
  begin
    intro hxin,
    induction cases,
    cases hxin,
    -- I might want to show it is greater than even 1
    have hx : 1 ≤ x.sizeof,
    {
      induction x,
      rw faustusCase.case.sizeof_spec,
      have himp : 0 ≤ sizeof x_fa + sizeof x_fc → 1 ≤ 1 + sizeof x_fa + sizeof x_fc,
      {
        intro h0,
        rw add_assoc,
        exact nat.lt_one_add_iff.mpr h0,
      },
      apply himp,
      apply add_nonneg,
      repeat{simp,},
    },
    have h : (cases_hd :: cases_tl).sizeof = 1 + cases_hd.sizeof + cases_tl.sizeof, from rfl,
    rw h,
    cases hxin,
    {
      induction (caseToContract x),
      {
        rw faustusContract.close.sizeof_spec,
        rw hxin at hx,
        apply lt_of_lt_of_le,
        have h : 1 < 1 + cases_hd.sizeof,
        {
          simp,
          apply lt_of_lt_of_le,
          exact nat.one_pos,
          exact hx,
        },
        exact h,
        simp,
      },
      {
        rw faustusContract.pay.sizeof_spec,
        rw hxin at hx,
        
        
        sorry,
      },
      {
        rw faustusContract.If.sizeof_spec,
        
        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
      {

        sorry,
      },
    },
    {
      transitivity,
      apply cases_ih,
      exact hxin,
      simp,
      rw nat.lt_one_add_iff,
      simp,
    },
  end
#check caseToContract_Size_LT

lemma caseToContractTest {cases : list faustusCase} {fc : faustusCase} : 
  (caseToContract fc).sizeof < 1 + fc.sizeof + cases.tail.sizeof :=
  begin
    induction (caseToContract fc),
    rw faustusContract.close.sizeof_spec,
    have h : 1 ≤ cases.tail.sizeof ,
    {

      sorry,
    },
    rw add_assoc,
    repeat{sorry,},
  end 
--list.length for size
mutual
def faustusContractSize , faustusCasesSize 
with faustusContractSize : faustusContract → nat 
| faustusContract.close := 1
| (faustusContract.pay pay accId tok val cont) := 1 + (faustusContractSize cont)
| (faustusContract.If obs trueCont falseCont) := 1 + (faustusContractSize trueCont) + (faustusContractSize falseCont)
| (faustusContract.when cases t tCont) := 1 + (faustusContractSize tCont) + (faustusCasesSize cases)
| (faustusContract.Let vid val cont) := 1 + (faustusContractSize cont)
| (faustusContract.letObservation obsId obs cont) := 1 + (faustusContractSize cont)
| (faustusContract.assert obs cont) := 1 + (faustusContractSize cont)
| (faustusContract.letC cid params boundCon cont) := 1 + list.length params + faustusContractSize boundCon + faustusContractSize cont
| (faustusContract.useC cid args) := 1 + list.length args
| (faustusContract.letPubKey pkId pk cont) := 1 + faustusContractSize cont 
with faustusCasesSize : list (faustusCase) → nat
| list.nil := 0 
| (fc :: cs) := 1 + (faustusContractSize (caseToContract fc)) + faustusCasesSize cs

#check faustusContractSize 
#print faustusCasesSize 


@[simp]lemma faustusContractSize_GT0 {con : faustusContract} :
  0 < faustusContractSize con :=
  begin
    induction con,
    {
      rw faustusContractSize,
      simp,
    },
    repeat 
    {
      rw faustusContractSize,
      apply add_pos,
      exact nat.one_pos,
      exact con_x,
    },
    {
      rw faustusContractSize,
      repeat {apply add_pos,},
      exact nat.one_pos,
      exact con_x,
      exact con_x_1,
    },
    {
      rw faustusContractSize,
      induction con_l,
      rw faustusCasesSize,
      simp,
      transitivity,
      exact con_x,
      simp,
      rw faustusCasesSize,
      transitivity,
      exact con_l_ih,
      simp,
      ring,
      apply nat.lt_add_left,
      simp,
    },
    {
      rw faustusContractSize,
      induction con_l,
      simp,
      apply add_pos,
      apply add_pos,
      exact nat.one_pos,
      exact con_x,
      exact con_x_1,
      simp,
      have hl : 1 + con_l_tl.length + faustusContractSize con_fc1 + faustusContractSize con_fc2 
        < 1 + (con_l_tl.length + 1) + faustusContractSize con_fc1 + faustusContractSize con_fc2, by simp,
      transitivity,
      exact con_l_ih,
      exact hl,
    },
    {
      rw faustusContractSize,
      induction con_l,
      simp,
      simp,
      have hl : 1 + con_l_tl.length < 1 + (con_l_tl.length + 1), by simp,
      transitivity,
      exact con_l_ih,
      exact hl,
    }

  end
#check faustusContractSize_GT0

#check list.has_sizeof

inductive abstractionTypeInformation : Type
| valueType
| observationType
| pubKeyType
| contractType (l : list faustusParameter)

@[reducible]def typeContext := list (identifier × abstractionTypeInformation)

def paramsToTypeContext : list (faustusParameter) → typeContext 
| list.nil := list.nil
| ((faustusParameter.valueParameter i) :: rest) := ((i, abstractionTypeInformation.valueType) :: (paramsToTypeContext rest))
| ((faustusParameter.observationParameter i):: rest) := ((i, abstractionTypeInformation.observationType) :: paramsToTypeContext rest)
| ((faustusParameter.pubKeyParameter i) :: rest) := ((i, abstractionTypeInformation.pubKeyType) :: (paramsToTypeContext rest))

inductive abstractionInformation 
| valueAbstraction (i : identifier)
| observationAbstraction (i : identifier)
| pubKeyAbstraction (i : identifier)
| contractAbstraction (l : identifier × list(faustusParameter) × faustusContract)

@[reducible]def faustusContext := list abstractionInformation

def paramsToFaustusContext : list faustusParameter → faustusContext
| list.nil := list.nil 
| ((faustusParameter.valueParameter vid)::rest) := ((abstractionInformation.valueAbstraction vid) :: (paramsToFaustusContext rest))
| ((faustusParameter.observationParameter obsId)::rest) := ((abstractionInformation.observationAbstraction obsId)::paramsToFaustusContext rest)
| ((faustusParameter.pubKeyParameter pkId)::rest) := ((abstractionInformation.pubKeyAbstraction pkId)::(paramsToFaustusContext rest))

-- Isabelle uses record 
structure faustusState :=
(acc : Semantics.accounts)
(choices : list (Semantics.choiceID × Semantics.chosenNum))
(boundValues : list (identifier × int))
(boundPubKeys : list (identifier × Semantics.pubkey))
(minSlot : Semantics.slot)

def valid_faustus_state : faustusState → Prop :=
  λ fstate, 
  (MList.valid_map fstate.acc 
  ∧ MList.valid_map fstate.choices 
  ∧ MList.valid_map fstate.boundValues 
  ∧ MList.valid_map fstate.boundPubKeys)
#check valid_faustus_state

def wellTypedPubKey : typeContext → faustusPubKey → Prop
| tyCtx (faustusPubKey.constantPubKey pk) := true
| list.nil (faustusPubKey.usePubKey pkId) := false
| ((boundPkId, ty) :: rest) (faustusPubKey.usePubKey pkId) := 
  (((boundPkId = pkId ∧ ty = abstractionTypeInformation.pubKeyType))
  ∨ (boundPkId ≠ pkId ∧ wellTypedPubKey rest (faustusPubKey.usePubKey pkId)))
#check wellTypedPubKey

def wellTypedPayee : typeContext → faustusPayee → Prop
| tyCtx (faustusPayee.account pk) := wellTypedPubKey tyCtx pk
| tyCtx (faustusPayee.party pk) := wellTypedPubKey tyCtx pk
#check wellTypedPayee

mutual
def wellTypedValue, wellTypedObservation 
with wellTypedValue : typeContext → faustusValue → Prop
| tyCtx (faustusValue.availableMoney pk token) := wellTypedPubKey tyCtx pk
| tyCtx (faustusValue.Constant integer) := true
| tyCtx (faustusValue.negValue val) := wellTypedValue tyCtx val
| tyCtx (faustusValue.addValue lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusValue.subValue lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusValue.mulValue lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusValue.scale n d rhs) := wellTypedValue tyCtx rhs
| tyCtx (faustusValue.choiceValue choId) := true
| tyCtx (faustusValue.slotIntervalStart) := true 
| tyCtx (faustusValue.slotIntervalEnd) := true 
| list.nil (faustusValue.useValue valId) := false
| ((boundValId, ty) ::rest) (faustusValue.useValue valId) :=
  ((boundValId = valId ∧ ty = abstractionTypeInformation.valueType) 
  ∨ (boundValId ≠ valId ∧ wellTypedValue rest (faustusValue.useValue valId)))
|tyCtx (faustusValue.cond cond thn els) := 
  (wellTypedObservation tyCtx cond 
  ∧ wellTypedValue tyCtx thn 
  ∧ wellTypedValue tyCtx els)
with wellTypedObservation : typeContext → faustusObservation → Prop
| tyCtx (faustusObservation.andObs lhs rhs) := ((wellTypedObservation tyCtx lhs) ∧ (wellTypedObservation tyCtx rhs))
| tyCtx (faustusObservation.orObs lhs rhs) := ((wellTypedObservation tyCtx lhs) ∧ (wellTypedObservation tyCtx rhs))
| tyCtx (faustusObservation.notObs subObs) := wellTypedObservation tyCtx subObs
| tyCtx (faustusObservation.choseSomething choId) := true
| tyCtx (faustusObservation.valueGE lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusObservation.valueGT lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusObservation.valueLT lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusObservation.valueLE lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| tyCtx (faustusObservation.valueEQ lhs rhs) := ((wellTypedValue tyCtx lhs) ∧ (wellTypedValue tyCtx rhs))
| list.nil (faustusObservation.useObservation obsId) := false
| ((boundObsId, ty) :: rest) (faustusObservation.useObservation obsId) := 
  ((boundObsId = obsId ∧ ty = abstractionTypeInformation.observationType) 
  ∨ boundObsId  ≠ obsId ∧ wellTypedObservation rest (faustusObservation.useObservation obsId))
| tyCtx faustusObservation.trueObs := true
| tyCtx faustusObservation.falseObs := true
#check wellTypedValue
#check wellTypedObservation

def wellTypedArgument : typeContext → faustusParameter → faustusArgument → Prop
| tyCtx (faustusParameter.valueParameter vid) (faustusArgument.valueArgument val) := wellTypedValue tyCtx val
| tyCtx (faustusParameter.observationParameter obsId) (faustusArgument.observationArgument obs) := wellTypedObservation tyCtx obs
| tyCtx (faustusParameter.pubKeyParameter pkId) (faustusArgument.pubKeyArgument pk) := wellTypedPubKey tyCtx pk
| tyCtx p a := false

def wellTypedArguments : typeContext → list (faustusParameter) → list (faustusArgument) → Prop
| tyCtx list.nil list.nil := true
| tyCtx p list.nil := false 
| tyCtx list.nil a := false 
| tyCtx (firstParam :: restParams) (firstArg :: restArgs) := (wellTypedArgument tyCtx firstParam firstArg ∧ wellTypedArguments tyCtx restParams restArgs)


--make sure noncomputable still is ok to use
noncomputable def lookupContractIdParamTypeInformation : typeContext → identifier → option ((list (faustusParameter) × typeContext))
  | list.nil cid := none
  | ((i, ty) :: restTy) cid := 
  if (i = cid) 
  then 
    match ty with 
    | abstractionTypeInformation.contractType params := some (params, restTy)
    | _ := none
    end
  else lookupContractIdParamTypeInformation restTy cid
#check lookupContractIdParamTypeInformation


mutual
def wellTypedContract, wellTypedCases 
with wellTypedContract : typeContext → faustusContract → Prop
--| tyCtx close := true (moving to end solves some issues for some reason)
  | tyCtx (faustusContract.pay accId payee token val cont) :=
    (wellTypedPubKey tyCtx accId ∧ wellTypedPayee tyCtx payee ∧ wellTypedValue tyCtx val ∧ wellTypedContract tyCtx cont)
  | tyCtx (faustusContract.If obs cont1 cont2) :=
    (wellTypedObservation tyCtx obs ∧ wellTypedContract tyCtx cont1 ∧ wellTypedContract tyCtx cont2)
  | tyCtx (faustusContract.when cases t cont) := (wellTypedCases tyCtx cases ∧ wellTypedContract tyCtx cont)
  | tyCtx (faustusContract.assert obs cont) := (wellTypedObservation tyCtx obs ∧ wellTypedContract tyCtx cont)
  --((valId, abstractionTypeInformation.valueType) ::tyCtx)
  | tyCtx (faustusContract.Let valId val cont) := (wellTypedValue tyCtx val ∧ wellTypedContract tyCtx cont)
  --((obsId, abstractionTypeInformation.observationType) ::tyCtx)
  | tyCtx (faustusContract.letObservation obsId obs cont) := (wellTypedObservation tyCtx obs ∧ wellTypedContract tyCtx cont)
  --((pkId, abstractionTypeInformation.pubKeyType) :: tyCtx)
  | tyCtx (faustusContract.letPubKey pkId pk cont) := (wellTypedPubKey tyCtx pk ∧ wellTypedContract tyCtx cont)
  --(list.append (paramsToTypeContext params) tyCtx)      ((cid, abstractionTypeInformation.contractType params) ::tyCtx) 
  | tyCtx (faustusContract.letC cid params body cont) := (wellTypedContract tyCtx body ∧ wellTypedContract tyCtx cont)
  | tyCtx (faustusContract.useC cid args) := 
    match (lookupContractIdParamTypeInformation tyCtx cid) with 
    | some (params, innerTyCtx) := wellTypedArguments tyCtx params args
    | _ := false
    end
  | tyCtx close := true
with wellTypedCases : typeContext → list (faustusCase) → Prop
  | tyCtx list.nil := true
  | tyCtx ((faustusCase.case (faustusAction.deposit fromPk toPk token value) c) :: rest) :=
    ((wellTypedPubKey tyCtx fromPk) ∧ (wellTypedPubKey tyCtx toPk) ∧ (wellTypedValue tyCtx value) ∧ (wellTypedContract tyCtx c) ∧ (wellTypedCases tyCtx rest))
  | tyCtx ((faustusCase.case (faustusAction.choice choiceId bounds) c )::rest) := ((wellTypedContract tyCtx c) ∧ (wellTypedCases tyCtx rest))
  | tyCtx ((faustusCase.case (faustusAction.notify observation) c) ::rest) := ((wellTypedObservation tyCtx observation) ∧ (wellTypedContract tyCtx c) ∧ (wellTypedCases tyCtx rest)) 
#check wellTypedContract
#check wellTypedCases

#check abstractionInformation.contractAbstraction


def wellTypedFaustusContext : typeContext → faustusContext → Prop
  | list.nil list.nil := true
  | tyCtx list.nil := false
  | list.nil ctx := false
  | ((tyValId, abstractionTypeInformation.valueType)::restTypes) ((abstractionInformation.valueAbstraction absValId)::restAbstractions) := 
    (tyValId = absValId ∧ wellTypedFaustusContext restTypes restAbstractions)
  | ((tyPkId,  abstractionTypeInformation.pubKeyType)::restTypes) ((abstractionInformation.pubKeyAbstraction absPkId)::restAbstractions) := 
    (tyPkId = absPkId ∧ wellTypedFaustusContext restTypes restAbstractions)
  | ((tyObsId, abstractionTypeInformation.observationType)::restTypes) ((abstractionInformation.observationAbstraction absObsId)::restAbstractions) :=
    (tyObsId = absObsId ∧ wellTypedFaustusContext restTypes restAbstractions)
  | ((tyCid, abstractionTypeInformation.contractType tyParams)::restTypes) 
    ((abstractionInformation.contractAbstraction (absCid, absParams, absBody))::restAbstractions) :=
    (tyCid = absCid ∧ tyParams = absParams ∧ wellTypedContract (list.append (paramsToTypeContext absParams) restTypes) absBody
    ∧ wellTypedFaustusContext restTypes restAbstractions)
  | (someType ::restTypes) (someAbs:: restAbs) := false
#check wellTypedFaustusContext

lemma consValueWellTypedContext (tyCtx : typeContext) (ctx : faustusContext) (valId : identifier): wellTypedFaustusContext tyCtx ctx ↔ 
  wellTypedFaustusContext ((valId, abstractionTypeInformation.valueType)::tyCtx) (abstractionInformation.valueAbstraction valId ::ctx) :=
  begin
    rw wellTypedFaustusContext,
    split,
    {
      intro hf,
      split,
      {
        refl,
      },
      {
        exact hf,
      },
    },
    {
      intro hand,
      cases hand with he hf,
      exact hf,
    },
  end

lemma consObservationWellTypedContext (tyCtx : typeContext) (ctx : faustusContext) (obsId : identifier): wellTypedFaustusContext tyCtx ctx ↔ 
  wellTypedFaustusContext ((obsId, abstractionTypeInformation.observationType)::tyCtx) (abstractionInformation.observationAbstraction obsId ::ctx) :=
  begin
    rw wellTypedFaustusContext,
    split,
    {
      intro hf,
      split,
      {
        refl,
      },
      {
        exact hf,
      },
    },
    {
      intro hand,
      cases hand with he hf,
      exact hf,
    },
  end

lemma consPubKeyWellTypedContext (tyCtx : typeContext) (ctx : faustusContext) (pkId : identifier): wellTypedFaustusContext tyCtx ctx ↔ 
  wellTypedFaustusContext ((pkId, abstractionTypeInformation.pubKeyType)::tyCtx) (abstractionInformation.pubKeyAbstraction pkId ::ctx) :=
  begin
    rw wellTypedFaustusContext,
    split,
    {
      intro hf,
      split,
      {
        refl,
      },
      {
        exact hf,
      },
    },
    {
      intro hand,
      cases hand with he hf,
      exact hf,
    },
  end

lemma consContractWellTypedContext (tyCtx : typeContext) (ctx : faustusContext) (params : list faustusParameter) (bodyCont : faustusContract) (cid : identifier):
  wellTypedFaustusContext tyCtx ctx ∧ wellTypedContract (list.append (paramsToTypeContext params) tyCtx) bodyCont ↔ 
  wellTypedFaustusContext ((cid, abstractionTypeInformation.contractType params)::tyCtx) (abstractionInformation.contractAbstraction (cid, params, bodyCont)::ctx) :=
  begin
    rw wellTypedFaustusContext,
    split,
    {
      intro hand,
      cases hand with hfc happ,
      split,
      {
        refl,
      },
      {
        split,
        {
          refl,
        },
        {
          split,
          {
            exact happ,
          },
          {
            exact hfc,
          },
        },
      },
    },
    {
      intro hands,
      cases hands with h1 hands,
      cases hands with h2 hands,
      cases hands with h3 h4,
      split,
      {
        exact h4,
      },
      {
        exact h3,
      }
    },
  end 
  
lemma paramsWellTypedContext (params : list faustusParameter) : wellTypedFaustusContext (paramsToTypeContext params) (paramsToFaustusContext params) :=
  begin
    induction params,
    rw paramsToTypeContext,
    rw paramsToFaustusContext,
    rw wellTypedFaustusContext,
    finish,
    cases params_hd,
    rw paramsToTypeContext,
    rw paramsToFaustusContext,
    rw ← consValueWellTypedContext,
    exact params_ih,
    rw paramsToTypeContext,
    rw paramsToFaustusContext,
    rw ← consObservationWellTypedContext,
    exact params_ih,
    rw paramsToTypeContext,
    rw paramsToFaustusContext,
    rw ← consPubKeyWellTypedContext,
    exact params_ih,
  end

lemma wellTypedPubKeyAppendTypes (tyCtx tyCtx2: typeContext) (pk : faustusPubKey) : 
  wellTypedPubKey tyCtx pk → wellTypedPubKey (list.append tyCtx tyCtx2) pk :=
  begin
    intro hpk,
    induction tyCtx,
    simp,
    cases pk,
    {
      rw wellTypedPubKey at hpk,
      induction tyCtx2,
      {
        rw wellTypedPubKey,
        exact hpk,
      },
      {
        cases tyCtx2_hd,
        rw wellTypedPubKey,
        exact hpk,
      },
    },
    {
      exfalso,
      exact hpk,
    },
    {
      simp,
      cases pk,
      {
        cases tyCtx_hd,
        cases tyCtx2,
        {
          rw wellTypedPubKey,
          exact hpk,
        },
        {
          cases tyCtx2_hd,
          rw wellTypedPubKey,
          exact hpk,
        },
      },
      {
        cases tyCtx_hd,
        cases tyCtx2,
        {
          rw wellTypedPubKey,
          rw wellTypedPubKey at hpk,
          have h : tyCtx_tl.append list.nil = tyCtx_tl,
          {
            exact is_right_id.right_id tyCtx_tl,
          },
          rw h,
          exact hpk,
        },
        {
          cases tyCtx2_hd,
          rw wellTypedPubKey,
          rw wellTypedPubKey at hpk,
          cases hpk,
          {
            left,
            exact hpk,
          },
          {
            right,
            cases hpk with hne hpk,
            split,
            {
              exact hne,
            },
            {
              exact tyCtx_ih hpk,
            },
          },
        },
      },
    },
  end

lemma wellTypedPayeeAppendTypes (tyCtx tyCtx2 : typeContext) (payee : faustusPayee) : wellTypedPayee tyCtx payee → 
  wellTypedPayee (list.append tyCtx tyCtx2) payee :=
  begin 
    intro hp,
    induction tyCtx,
    {
      cases payee,
      {
        rw wellTypedPayee at hp,
        simp,
        rw wellTypedPayee,
        cases tyCtx2,
        {
          exact hp,
        },
        {
          cases payee,
          {
            rw wellTypedPubKey at hp,
            cases tyCtx2_hd,
            rw wellTypedPubKey,
            exact hp,
          },
          {
            cases tyCtx2_hd,
            rw wellTypedPubKey at hp,
            exfalso,
            exact hp,
          },
        },
      },
      {
        simp,
        cases tyCtx2,
        {
          exact hp,
        },
        {
          cases tyCtx2_hd,
          rw wellTypedPayee,
          rw wellTypedPayee at hp,
          cases payee,
          {
            rw wellTypedPubKey,
            exact hp,
          },
          {
            exfalso,
            exact hp,
          },
        },
      },
    },
    {
      cases tyCtx_hd,
      cases payee,
      {
        rw wellTypedPayee,
        rw wellTypedPayee at hp,
        apply wellTypedPubKeyAppendTypes,
        exact hp,
      },
      {
        rw wellTypedPayee,
        rw wellTypedPayee at hp,
        apply wellTypedPubKeyAppendTypes,
        exact hp,
      },
    },
  end

--Iff possiblities
lemma WellTypedObsValAppend_aux {tyCtx tyCtx2 : typeContext} :
  (∀(val : faustusValue), wellTypedValue tyCtx val → wellTypedValue (list.append tyCtx tyCtx2) val) ↔
  (∀(obs : faustusObservation), wellTypedObservation tyCtx obs → wellTypedObservation (list.append tyCtx tyCtx2) obs) :=
  begin
    split,
    {
      intros himp obs hwo,
      induction obs,
      repeat
      {
        cases tyCtx,
        cases tyCtx2,
        simp,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        rw wellTypedObservation at hwo,
        cases hwo with h1 h2,
        split,
        {
          exact obs_x h1,
        },
        {
          exact obs_x_1 h2,
        },
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        exact hwo,
        cases hwo with h1 h2,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        split,
        {
          exact obs_x h1,
        },
        {
          exact obs_x_1 h2,
        },
      },
      {
        cases tyCtx,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        simp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        exact obs_x hwo,
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        exact obs_x hwo,
      },
      {
        cases tyCtx,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        simp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        exact hwo,
      },
      {
        induction tyCtx,
        rw wellTypedObservation at hwo,
        exfalso,
        exact hwo,
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        induction tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        {
          exact hwo,
        },
        {
          simp,
          rw wellTypedObservation,
          left,
          {
            cases hwo,
            exact hwo,
            
            sorry,
          },
        },

        /-
        induction tyCtx,
        rw wellTypedObservation at hwo,
        exfalso,
        exact hwo,
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        induction hwo,
        left,
        exact hwo,
        cases hwo with hne hwo,
        right,
        split,
        {
          exact hne,
        },
        {
          apply tyCtx_ih,
          {
            intros val hval,
            induction tyCtx_tl,
            {
              simp,
              rw wellTypedObservation at hwo,
              exfalso,
              exact hwo,
            },
            {
              cases tyCtx_tl_hd,
              rw wellTypedObservation at hwo,
              cases tyCtx_tl_hd_snd,
              {
                rw wellTypedValue,
                sorry,
              },
              {
                sorry,
              },
              {
                sorry,
              },
              {
                sorry,
              },
            },
          },
          {
            exact hwo,
          },
        },
        -/
      },
      repeat
      {
        cases tyCtx,
        cases tyCtx2,
        simp,
        cases tyCtx2_hd,
        simp,
        cases tyCtx_hd,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        exact hwo,
        cases tyCtx2_hd,
        simp,
      },
      repeat
      {
        cases tyCtx,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        simp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        cases hwo with h1 h2,
        split,
        {
          specialize himp obs_fv1,
          exact himp h1,
        },
        {
          specialize himp obs_fv2,
          exact himp h2,
        },
        cases tyCtx_hd,
        rw wellTypedObservation at hwo,
        cases tyCtx2,
        have hsimp : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        { 
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw hsimp,
        rw wellTypedObservation,
        exact hwo,
        cases tyCtx2_hd,
        simp,
        rw wellTypedObservation,
        cases hwo with h1 h2,
        split,
        apply himp,
        exact h1,
        apply himp,
        exact h2,
      },
    },
    {
      intros himp val hwv,
      induction val,
      {
      cases tyCtx,
      cases tyCtx2,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      cases val_fpk,
      {
        rw wellTypedPubKey at hwv,
        rw wellTypedPubKey,
        exact hwv,
      },
      {
        rw wellTypedPubKey at hwv,
        exfalso,
        exact hwv,
      },
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      rw ← list.append,
      apply wellTypedPubKeyAppendTypes,
      exact hwv,
      },
      repeat
      {
        cases tyCtx,
        simp,
        cases tyCtx2,
        exact hwv,
        cases tyCtx2_hd,
        rw wellTypedValue at hwv,
        rw wellTypedValue,
        exact hwv,
        cases tyCtx_hd,
        cases tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        exact hwv,
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        exact hwv,
      },
      repeat
      {
        cases tyCtx,
        cases tyCtx2,
        simp,
        exact hwv,
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        exact val_x hwv,
        cases tyCtx_hd,
        cases tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        rw h at val_x,
        exact val_x hwv,
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        exact val_x hwv,
      },
      repeat
      {
        cases tyCtx,
        rw wellTypedValue at hwv,
        simp,
        cases tyCtx2,
        rw wellTypedValue,
        exact hwv,
        cases tyCtx2_hd,
        cases hwv,
        rw wellTypedValue,
        split,
        {
          exact val_x hwv_left,
        },
        {
          exact val_x_1 hwv_right,
        },
        cases tyCtx_hd,
        cases tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        cases hwv,
        split,
        {
          rw h at val_x,
          exact val_x hwv_left,
        },
        {
          rw h at val_x_1,
          exact val_x_1 hwv_right,
        },
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        cases hwv,
        split,
        {
          exact val_x hwv_left,
        },
        {
          exact val_x_1 hwv_right,
        },
      },
      repeat
      {
        cases tyCtx,
        rw wellTypedValue at hwv,
        cases tyCtx2,
        simp,
        cases tyCtx2_hd,
        simp,
        cases tyCtx_hd,
        rw wellTypedValue at hwv,
        simp,
      },
      {
        induction tyCtx,
        rw wellTypedValue at hwv,
        exfalso,
        exact hwv,
        cases tyCtx_hd,
        induction tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        exact hwv,
        simp,
        cases tyCtx2_hd,
        rw wellTypedValue,
        rw wellTypedValue at hwv,
        cases hwv,
        left,
        exact hwv,
        right,
        cases hwv with hne hwv,
        split,
        {
          exact hne,
        },
        {
          apply tyCtx_ih,
          {
            intros obs hwo,
            specialize himp obs,
            sorry,
          },
          exact hwv,
        },
      },
      {
        cases tyCtx,
        rw wellTypedValue at hwv,
        cases tyCtx2,
        simp,
        rw wellTypedValue,
        exact hwv,
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        cases hwv with h1 hwv,
        cases hwv with h2 h3,
        split,
        {
          specialize himp val_fo,
          exact himp h1,
        },
        {
          split,
          {
            exact val_x h2,
          },
          {
            exact val_x_1 h3,
          },
        },
        cases tyCtx_hd,
        cases tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        exact hwv,
        rw wellTypedValue at hwv,
        cases hwv with h1 hwv,
        cases hwv with h2 h3,
        cases tyCtx2_hd,
        simp,
        rw wellTypedValue,
        split,
        {
          specialize himp val_fo,
          exact himp h1,
        },
        {
          split,
          {
            exact val_x h2,
          },
          {
            exact val_x_1 h3,
          },
        },
      },
    },
  end

#check WellTypedObsValAppend_aux
#check iff.mp

lemma wellTypedObservationAppendTypes(tyCtx tyCtx2: typeContext): 
  ∀ (obs : faustusObservation), wellTypedObservation tyCtx obs → wellTypedObservation (list.append tyCtx tyCtx2) obs := sorry
  --begin
    
    /-
    --intro himp,
    intro tyCtx2,
    --specialize himp tyCtx2,
    intro hwo,
    induction obs,
    {
      cases tyCtx,
      cases tyCtx2,
      simp,
      exact hwo,
      cases tyCtx2_hd,
      simp,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      cases hwo,
      split,
      {
        exact obs_x hwo_left,
      },
      {
        exact obs_x_1 hwo_right,
      },
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact hwo,
      cases tyCtx2_hd,
      simp,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      cases hwo,
      split,
      {
        exact obs_x hwo_left,
      },
      {
        exact obs_x_1 hwo_right,
      },
    },
    {
      cases tyCtx,
      cases tyCtx2,
      simp,
      exact hwo,
      cases tyCtx2_hd,
      simp,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      cases hwo,
      split,
      {
        exact obs_x hwo_left,
      },
      {
        exact obs_x_1 hwo_right,
      },
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwo,
      simp,
      cases tyCtx2_hd,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      cases hwo,
      split,
      {
        exact obs_x hwo_left,
      },
      {
        exact obs_x_1 hwo_right,
      },
    },
    {
      cases tyCtx,
      cases tyCtx2,
      exact hwo,
      simp,
      cases tyCtx2_hd,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact obs_x hwo,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwo,
      simp,
      cases tyCtx2_hd,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact obs_x hwo,
    },
    {
      cases tyCtx,
      cases tyCtx2,
      exact hwo,
      simp,
      cases tyCtx2_hd,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact hwo,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact hwo,
      cases tyCtx2_hd,
      simp,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      exact hwo,
    },
    {
      cases tyCtx,
      rw wellTypedObservation at hwo,
      exfalso,
      exact hwo,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwo,
      simp,
      rw wellTypedObservation,
      rw wellTypedObservation at hwo,
      sorry,
    },
    repeat
    {
      cases tyCtx,
      cases tyCtx2,
      exact hwo,
      cases tyCtx2_hd,
      simp,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwo,
      cases tyCtx2_hd,
      simp,
    },
    repeat
    {
      induction tyCtx,
      rw wellTypedObservation at hwo,
      cases tyCtx2,
      simp,
      rw wellTypedObservation,
      exact hwo,
      simp,
      cases tyCtx2_hd,
      rw wellTypedObservation,
      cases hwo with hw1 hw2,
      split,
      {
        induction obs_fv1,
        {
          rw wellTypedValue,
          rw wellTypedValue at hw1,
          cases obs_fv1_fpk,
          rw wellTypedPubKey,
          rw wellTypedPubKey at hw1,
          exact hw1,
          rw wellTypedPubKey at hw1,
          exfalso,
          exact hw1,
        },
        repeat
        {
          rw wellTypedValue at hw1,
          rw wellTypedValue,
          exact hw1,
        },
        repeat
        {
          rw wellTypedValue at hw1,
          rw wellTypedValue,
          exact obs_fv1_x hw1,
        },
        repeat
        {
          rw wellTypedValue at hw1,
          cases hw1,
          rw wellTypedValue,
          split,
          {
            exact obs_fv1_x hw1_left,
          },
          {
            exact obs_fv1_x_1 hw1_right,
          },
        },
        {
          rw wellTypedValue at hw1,
          exfalso,
          exact hw1,
        },
        {
          rw wellTypedValue at hw1,
          rw wellTypedValue,
          cases hw1 with hwo hw1,
          cases hw1 with hw1 hw2,
          split,
          {

            sorry,
          },
          {
            split,
            {
              exact obs_fv1_x hw1,
            },
            {
              exact obs_fv1_x_1 hw2,
            },
          },
        },
      },
      repeat{sorry,},
      
    },
    -/
  --end
lemma wellTypedValueAppendTypes (tyCtx : typeContext): 
  (∀(tyCtx2 : typeContext) (val : faustusValue), wellTypedValue tyCtx val →  wellTypedValue (list.append tyCtx tyCtx2) val) :=-- ↔ 
  --(∀(tyCtx2 : typeContext)(obs : faustusObservation), wellTypedObservation tyCtx obs → wellTypedObservation (list.append tyCtx tyCtx2) obs):=
  begin
    intros tyCtx2 val hwv,
    induction val,
    {
      cases tyCtx,
      cases tyCtx2,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      cases val_fpk,
      {
        rw wellTypedPubKey at hwv,
        rw wellTypedPubKey,
        exact hwv,
      },
      {
        rw wellTypedPubKey at hwv,
        exfalso,
        exact hwv,
      },
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      rw ← list.append,
      apply wellTypedPubKeyAppendTypes,
      exact hwv,
    },
    repeat
    {
      cases tyCtx,
      --rw wellTypedValue at hwv,
      simp,
      cases tyCtx2,
      exact hwv,
      cases tyCtx2_hd,
      rw wellTypedValue at hwv,
      rw wellTypedValue,
      exact hwv,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      exact hwv,
    },
    repeat
    {
      cases tyCtx,
      cases tyCtx2,
      simp,
      exact hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      exact val_x hwv,
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      rw h at val_x,
      exact val_x hwv,
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      exact val_x hwv,
    },
    repeat
    {
      cases tyCtx,
      rw wellTypedValue at hwv,
      simp,
      cases tyCtx2,
      rw wellTypedValue,
      exact hwv,
      cases tyCtx2_hd,
      cases hwv,
      rw wellTypedValue,
      split,
      {
        exact val_x hwv_left,
      },
      {
        exact val_x_1 hwv_right,
      },
      cases tyCtx_hd,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      cases hwv,
      split,
      {
        rw h at val_x,
        exact val_x hwv_left,
      },
      {
        rw h at val_x_1,
        exact val_x_1 hwv_right,
      },
      cases tyCtx2_hd,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      cases hwv,
      split,
      {
        exact val_x hwv_left,
      },
      {
        exact val_x_1 hwv_right,
      },
    },
    repeat
    {
      cases tyCtx,
      rw wellTypedValue at hwv,
      cases tyCtx2,
      simp,
      cases tyCtx2_hd,
      simp,
      cases tyCtx_hd,
      rw wellTypedValue at hwv,
      simp,
    },
    {
      cases tyCtx,
      rw wellTypedValue at hwv,
      exfalso,
      exact hwv,
      cases tyCtx_hd,
      --rw wellTypedValue at hwv,
      cases tyCtx2,
      have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
      {
        exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
      },
      rw h,
      exact hwv,
      simp,
      rw wellTypedValue,
      rw wellTypedValue at hwv,
      cases hwv,
      left,
      exact hwv,
      right,
      cases hwv with hne hwv,
      split,
      {
        exact hne,
      },
      {
        sorry,
      },
    },
    {

      sorry,
    },
  end
--End of welltypedObs/val

lemma wellTypedArgumentAppendTypes (tyCtx : typeContext) (param : faustusParameter) (arg : faustusArgument) :
  ∀ (tyCtx2 : typeContext), wellTypedArgument tyCtx param arg → wellTypedArgument (list.append tyCtx tyCtx2) param arg :=
  begin
    intros tyCtx2 hwa,
    induction arg,
    {
      induction param,
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        rw wellTypedArgument,
      
        apply wellTypedValueAppendTypes,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument at hwa,
        cases tyCtx2,
        have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
        {
          exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
        },
        rw h,
        rw wellTypedArgument,
        exact hwa,
        cases tyCtx2_hd,
        rw wellTypedArgument,
        apply wellTypedValueAppendTypes,
        exact hwa,
      },
      repeat
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
      },
    },
    {
      induction param,
      repeat
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
      },
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        rw wellTypedArgument,
        apply wellTypedObservationAppendTypes,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument,
        rw wellTypedArgument at hwa,
        apply wellTypedObservationAppendTypes,
        exact hwa,
      },
    },
    {
      induction param,
      repeat
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument at hwa,
        exfalso,
        exact hwa,
      },
      {
        cases tyCtx,
        rw wellTypedArgument at hwa,
        rw wellTypedArgument,
        apply wellTypedPubKeyAppendTypes,
        exact hwa,
        cases tyCtx_hd,
        rw wellTypedArgument,
        rw wellTypedArgument at hwa,
        apply wellTypedPubKeyAppendTypes,
        exact hwa,
      },
    },
  end

lemma wellTypedArgumentsAppendTypes (tyCtx : typeContext) (params : list faustusParameter) (args : list faustusArgument) :
  ∀(tyCtx2 : typeContext), wellTypedArguments tyCtx params args → wellTypedArguments (list.append tyCtx tyCtx2) params args :=
  begin
    intros tyCtx2 hwas,
    induction args,
    {
      induction params,
      {
        rw wellTypedArguments,
        exact hwas,
      },
      {
        rw wellTypedArguments,
        rw wellTypedArguments at hwas,
        exact hwas,
      },
    },
    {
      induction params,
      {
        rw wellTypedArguments,
        rw wellTypedArguments at hwas,
        exact hwas,
      },
      {
        induction tyCtx2,
        have hsimp : list.append tyCtx list.nil = tyCtx,
        {
          exact is_right_id.right_id tyCtx,
        },
        rw hsimp,
        exact hwas,
        
          

        sorry,
      },
    },
  end

lemma lookupCidAppendTypes (tyCtx : typeContext) (cid : identifier) (restTy :list (identifier × abstractionTypeInformation)) (params : list faustusParameter):
  ∀(tyCtx2 : typeContext), lookupContractIdParamTypeInformation tyCtx cid = some (params, restTy) → 
  lookupContractIdParamTypeInformation (list.append tyCtx tyCtx2) cid = some (params, (list.append restTy tyCtx2)) :=
  begin
    intros tyCtx2 hlook,
    induction tyCtx,
    rw lookupContractIdParamTypeInformation at hlook,
    simp at hlook,
    exfalso,
    exact hlook,
    cases tyCtx_hd,
    rw lookupContractIdParamTypeInformation at hlook,
    induction tyCtx2,
    have h : ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl).append list.nil = ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl),
    {
      exact is_right_id.right_id ((tyCtx_hd_fst, tyCtx_hd_snd) :: tyCtx_tl), 
    },
    rw h,
    rw lookupContractIdParamTypeInformation,
    have h2 : restTy.append list.nil = restTy,
    {
      exact is_right_id.right_id restTy,
    },
    rw h2,
    exact hlook,
    cases tyCtx2_hd,
    simp,
    rw lookupContractIdParamTypeInformation,
    split_ifs,
    split_ifs at hlook,
    sorry,
    split_ifs at hlook,
    
    

    sorry,
  end

lemma wellTypedContractAppendTypes (tyCtx tyCtx2: typeContext) :
  (∀(cont : faustusContract), wellTypedContract tyCtx cont → wellTypedContract (list.append tyCtx tyCtx2) cont) ↔
  (∀ (cases : list faustusCase), wellTypedCases tyCtx cases → wellTypedCases (list.append tyCtx tyCtx2) cases) :=
  begin
    split,
    {
      intros himp cases hwc,
      induction cases,
      {
        rw wellTypedCases,
        exact hwc,
      },
      {
        cases cases_hd,
        induction cases_hd_fa,
        {
          rw wellTypedCases,
          rw wellTypedCases at hwc,
          cases hwc with h1 hwc,
          cases hwc with h2 hwc,
          cases hwc with h3 hwc,
          cases hwc with h4 h5,
          split,
          {
            apply wellTypedPubKeyAppendTypes,
            exact h1,
          },
          {
            split,
            {
              apply wellTypedPubKeyAppendTypes,
              exact h2,
            },
            {
              split,
              {
                apply wellTypedValueAppendTypes,
                exact h3,
              },
              {
                split,
                {
                  specialize himp cases_hd_fc,
                  exact himp h4,
                },
                {
                  exact cases_ih h5,
                },
              },
            },
          },
        },
        {
          rw wellTypedCases,
          rw wellTypedCases at hwc,
          cases hwc with h1 h2,
          split,
          {
            specialize himp cases_hd_fc,
            exact himp h1,
          },
          {
            exact cases_ih h2,
          },
        },
        {
          rw wellTypedCases,
          rw wellTypedCases at hwc,
          cases hwc with h1 hwc,
          cases hwc with h2 h3,
          split,
          {
            apply wellTypedObservationAppendTypes,
            exact h1,
          },
          {
            split,
            {
              specialize himp cases_hd_fc,
              exact himp h2,
            },
            {
              exact cases_ih h3,
            },
          },
        },
      },
    },
    {
      intros himp cont hwc,
      induction cont,
      {
        rw wellTypedContract at hwc,
        rw wellTypedContract,
        exact hwc,
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 hwc,
        cases hwc with h2 hwc,
        cases hwc with h3 h4,
        split,
        {
          apply wellTypedPubKeyAppendTypes,
          exact h1,
        },
        {
          split,
          {
            apply wellTypedPayeeAppendTypes,
            exact h2,
          },
          {
            split,
            {
              apply wellTypedValueAppendTypes,
              exact h3,
            },
            {
              exact cont_x h4,
            },
          },
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        cases h2 with h2 h3,
        split,
        {
          apply wellTypedObservationAppendTypes,
          exact h1,
        },
        {
          split,
          {
            exact cont_x h2,
          },
          {
            exact cont_x_1 h3,
          },
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        split,
        {
          specialize himp cont_l,
          exact himp h1,
        },
        {
          exact cont_x h2,
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        split,
        {
          apply wellTypedValueAppendTypes,
          exact h1,
        },
        {
          sorry,
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        split,
        {
          apply wellTypedObservationAppendTypes,
          exact h1,
        },
        {
          sorry,
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        sorry,
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        split,
        {
          apply wellTypedObservationAppendTypes,
          exact h1,
        },
        {
          exact cont_x h2,
        },
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        cases hwc with h1 h2,
        sorry,
      },
      {
        rw wellTypedContract,
        rw wellTypedContract at hwc,
        sorry,
      },
    },
  end

  lemma concatWellTypedContexts (tyCtx1 tyCtx2 : typeContext) (ctx1 ctx2 : faustusContext) : 
  wellTypedFaustusContext tyCtx1 ctx1 → wellTypedFaustusContext tyCtx2 ctx2 → wellTypedFaustusContext (list.append tyCtx2 tyCtx1) (list.append ctx2 ctx1) :=
  begin
    intros hw1 hw2,
    induction tyCtx2,
    simp,
    induction ctx2,
    simp,
    cases tyCtx1,
    exact hw1,
    exact hw1,
    cases ctx1,
    have h: ((ctx2_hd :: ctx2_tl).append list.nil) = ((ctx2_hd :: ctx2_tl)),
    {
      exact is_right_id.right_id  ((ctx2_hd :: ctx2_tl)), 
    },
    rw h,
    cases tyCtx1,
    exact hw2,
    cases tyCtx1_hd,
    repeat{sorry,},
  end

def wellTypedState : typeContext → faustusState → Prop
  | list.nil s := true
  | ((valId, abstractionTypeInformation.valueType)::restTypes) s := (MList.member valId (s.boundValues) ∧ wellTypedState restTypes s)
  | ((obsId, abstractionTypeInformation.observationType)::restTypes) s := (MList.member obsId (s.boundValues) ∧ wellTypedState restTypes s)
  | ((pkId, abstractionTypeInformation.pubKeyType)::restTypes) s := (MList.member pkId (s.boundPubKeys) ∧ wellTypedState restTypes s)
  | ((cid, (abstractionTypeInformation.contractType params) )::restTypes) s := wellTypedState restTypes s
#check wellTypedState

lemma insertValueWellTypedStateIsWellTyped (tyCtx : typeContext) (s : faustusState)(vid : identifier) (val : ℤ): 
  wellTypedState tyCtx s →
   wellTypedState tyCtx (faustusState.mk (s.acc)(s.choices)(MList.insert_map vid val (s.boundValues))(s.boundPubKeys)(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    exact hws,
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        rw MList.member,
        simp,
        have hem := em (vid = tyCtx_hd_fst),
        cases hem,
        rw hem,
        rw MList.insert_lookup_some,
        simp,
        rw MList.insert_lookup_different,
        rw MList.member at hm,
        exact hm,
        symmetry,
        exact hem,
      },
      {
        exact tyCtx_ih hws,
      },
    },
    rw wellTypedState,
    rw wellTypedState at hws,
    cases hws with hm hws,
    split,
    {
      rw MList.member,
      simp,
      rw MList.member at hm,
      exact hm,
    },
    {
      exact tyCtx_ih hws,
    },
    rw wellTypedState,
    rw wellTypedState at hws,
    exact tyCtx_ih hws,
  end

lemma insertAppendValueWellTypedStateIsWellTyped (tyCtx : typeContext) (s : faustusState) (vid : identifier) (val : ℤ):  
  wellTypedState tyCtx s →
  wellTypedState ((vid, abstractionTypeInformation.valueType)::tyCtx) 
  (faustusState.mk (s.acc)(s.choices)(MList.insert_map vid val (s.boundValues))(s.boundPubKeys)(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    split,
    {
      rw MList.member,
      rw MList.insert_lookup_some,
      simp,
    },
    {
      rw wellTypedState,
      exact hws,
    },
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        apply insertValueWellTypedStateIsWellTyped,
        rw wellTypedState,
        split,
        {
          exact hm,
        },
        {
          exact hws,
        },
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        rw wellTypedState,
        apply insertValueWellTypedStateIsWellTyped,
        exact hws,
      },
    },
  end


lemma insertAppendObservationWellTypedStateIsWellTyped (tyCtx : typeContext) (s : faustusState) (vid : identifier) (val : ℤ):  
  wellTypedState tyCtx s →
  wellTypedState ((vid, abstractionTypeInformation.observationType)::tyCtx) 
  (faustusState.mk (s.acc)(s.choices)(MList.insert_map vid val (s.boundValues))(s.boundPubKeys)(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    split,
    {
      rw MList.member,
      rw MList.insert_lookup_some,
      simp,
    },
    {
      rw wellTypedState,
      exact hws,
    },
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        apply insertValueWellTypedStateIsWellTyped,
        rw wellTypedState,
        split,
        {
          exact hm,
        },
        {
          exact hws,
        },
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        rw wellTypedState,
        apply insertValueWellTypedStateIsWellTyped,
        exact hws,
      },
    },
  end

lemma insertPubKeyWellTypedStateIsWellTyped (tyCtx : typeContext) (s : faustusState) (vid : identifier) (val : ℤ):  
  wellTypedState tyCtx s → 
  wellTypedState tyCtx
  (faustusState.mk (s.acc)(s.choices)(s.boundValues)(MList.insert_map vid val (s.boundPubKeys))(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    exact hws,
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        simp,
        exact hm,
      },
      {
        exact tyCtx_ih hws,
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        simp,
        rw MList.member,
        have hem := em (vid = tyCtx_hd_fst),
        cases hem,
        rw hem,
        rw MList.insert_lookup_some,
        simp,
        rw MList.insert_lookup_different,
        rw MList.member at hm,
        exact hm,
        symmetry,
        exact hem,
      },
      {
        exact tyCtx_ih hws,
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      exact tyCtx_ih hws,
    },
  end

lemma insertAppendPubKeyWellTypedStateIsWellTyped (tyCtx : typeContext) (s : faustusState) (vid : identifier) (val : ℤ):
  wellTypedState tyCtx s →
  wellTypedState ((vid, abstractionTypeInformation.pubKeyType)::tyCtx) 
  (faustusState.mk (s.acc)(s.choices)(s.boundValues)(MList.insert_map vid val (s.boundPubKeys))(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    split,
    {
      simp,
      rw MList.member,
      rw MList.insert_lookup_some,
      simp,
    },
    {
      exact insertPubKeyWellTypedStateIsWellTyped list.nil s vid val hws,
    },
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        rw wellTypedState,
        split,
        {
          simp,
          exact hm,
        },
        {
          apply insertPubKeyWellTypedStateIsWellTyped,
          exact hws,
        },
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        rw MList.member,
        simp,
        rw MList.insert_lookup_some,
        simp,
      },
      {
        apply insertPubKeyWellTypedStateIsWellTyped,
        rw wellTypedState,
        split,
        {
          exact hm,
        },
        {
          exact hws,
        },
      },
    },
    {
      rw wellTypedState at hws,
      rw wellTypedState,
      split,
    {
      simp,
      rw MList.member,
      rw MList.insert_lookup_some,
      simp,
    },
    {
      apply insertPubKeyWellTypedStateIsWellTyped,
      rw wellTypedState,
      exact hws,
    },
  },
  end 

lemma wellTypedStateAccountsArbitrary (tyCtx : typeContext) (s : faustusState) (newAccounts : Semantics.accounts):  
  wellTypedState tyCtx s → 
  wellTypedState tyCtx
  (faustusState.mk (newAccounts)(s.choices)(s.boundValues)(s.boundPubKeys)(s.minSlot )) :=
  begin
    intro hws,
    induction tyCtx,
    rw wellTypedState,
    exact hws,
    cases tyCtx_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      cases hws with hm hws,
      split,
      {
        simp,
        exact hm,
      },
      {
        exact tyCtx_ih hws,
      },
    },
    {
      rw wellTypedState,
      rw wellTypedState at hws,
      exact tyCtx_ih hws,
    },
  end

lemma wellTypedStateCombineTypeContexts (tyCtx tyCtx2: typeContext) (s : faustusState) :
  wellTypedState tyCtx s → wellTypedState tyCtx2 s → wellTypedState (list.append tyCtx tyCtx2) s :=
  begin 
    intros hws1 hws2,
    induction tyCtx,
    cases tyCtx2,
    simp,
    simp,
    exact hws2,
    cases tyCtx2,
    have h : (tyCtx_hd :: tyCtx_tl).append list.nil = tyCtx_hd :: tyCtx_tl,
    {
      exact is_right_id.right_id  ((tyCtx_hd :: tyCtx_tl)),
    },
    rw h,
    exact hws1,
    cases tyCtx_hd,
    cases tyCtx2_hd,
    cases tyCtx_hd_snd,
    repeat
    {
      simp,
      rw wellTypedState,
      rw wellTypedState at hws1,
      cases hws1 with hm1 hws1,
      split,
      {
        exact hm1,
      },
      {
        exact tyCtx_ih hws1,
      },
    },
    {
      simp,
      rw wellTypedState,
      rw wellTypedState at hws1,
      exact tyCtx_ih hws1,
    },
  end

noncomputable def evalFaustusPubKey : faustusState → faustusPubKey → option Semantics.pubkey 
| s (faustusPubKey.constantPubKey pk) := some pk 
| s (faustusPubKey.usePubKey pkId) := MList.lookup pkId (s.boundPubKeys)

lemma evalFaustusPubKey_SamePubKeyDomainImpliesExecution {s otherState: faustusState} {pk : faustusPubKey}:
  ∀ (evaluatedPubKey : Semantics.pubkey), ∃ (otherEvaluatedPubKey : Semantics.pubkey), 
  (∀ (pubKeyId : identifier), MList.member pubKeyId (s.boundPubKeys) = MList.member pubKeyId (otherState.boundPubKeys)) →
  evalFaustusPubKey s pk = some evaluatedPubKey → evalFaustusPubKey otherState pk = some otherEvaluatedPubKey :=
  begin
    intro evpk,
    use evpk,
    --split,
    intros hall he,
    cases pk,
    {
      
      sorry,
    },
    {
      rw evalFaustusPubKey,
      rw evalFaustusPubKey at he,
      rw ← he,
      induction s.boundPubKeys,
      induction otherState.boundPubKeys,
      refl,
      cases hd,
      rw MList.lookup,
      split_ifs,
      {
        rw MList.lookup,
        cases s.boundPubKeys,
        rw MList.lookup at he,
        simp,
        simp at he,
        exact he,
        rw MList.lookup at ih,
        rw ← ih,
        sorry,
      },
      {
        rw MList.lookup,
        sorry,
      },
      sorry,
      sorry,
    },
  end

lemma wellTypedPubKeyEvaluates (tyCtx : typeContext) (s : faustusState) (pk : faustusPubKey) :
  wellTypedPubKey tyCtx pk → wellTypedState tyCtx s → evalFaustusPubKey s pk ≠ none :=
  begin
    intros hwp hws,
    induction pk,
    {
      rw evalFaustusPubKey,
      simp,
    },
    {
      rw evalFaustusPubKey,
      induction tyCtx,
      rw wellTypedPubKey at hwp,
      exfalso,
      exact hwp,
      cases tyCtx_hd,
      rw wellTypedPubKey at hwp,
      cases tyCtx_hd_snd,
      repeat
      {
        rw wellTypedState at hws,
        apply tyCtx_ih,
        cases hws,
        exact hws_right,
        cases hwp,
        {
          cases hwp,
          exfalso,
          simp at hwp_right,
          exact hwp_right,
        },
        {
          cases hwp,
          exact hwp_right,
        },
      },
      {
        rw wellTypedState at hws,
        cases hws,
        cases s.boundPubKeys,
        rw MList.member at hws_left,
        rw MList.lookup,
        rw MList.lookup at hws_left,
        exact hws_left,
        cases hd,
        rw MList.member at hws_left,
        cases hwp,
        cases hwp,
        rw ← hwp_left,
        exact hws_left,
        apply tyCtx_ih,
        exact hws_right,
        cases hwp,
        exact hwp_right,
      },
      {
        apply tyCtx_ih,
        rw wellTypedState at hws,
        exact hws,
        cases hwp,
        {
          cases hwp,
          simp at hwp_right,
          exfalso,
          exact hwp_right,
        },
        {
          cases hwp,
          exact hwp_right,
        },
      },
    },
  end

noncomputable def evalFaustusPayee : faustusState → faustusPayee → option Semantics.payee
  | s (faustusPayee.account acc) := match evalFaustusPubKey s acc with
                                    | none := none
                                    | some pk := some (Semantics.payee.account pk)
                                    end
  | s (faustusPayee.party p) := match evalFaustusPubKey s p with
                                | none := none
                                | some pk := some (Semantics.payee.party pk) 
                                end

lemma wellTypedPayeeEvaluates (tyCtx : typeContext) (s: faustusState) (payee : faustusPayee) :
  wellTypedPayee tyCtx payee → wellTypedState tyCtx s → evalFaustusPayee s payee ≠ none :=
  begin 
    intros hwp hws,
    induction payee,
    {
      rw evalFaustusPayee,
      induction payee,
      {
        rw evalFaustusPubKey,
        rw evalFaustusPayee._match_1,
        simp,
      },
      {
        rw evalFaustusPubKey,
        cases s.boundPubKeys,
        {
          rw MList.lookup,
          rw evalFaustusPayee._match_1,
          rw wellTypedPayee at hwp,
          induction tyCtx,
          rw wellTypedPubKey at hwp,
          simp,
          exact hwp,
          cases tyCtx_hd,
          cases tyCtx_hd_snd,
          repeat
          {
            rw wellTypedState at hws,
            rw wellTypedPubKey at hwp,
            cases hwp,
            simp at hwp,
            simp,
            exact hwp,
            apply tyCtx_ih,
            cases hws,
            exact hws_right,
            cases hwp,
            exact hwp_right,
          },
          {
            rw wellTypedState at hws,
            rw wellTypedPubKey at hwp,
            cases hws,
            rw MList.member at hws_left,
            cases s.boundPubKeys,
            rw MList.lookup at hws_left,
            simp,
            simp at hws_left,
            exact hws_left,
            cases hd,
            rw MList.lookup at hws_left,
            cases hwp,
            cases hwp,
            sorry,
            apply tyCtx_ih,
            exact hws_right,
            cases hwp,
            exact hwp_right,
          },
          {
            apply tyCtx_ih,
            rw wellTypedState at hws,
            exact hws,
            rw wellTypedPubKey at hwp,
            simp at hwp,
            cases hwp,
            exact hwp_right,
          },
        },
        {
          cases hd,
          rw MList.lookup,
          split_ifs,
          rw evalFaustusPayee._match_1,
          simp,
          rw wellTypedPayee at hwp,
          induction tyCtx,
          rw wellTypedPubKey at hwp,
          exfalso,
          exact hwp,
          cases tyCtx_hd,
          cases tyCtx_hd_snd,
          repeat
          {
            rw wellTypedState at hws,
            cases hws,
            apply tyCtx_ih,
            exact hws_right,
            rw wellTypedPubKey at hwp,
            simp at hwp,
            cases hwp,
            exact hwp_right,
          },
          {
            sorry,
          },
          {
            rw wellTypedState at hws,
            apply tyCtx_ih,
            exact hws,
            rw wellTypedPubKey at hwp,
            simp at hwp,
            cases hwp,
            exact hwp_right,
          },
          rw evalFaustusPayee._match_1,
          induction tyCtx,
          rw wellTypedPayee at hwp,
          rw wellTypedPubKey at hwp,
          exfalso,
          exact hwp,
          cases tyCtx_hd,
          cases tyCtx_hd_snd,
          repeat
          {
            rw wellTypedState at hws,
            cases hws,
            apply tyCtx_ih,
            exact hws_right,
            rw wellTypedPayee,
            rw wellTypedPayee at hwp,
            rw wellTypedPubKey at hwp,
            simp at hwp,
            cases hwp,
            exact hwp_right,
          },
          {
            sorry,
          },
          {
            rw wellTypedState at hws,
            apply tyCtx_ih,
            exact hws,
            rw wellTypedPayee,
            rw wellTypedPayee at hwp,
            rw wellTypedPubKey at hwp,
            simp at hwp,
            cases hwp,
            exact hwp_right,
          },
        },
      },
    },
    {
      rw evalFaustusPayee,
      induction payee,
      sorry,
      sorry,
    },

  end

noncomputable mutual def evalFaustusValue, evalFaustusObservation
with evalFaustusValue : Semantics.environment → faustusState → faustusValue → option int
  | env s (faustusValue.availableMoney faustusPubKey token) := match evalFaustusPubKey s faustusPubKey with
                                                               | none := none
                                                               | some accId := some (MList.findWithDefault 0 (accId, token) (s.acc))
                                                               end
  | env s (faustusValue.Constant integer) := some integer
  | env s (faustusValue.negValue val) := match evalFaustusValue env s val with 
                                         | none := none
                                         | some res := some (-res)
                                         end
  | env s (faustusValue.addValue lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                             | (none, _) := none 
                                             | (_, none) := none
                                             | (some res1, some res2) := some (res1 + res2)
                                             end
  | env s (faustusValue.subValue lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                             | (none, _) := none 
                                             | (_, none) := none
                                             | (some res1, some res2) := some (res1 - res2)
                                             end
  | env s (faustusValue.mulValue lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                             | (none, _) := none 
                                             | (_, none) := none
                                             | (some res1, some res2) := some (res1 * res2)
                                             end
  | env s (faustusValue.scale n d rhs) := match evalFaustusValue env s rhs with
                                          | none := none 
                                          | some res := (let nn := res*n in 
                                            let (q, r) := Semantics.quotRem nn d in 
                                              some (if abs r * 2 < abs d then q else q + Semantics.signum nn * Semantics.signum d))
                                          end
  | env s (faustusValue.choiceValue choId) := some (MList.findWithDefault 0 choId (s.choices))
  | env s (faustusValue.slotIntervalStart) := some ((env.slotInterval).fst)
  | env s (faustusValue.slotIntervalEnd) := some ((env.slotInterval).snd)
  | env s (faustusValue.useValue valId) := MList.lookup valId (s.boundValues)
  | env s (faustusValue.cond cond thn els) := match (evalFaustusObservation env s cond, evalFaustusValue env s thn, evalFaustusValue env s els) with
                                              | (none, _, _) := none
                                              | (_, none, _) := none
                                              | (_, _, none) := none
                                              --initially if condRes, trying if condRes=true
                                              | (some condRes, some thenRes, some elseRes) := some (if (condRes=true) then thenRes else elseRes)
                                              end
with evalFaustusObservation : Semantics.environment → faustusState → faustusObservation → option Prop
  | env s (faustusObservation.andObs lhs rhs) := match (evalFaustusObservation env s lhs, evalFaustusObservation env s rhs) with 
                                                 | (none, _) := none
                                                 | (_, none) := none
                                                 | (some res1, some res2) := some (res1 ∧ res2)
                                                 end
  | env s (faustusObservation.orObs lhs rhs) := match (evalFaustusObservation env s lhs, evalFaustusObservation env s rhs) with 
                                                 | (none, _) := none
                                                 | (_, none) := none
                                                 | (some res1, some res2) := some (res1 ∨ res2)
                                                 end
  | env s (faustusObservation.notObs subObs) := match evalFaustusObservation env s subObs with 
                                                | none := none
                                                | some res := some (¬res)
                                                end
  | env s (faustusObservation.choseSomething choId) := some (MList.member choId (s.choices))
  | env s (faustusObservation.valueGE lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                                  | (none, _) := none
                                                  | (_, none) := none
                                                  | (some res1, some res2) := some (res1 ≥ res2)
                                                  end
  | env s (faustusObservation.valueGT lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                                  | (none, _) := none
                                                  | (_, none) := none
                                                  | (some res1, some res2) := some (res1 > res2)
                                                  end
  | env s (faustusObservation.valueLT lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                                  | (none, _) := none
                                                  | (_, none) := none
                                                  | (some res1, some res2) := some (res1 < res2)
                                                  end
  | env s (faustusObservation.valueLE lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                                  | (none, _) := none
                                                  | (_, none) := none
                                                  | (some res1, some res2) := some (res1 ≤ res2)
                                                  end
  | env s (faustusObservation.valueEQ lhs rhs) := match (evalFaustusValue env s lhs, evalFaustusValue env s rhs) with
                                                  | (none, _) := none
                                                  | (_, none) := none
                                                  | (some res1, some res2) := some (res1 = res2)
                                                  end
  | env s (faustusObservation.useObservation obsId) := match MList.lookup obsId (s.boundValues) with
                                                       | none := none 
                                                       | some v := some (v ≠ 0)
                                                       end
  | env s faustusObservation.trueObs := some true
  | env s faustusObservation.falseObs := some false
#check evalFaustusValue
#check evalFaustusObservation

--Need to look more into faustusValues
lemma evalFaustusValue_InsertDifferentValueStillExecutes {s otherState: faustusState}{e : Semantics.environment}{val : faustusValue}{obs : faustusObservation}:
  (∀ (evaluatedValue : int), ∃ (otherEvaluatedValue : int), 
  (∀ (valId : identifier), MList.member valId (s.boundValues) = MList.member valId (otherState.boundValues)) → 
  (∀ (pubKeyId : identifier), MList.member pubKeyId (s.boundPubKeys) = MList.member pubKeyId (otherState.boundPubKeys)) →
  evalFaustusValue e s val = some evaluatedValue → evalFaustusValue e otherState val = some otherEvaluatedValue) ↔
  (∀ (evaluatedObservation : Prop), ∃ (otherEvaluatedObservation : Prop), 
  (∀ (valId : identifier), MList.member valId (s.boundValues) = MList.member valId (otherState.boundValues)) →
  (∀ (pubKeyId : identifier), MList.member pubKeyId (s.boundPubKeys) = MList.member pubKeyId (otherState.boundPubKeys)) →
  evalFaustusObservation e s obs = some evaluatedObservation →
  evalFaustusObservation e otherState obs = some otherEvaluatedObservation) :=
  begin
    split,
    {
      intros hvs eo,
      split,
      {
        intros hvs hpks he,
        induction obs,
        {
          rw evalFaustusObservation,
          rw evalFaustusObservation at he,
          cases (evalFaustusObservation e s obs_fo1),
          {
            cases (evalFaustusObservation e s obs_fo2),
            {
              rw evalFaustusObservation._match_1 at he,
              simp at he,
              exfalso,
              exact he,
            },  
            {
              rw evalFaustusObservation._match_1 at he,
              simp at he,
              exfalso,
              exact he,
            },
          },
          {
            cases (evalFaustusObservation e s obs_fo2),
            {
              rw evalFaustusObservation._match_1 at he,
              simp at he,
              exfalso,
              exact he,
            },
            {
              rw evalFaustusObservation._match_1 at he,
              cases (evalFaustusObservation e otherState obs_fo1),
              {
                cases (evalFaustusObservation e otherState obs_fo2),
                {
                  rw evalFaustusObservation._match_1,
                  simp,
                  simp at obs_x,
                  simp at obs_x_1,
                  simp at he,
                  apply obs_x,
                  sorry,
                },
                {
                  rw evalFaustusObservation._match_1,
                  simp,
                  simp at obs_x,
                  apply obs_x,
                  simp at he,
                  sorry,
                },
              },
              {
                sorry,
              },
            },
          },
        },
        {
          rw evalFaustusObservation,
          rw evalFaustusObservation at he,
          cases (evalFaustusObservation e s obs_fo1),
          {
            cases (evalFaustusObservation e s obs_fo2),
            repeat
            {
              rw evalFaustusObservation._match_2 at he,
              exfalso,
              simp at he,
              exact he,
            },
          },
          {
            cases (evalFaustusObservation e s obs_fo2),
            repeat
            {
              rw evalFaustusObservation._match_2 at he,
              simp at he,
              exfalso,
              exact he,
            },
            rw evalFaustusObservation._match_2 at he,
            simp at he,
            cases (evalFaustusObservation e otherState obs_fo1),
            {
              sorry,
            },
            sorry,
          },
        },
        repeat
        {
          sorry,
        },
      },
      --exists
      {
        sorry,
      }
    },
    -- iff part
    {
      intros hos ev,
      split,
      {
        intros hvs hpks he,
        induction val,
        {
          rw evalFaustusValue,
          rw evalFaustusValue at he,
          cases (evalFaustusPubKey otherState val_fpk),
          {
            rw evalFaustusValue._match_1,
            cases (evalFaustusPubKey s val_fpk),
            {
              rw evalFaustusValue._match_1 at he,
              simp,
              simp at he,
              exact he,
            },
            {
              rw evalFaustusValue._match_1 at he,
              simp at he,
              rw MList.findWithDefault at he,
              sorry,
            },
          },
          sorry,
        },
        repeat
        {
          sorry,
        },
      },
      --exists
      {
        sorry,
      },
    },
  end
--Might work with previous lemma, will come back to it
lemma wellTypedValueObservationExecutes {tyCtx : typeContext} {s : faustusState}{env : Semantics.environment} :
  (∀ (val : faustusValue), wellTypedValue tyCtx val → 
  wellTypedState tyCtx s → 
  evalFaustusValue env s val ≠ none) ↔
  (∀ (obs : faustusObservation), wellTypedObservation tyCtx obs → 
  wellTypedState tyCtx s → 
  evalFaustusObservation env s obs ≠ none) :=
  begin
    split,
    --val to obs
    {
      intros himp obs hwo hws,
      sorry,
    },
    --obs to val
    {
      intros himp val hwv hws,
      sorry,
    },
  end                     

--definitions might need revisited as meta instead of noncomputable
noncomputable def evalFaustusArguments : Semantics.environment → faustusState → list (faustusParameter) → list (faustusArgument) → option (faustusState) 
  |  e s list.nil list.nil := some s
  |  e s ((faustusParameter.valueParameter vid)::restParams) ((faustusArgument.valueArgument val)::restArgs) :=
    match evalFaustusValue e s val with
    | none := none
    | some res := evalFaustusArguments e (faustusState.mk (s.acc)(s.choices)(MList.insert_map vid res (s.boundValues))(s.boundPubKeys)(s.minSlot)) restParams restArgs
    end
  | e s ((faustusParameter.observationParameter obsId)::restParams) ((faustusArgument.observationArgument obs)::restArgs) :=
    match evalFaustusObservation e s obs with
    | none := none
    | some res := evalFaustusArguments e (faustusState.mk (s.acc)(s.choices)(MList.insert_map obsId (if (res=true) then 1 else 0) (s.boundValues))(s.boundPubKeys)(s.minSlot)) restParams restArgs
    end
  | e s ((faustusParameter.pubKeyParameter pkId)::restParams) ((faustusArgument.pubKeyArgument pk)::restArgs) :=
    match evalFaustusPubKey s pk with
    | none := none
    | some res := evalFaustusArguments e (faustusState.mk (s.acc)(s.choices)(MList.insert_map pkId res (s.boundValues))(s.boundPubKeys)(s.minSlot)) restParams restArgs
    end
  | e s (firstParam :: restParams) (firstArg::restArgs) := none
  | e s params list.nil := none
  | e s list.nil args := none
#check evalFaustusArguments

lemma evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys {s : faustusState}{e : Semantics.environment}{params : list faustusParameter}{args : list faustusArgument}:
  ∀ (evaluatedState otherState otherEvaluatedState: faustusState), evalFaustusArguments e s params args = some evaluatedState →
  evalFaustusArguments e (faustusState.mk (otherState.acc)(otherState.choices)(otherState.boundValues)(s.boundPubKeys)(otherState.minSlot)) params args = some otherEvaluatedState →
  evaluatedState.boundPubKeys = otherEvaluatedState.boundPubKeys :=
  begin
    intros evaluatedState otherState otherEvaluatedState hstate hostate,
    induction params,
    {
      cases args,
      {
        cases hostate,
        cases hstate,
        simp,
      },
      {
        cases hostate,
      },
    },
    {
      cases params_hd,
      induction args,
      {
        repeat
        {
          cases hstate,
        },
      },
      {
        cases args_hd,
        {
          
          sorry,
        },
        {
          sorry,
        },
        {
          sorry,
        },
      },
      {
        sorry,
      },
      {
        sorry,
      },
    },
  end

#check list.sorted



lemma evalFaustusPubKey_SamePubKeyDomainImpliesExecution:
  "∀evaluatedPubKey. ∃otherEvaluatedPubKey.
    (∀pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) ⟶
    evalFaustusPubKey s pk = Some evaluatedPubKey ⟶
    evalFaustusPubKey otherState pk = Some otherEvaluatedPubKey"
    apply auto
    subgoal premises ps proof (cases pk)
      case (ConstantPubKey x1)
      then show ?thesis by auto
    next
      case (UsePubKey x2)
      then show ?thesis using ps by auto
    qed
    done

lemma wellTypedPubKeyEvaluates:
  "wellTypedPubKey tyCtx pk ⟹
    wellTypedState tyCtx s ⟹
    evalFaustusPubKey s pk ≠ None"
  proof (induction rule: wellTypedPubKey.induct)
    case (1 tyCtx pk)
    then show ?case by auto
  next
    case (2 pkId)
    then show ?case by auto
  next
    case (3 boundPkId ty rest pkId)
    then show ?case apply auto
      by (cases ty, auto)
  qed

lemma wellTypedPayeeEvaluates:
  "wellTypedPayee tyCtx payee ⟹
  wellTypedState tyCtx s ⟹
  evalFaustusPayee s payee ≠ None"
  by (metis (no_types, lifting) evalFaustusPayee.simps option.case_eq_if option.simps(3) wellTypedPayee.elims(2) wellTypedPubKeyEvaluates)


lemma evalFaustusValue_InsertDifferentValueStillExecutes:
  "∀evaluatedValue. ∃otherEvaluatedValue.
    (∀valId . member valId (boundValues s) = member valId (boundValues otherState)) ⟶
    (∀pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) ⟶
    evalFaustusValue e s val = Some evaluatedValue ⟶
    evalFaustusValue e otherState val = Some otherEvaluatedValue"
  "∀evaluatedObservation. ∃otherEvaluatedObservation.
    (∀valId . member valId (boundValues s) = member valId (boundValues otherState)) ⟶
    (∀pubKeyId . member pubKeyId (boundPubKeys s) = member pubKeyId (boundPubKeys otherState)) ⟶
    evalFaustusObservation e s obs = Some evaluatedObservation ⟶
    evalFaustusObservation e otherState obs = Some otherEvaluatedObservation"
  proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
    case (1 env state faustusPubKey token)
    then show ?case apply auto
      apply (cases faustusPubKey, auto)
      by (split option.split, auto)
  next
  case (2 env state integer)
  then show ?case by auto
  next
  case (3 env state val)
  then show ?case by auto
  next
  case (4 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (5 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (6 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (7 env state n d rhs)
  then show ?case apply auto
    apply (split option.split_asm, auto)
    by metis
  next
  case (8 env state choId)
  then show ?case by auto
  next
  case (9 env state)
  then show ?case by auto
  next
  case (10 env state)
  then show ?case by auto
  next
  case (11 env state valId)
    then show ?case by auto
  next
  case (12 env state cond thn els)
  then show ?case apply auto
        apply (metis (no_types, lifting) option.case_eq_if)
       apply (metis (no_types, lifting) option.case_eq_if)
      apply (metis (no_types, lifting) option.case_eq_if)
     apply (metis (no_types, lifting) option.case_eq_if)
    by (metis (no_types, lifting) option.case_eq_if)
  next
  case (13 env state lhs rhs)
  then show ?case apply auto
    by (metis (no_types, lifting) option.case_eq_if)
  next
  case (14 env state lhs rhs)
  then show ?case apply auto
    by (metis (no_types, lifting) option.case_eq_if)
  next
  case (15 env state subObs)
  then show ?case by auto
  next
  case (16 env state choId)
  then show ?case by auto
  next
  case (17 env state lhs rhs)
  then show ?case apply (simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    apply (split option.split, simp)
    by auto
  next
  case (18 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (19 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (20 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (21 env state lhs rhs)
  then show ?case apply auto
    apply (split option.split, auto)
    by (split option.split_asm, auto)
  next
  case (22 env state obsId)
  then show ?case apply auto
    by (split option.split, auto)
  next
  case (23 env state)
  then show ?case by auto
  next
  case (24 env state)
  then show ?case by auto
  qed

lemma wellTypedValueObservationExecutes:
  "wellTypedValue tyCtx val ⟹
    wellTypedState tyCtx s ⟹
    evalFaustusValue env s val ≠ None"
  "wellTypedObservation tyCtx obs ⟹
    wellTypedState tyCtx s ⟹
    evalFaustusObservation env s obs ≠ None"
  proof (induction rule: wellTypedValue_wellTypedObservation.induct)
  case (1 tyCtx pk token)
  then show ?case by (simp add: option.case_eq_if wellTypedPubKeyEvaluates)
  next
  case (2 tyCtx integer)
  then show ?case by simp
  next
  case (3 tyCtx val)
  then show ?case by auto
  next
  case (4 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (5 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (6 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (7 tyCtx n d rhs)
  then show ?case apply auto
    by metis
  next
  case (8 tyCtx choId)
  then show ?case by auto
  next
  case (9 tyCtx)
  then show ?case by auto
  next
  case (10 tyCtx)
  then show ?case by auto
  next
  case (11 valId)
  then show ?case by auto
  next
  case (12 boundValId ty rest valId)
  then show ?case by (cases ty, auto)
  next
  case (13 tyCtx cond thn els)
  then show ?case by auto
  next
  case (14 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (15 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (16 tyCtx subObs)
  then show ?case by auto
  next
  case (17 tyCtx choId)
  then show ?case by auto
  next
  case (18 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (19 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (20 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (21 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (22 tyCtx lhs rhs)
  then show ?case by auto
  next
  case (23 obsId)
  then show ?case by auto
  next
  case (24 boundObsId ty rest obsId)
  then show ?case by (cases ty, auto)
  next
  case (25 tyCtx)
  then show ?case by auto
  next
  case (26 tyCtx)
  then show ?case by auto
  qed

(* Needs to pass modified state to maintain parity with compiled Marlowe contract. Evaluate sequentially left to right with previous results available to next arguments. *)


lemma evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys:
"∀evaluatedState otherState otherEvaluatedState .
  evalFaustusArguments e s params args = Some evaluatedState ⟶
  evalFaustusArguments e (otherState⦇boundPubKeys := boundPubKeys s⦈) params args = Some otherEvaluatedState ⟶
  boundPubKeys evaluatedState = boundPubKeys otherEvaluatedState"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusValue e s val", auto)
    apply (split option.split_asm, auto)
    by (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    apply (split option.split_asm, simp)
    apply (split option.split_asm, simp)
    apply (cases "the (evalFaustusObservation e s obs)", auto)
     apply (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
    by (metis FaustusState.surjective FaustusState.update_convs(3) FaustusState.update_convs(4))
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply (cases pk)
     apply simp
    apply auto
    apply (split option.split_asm, simp)
    apply (split option.split_asm)
     apply blast
    by (metis (no_types) FaustusState.surjective FaustusState.update_convs(4) option.inject)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsExecute:
"wellTypedArguments tyCtx params args ⟹
  wellTypedState tyCtx s ⟹
  evalFaustusArguments e s params args ≠ None"
proof (induction arbitrary: s rule: wellTypedArguments.induct)
  case (1 tyCtx)
  then show ?case by auto
next
  case (2 tyCtx v va)
  then show ?case by auto
next
  case (3 tyCtx v va)
  then show ?case by auto
next
  case (4 tyCtx firstParam restParams firstArg restArgs)
  then show ?case apply auto
    subgoal premises ps proof (cases firstParam firstArg rule: FaustusParameter.exhaust[case_product FaustusArgument.exhaust])
      case (ValueParameter_ValueArgument valId val)
      then show ?thesis using ps apply auto
        by (cases "evalFaustusValue e s val", auto simp add: wellTypedValueObservationExecutes(1) insertValueWellTypedStateIsWellTyped ps)
    next
      case (ValueParameter_ObservationArgument x1 x2)
      then show ?thesis using ps by auto
    next
      case (ValueParameter_PubKeyArgument x1 x3)
      then show ?thesis using ps by auto
    next
      case (ObservationParameter_ValueArgument x2 x1)
      then show ?thesis using ps by auto
    next
      case (ObservationParameter_ObservationArgument obsId obs)
      then show ?thesis using ps apply auto
        apply (cases "evalFaustusObservation e s obs", auto)
        using wellTypedValueObservationExecutes(2) insertValueWellTypedStateIsWellTyped by auto
    next
      case (ObservationParameter_PubKeyArgument x2 x3)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_ValueArgument x3 x1)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_ObservationArgument x3 x2)
      then show ?thesis using ps by auto
    next
      case (PubKeyParameter_PubKeyArgument pkId pk)
      then show ?thesis using ps apply auto
        apply (cases "evalFaustusPubKey s pk", auto)
        using wellTypedPubKeyEvaluates insertPubKeyWellTypedStateIsWellTyped by auto
    qed
    done
qed

lemma wellTypedArgumentsOnlyAddIdsToState:
"(wellTypedState tyCtx s ⟶
  wellTypedArguments tyCtx params args ⟶ 
  Some newS = evalFaustusArguments e s params args ⟶
  wellTypedState tyCtx newS)"
proof (induction arbitrary: newS rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusValue e s val", auto)
    by (cases "evalFaustusArguments e s restParams restArgs", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    by (cases "evalFaustusObservation e s obs", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
    by (cases "evalFaustusPubKey s pk", auto simp add: insertPubKeyWellTypedStateIsWellTyped)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsLookupPreviousBoundValueIds:
"(∀newS vid boundVal. ∃otherBoundVal. Some newS = evalFaustusArguments e s params args ⟶
  MList.lookup vid (boundValues s) = Some boundVal ⟶
  MList.lookup vid (boundValues newS) = Some otherBoundVal)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply (cases "evalFaustusValue e s val", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusObservation e s obs", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case by (cases "evalFaustusPubKey s pk", auto)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsLookupPreviousBoundPubKeyIds:
"(∀newS pkId boundVal. ∃otherBoundVal. Some newS = evalFaustusArguments e s params args ⟶
  MList.lookup pkId (boundPubKeys s) = Some boundVal ⟶
  MList.lookup pkId (boundPubKeys newS) = Some otherBoundVal)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case by (cases "evalFaustusValue e s val", auto)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case by (cases "evalFaustusObservation e s obs", auto)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply (cases "evalFaustusPubKey s pk", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed


lemma wellTypedArgumentsAddParamsToState:
"(∀ newS . Some newS = evalFaustusArguments e s params args ⟶ wellTypedState (paramsToTypeContext params) newS)"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusValue e s val", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundValueIds apply fastforce
    by (cases "evalFaustusValue e s val", auto)
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusObservation e s obs", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundValueIds apply fastforce
    by (cases "evalFaustusObservation e s obs", auto)
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
     apply (cases "evalFaustusPubKey s pk", auto)
    using MList.insert_lookup_Some wellTypedArgumentsLookupPreviousBoundPubKeyIds apply fastforce
    by (cases "evalFaustusPubKey s pk", auto)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

lemma wellTypedArgumentsResultWellTypedState:
"wellTypedArguments restTyCtx params args ⟹
    wellTypedState restTyCtx s ⟹
    Some newS = evalFaustusArguments e s params args ⟹ wellTypedState (paramsToTypeContext params @ restTyCtx) newS"
  by (metis wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedStateCombineTypeContexts)

fun lookupContractIdAbsInformation :: "FaustusContext ⇒ Identifier ⇒ (FaustusParameter list × FaustusContract × FaustusContext) option" where
"lookupContractIdAbsInformation [] cid = None" |
"lookupContractIdAbsInformation (ContractAbstraction (boundCid, params, bodyCont)#restAbs) cid = (if boundCid = cid
  then Some (params, bodyCont, restAbs)
  else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ValueAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (ObservationAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)" |
"lookupContractIdAbsInformation (PubKeyAbstraction i#restAbs) cid = (if i = cid then None else lookupContractIdAbsInformation restAbs cid)"

lemma lookupContractTypeWellTypedContext:
"(∀cid params bodyCont restCtx. ∃restTy . wellTypedFaustusContext tyCtx ctx ⟶
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) ⟶
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy))"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractAbstractionWellTypedContext:
"(∀cid params restTy. ∃bodyCont restCtx . wellTypedFaustusContext tyCtx ctx ⟶
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) ⟶
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx))"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractRestIsWellTyped:
"(∀cid params restTy bodyCont restCtx . wellTypedFaustusContext tyCtx ctx ⟶
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) ⟶
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) ⟶
  wellTypedFaustusContext restTy restCtx)"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractFoundContractIsWellTyped:
"(∀cid params restTy bodyCont restCtx . wellTypedFaustusContext tyCtx ctx ⟶
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) ⟶
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restCtx) ⟶
  wellTypedContract (paramsToTypeContext params @ restTy) bodyCont)"
  by (induction tyCtx ctx rule: wellTypedFaustusContext.induct, auto)

lemma lookupContractStateStaysWellTyped:
"(∀cid params restTy restCtx .
  wellTypedState tyCtx s ⟶
  lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy) ⟶
  wellTypedState restTy s)"
  by (induction tyCtx s rule: wellTypedState.induct, auto)

type_synonym FaustusConfiguration = "FaustusContract * FaustusContext * FaustusState * Environment * (ReduceWarning list) * (Payment list)"
/-
inductive
  small_step_reduce :: "FaustusConfiguration ⇒ FaustusConfiguration ⇒ bool" (infix "→f" 55)
where
CloseRefund:  "refundOne (accounts s) = Some ((party, token, money), newAccount) ⟹ 
  (Close, ctx, s, env, warns, payments) →f 
  (Close, ctx, (s⦇accounts := newAccount⦈), env, warns @ [ReduceNoWarning], payments @ [Payment party token money])" |
PayNonPositive: "⟦evalFaustusValue env s val = Some res; res ≤ 0; Some fromAcc = (evalFaustusPubKey s accId); Some toPayee = (evalFaustusPayee s payee)⟧ ⟹
  (Pay accId payee token val cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s, env, warns @ [ReduceNonPositivePay fromAcc toPayee token res], payments)" |
PayPositivePartialWithPayment: "⟦evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment⟧ ⟹
  (Pay accId payee token val cont, ctx, s, env, warns, payments) →f
  ((cont, ctx, s⦇accounts := finalAccs⦈, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments @ [somePayment]))" |
PayPositivePartialWithoutPayment: "⟦evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res > moneyInAccount fromAcc token (accounts s);
  updateMoneyInAccount fromAcc token 0 (accounts s) = newAccs;
  moneyInAccount fromAcc token (accounts s) = moneyToPay;
  giveMoney toPayee token (moneyToPay) newAccs = (payment, finalAccs);
  payment = ReduceNoPayment⟧ ⟹
  (Pay accId payee token val cont, ctx, s, env, warns, payments) →f
  ((cont, ctx, s⦇accounts := finalAccs⦈, env, warns @ [ReducePartialPay fromAcc toPayee token moneyToPay res], payments))" |
PayPositiveFullWithPayment: "⟦evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res ≤ moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceWithPayment somePayment⟧ ⟹
  (Pay accId payee token val cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s⦇accounts := finalAccs⦈, env, warns @ [ReduceNoWarning], payments @ [somePayment])" |
PayPositiveFullWithoutPayment: "⟦evalFaustusValue env s val = Some res;
  res > 0;
  Some fromAcc = (evalFaustusPubKey s accId);
  Some toPayee = (evalFaustusPayee s payee);
  res ≤ moneyInAccount fromAcc token (accounts s);
  moneyInAccount fromAcc token (accounts s) = moneyInAcc;
  moneyInAcc - res = newBalance;
  updateMoneyInAccount fromAcc token newBalance (accounts s) = newAccs;
  giveMoney toPayee token res newAccs = (payment, finalAccs);
  payment = ReduceNoPayment⟧ ⟹
  (Pay accId payee token val cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s⦇accounts := finalAccs⦈, env, warns @ [ReduceNoWarning], payments)" |
IfTrue: "evalFaustusObservation env s obs = Some True ⟹ 
  (If obs cont1 cont2, ctx, s, env, warns, payments) →f
  (cont1, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
IfFalse: "evalFaustusObservation env s obs = Some False ⟹ 
  (If obs cont1 cont2, ctx, s, env, warns, payments) →f
  (cont2, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
WhenTimeout: "⟦slotInterval env = (startSlot, endSlot);
  endSlot ≥ timeout;
  startSlot ≥ timeout⟧ ⟹
  (When cases timeout cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
LetShadow: "⟦lookup valId (boundValues s) = Some oldVal; evalFaustusValue env s val = Some res⟧ ⟹
  (Let valId val cont, ctx, s, env, warns, payments) →f
  (cont, ValueAbstraction valId#ctx, s⦇ boundValues := MList.insert valId res (boundValues s)⦈, env, warns @ [ReduceShadowing valId oldVal res], payments)" |
LetNoShadow: "⟦lookup valId (boundValues s) = None; evalFaustusValue env s val = Some res⟧ ⟹
  (Let valId val cont, ctx, s, env, warns, payments) →f
  (cont, ValueAbstraction valId#ctx, s⦇ boundValues := MList.insert valId res (boundValues s)⦈, env, warns @ [ReduceNoWarning], payments)" |
LetObservation: "⟦evalFaustusObservation env s obs = Some res⟧ ⟹
  (LetObservation obsId obs cont, ctx, s, env, warns, payments) →f
  (cont, ObservationAbstraction obsId#ctx, s⦇boundValues := MList.insert obsId (if res then 1 else 0) (boundValues s)⦈, env, warns @ [ReduceNoWarning], payments)" |
LetPubKey: "⟦evalFaustusPubKey s pk = Some res⟧ ⟹
  (LetPubKey pkId pk cont, ctx, s, env, warns, payments) →f
  (cont, PubKeyAbstraction pkId#ctx, s⦇ boundPubKeys := MList.insert pkId res (boundPubKeys s)⦈, env, warns @ [ReduceNoWarning], payments)" |
AssertTrue: "evalFaustusObservation env s obs = Some True ⟹
  (Assert obs cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s, env, warns @ [ReduceNoWarning], payments)" |
AssertFalse: "evalFaustusObservation env s obs = Some False ⟹
  (Assert obs cont, ctx, s, env, warns, payments) →f
  (cont, ctx, s, env, warns @ [ReduceAssertionFailed], payments)" |
LetC: "(LetC cid params boundCon cont, ctx, s, env, warns, payments) →f
  (cont, ContractAbstraction (cid, params, boundCon)#ctx, s, env, warns @ [ReduceNoWarning], payments)" |
UseCFound: "⟦lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx);
  Some newState = evalFaustusArguments env s params args⟧ ⟹
  (UseC cid args, ctx, s, env, warns, payments) →f
  (bodyCont, (paramsToFaustusContext params)@innerCtx, newState, env, warns @ [ReduceNoWarning], payments)"

abbreviation
  small_step_reduces :: "FaustusConfiguration ⇒ FaustusConfiguration ⇒ bool" (infix "→f*" 55)
where "x →f* y == star small_step_reduce x y"

thm small_step_reduce.induct
lemmas small_step_reduce_induct = small_step_reduce.induct[split_format(complete)]
declare small_step_reduce.intros[simp,intro]

inductive_cases CloseE[elim!]: "(Close, ctx, s, env, warns, payments) →f ct"
thm CloseE
inductive_cases PayE[elim!]: "(Pay accId payee token val cont, ctx, s, env, warns, payments) →f ct"
thm PayE
inductive_cases IfE[elim!]: "(If obs cont1 cont2, ctx, s, env, warns, payments) →f ct"
thm IfE
inductive_cases WhenE[elim!]: "(When cases timeout cont, ctx, s, env, warns, payments) →f ct"
thm WhenE
inductive_cases LetE[elim!]: "(Let valId val cont, ctx, s, env, warns, payments) →f ct"
thm LetE
inductive_cases LetObservationE[elim!]: "(LetObservation obsId obs cont, ctx, s, env, warns, payments) →f ct"
thm LetObservationE
inductive_cases LetPubKeyE[elim!]: "(LetPubKey pkId pk cont, ctx, s, env, warns, payments) →f ct"
thm LetPubKeyE
inductive_cases AssertE[elim!]: "(Assert obs cont, ctx, s, env, warns, payments) →f ct"
thm AssertE
inductive_cases LetCE[elim!]: "(LetC cid boundCon params cont, ctx, s, env, warns, payments) →f ct"
thm LetCE
inductive_cases UseCE[elim!]: "(UseC cid args, ctx, s, env, warns, payments) →f ct"
thm UseCE

lemma deterministic:
  "cs →f cs' ⟹ cs →f cs'' ⟹ cs'' = cs'"
  apply(induction arbitrary: cs'' rule: small_step_reduce.induct)
                   apply auto
                      apply (metis option.inject)
                      apply (metis option.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis ReduceEffect.inject option.inject prod.inject)
                      apply (metis ReduceEffect.distinct(1) option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.inject prod.inject)
                      apply (metis option.inject prod.inject)
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis leD option.simps(5))
                      apply (metis ReduceEffect.distinct(1) option.inject prod.inject)
                     apply (metis leD option.simps(5))
                    apply (metis leD option.simps(5))
                   apply (metis leD option.simps(5))
                  apply (metis option.inject prod.inject)
                 apply (metis option.inject prod.inject)
                apply (metis leD option.simps(5))
               apply (metis leD option.simps(5))
              apply (metis leD option.simps(5))
             apply (metis leD option.simps(5))
            apply (metis ReduceEffect.inject option.inject prod.inject)
           apply (metis Pair_inject ReduceEffect.distinct(1) option.inject)
          apply (metis leD option.simps(5))
         apply (metis leD option.simps(5))
        apply (metis option.inject prod.inject)
       apply (metis option.inject prod.inject)
      apply (metis leD option.simps(5))
     apply (metis leD option.simps(5))
    apply (metis leD option.simps(5))
   apply (metis Pair_inject ReduceEffect.distinct(1) option.inject)
  by (metis option.inject)

lemma smallStepWarningsAreArbitrary:
"(c, bc, s, e, w, p) →f (c', bc', s', e', w', p') ⟹
  (∀w'' . ∃w''' . (c, bc, s, e, w'', p) →f (c', bc', s', e', w''', p'))"
proof (induction c bc s e w p c' bc' s' e' w' p' rule: small_step_reduce_induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  then show ?case by auto
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  then show ?case by auto
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  then show ?case by fastforce
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  then show ?case by fastforce
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  then show ?case by auto
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  then show ?case by auto
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  then show ?case by auto
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  then show ?case by auto
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  then show ?case by auto
next
  case (LetObservation env s obs res obsId cont ctx warns payments)
  then show ?case by fastforce
next
  case (LetPubKey s pk res pkId cont ctx env warns payments)
  then show ?case by auto
next
  case (AssertTrue env s obs cont ctx warns payments)
  then show ?case by auto
next
  case (AssertFalse env s obs cont ctx warns payments)
  then show ?case by auto
next
  case (LetC cid params boundCon cont ctx s env warns payments)
  then show ?case by auto
next
  case (UseCFound ctx cid params bodyCont innerCtx newState env s args warns payments)
  then show ?case by auto
qed

lemma smallStepStarWarningsAreArbitrary:
  "(c, bc, s, e, w, p) →f* (c', bc', s', e', w'', p') ⟹
    (∀w'' . ∃w''' . (c, bc, s, e, w'', p) →f* (c', bc', s', e', w''', p'))"
  apply (induction rule: star.induct[of "small_step_reduce", split_format(complete)], auto)
  by (meson smallStepWarningsAreArbitrary star.step)

definition "final cs ⟷ ¬(∃cs'. cs →f cs')"

lemma wellTypedPayContinues:
  assumes "wellTypedContract tyCtx (Pay accId payee token amt cont) ∧
  wellTypedFaustusContext tyCtx ctx ∧
  wellTypedState tyCtx s"
  shows "(∃ newS newW newP .(Pay accId payee token amt cont, ctx, s, e, w, p) →f (cont, ctx, newS, e, newW, newP))"
proof -
  obtain res where "evalFaustusValue e s amt = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationExecutes(1))
  show ?thesis proof (cases "res ≤ 0")
    case True
    then show ?thesis using assms apply auto
      using ‹evalFaustusValue e s amt = Some res› wellTypedPayeeEvaluates wellTypedPubKeyEvaluates by fastforce
  next
    case False
    then show ?thesis proof -
      obtain fromAcc where "evalFaustusPubKey s accId = Some fromAcc" using assms apply auto
        by (meson option.exhaust_sel wellTypedPubKeyEvaluates)
      moreover obtain toPayee where "evalFaustusPayee s payee = Some toPayee" using assms apply auto
        by (meson option.exhaust wellTypedPayeeEvaluates)
      ultimately show ?thesis proof (cases "res > moneyInAccount fromAcc token (accounts s)")
        case True
        then show ?thesis using assms apply auto
          by (metis FaustusSemantics.small_step_reduce.PayNonPositive FaustusSemantics.small_step_reduce.PayPositiveFullWithPayment FaustusSemantics.small_step_reduce.PayPositiveFullWithoutPayment FaustusSemantics.small_step_reduce.PayPositivePartialWithPayment FaustusSemantics.small_step_reduce.PayPositivePartialWithoutPayment ‹⋀thesis. (⋀res. evalFaustusValue e s amt = Some res ⟹ thesis) ⟹ thesis› ‹evalFaustusPayee s payee = Some toPayee› ‹evalFaustusPubKey s accId = Some fromAcc› giveMoney.elims not_less)
      next
        case False
        then show ?thesis using assms apply auto
          by (metis False FaustusSemantics.small_step_reduce.PayNonPositive FaustusSemantics.small_step_reduce.PayPositiveFullWithPayment FaustusSemantics.small_step_reduce.PayPositiveFullWithoutPayment ‹evalFaustusPayee s payee = Some toPayee› ‹evalFaustusPubKey s accId = Some fromAcc› ‹evalFaustusValue e s amt = Some res› giveMoney.elims not_less)
      qed
    qed
  qed
qed

lemma wellTypedAssertContinues:
  assumes "wellTypedContract tyCtx (Assert obs cont) ∧
  wellTypedFaustusContext tyCtx ctx ∧
  wellTypedState tyCtx s"
  shows "(∃ newW newP .(Assert obs cont, ctx, s, e, w, p) →f (cont, ctx, s, e, newW, newP))"
proof -
  obtain res where "evalFaustusObservation e s obs = Some res" using assms apply auto
    by (meson option.exhaust wellTypedValueObservationExecutes(2))
  show ?thesis proof (cases res)
    case True
    then show ?thesis using assms ‹evalFaustusObservation e s obs = Some res› by force
  next
    case False
    then show ?thesis using assms ‹evalFaustusObservation e s obs = Some res› by force
  qed
qed

lemma wellTypedUseCContinues:
  assumes "wellTypedContract tyCtx (UseC cid args) ∧
  wellTypedFaustusContext tyCtx ctx ∧
  wellTypedState tyCtx s"
  shows "(∃ newCont newCtx newS newW .(UseC cid args, ctx, s, e, w, p) →f (newCont, newCtx, newS, e, newW, p))"
  using assms apply (cases "lookupContractIdParamTypeInformation tyCtx cid", auto)
  by (metis UseCFound lookupContractAbstractionWellTypedContext option.exhaust_sel wellTypedArgumentsExecute)

(* A well typed final configuration only has Close or When contracts. *)
lemma finalD: "(final (contract, ctx, s, e, w, p) ∧ (wellTypedContract tyCtx contract) ∧ wellTypedFaustusContext tyCtx ctx ∧ wellTypedState tyCtx s) ⟹ contract = Close ∨ (∃ cases timeout continue . contract = When cases timeout continue)"
  apply (auto simp add: final_def)
  apply (cases contract, auto)
         apply (meson wellTypedContract.simps(2) wellTypedPayContinues)
        apply (smt FaustusSemantics.small_step_reduce.IfFalse FaustusSemantics.small_step_reduce.intros(7) option.exhaust_sel wellTypedValueObservationExecutes(2))
       apply (metis FaustusSemantics.small_step_reduce.LetNoShadow FaustusSemantics.small_step_reduce.LetShadow not_Some_eq wellTypedValueObservationExecutes(1))
      apply (meson FaustusSemantics.small_step_reduce.intros(12) option.exhaust_sel wellTypedValueObservationExecutes(2))
     apply (meson FaustusSemantics.small_step_reduce.intros(13) option.exhaust_sel wellTypedPubKeyEvaluates)
    apply (meson wellTypedAssertContinues wellTypedContract.simps)
   apply blast
  by (metis wellTypedContract.simps(10) wellTypedUseCContinues)

lemma insertGivesSameDomainAsConsCompared:
"set (map fst m) ⊆ set c ⟹ set (map fst (MList.insert newKey (newVal) (m))) ⊆ set ((newKey)#c)"
  by (induction m rule: MList.insert.induct, auto)

lemma typePreservationUseC:
  assumes"(UseC cid args, ContractAbstraction (cid, params, newContract) # innerCtx, s, e, w, p) →f (newContract, newCtx, newS, newE, newW, newP) ∧
    wellTypedContract ((cid, ContractType params) # restTyCtx) (UseC cid args) ∧
    wellTypedFaustusContext ((cid, ContractType params) # restTyCtx) (ContractAbstraction (cid, params, newContract) # innerCtx) ∧
    wellTypedState ((cid, ContractType params) # restTyCtx) s"
  shows "wellTypedContract (paramsToTypeContext params@restTyCtx) newContract ∧
    wellTypedFaustusContext (paramsToTypeContext params@restTyCtx) newCtx ∧
    wellTypedState (paramsToTypeContext params@restTyCtx) newS"
  using assms apply auto
  using concatWellTypedContexts paramsWellTypedContext apply blast
  using wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedState.simps(5) wellTypedStateCombineTypeContexts by blast

(* A single step of reduction maintains type correctness. *)
lemma typePreservation:
  assumes "(contract, ctx, s, e, w, p) →f (newContract, newCtx, newS, newE, newW, newP) ∧
    wellTypedContract tyCtx contract ∧
    wellTypedFaustusContext tyCtx ctx ∧
    wellTypedState tyCtx s"
  shows "(∃newTyCtx . wellTypedContract newTyCtx newContract ∧
    wellTypedFaustusContext newTyCtx newCtx ∧
    wellTypedState newTyCtx newS)"
  using assms apply (cases contract, auto)
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  using wellTypedStateAccountsArbitrary apply blast
  subgoal premises ps proof -
    obtain vid val where "contract = Let vid val newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((vid, ValueType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain vid val where "contract = Let vid val newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((vid, ValueType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((vid, ValueType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain obsId obs where "contract = LetObservation obsId obs newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((obsId, ObservationType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((obsId, ObservationType)#tyCtx) newS" using calculation ps assms apply simp
      apply (meson calculation ps assms insertValueWellTypedStateIsWellTyped MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain obsId obs where "contract = LetObservation obsId obs newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((obsId, ObservationType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((obsId, ObservationType)#tyCtx) newS" using calculation ps assms apply simp
      apply (meson calculation ps assms insertValueWellTypedStateIsWellTyped MList.insert_lookup_Some)
      using insertValueWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain pkId pk where "contract = LetPubKey pkId pk newContract" using ps by auto
    moreover have "wellTypedFaustusContext ((pkId, PubKeyType)#tyCtx) newCtx" using calculation ps assms by auto
    moreover have "wellTypedState ((pkId, PubKeyType)#tyCtx) newS" using calculation ps assms apply (auto simp add: MList.insert_lookup_Some)
      using insertPubKeyWellTypedStateIsWellTyped by blast
    ultimately show ?thesis using ps by blast
  qed
  subgoal premises ps proof -
    obtain cid args where "contract = UseC cid args" using ps by auto
    moreover obtain params restTy where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, restTy)" using ps assms calculation by (cases "lookupContractIdParamTypeInformation tyCtx cid", auto)
    moreover obtain  innerCtx where "lookupContractIdAbsInformation ctx cid = Some (params, newContract, innerCtx)" using ps assms calculation by (metis FaustusContract.inject(9) Pair_inject lookupContractTypeWellTypedContext option.inject)
    moreover have "wellTypedFaustusContext restTy innerCtx" using calculation(2) calculation(3) lookupContractRestIsWellTyped ps(5) by blast
    moreover have "wellTypedFaustusContext (paramsToTypeContext params @ restTy) ((paramsToFaustusContext params) @ innerCtx)" by (simp add: calculation(4) concatWellTypedContexts paramsWellTypedContext)
    ultimately show ?thesis using ps assms apply auto
      by (meson lookupContractFoundContractIsWellTyped lookupContractStateStaysWellTyped wellTypedArgumentsAddParamsToState wellTypedArgumentsOnlyAddIdsToState wellTypedStateCombineTypeContexts)
  qed
  done

(* Many steps of reduction maintains type correctness. *)
lemma typePreservationManySteps:
assumes"(contract, ctx, s, e, w, p) →f* (newContract, newCtx, newS, newE, newW, newP) ∧
    wellTypedContract tyCtx contract ∧
    wellTypedFaustusContext tyCtx ctx ∧
    wellTypedState tyCtx s"
shows "(∃newTyCtx . wellTypedContract newTyCtx newContract ∧
    wellTypedFaustusContext newTyCtx newCtx ∧
    wellTypedState newTyCtx newS)"
  using assms apply auto
  subgoal proof (induction arbitrary: tyCtx rule:star.induct[of small_step_reduce, split_format(complete)])
    case (refl)
    then show ?case by auto
  next
    case (step)
    then show ?case using typePreservation by blast
  qed
  done

(* A well typed contract will be able to continue unless in a final state. *)
lemma contractProgress:
  assumes "wellTypedContract tyCtx contract ∧
    wellTypedFaustusContext tyCtx ctx ∧
    wellTypedState tyCtx s ∧
    ¬(final (contract, ctx, s, e, w, p))"
  shows "∃cs'. (contract, ctx, s, e, w, p) →f cs'"
  using assms by (auto simp add: final_def)

(* A well typed program can get to a final state. *)
lemma typeSoundness:
  assumes "(contract, ctx, s, env, warns, payments) →f* (newContract, newCtx, newS, newEnv, newWarns, newPayments) ∧
            ¬(final (newContract, newCtx, newS, newEnv, newWarns, newPayments)) ∧
            wellTypedContract tyCtx contract ∧
            wellTypedFaustusContext tyCtx ctx ∧
            wellTypedState tyCtx s"
  shows "∃cs''. (newContract, newCtx, newS, newEnv, newWarns, newPayments) →f cs''"
  using assms by (auto simp add: final_def)

fun compileFaustusPubKey :: "FaustusPubKey ⇒ FaustusState ⇒ PubKey option" where
"compileFaustusPubKey (UsePubKey pkId) fs = MList.lookup pkId (boundPubKeys fs)" |
"compileFaustusPubKey (ConstantPubKey pk) s = Some pk"

fun compileFaustusPayee :: "FaustusPayee ⇒ FaustusState ⇒ Payee option" where
"compileFaustusPayee (Account pk) fs = (case compileFaustusPubKey pk fs of
  Some mPk ⇒ Some (Semantics.Account mPk)
  | None ⇒ None)" |
"compileFaustusPayee (Party pk) fs = (case compileFaustusPubKey pk fs of
  Some mPk ⇒ Some (Semantics.Party mPk)
  | None ⇒ None)"

(* Convert observation abstractions to values. 0 is false and 1 is true. *)
fun compileFaustusValue :: "FaustusValue ⇒ FaustusState ⇒ Value option" and
  compileFaustusObservation :: "FaustusObservation ⇒ FaustusState ⇒ Observation option" where
"compileFaustusValue (AvailableMoney faustusPubKey token) s = (case compileFaustusPubKey faustusPubKey s of
  Some mPk ⇒ Some (Semantics.AvailableMoney mPk token)
  | None ⇒ None)" |
"compileFaustusValue (Constant v) s = Some (Semantics.Constant v)" |
"compileFaustusValue (NegValue v) s = (case compileFaustusValue v s of
  Some mv ⇒ Some (Semantics.NegValue mv)
  | None ⇒ None)" |
"compileFaustusValue (AddValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.AddValue compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusValue (SubValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.SubValue compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusValue (MulValue lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.MulValue compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusValue (Scale i1 i2 val) s = (case (compileFaustusValue val s) of
  Some compVal ⇒ Some (Semantics.Scale i1 i2 compVal)
  | None ⇒ None)" |
"compileFaustusValue (ChoiceValue choiceId) s = Some (Semantics.ChoiceValue choiceId)" |
"compileFaustusValue SlotIntervalStart s = Some Semantics.SlotIntervalStart" |
"compileFaustusValue SlotIntervalEnd s = Some Semantics.SlotIntervalEnd" |
"compileFaustusValue (UseValue valId) s = Some (Semantics.UseValue valId)" |
"compileFaustusValue (Cond obs trueVal falseVal) s = (case (compileFaustusObservation obs s, compileFaustusValue trueVal s, compileFaustusValue falseVal s) of
  (Some compObs, Some compTrueVal, Some compFalseVal) ⇒ Some (Semantics.Cond compObs compTrueVal compFalseVal)
  | _ ⇒ None)" |
"compileFaustusObservation (AndObs lhs rhs) s = (case (compileFaustusObservation lhs s, compileFaustusObservation rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.AndObs compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (OrObs lhs rhs) s = (case (compileFaustusObservation lhs s, compileFaustusObservation rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.OrObs compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (NotObs obs) s = (case compileFaustusObservation obs s of
  Some compObs ⇒ Some (Semantics.NotObs compObs)
  | None ⇒ None)" |
"compileFaustusObservation (ValueGE lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.ValueGE compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (ValueGT lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.ValueGT compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (ValueLT lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.ValueLT compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (ValueLE lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.ValueLE compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (ValueEQ lhs rhs) s = (case (compileFaustusValue lhs s, compileFaustusValue rhs s) of
  (Some compLhs, Some compRhs) ⇒ Some (Semantics.ValueEQ compLhs compRhs)
  | _ ⇒ None)" |
"compileFaustusObservation (UseObservation obsId) s = Some (Semantics.NotObs (Semantics.ValueEQ (Semantics.UseValue obsId) (Semantics.Constant 0)))" |
"compileFaustusObservation (ChoseSomething choiceId) s = Some (Semantics.ChoseSomething choiceId)" |
"compileFaustusObservation TrueObs s = Some Semantics.TrueObs" |
"compileFaustusObservation FalseObs s = Some Semantics.FalseObs"

fun compileAction :: "FaustusAction ⇒ FaustusState ⇒ Semantics.Action option" where
"compileAction (Deposit fromPk toPk token value) s = (case (compileFaustusPubKey fromPk s, compileFaustusPubKey toPk s, compileFaustusValue value s) of
  (Some compiledFromPk, Some compiledToPk, Some compiledValue) ⇒ Some (Semantics.Deposit compiledFromPk compiledToPk token compiledValue)
  | _ ⇒ None)" |
"compileAction (Choice choiceId bounds) s = Some (Semantics.Choice choiceId bounds)" |
"compileAction (Notify observation) s = (case compileFaustusObservation observation s of
  Some compiledObservation ⇒ Some (Semantics.Notify compiledObservation)
  | None ⇒ None)"

fun argumentsToCompilerState :: "FaustusParameter list ⇒ FaustusArgument list ⇒ FaustusState ⇒ FaustusState option" where
"argumentsToCompilerState [] [] fs = Some fs" |
"argumentsToCompilerState params [] fs = None" |
"argumentsToCompilerState [] args fs = None" |
"argumentsToCompilerState (PubKeyParameter pkId#restParams) (PubKeyArgument pk#restArgs) fs = 
  (case compileFaustusPubKey pk fs of
    Some pkCompiled ⇒ argumentsToCompilerState restParams restArgs (fs⦇boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys fs)⦈) 
    | None ⇒ None)" |
"argumentsToCompilerState (ValueParameter valId#restParams) (ValueArgument val#restArgs) ctx = argumentsToCompilerState restParams restArgs (ctx⦇boundValues := MList.insert valId 0 (boundValues ctx)⦈)" |
"argumentsToCompilerState (ObservationParameter obsId#restParams) (ObservationArgument obs#restArgs) ctx = argumentsToCompilerState restParams restArgs (ctx⦇boundValues := MList.insert obsId 0 (boundValues ctx)⦈)" |
"argumentsToCompilerState (firstParam#restParams) (firstArg#restArgs) ctx = None"

(* Creates Marlowe contract that executes parameters sequentially. *)
fun compileArguments :: "FaustusParameter list ⇒ FaustusArgument list ⇒ FaustusState ⇒ Contract ⇒ Contract option" where
"compileArguments [] [] fs cont = Some cont" |
"compileArguments params [] fs cont = None" |
"compileArguments [] args fs cont = None" |
"compileArguments (ValueParameter valId#restParams) (ValueArgument val#restArgs) fs cont = 
  (case compileFaustusValue val fs of
  Some valCompiled ⇒ (case compileArguments restParams restArgs (fs⦇boundValues := MList.insert valId 0 (boundValues fs)⦈) cont of
    Some compiledRest ⇒ Some (Semantics.Let valId valCompiled compiledRest)
    | None ⇒ None)
  | None ⇒ None)" |
"compileArguments (ObservationParameter obsId#restParams) (ObservationArgument obs#restArgs) fs cont = 
  (case compileFaustusObservation obs fs of
  Some obsCompiled ⇒ (case compileArguments restParams restArgs (fs⦇boundValues := MList.insert obsId 0 (boundValues fs)⦈) cont of
    Some compiledRest ⇒ Some (Semantics.Let obsId (Semantics.Cond obsCompiled (Semantics.Constant 1) (Semantics.Constant 0)) compiledRest)
    | None ⇒ None)
  | None ⇒ None)" |
"compileArguments (PubKeyParameter pkId#restParams) (PubKeyArgument pk#restArgs) fs cont = 
  (case compileFaustusPubKey pk fs of
  Some pkCompiled ⇒ compileArguments restParams restArgs (fs⦇boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys fs)⦈) cont
  | None ⇒ None)" |
"compileArguments (firstParam#restParams) (firstArg#restArgs) ctx cont = None"

fun abstractionInformationSize :: "AbstractionInformation ⇒ nat" where
"abstractionInformationSize (PubKeyAbstraction pkId) = 0" |
"abstractionInformationSize (ValueAbstraction valId) = 0" |
"abstractionInformationSize (ObservationAbstraction obsId) = 0" |
"abstractionInformationSize (ContractAbstraction (cid, params, bodyCont)) = size params + faustusContractSize bodyCont"

lemma abstractionInformationSize_GE0:
"(abstractionInformationSize a) ≥ 0"
  by (cases a, auto simp add: faustusContractSize_GT0)

lemma paramsToFaustusContextNoSize:
"paramsToFaustusContext params = newCtx ⟹
  fold (+) (map (abstractionInformationSize) newCtx) 0 = 0"
  by (induction arbitrary: newCtx rule: paramsToFaustusContext.induct, auto)

lemma abstractionInformationSizeFold0:
"fold (+) (map (abstractionInformationSize ∘ snd) ctx) 0 = 0 ⟹
 fold (+) (map (abstractionInformationSize ∘ snd) (ctx@ctx2)) 0 = fold (+) (map (abstractionInformationSize ∘ snd) ctx2) 0"
  by auto

lemma lookupContractCompilerContextSmaller:
"lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) ⟹
  1 + fold (+) (map (abstractionInformationSize) ctx) 0 > faustusContractSize bodyCont + fold (+) (map (abstractionInformationSize) (innerCtx)) 0"
  apply (induction rule: lookupContractIdAbsInformation.induct, auto)
     apply (smt add.commute add.right_neutral fold_plus_sum_list_rev less_add_Suc1 less_trans nat_add_left_cancel_less not_less_eq not_less_iff_gr_or_eq option.inject prod.inject)
    apply (meson option.distinct(1))
   apply (meson option.distinct(1))
  by (meson option.distinct(1))

function compile :: "FaustusContract ⇒ FaustusContext ⇒ FaustusState ⇒ Contract option"
  and compileCases :: "FaustusCase list ⇒ FaustusContext ⇒ FaustusState ⇒ Semantics.Case list option" where
"compile Close ctx state = Some Semantics.Close"|
"compile (Pay accId payee tok val cont) ctx state = (let innerCompilation = compile cont ctx state in
  let compiledAccIdOption = compileFaustusPubKey accId state in
  let compiledPayeeOption = compileFaustusPayee payee state in
  let compiledValueOption = compileFaustusValue val state in
  case (innerCompilation, compiledAccIdOption, compiledPayeeOption, compiledValueOption) of
    (Some innerCompiled, Some compiledAccId, Some compiledPayee, Some compiledValue) ⇒ Some (Semantics.Pay compiledAccId compiledPayee tok compiledValue innerCompiled)
  | _ ⇒ None)"|
"compile (If obs trueCont falseCont) ctx state = (let trueCompilation = compile trueCont ctx state in
  let falseCompilation = compile falseCont ctx state in
  let obsCompilation = compileFaustusObservation obs state in
  case (trueCompilation, falseCompilation, obsCompilation) of
    (Some trueCompiled, Some falseCompiled, Some obsCompiled) ⇒ Some (Semantics.If obsCompiled trueCompiled falseCompiled)
  | _ ⇒ None)"|
"compile (When cases t tCont) ctx state = (
    let compiledT = compile tCont ctx state in
    let newCases = compileCases cases ctx state in
    case (compiledT, newCases) of
      (Some compiledTCont, Some compiledCases) ⇒ Some (Semantics.When compiledCases t compiledTCont)
    | _ ⇒ None)"|
"compile (Let valId val cont) ctx state = (let innerCompilation = compile cont ((ValueAbstraction valId)#ctx) (state⦇boundValues := MList.insert valId 0 (boundValues state)⦈) in
  let valCompilation = compileFaustusValue val state in
  case (innerCompilation, valCompilation) of
    (Some innerCompiled, Some valCompiled) ⇒ Some (Semantics.Let valId valCompiled innerCompiled)
  | _ ⇒ None)"|
"compile (LetObservation obsId obs cont) ctx state = (let innerCompilation = compile cont ((ObservationAbstraction obsId)#ctx) (state⦇boundValues := MList.insert obsId 0 (boundValues state)⦈) in
  let obsCompilation = compileFaustusObservation obs state in
  case (innerCompilation, obsCompilation) of
    (Some innerCompiled, Some obsCompiled) ⇒ Some (Semantics.Let obsId (Semantics.Cond obsCompiled (Semantics.Constant 1) (Semantics.Constant 0)) innerCompiled)
  | _ ⇒ None)"|
"compile (LetPubKey pkId pk cont) ctx state = (case compileFaustusPubKey pk state of
  Some pkCompiled ⇒ compile cont ((PubKeyAbstraction pkId)#ctx) (state⦇boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys state)⦈)
  | None ⇒ None)"|
"compile (Assert obs cont) ctx state = (let innerCompilation = compile cont ctx state in
  let obsCompilation = compileFaustusObservation obs state in
  case (innerCompilation, obsCompilation) of
    (Some innerCompiled, Some obsCompiled) ⇒ Some (Semantics.Assert obsCompiled innerCompiled)
  | _ ⇒ None)"|
"compile (LetC cid params boundCon cont) ctx state = compile cont ((ContractAbstraction (cid, params, boundCon))#ctx) state" |
"compile (UseC cid args) ctx state = (case lookupContractIdAbsInformation ctx cid of
  Some (params, bodyCont, innerCtx) ⇒ (case argumentsToCompilerState params args state of
    Some newState ⇒ (case compile bodyCont ((paramsToFaustusContext params)@innerCtx) newState of
      Some bodyCompiled ⇒ compileArguments params args state bodyCompiled
      | None ⇒ None)
    | None ⇒ None)
  | None ⇒ None)" |
"compileCases ((Case a c) # rest) ctx state = 
  (case (compile c ctx state, compileCases rest ctx state, compileAction a state) of 
    (Some compiledCaseCont, Some compiledCases, Some compiledAction) ⇒ Some ((Semantics.Case compiledAction compiledCaseCont) # compiledCases)
    | _ ⇒ None)" |
"compileCases [] ctx state = Some []"
  by pat_completeness auto

termination
  apply (relation "measures [(λx::((FaustusContract × FaustusContext × FaustusState) + (FaustusCase list × FaustusContext × FaustusState)) . case x of 
    Inl (contract, ctx, state) ⇒ (faustusContractSize contract) + (fold (+) (map (abstractionInformationSize) ctx) 0)
    | Inr (cases, ctx, state) ⇒ (faustusCasesSize cases) + (fold (+) (map (abstractionInformationSize) ctx) 0)),
    (λx::((FaustusContract × FaustusContext × FaustusState) + (FaustusCase list × FaustusContext × FaustusState)) . case x of
    Inl (contract, ctx, state) ⇒ length ctx
    | Inr (cases, ctx, state) ⇒ length ctx)]")
               apply (auto)[1]
              apply (auto)[1]
             apply (auto)[1]
            apply (auto)[1]
           apply (auto)[1]
          apply (auto)[1]
         apply (auto)[1]
        apply (auto)[1]
       apply (auto)[1]
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
     apply (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]
    apply (auto)[1]
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
  using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller apply fastforce
   apply (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]
  by (auto simp add: fold_plus_sum_list_rev faustusContractSize_GT0 abstractionInformationSize_GE0)[1]

lemma compileCorrectness_Close:
"compile Close ctx state = Some (Semantics.Close)"
  by auto

lemma compileCorrectness_Pay:
"compile (Pay accId payee tok val cont) ctx state = Some (mCont) ⟹ 
  ∃innerMCont compiledAccId compiledPayee compiledVal .compile cont ctx state = Some (innerMCont) ∧
  compileFaustusPubKey accId state = Some compiledAccId ∧
  compileFaustusPayee payee state = Some compiledPayee ∧
  compileFaustusValue val state = Some compiledVal ∧
  mCont = (Semantics.Pay compiledAccId compiledPayee tok compiledVal innerMCont)"
  apply auto
  apply (cases "compile cont ctx state", auto)
  apply (cases "compileFaustusPubKey accId state", auto)
  apply (cases "compileFaustusPayee payee state", auto)
  by (cases "compileFaustusValue val state", auto)

lemma compileCorrectness_If:
"compile (If obs trueCont falseCont) ctx state = Some (mCont) ⟹
  ∃trueMCont falseMCont compiledObs. compile trueCont ctx state = Some (trueMCont) ∧
  compile falseCont ctx state = Some (falseMCont) ∧ 
  compileFaustusObservation obs state = Some compiledObs ∧
  (mCont = Semantics.If compiledObs trueMCont falseMCont)"
  apply auto
  apply (cases "compile trueCont ctx state", auto)
  apply (cases "compile falseCont ctx state", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_When:
"compile (When cases t tCont) ctx state = Some (mCont) ⟹
∃mcs mTCont. compile tCont ctx state = Some (mTCont) ∧
  mCont = (Semantics.When mcs t mTCont)"
  apply auto
  apply (cases "compile tCont ctx state", auto)
  by(cases "compileCases cases ctx state", auto)

lemma compileCorrectness_Let:
"compile (Let vid val cont) ctx state = Some (mCont) ⟹
  ∃innerMCont valId compiledVal . compile cont ((ValueAbstraction vid)#ctx) (state⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues state)⦈) = Some (innerMCont) ∧
  compileFaustusValue val state = Some compiledVal ∧
  mCont = (Semantics.Let valId compiledVal innerMCont)"
  apply auto
  apply (cases "compile cont ((ValueAbstraction vid)#ctx) (state⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues state)⦈)", auto)
  by (cases "compileFaustusValue val state", auto)

lemma compileCorrectness_LetObservation:
"compile (LetObservation obsId obs cont) ctx state = Some (mCont) ⟹
  ∃innerMCont valId compiledObs . compile cont ((ObservationAbstraction obsId)#ctx) (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈) = Some (innerMCont) ∧
  compileFaustusObservation obs state = Some compiledObs ∧
  mCont = (Semantics.Let valId (Semantics.Cond compiledObs (Semantics.Constant 1) (Semantics.Constant 0)) innerMCont)"
  apply auto
  apply (cases "compile cont ((ObservationAbstraction obsId)#ctx) (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_LetPubKey:
"compile (LetPubKey pkId pk cont) ctx state = Some (mCont) ⟹
  ∃innerMCont valId compiledPk . compile cont ((PubKeyAbstraction pkId)#ctx) (state⦇boundPubKeys := MList.insert pkId compiledPk (boundPubKeys state)⦈) = Some (innerMCont) ∧
  compileFaustusPubKey pk state = Some compiledPk ∧
  mCont = innerMCont"
  apply auto
  by (cases "compileFaustusPubKey pk state", auto)

lemma compileCorrectness_Assert:
"compile (Assert obs cont) ctx state = Some (mCont) ⟹
  ∃innerMCont compiledObs. compile cont ctx state = Some (innerMCont) ∧
  compileFaustusObservation obs state = Some compiledObs ∧
  mCont = (Semantics.Assert compiledObs innerMCont)"
  apply auto
  apply (cases "compile cont ctx state", auto)
  by (cases "compileFaustusObservation obs state", auto)

lemma compileCorrectness_LetC:
"compile (LetC cid params boundCon cont) ctx state = Some (mCont) ⟹
  ∃innerMCont . compile cont ((ContractAbstraction (cid, params, boundCon))#ctx) state = Some (innerMCont) ∧
  mCont = innerMCont"
  by auto

lemma compileCorrectness_UseC:
"compile (UseC cid args) ctx state = Some (mCont) ⟹
  ∃params bodyCont innerCtx newState bodyCompiled innerMCont . lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) ∧
  argumentsToCompilerState params args state = Some newState ∧
  compile bodyCont ((paramsToFaustusContext params)@innerCtx) newState = Some bodyCompiled ∧
  compileArguments params args state bodyCompiled = Some (innerMCont) ∧
  mCont = innerMCont"
  by (auto split: option.split_asm)

lemma wellTypedPubKeyCompiles:
"(∃compiledPk. wellTypedPubKey tyCtx pk ⟶
  wellTypedState tyCtx state ⟶
  compileFaustusPubKey pk state = Some (compiledPk))"
proof (induction rule: wellTypedState.induct)
  case (1 s)
  then show ?case by (cases pk, auto)
next
  case (2 valId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (3 obsId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (4 pkId restTypes s)
  then show ?case by (cases pk, auto)
next
  case (5 cid params restTypes s)
  then show ?case by (cases pk, auto)
qed

lemma wellTypedPayeeCompiles:
"wellTypedPayee tyCtx payee ⟶
  wellTypedState tyCtx state ⟶
  (∃compiledPayee . compileFaustusPayee payee state = Some (compiledPayee))"
  apply (cases payee, auto)
  using wellTypedPubKeyCompiles apply force
  using wellTypedPubKeyCompiles by fastforce

lemma wellTypedValueObservationCompiles:
"wellTypedValue tyCtx val ⟶
  wellTypedState tyCtx state ⟶
  (∃compiledVal . compileFaustusValue val state = Some compiledVal)"
"wellTypedObservation tyCtx obs ⟶
  wellTypedState tyCtx state ⟶
  (∃compiledObs . compileFaustusObservation obs state = Some compiledObs)"
   apply (induction rule: compileFaustusValue_compileFaustusObservation.induct, auto)
  using wellTypedPubKeyCompiles by fastforce

lemma wellTypedArgumentsCompilerContext:
"(∀tyCtx . ∃newState . wellTypedState tyCtx state ⟶
  wellTypedArguments tyCtx params args ⟶
  argumentsToCompilerState params args state = Some newState)"
  apply (induction rule: argumentsToCompilerState.induct, auto)
    apply (metis (mono_tags, lifting) FaustusState.fold_congs(4) insertPubKeyWellTypedStateIsWellTyped option.simps(5) wellTypedPubKeyCompiles)
  using insertValueWellTypedStateIsWellTyped apply blast
  using insertValueWellTypedStateIsWellTyped by blast

lemma argumentsCompilerContextLookupPreviousBoundValueIds:
"(∀newS vid boundVal. ∃otherBoundVal. Some newS = argumentsToCompilerState params args state ⟶
  MList.lookup vid (boundValues state) = Some boundVal ⟶
  MList.lookup vid (boundValues newS) = Some otherBoundVal)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case by (cases "compileFaustusPubKey pk fs", auto)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply auto
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply auto
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsCompilerContextLookupPreviousBoundPubKeysIds:
"(∀newS pkId boundVal. ∃otherBoundVal. Some newS = argumentsToCompilerState params args state ⟶
  MList.lookup pkId (boundPubKeys state) = Some boundVal ⟶
  MList.lookup pkId (boundPubKeys newS) = Some otherBoundVal)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (metis MList.insert_lookup_Some insert_lookup_different)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case by auto
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case by auto
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedArgumentsCompiles:
"(∀tyCtx innerCont ctx. ∃mCont. wellTypedArguments tyCtx params args ⟶
  wellTypedState tyCtx state ⟶
  compileArguments params args state innerCont = Some mCont)"
  apply (induction rule: compileArguments.induct, auto)
  using wellTypedValueObservationCompiles(1) apply (split option.split, auto)
  using insertValueWellTypedStateIsWellTyped apply (split option.split, auto)
  using wellTypedValueObservationCompiles(2) apply (split option.split, auto)
  apply (split option.split, auto)
  apply (split option.split, auto)
  using wellTypedPubKeyCompiles apply fastforce
  by (simp add: insertPubKeyWellTypedStateIsWellTyped)

lemma wellTypedArgumentsAddsParamsToCompilerState:
"(∀newState . argumentsToCompilerState params args state = Some newState ⟶ wellTypedState (paramsToTypeContext params) newState)"
proof (induction rule: argumentsToCompilerState.induct)
case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  moreover have "∀a . ∃pk . lookup pkId (boundPubKeys (fs⦇boundPubKeys := MList.insert pkId a (boundPubKeys fs)⦈)) = Some pk" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (metis ‹∀a. ∃pk. lookup pkId (boundPubKeys (fs⦇boundPubKeys := MList.insert pkId a (boundPubKeys fs)⦈)) = Some pk› argumentsCompilerContextLookupPreviousBoundPubKeysIds)
next
  case (5 valId restParams val restArgs ctx)
  moreover have "∃y. lookup valId (FaustusState.boundValues (ctx⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues ctx)⦈)) = Some y" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply auto
    by (metis ‹∃y. lookup valId (FaustusState.boundValues (ctx⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues ctx)⦈)) = Some y› argumentsCompilerContextLookupPreviousBoundValueIds)
next
  case (6 obsId restParams obs restArgs ctx)
  moreover have "∃y. lookup obsId (FaustusState.boundValues (ctx⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues ctx)⦈)) = Some y" by (simp add: MList.insert_lookup_Some)
  ultimately show ?case apply auto
    by (metis ‹∃y. lookup obsId (FaustusState.boundValues (ctx⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues ctx)⦈)) = Some y› argumentsCompilerContextLookupPreviousBoundValueIds)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedArgumentsOnlyAddIdsToCompilerState:
"(wellTypedState tyCtx s ⟶
  wellTypedArguments tyCtx params args ⟶ 
  Some newS = argumentsToCompilerState params args s ⟶
  wellTypedState tyCtx newS)"
proof (induction arbitrary: newS rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    by (cases "compileFaustusPubKey pk fs", auto simp add: insertPubKeyWellTypedStateIsWellTyped)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: MList.insert_lookup_Some)
    by (auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: MList.insert_lookup_Some)
    by (cases "argumentsToCompilerState restParams restArgs ctx", auto simp add: insertValueWellTypedStateIsWellTyped)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma wellTypedCompiles_UseC_Inductive:
  assumes "lookupContractIdParamTypeInformation tyCtx cid = Some (x, restTy) ∧
  argumentsToCompilerState params args state = Some argCompilerState ∧
  lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, restC) ∧
  (∀tyCtx.
        wellTypedContract tyCtx bodyCont ⟶
        wellTypedState tyCtx argCompilerState ⟶
        wellTypedFaustusContext tyCtx (paramsToFaustusContext params @ restC) ⟶
        (∃mCont. compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState = Some mCont)) ∧
  wellTypedState tyCtx state ∧
  wellTypedFaustusContext tyCtx ctx ∧
  wellTypedArguments tyCtx x args"
  shows"∃mCont.
    (case compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState of None ⇒ None | Some x ⇒ compileArguments params args state x) =
    Some mCont"
proof -
  have "x = params" using lookupContractAbstractionWellTypedContext assms by fastforce
  moreover have "wellTypedContract (paramsToTypeContext params @ restTy) bodyCont" using calculation lookupContractFoundContractIsWellTyped assms by auto
  moreover have "wellTypedState (paramsToTypeContext params @ restTy) argCompilerState" by (metis assms calculation(1) lookupContractStateStaysWellTyped wellTypedArgumentsAddsParamsToCompilerState wellTypedArgumentsOnlyAddIdsToCompilerState wellTypedStateCombineTypeContexts)
  moreover have "wellTypedFaustusContext (paramsToTypeContext params @ restTy) (paramsToFaustusContext params @ restC)" using lookupContractRestIsWellTyped assms calculation concatWellTypedContexts paramsWellTypedContext by blast
  moreover obtain bodyMCont where "compile bodyCont (paramsToFaustusContext params @ restC) argCompilerState = Some bodyMCont" using calculation assms by auto
  ultimately show ?thesis using assms wellTypedArgumentsCompiles wellTypedArgumentsOnlyAddIdsToCompilerState by auto
qed

(* A well typed contract/cases will compile
  to a Marlowe contract/cases *)
lemma wellTypedCompiles:
"(∀tyCtx . (wellTypedContract tyCtx contract ⟶
  wellTypedState tyCtx state ⟶
  wellTypedFaustusContext tyCtx ctx ⟶
  (∃mCont . compile contract ctx state = Some mCont)))"
"(∀tyCtx .(wellTypedCases tyCtx cases ⟶
  wellTypedState tyCtx state ⟶
  wellTypedFaustusContext tyCtx ctx ⟶
  (∃mCases. compileCases cases ctx state = Some mCases)))"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx)
  then show ?case using wellTypedPayeeCompiles wellTypedPubKeyCompiles wellTypedValueObservationCompiles(1) by fastforce
next
  case (3 obs trueCont falseCont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) by fastforce
next
  case (4 cases t tCont ctx)
  then show ?case by fastforce
next
  case (5 vid val cont ctx state)
  then show ?case apply auto
    using wellTypedValueObservationCompiles insertValueWellTypedStateIsWellTyped insertAppendValueWellTypedStateIsWellTyped by fastforce
next
  case (6 obsId obs cont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) insertAppendValueWellTypedStateIsWellTyped by fastforce
next
  case (7 pkId pk cont ctx)
  then show ?case using wellTypedPubKeyCompiles insertAppendPubKeyWellTypedStateIsWellTyped by fastforce
next
  case (8 obs cont ctx)
  then show ?case using wellTypedValueObservationCompiles(2) by fastforce
next
  case (9 cid params boundCon cont ctx)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof(cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps apply auto
        by (smt case_optionE case_prodE lookupContractAbstractionWellTypedContext option.distinct(1))
    next
      case (Some compilerCidInfo)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases compilerCidInfo)
          case (fields params bodyCont restC)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using lookupContractTypeWellTypedContext ps wellTypedArgumentsCompilerContext by fastforce
            next
              case (Some argCompilerState)
              moreover obtain tyCtx where "(case lookupContractIdParamTypeInformation tyCtx cid of None ⇒ False | Some (params, innerTyCtx) ⇒ wellTypedArguments tyCtx params args) ∧ wellTypedState tyCtx state ∧ wellTypedFaustusContext tyCtx ctx" using ps by auto
              ultimately show ?thesis using ps apply auto
                subgoal premises ps proof (cases "lookupContractIdParamTypeInformation tyCtx cid")
                  case None
                  then show ?thesis using ps by auto
                next
                  case (Some contractIdParamTypeInfo)
                  then show ?thesis using ps wellTypedCompiles_UseC_Inductive by auto
                qed
                done
            qed
            done
        qed
        done
    qed
    done
next
  case (11 act cont rest ctx)
  then show ?case apply auto
    subgoal premises ps proof (cases act)
      case (Deposit x11 x12 x13 x14)
      then show ?thesis using ps apply auto
        by (smt case_prod_conv compileAction.simps(1) option.case_eq_if option.distinct(1) wellTypedPubKeyCompiles wellTypedValueObservationCompiles(1))
    next
      case (Choice x21 x22)
      then show ?thesis using ps by fastforce
    next
      case (Notify obs)
      then show ?thesis using ps apply auto
        by (smt case_prod_conv compileAction.simps(3) option.case_eq_if option.distinct(1) wellTypedPubKeyCompiles wellTypedValueObservationCompiles(2))
    qed
    done
next
  case (12 ctx)
  then show ?case by auto
qed

lemma compilePubKeyAccountsArbitrary:
"compileFaustusPubKey pk state = Some compiledPk ⟶
  compileFaustusPubKey pk (state⦇accounts := newAccs⦈) = Some compiledPk"
  by (cases pk, auto)

lemma compilePayeeAccountsArbitrary:
"compileFaustusPayee pk state = Some compiledPk ⟶
  compileFaustusPayee pk (state⦇accounts := newAccs⦈) = Some compiledPk"
proof (cases pk)
  case (Account acc)
  then show ?thesis by (cases acc, auto)
next
  case (Party p)
  then show ?thesis by (cases p, auto)
qed

lemma compileValueAccountsArbitrary:
"(∀compiledVal newAccs. compileFaustusValue val state = Some compiledVal ⟶
  compileFaustusValue val (state⦇accounts := newAccs⦈) = Some compiledVal)"
"(∀compiledObs newAccs. compileFaustusObservation obs state = Some compiledObs ⟶
  compileFaustusObservation obs (state⦇accounts := newAccs⦈) = Some compiledObs)"
proof (induction rule: compileFaustusValue_compileFaustusObservation.induct)
  case (1 faustusPubKey token s)
  then show ?case using compilePubKeyAccountsArbitrary by (cases "compileFaustusPubKey faustusPubKey s" "compileFaustusPubKey faustusPubKey (s⦇FaustusState.accounts := newAccs⦈)" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (2 v s)
  then show ?case by auto
next
  case (3 v s)
  then show ?case by (cases "compileFaustusValue v s", auto)
next
  case (4 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (6 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (7 i1 i2 val s)
  then show ?case by (cases "compileFaustusValue val s", auto)
next
  case (8 choiceId s)
  then show ?case by auto
next
  case (9 s)
  then show ?case by auto
next
  case (10 s)
  then show ?case by auto
next
  case (11 valId s)
  then show ?case by auto
next
  case (12 obs trueVal falseVal s)
  then show ?case by (cases "compileFaustusObservation obs s" "compileFaustusValue trueVal s" "compileFaustusValue falseVal s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
next
  case (13 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (14 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (15 obs s)
  then show ?case by (cases "compileFaustusObservation obs s", auto)
next
  case (16 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (17 lhs rhs s)
then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (18 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (19 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (20 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (21 obsId s)
  then show ?case by auto
next
  case (22 choiceId s)
  then show ?case by auto
next
  case (23 s)
  then show ?case by auto
next
  case (24 s)
  then show ?case by auto
qed

lemma stateModificationOrderArbitrary:
"state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state), FaustusState.accounts := newAccs⦈ =
  state⦇FaustusState.accounts := newAccs, FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈"
"state⦇FaustusState.accounts := newAccs, boundPubKeys := MList.insert pkId a (boundPubKeys state)⦈ =
  state⦇boundPubKeys := MList.insert pkId a (boundPubKeys state), FaustusState.accounts := newAccs⦈"
"state⦇FaustusState.boundValues := newVals, boundPubKeys := newPubKeys⦈ =
  state⦇boundPubKeys := newPubKeys, FaustusState.boundValues := newVals⦈"
  by auto

lemma argumentsToContextAccountsArbitrary:
"(∀newAccs newState . argumentsToCompilerState params args state = Some newState ⟶
  argumentsToCompilerState params args (state⦇FaustusState.accounts := newAccs⦈) = Some (newState⦇FaustusState.accounts := newAccs⦈))"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply (cases "compileFaustusPubKey pk fs", auto)
    by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma compileArgumentsAccountsArbitrary:
"(∀newAccs compiledContract . compileArguments params args argCtx bodyCont = Some compiledContract ⟶
  compileArguments params args (argCtx⦇accounts := newAccs⦈) bodyCont = Some compiledContract)"
proof (induction rule: compileArguments.induct)
  case (1 fs cont)
  then show ?case by auto
next
  case (2 v va fs cont)
  then show ?case by auto
next
  case (3 v va fs cont)
  then show ?case by auto
next
  case (4 valId restParams val restArgs fs cont)
  then show ?case apply (cases "compileFaustusValue val fs", auto)
    apply (cases "compileArguments restParams restArgs (fs⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues fs)⦈) cont", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (5 obsId restParams obs restArgs fs cont)
  then show ?case apply (cases "compileFaustusObservation obs fs", auto)
    apply (cases "compileArguments restParams restArgs (fs⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues fs)⦈) cont", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (6 pkId restParams pk restArgs fs cont)
  then show ?case by (cases "compileFaustusPubKey pk fs", auto simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case ("7_1")
  then show ?case by auto
next
  case ("7_2")
  then show ?case by auto
next
  case ("7_3")
  then show ?case by auto
next
  case ("7_4")
  then show ?case by auto
next
  case ("7_5")
  then show ?case by auto
next
  case ("7_6")
  then show ?case by auto
next
  case ("7_7")
  then show ?case by auto
next
  case ("7_8")
  then show ?case by auto
qed

lemma compileActionAccountsArbitrary:
"∀newAccs compiledAct . compileAction act state = Some compiledAct ⟶
  compileAction act (state⦇accounts := newAccs⦈) = Some compiledAct"
proof (cases act)
  case (Deposit x11 x12 x13 x14)
  then show ?thesis apply (cases "compileFaustusPubKey x11 state", auto)
    apply (cases "compileFaustusPubKey x12 state", auto)
    apply (cases "compileFaustusValue x14 state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary compilePubKeyAccountsArbitrary)
next
  case (Choice x21 x22)
  then show ?thesis by auto
next
  case (Notify x3)
  then show ?thesis apply (cases "compileFaustusObservation x3 state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary compilePubKeyAccountsArbitrary)
qed

lemma compileAccountsArbitrary:
"(∀newAccs compiledContract . compile contract ctx state = Some compiledContract ⟶
  compile contract ctx (state⦇accounts := newAccs⦈) = Some compiledContract)"
"(∀newAccs compiledCases . compileCases cases ctx state = Some compiledCases ⟶
  compileCases cases ctx (state⦇accounts := newAccs⦈) = Some compiledCases)"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx state)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusPubKey accId state", auto)
    apply (cases "compileFaustusPayee payee state", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: compilePayeeAccountsArbitrary compilePubKeyAccountsArbitrary compileValueAccountsArbitrary(1))
next
  case (3 obs trueCont falseCont ctx state)
  then show ?case apply (cases "compile trueCont ctx state" "compile falseCont ctx state" rule: option.exhaust[case_product option.exhaust], auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: compileValueAccountsArbitrary(2))
next
  case (4 cases t tCont ctx state)
  then show ?case by (cases "compile tCont ctx state" "compileCases cases ctx state" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 valId val cont ctx state)
  then show ?case apply (cases "compile cont (ValueAbstraction valId # ctx) (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈)", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (6 obsId obs cont ctx state)
  then show ?case apply (cases "compile cont (ObservationAbstraction obsId # ctx) (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (7 pkId pk cont ctx state)
  then show ?case apply (cases "compileFaustusPubKey pk state", auto)
    by (simp add: stateModificationOrderArbitrary compilePubKeyAccountsArbitrary)
next
  case (8 obs cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (simp add: stateModificationOrderArbitrary compileValueAccountsArbitrary)
next
  case (9 cid params boundCon cont ctx state)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof (cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps by auto
    next
      case (Some paramsBodyInnerCtx)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases paramsBodyInnerCtx)
          case (fields params bodyCont innerCtx)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using ps by auto
            next
              case (Some argCtx)
              then show ?thesis using ps apply auto
                apply (cases "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx", auto)
                by (simp add: argumentsToContextAccountsArbitrary compileArgumentsAccountsArbitrary)
            qed
            done
        qed
        done
    qed
    done
next
  case (11 a c rest ctx state)
  then show ?case apply auto
    apply (cases "compile c ctx state", auto)
    apply (cases "compileCases rest ctx state", auto)
    apply (cases "compileAction a state", auto)
    by (simp add: stateModificationOrderArbitrary compileActionAccountsArbitrary)
next
  case (12 ctx state)
  then show ?case by auto
qed

lemma compilePubKeyOnlyBoundPubKeysMatter:
"∀compiledPk . compileFaustusPubKey pk state = Some compiledPk ⟶
  compileFaustusPubKey pk (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some compiledPk"
  by (cases pk, auto)

lemma compilePayeeOnlyBoundPubKeysMatter:
"∀compiledPk . compileFaustusPayee payee state = Some compiledPk ⟶
  compileFaustusPayee payee (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some compiledPk"
proof (cases payee)
  case (Account x1)
  then show ?thesis by (cases x1, auto)
next
  case (Party x2)
  then show ?thesis by (cases x2, auto)
qed

lemma compileValueOnlyBoundPubKeysMatter:
"∀compiledVal newVals newState. compileFaustusValue val state = Some compiledVal ⟶
  compileFaustusValue val (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some compiledVal"
"∀compiledObs newVals newState. compileFaustusObservation obs state = Some compiledObs ⟶
  compileFaustusObservation obs (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some compiledObs"
proof (induction rule: compileFaustusValue_compileFaustusObservation.induct)
  case (1 faustusPubKey token s)
  then show ?case apply auto
    by (cases "compileFaustusPubKey faustusPubKey s", auto simp add: compilePubKeyOnlyBoundPubKeysMatter)
next
  case (2 v s)
  then show ?case by auto
next
  case (3 v s)
  then show ?case by (cases "compileFaustusValue v s", auto)
next
  case (4 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (6 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (7 i1 i2 val s)
  then show ?case by (cases "compileFaustusValue val s", auto)
next
  case (8 choiceId s)
  then show ?case by auto
next
  case (9 s)
  then show ?case by auto
next
  case (10 s)
  then show ?case by auto
next
  case (11 valId s)
  then show ?case by auto
next
  case (12 obs trueVal falseVal s)
  then show ?case by (cases "compileFaustusObservation obs s" "compileFaustusValue trueVal s" "compileFaustusValue falseVal s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
next
  case (13 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (14 lhs rhs s)
  then show ?case by (cases "compileFaustusObservation lhs s" "compileFaustusObservation rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (15 obs s)
  then show ?case by (cases "compileFaustusObservation obs s", auto)
next
  case (16 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (17 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (18 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (19 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (20 lhs rhs s)
  then show ?case by (cases "compileFaustusValue lhs s" "compileFaustusValue rhs s" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (21 obsId s)
  then show ?case by auto
next
  case (22 choiceId s)
  then show ?case by auto
next
  case (23 s)
  then show ?case by auto
next
  case (24 s)
  then show ?case by auto
qed

lemma argumentsToContextOnlyBoundPubKeysMatter:
"(∀newVals argState newState . ∃modifiedState . argumentsToCompilerState params args state = Some argState ⟶
  argumentsToCompilerState params args (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some modifiedState)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs", auto)
    by (split option.split, auto simp add: stateModificationOrderArbitrary compilePubKeyOnlyBoundPubKeysMatter option.case_eq_if)
next
  case (5 valId restParams val restArgs ctx)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case (6 obsId restParams obs restArgs ctx)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsToContextBoundPubKeysDeterministic:
"(∀newVals argState newState modifiedArgState . argumentsToCompilerState params args state = Some argState ⟶
  argumentsToCompilerState params args (newState⦇boundPubKeys := (boundPubKeys state)⦈) = Some modifiedArgState ⟶
  (boundPubKeys argState = boundPubKeys modifiedArgState))"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs")
     apply simp
    apply (split option.split_asm)
    apply simp
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter)
next
  case (5 valId restParams val restArgs fs)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case (6 obsId restParams obs restArgs fs)
  then show ?case apply auto
    by (metis stateModificationOrderArbitrary(3))
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma argumentsToContextBoundPubKeysSameAsEvaluateArguments:
"(∀argState evalState newState modifiedArgState tyCtx . wellTypedState tyCtx state ⟶
  wellTypedArguments tyCtx params args ⟶
  argumentsToCompilerState params args state = Some argState ⟶
  evalFaustusArguments env state params args = Some evalState ⟶
  boundPubKeys argState = boundPubKeys evalState)"
proof (induction rule: argumentsToCompilerState.induct)
  case (1 fs)
  then show ?case by auto
next
  case (2 v va fs)
  then show ?case by auto
next
  case (3 v va fs)
  then show ?case by auto
next
  case (4 pkId restParams pk restArgs fs)
  then show ?case apply auto
    apply (cases "compileFaustusPubKey pk fs", auto)
    apply (cases "evalFaustusPubKey fs pk", auto)
    by (metis compileFaustusPubKey.elims evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) insertPubKeyWellTypedStateIsWellTyped option.inject)
next
  case (5 valId restParams val restArgs fs)
  then show ?case apply auto
    subgoal premises ps proof (cases "evalFaustusValue env fs val")
      case None
      then show ?thesis using ps by auto
    next
      case (Some a)
      moreover obtain evaluatedArgs where "evalFaustusArguments env (fs⦇FaustusState.boundValues := MList.insert valId a (FaustusState.boundValues fs)⦈) restParams restArgs = Some evaluatedArgs" using ps calculation by auto
      moreover obtain tyCtx where "wellTypedState tyCtx fs ∧ wellTypedArguments tyCtx restParams restArgs" using ps by auto
      moreover have "wellTypedState tyCtx (fs⦇boundValues := MList.insert valId 0 (boundValues fs)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
      moreover obtain otherEvaluatedArgs where "(evalFaustusArguments env (fs⦇boundValues:=MList.insert valId 0 (boundValues fs)⦈) restParams restArgs = Some otherEvaluatedArgs)" using ps calculation wellTypedArgumentsExecute by fastforce
      moreover have "(fs⦇boundValues:=MList.insert valId 0 (boundValues fs)⦈) = (fs⦇boundValues:=MList.insert valId 0 (boundValues fs)⦈)⦇boundPubKeys := boundPubKeys (fs⦇FaustusState.boundValues := MList.insert valId a (FaustusState.boundValues fs)⦈)⦈" using ps by auto
      moreover have "boundPubKeys otherEvaluatedArgs = boundPubKeys evaluatedArgs" using ps calculation evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys apply auto
        by (metis calculation(6))
      ultimately show ?thesis using ps by auto
    qed
    done
next
  case (6 obsId restParams obs restArgs fs)
  then show ?case apply auto
    subgoal premises ps proof (cases "evalFaustusObservation env fs obs")
      case None
      then show ?thesis using ps by auto
    next
      case (Some a)
      moreover obtain evaluatedArgs where "evalFaustusArguments env (fs⦇FaustusState.boundValues := MList.insert obsId (if a then 1 else 0) (FaustusState.boundValues fs)⦈) restParams restArgs = Some evaluatedArgs" using ps calculation by auto
      moreover obtain tyCtx where "wellTypedState tyCtx fs ∧ wellTypedArguments tyCtx restParams restArgs" using ps by auto
      moreover have "wellTypedState tyCtx (fs⦇boundValues := MList.insert obsId 0 (boundValues fs)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
      moreover obtain otherEvaluatedArgs where "(evalFaustusArguments env (fs⦇boundValues:=MList.insert obsId 0 (boundValues fs)⦈) restParams restArgs = Some otherEvaluatedArgs)" using ps calculation wellTypedArgumentsExecute by fastforce
      moreover have "(fs⦇boundValues:=MList.insert obsId 0 (boundValues fs)⦈) = (fs⦇boundValues:=MList.insert obsId 0 (boundValues fs)⦈)⦇boundPubKeys := boundPubKeys (fs⦇FaustusState.boundValues := MList.insert obsId (if a then 1 else 0) (FaustusState.boundValues fs)⦈)⦈" using ps by auto
      moreover have "boundPubKeys otherEvaluatedArgs = boundPubKeys evaluatedArgs" using ps calculation evalFaustusArguments_OnlyBoundPubKeysChangeBoundPubKeys apply auto
        by (metis calculation(6))
      ultimately show ?thesis using ps by auto
    qed
    done
next
  case ("7_1" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_2" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_3" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_4" v restParams va restArgs ctx)
  then show ?case by auto
next
  case ("7_5" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_6" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_7" va restParams v restArgs ctx)
  then show ?case by auto
next
  case ("7_8" va restParams v restArgs ctx)
  then show ?case by auto
qed

lemma compileArgumentsOnlyBoundPubKeysMatter:
"(∀newState compiledContract . compileArguments params args state bodyCont = Some compiledContract ⟶
  compileArguments params args (newState⦇boundPubKeys := (boundPubKeys state)⦈) bodyCont = Some compiledContract)"
proof (induction rule: compileArguments.induct)
  case (1 fs cont)
  then show ?case by auto
next
  case (2 v va fs cont)
  then show ?case by auto
next
  case (3 v va fs cont)
  then show ?case by auto
next
  case (4 valId restParams val restArgs fs cont)
  then show ?case apply (cases "compileFaustusValue val fs", auto)
    apply (split option.split_asm, auto)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
     apply (metis stateModificationOrderArbitrary(3))
    by (metis option.sel stateModificationOrderArbitrary(3))
next
  case (5 obsId restParams obs restArgs fs cont)
  then show ?case apply (cases "compileFaustusObservation obs fs", auto)
    apply (split option.split_asm, auto)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
    apply (split option.split, auto simp add: stateModificationOrderArbitrary compileValueOnlyBoundPubKeysMatter)
     apply (metis stateModificationOrderArbitrary(3))
    by (metis option.sel stateModificationOrderArbitrary(3))
next
  case (6 pkId restParams pk restArgs fs cont)
  then show ?case apply auto
    apply (split option.split_asm)
    apply simp
    apply (split option.split)
    using compilePubKeyOnlyBoundPubKeysMatter by fastforce
next
  case ("7_1")
  then show ?case by auto
next
  case ("7_2")
  then show ?case by auto
next
  case ("7_3")
  then show ?case by auto
next
  case ("7_4")
  then show ?case by auto
next
  case ("7_5")
  then show ?case by auto
next
  case ("7_6")
  then show ?case by auto
next
  case ("7_7")
  then show ?case by auto
next
  case ("7_8")
  then show ?case by auto
qed

lemma compileActionOnlyBoundPubKeysMatter:
"∀compiledAction newState . compileAction action state = Some compiledAction ⟶
  compileAction action (newState⦇FaustusState.boundPubKeys := (boundPubKeys state)⦈) = Some compiledAction"
proof (induction rule: compileAction.induct)
  case (1 fromPk toPk token "value" s)
  then show ?case apply (cases "compileFaustusPubKey fromPk s" "compileFaustusPubKey toPk s" "compileFaustusValue value s" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter)
next
  case (2 choiceId bounds s)
  then show ?case by auto
next
  case (3 observation s)
  then show ?case apply (cases "compileFaustusObservation observation s", auto)
    by (simp add: compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter)
qed

lemma compileContractOnlyBoundPubKeysMatter_UseC_Inductive:
"argumentsToCompilerState params args state = Some argCtx ⟹
          paramsBodyInnerCtx = (params, bodyCont, innerCtx) ⟹
          lookupContractIdAbsInformation ctx cid = Some (params, bodyCont, innerCtx) ⟹
          (argumentsToCompilerState params args state = Some argCtx ⟹
              ∀marloweContract.
                 compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some marloweContract ⟶
                 (∀newState. compile bodyCont (paramsToFaustusContext params @ innerCtx) (newState⦇boundPubKeys := boundPubKeys argCtx⦈) = Some marloweContract)) ⟹
          compileArguments params args state compiledBodyCont = Some arbMarloweContract ⟹
          compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some compiledBodyCont ⟹
          argumentsToCompilerState params args (arbNewState⦇boundPubKeys := boundPubKeys state⦈) = Some x2 ⟹
          (case compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 of None ⇒ None
           | Some x ⇒ compileArguments params args (arbNewState⦇boundPubKeys := boundPubKeys state⦈) x) =
          Some arbMarloweContract"
  apply auto
  subgoal premises ps proof -
    have "boundPubKeys x2 = boundPubKeys argCtx" using ps argumentsToContextBoundPubKeysDeterministic by presburger
    moreover have "x2 = x2⦇boundPubKeys := boundPubKeys argCtx⦈" using ps calculation by auto
    moreover have "compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 = compile bodyCont (paramsToFaustusContext params @ innerCtx) (x2⦇boundPubKeys := boundPubKeys argCtx⦈)" using ps calculation by (metis calculation(2))
    moreover have "compile bodyCont (paramsToFaustusContext params @ innerCtx) x2 = Some compiledBodyCont" using ps by (simp add: calculation(3))
    moreover have "compileArguments params args (arbNewState⦇boundPubKeys := boundPubKeys state⦈) compiledBodyCont = Some arbMarloweContract" by (metis ps(5) compileArgumentsOnlyBoundPubKeysMatter)
    ultimately show ?thesis by (metis option.simps(5))
  qed
  done

lemma compileContractOnlyBoundPubKeysMatter:
"∀marloweContract newState . compile faustusContract ctx state = Some marloweContract ⟶
  compile faustusContract ctx (newState⦇FaustusState.boundPubKeys := (boundPubKeys state)⦈) = Some marloweContract"
"∀marloweCases newState . compileCases faustusCases ctx state = Some marloweCases ⟶
  compileCases faustusCases ctx (newState⦇FaustusState.boundPubKeys := (boundPubKeys state)⦈) = Some marloweCases"
proof (induction rule: compile_compileCases.induct)
  case (1 ctx state)
  then show ?case by auto
next
  case (2 accId payee tok val cont ctx state)
  then show ?case apply (cases "compile cont ctx state", auto)
    apply (cases "compileFaustusPubKey accId state", auto)
    apply (cases "compileFaustusPayee payee state", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (simp add: compilePayeeOnlyBoundPubKeysMatter compilePubKeyOnlyBoundPubKeysMatter compileValueOnlyBoundPubKeysMatter(1))
next
  case (3 obs trueCont falseCont ctx state)
  then show ?case apply (cases "compile trueCont ctx state" "compile falseCont ctx state" rule: option.exhaust[case_product option.exhaust], auto)
    by (cases "compileFaustusObservation obs state", auto simp add: compileValueOnlyBoundPubKeysMatter(2))
next
  case (4 cases t tCont ctx state)
  then show ?case by (cases "compile tCont ctx state" "compileCases cases ctx state" rule: option.exhaust[case_product option.exhaust], auto)
next
  case (5 valId val cont ctx state)
  then show ?case apply (cases "compile cont (ValueAbstraction valId # ctx) (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈)", auto)
    apply (cases "compileFaustusValue val state", auto)
    by (metis option.simps(5) stateModificationOrderArbitrary(3) compileValueOnlyBoundPubKeysMatter(1))
next
  case (6 obsId obs cont ctx state)
  then show ?case apply (cases "compile cont (ObservationAbstraction obsId # ctx) (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)", auto)
    apply (cases "compileFaustusObservation obs state", auto)
    by (metis option.simps(5) stateModificationOrderArbitrary(3) compileValueOnlyBoundPubKeysMatter(2))
next
  case (7 pkId pk cont ctx state)
  then show ?case apply (simp (no_asm_simp) only: compile.simps)
    subgoal premises ps proof (cases "compileFaustusPubKey pk state")
      case None
      then show ?thesis by auto
    next
      case (Some pkCompiled)
      then show ?thesis using ps apply (cases "compile cont (PubKeyAbstraction pkId # ctx) (state⦇boundPubKeys := MList.insert pkId pkCompiled (boundPubKeys state)⦈)", simp (no_asm_simp))
        using compilePubKeyOnlyBoundPubKeysMatter stateModificationOrderArbitrary by fastforce
    qed
    done
next
  case (8 obs cont ctx state)
  then show ?case apply auto
    apply (cases "compile cont ctx state", auto)
    by (metis compileValueOnlyBoundPubKeysMatter(2) option.discI option.disc_eq_case(1) option.split_sel)
next
  case (9 cid params boundCon cont ctx state)
  then show ?case by auto
next
  case (10 cid args ctx state)
  then show ?case apply auto
    subgoal premises ps proof (cases "lookupContractIdAbsInformation ctx cid")
      case None
      then show ?thesis using ps by auto
    next
      case (Some paramsBodyInnerCtx)
      then show ?thesis using ps apply auto
        subgoal premises ps proof (cases paramsBodyInnerCtx)
          case (fields params bodyCont innerCtx)
          then show ?thesis using ps apply auto
            subgoal premises ps proof (cases "argumentsToCompilerState params args state")
              case None
              then show ?thesis using ps by auto
            next
              case (Some argCtx)
              moreover obtain compiledBodyCont where "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx = Some compiledBodyCont" using calculation ps by fastforce
              ultimately show ?thesis using ps apply auto
                apply (cases "compile bodyCont (paramsToFaustusContext params @ innerCtx) argCtx", auto)
                apply (split option.split, auto)
                using argumentsToContextOnlyBoundPubKeysMatter argumentsToContextBoundPubKeysDeterministic apply (auto)[1]
                using compileContractOnlyBoundPubKeysMatter_UseC_Inductive by fastforce
            qed
            done
        qed
        done
    qed
    done
next
  case (11 a c rest ctx state)
  then show ?case apply (cases "compile c ctx state" "compileCases rest ctx state" "compileAction a state" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    by (simp add: compileActionOnlyBoundPubKeysMatter)
next
  case (12 ctx state)
  then show ?case by auto
qed

(* Problem: What to do about shadowed ids of different types. Should we say ill typed in well typed?*)
fun faustusStateToMarloweState :: "FaustusState ⇒ State" where
"faustusStateToMarloweState fs = ⦇State.accounts = accounts fs,
  State.choices = choices fs,
  State.boundValues = boundValues fs,
  State.minSlot = minSlot fs⦈"

lemma faustusStateToMarloweStatePreservationOfBoundValues:
"boundValues state = State.boundValues (faustusStateToMarloweState state)"
  by auto

lemma preservationOfPubKeyEvaluation:
"(∀tyCtx env res. wellTypedPubKey tyCtx faustusPubKey ⟶
  wellTypedState tyCtx faustusState ⟶
  evalFaustusPubKey faustusState faustusPubKey = Some res ⟶
  compileFaustusPubKey faustusPubKey faustusState = Some res)"
  by (cases faustusPubKey, auto)

lemma preservationOfValueEvaluation:
"(∀tyCtx res. wellTypedValue tyCtx faustusValue ⟶
  wellTypedState tyCtx faustusState ⟶
  evalFaustusValue env faustusState faustusValue = Some res ⟶
  evalValue env (faustusStateToMarloweState faustusState) (the (compileFaustusValue faustusValue faustusState)) = res)"
"(∀tyCtx res. wellTypedObservation tyCtx faustusObservation ⟶
  wellTypedState tyCtx faustusState ⟶
  evalFaustusObservation env faustusState faustusObservation = Some res ⟶
  evalObservation env (faustusStateToMarloweState faustusState) (the (compileFaustusObservation faustusObservation faustusState)) = res)"
proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
  case (1 env state faustusPubKey token)
  then show ?case apply auto
    by (smt wellTypedPubKeyCompiles wellTypedPubKeyEvaluates FaustusPubKey.exhaust State.select_convs(1) compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) evalValue.simps(1) findWithDefault.simps option.simps(5) option.the_def)
next
  case (2 env state integer)
  then show ?case by auto
next
  case (3 env state val)
  then show ?case apply auto
    by (metis (no_types, lifting) evalValue.simps(3) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(1))
next
  case (4 env state lhs rhs)
  then show ?case apply auto
    by (smt evalValue.simps(4) not_None_eq option.case_eq_if option.exhaust_sel option.inject wellTypedValueObservationCompiles(1) wellTypedValueObservationExecutes(1))
next
  case (5 env state lhs rhs)
  then show ?case apply auto
  by (smt evalValue.simps(5) is_none_code(1) is_none_code(2) option.case_eq_if option.sel option.split_sel wellTypedValueObservationCompiles(1))
next
  case (6 env state lhs rhs)
  then show ?case apply auto
    by (smt evalValue.simps(6) is_none_code(2) option.case_eq_if option.discI option.sel option.split_sel wellTypedValueObservationCompiles(1))
next
  case (7 env state n d rhs)
  then show ?case apply auto
    subgoal premises ps proof -
      obtain evalRhs where "evalFaustusValue env state rhs = Some evalRhs" using ps wellTypedValueObservationExecutes by fastforce
      moreover obtain compiledRhs where "compileFaustusValue rhs state = Some compiledRhs" using ps wellTypedValueObservationCompiles by fastforce
      ultimately show ?thesis using ps apply auto
        apply (cases "evalRhs * n < 0" "d < 0" rule: bool.exhaust[case_product bool.exhaust], auto)
           apply (cases "¦(evalRhs * n) - (evalRhs * n) div d * d¦ * 2 < - d", auto)
          apply (cases "¦(evalRhs * n) + - (evalRhs * n) div d * d¦ * 2 < d", auto)
         apply (cases "¦(evalRhs * n) + (evalRhs * n) div - d * d¦ * 2 < - d", auto)
         apply (cases "0 < (evalRhs * n)", auto)
        apply (cases "¦(evalRhs * n) - (evalRhs * n) div d * d¦ * 2 < d", auto)
        by (cases "0 < (evalRhs * n)", auto)
    qed
    done
next
  case (8 env state choId)
  then show ?case by auto
next
  case (9 env state)
  then show ?case by auto
next
  case (10 env state)
  then show ?case by auto
next
  case (11 env state valId)
  then show ?case by auto
next
  case (12 env state cond thn els)
  then show ?case apply auto
    by (smt evalValue.simps(12) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (13 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(1) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(1) is_none_code(2) option.case_eq_if option.sel option.split_sel)
next
  case (14 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(2) option.case_eq_if option.collapse option.discI option.sel)
    by (smt evalObservation.simps(2) option.case_eq_if option.distinct(1) option.exhaust option.sel option.simps(5) wellTypedValueObservationCompiles(2))
next
  case (15 env state subObs)
  then show ?case apply auto
     apply (metis (no_types, lifting) evalObservation.simps(3) is_none_code(2) is_none_simps(1) option.case_eq_if option.collapse option.sel wellTypedValueObservationCompiles(2))
    by (metis evalObservation.simps(3) option.case_eq_if option.collapse option.discI option.sel wellTypedValueObservationCompiles(2))
next
  case (16 env state choId)
  then show ?case by auto
next
  case (17 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(5) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(5) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (18 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(6) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(6) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (19 env state lhs rhs)
  then show ?case apply auto
    apply (smt evalObservation.simps(7) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(7) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (20 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(8) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(8) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (21 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(9) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
    by (smt evalObservation.simps(9) is_none_code(2) is_none_simps(1) option.sel option.simps(5) option.split_sel wellTypedValueObservationCompiles(1))
next
  case (22 env state obsId)
  then show ?case apply auto
     apply (smt Collect_empty_eq None_eq_map_option_iff Option.is_none_def all_not_in_conv bind.bind_lunit bind_eq_None_conv bind_eq_Some_conv case_map_option case_optionE combine_options_def combine_options_simps(3) iszero_0 iszero_def map_option_eq_Some mem_Collect_eq not_None_eq not_Some_eq not_iszero_1 odd_nonzero option.case_distrib option.case_eq_if option.discI option.disc_eq_case(2) option.exhaust option.exhaust_sel option.inject option.map_ident option.map_sel option.sel option.simps(15) option.simps(4) option.simps(5) option.split_sel_asm option.the_def ospec split_option_all split_option_ex zero_neq_one)
    by (smt option.case_eq_if option.exhaust_sel option.simps(5) option.the_def)
next
  case (23 env state)
  then show ?case by auto
next
  case (24 env state)
  then show ?case by auto
qed

lemma preservationOfValueEvaluation_MarloweImpliesFaustus:
"(∀tyCtx res. wellTypedValue tyCtx faustusValue ⟶
  wellTypedState tyCtx faustusState ⟶
  evalValue env (faustusStateToMarloweState faustusState) (the (compileFaustusValue faustusValue faustusState)) = res ⟶
  evalFaustusValue env faustusState faustusValue = Some res)"
"(∀tyCtx res. wellTypedObservation tyCtx faustusObservation ⟶
  wellTypedState tyCtx faustusState ⟶
  evalObservation env (faustusStateToMarloweState faustusState) (the (compileFaustusObservation faustusObservation faustusState)) = res ⟶
  evalFaustusObservation env faustusState faustusObservation = Some res)"
proof (induction rule: evalFaustusValue_evalFaustusObservation.induct)
  case (1 env state faustusPubKey token)
  then show ?case apply auto
    apply (cases "evalFaustusPubKey state faustusPubKey", auto)
     apply (simp add: wellTypedPubKeyEvaluates)
    apply (cases "compileFaustusPubKey faustusPubKey state", auto)
    using wellTypedPubKeyCompiles apply fastforce
    by (metis FaustusPubKey.exhaust compileFaustusPubKey.simps(1) compileFaustusPubKey.simps(2) evalFaustusPubKey.simps(1) evalFaustusPubKey.simps(2) option.simps(5))
next
  case (2 env state integer)
  then show ?case by auto
next
  case (3 env state val)
  then show ?case apply auto
    apply (cases "compileFaustusValue val state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (4 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (5 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (6 env state lhs rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue lhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles by fastforce
next
  case (7 env state n d rhs)
  then show ?case apply auto
    apply (cases "compileFaustusValue rhs state", auto)
    using wellTypedValueObservationCompiles apply fastforce
    by metis
next
  case (8 env state choId)
  then show ?case by auto
next
  case (9 env state)
  then show ?case by auto
next
  case (10 env state)
  then show ?case by auto
next
  case (11 env state valId)
  then show ?case apply auto
    apply (cases "lookup valId (FaustusState.boundValues state)", auto)
    using wellTypedValueObservationExecutes(1) by fastforce
next
  case (12 env state cond thn els)
  then show ?case apply auto
     apply (smt evalValue.simps(12) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalValue.simps(12) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (13 env state lhs rhs)
  then show ?case apply auto
      apply (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
     apply (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(1) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (14 env state lhs rhs)
  then show ?case apply auto
      apply (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
     apply (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(2) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (15 env state subObs)
  then show ?case apply auto
     apply (smt evalObservation.simps(3) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(3) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (16 env state choId)
  then show ?case by auto
next
  case (17 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(5) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(5) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (18 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(6) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(6) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (19 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(7) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(7) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (20 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(8) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(8) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (21 env state lhs rhs)
  then show ?case apply auto
     apply (smt evalObservation.simps(9) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
    by (smt evalObservation.simps(9) option.sel option.simps(5) wellTypedValueObservationCompiles(1) wellTypedValueObservationCompiles(2))
next
  case (22 env state obsId)
  then show ?case apply auto
    apply (cases "lookup obsId (FaustusState.boundValues state)", auto)
    using wellTypedValueObservationExecutes(2) by fastforce
next
  case (23 env state)
  then show ?case by auto
next
  case (24 env state)
  then show ?case by auto
qed

lemma preservationOfArgumentEvaluation_ValueParameter_Induct:
"(⋀arbEvalVal. evaluatedVal = arbEvalVal ⟹
               ∀newState compiledBody compiledFullContract warnings payments tyCtx.
                  wellTypedArguments tyCtx restParams restArgs ⟶
                  wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)⦈) ⟶
                  evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)⦈) restParams restArgs =
                  Some newState ⟶
                  compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert vid arbEvalVal (FaustusState.boundValues s)⦈) compiledBody =
                  Some compiledFullContract ⟶
                  (∃newMWarnings.
                      (compiledFullContract,
                       ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s,
                          boundValues = MList.insert vid arbEvalVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈,
                       e, warnings, payments) →*
                      (compiledBody,
                       ⦇State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState,
                          boundValues = FaustusState.boundValues newState, minSlot = FaustusState.minSlot newState⦈,
                       e, newMWarnings, payments))) ⟹
       wellTypedValue tyCtx val ⟹
       wellTypedArguments tyCtx restParams restArgs ⟹
       wellTypedState tyCtx s ⟹
       evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈) restParams restArgs = Some newState ⟹
       (case compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)⦈) compiledBody of
        None ⇒ None | Some compiledRest ⇒ Some (Contract.Let vid compiledVal compiledRest)) =
       Some compiledFullContract ⟹
       evalFaustusValue e s val = Some evaluatedVal ⟹
       compileFaustusValue val s = Some compiledVal ⟹
       ∃newMWarnings.
          (compiledFullContract,
           ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = FaustusState.boundValues s,
              minSlot = FaustusState.minSlot s⦈,
           e, warnings, payments) →*
          (compiledBody,
           ⦇State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState, boundValues = FaustusState.boundValues newState,
              minSlot = FaustusState.minSlot newState⦈,
           e, newMWarnings, payments)"
  subgoal premises ps proof-
    have "evalValue e (faustusStateToMarloweState s) compiledVal = evaluatedVal" using ps preservationOfValueEvaluation(1) by fastforce
    moreover obtain tyCtx where "wellTypedValue tyCtx val ∧ wellTypedArguments tyCtx restParams restArgs ∧ wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover have "wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles apply auto
      by blast
    moreover have "s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈ = (s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈)⦇boundPubKeys:=(boundPubKeys (s⦇FaustusState.boundValues := MList.insert vid 0 (FaustusState.boundValues s)⦈))⦈" by auto
    moreover have "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert vid evaluatedVal (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation apply auto
      by (metis calculation(7) compileArgumentsOnlyBoundPubKeysMatter)
    moreover have "(∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(∀warnings payments. ∃newMWarnings. (Semantics.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, (faustusStateToMarloweState s)⦇State.boundValues := MList.insert vid evaluatedVal (State.boundValues (faustusStateToMarloweState s))⦈, e, newMWarnings, payments))" using ps calculation apply (cases "lookup vid (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply blast
      using SmallStep.LetShadow by blast
    moreover have "(faustusStateToMarloweState s)⦇State.boundValues := MList.insert vid evaluatedVal (State.boundValues (faustusStateToMarloweState s))⦈ = ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈" by auto
    moreover have "(∃newMWarnings. (Semantics.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt ‹∃newMWarnings. (Contract.Let vid compiledVal compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments)› ‹∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert vid evaluatedVal (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)› faustusStateToMarloweState.simps star.step)
  qed
  done

lemma preservationOfArgumentEvaluation_ObservationParameter_Induct:
"(⋀arbEvalObs. evaluatedObs = arbEvalObs ⟹
               ∀newState compiledBody compiledFullContract warnings payments tyCtx.
                  wellTypedArguments tyCtx restParams restArgs ⟶
                  wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)⦈) ⟶
                  evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)⦈)
                   restParams restArgs =
                  Some newState ⟶
                  compileArguments restParams restArgs
                   (s⦇FaustusState.boundValues := MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s)⦈) compiledBody =
                  Some compiledFullContract ⟶
                  (∃newMWarnings.
                      (compiledFullContract,
                       ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s,
                          boundValues = MList.insert obsId (if arbEvalObs then 1 else 0) (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈,
                       e, warnings, payments) →*
                      (compiledBody,
                       ⦇State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState,
                          boundValues = FaustusState.boundValues newState, minSlot = FaustusState.minSlot newState⦈,
                       e, newMWarnings, payments))) ⟹
       evalFaustusObservation e s obs = Some evaluatedObs ⟹
       compileFaustusObservation obs s = Some compiledObs ⟹
       wellTypedObservation tyCtx obs ⟹
       wellTypedArguments tyCtx restParams restArgs ⟹
       wellTypedState tyCtx s ⟹
       evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert obsId (if evaluatedObs then 1 else 0) (FaustusState.boundValues s)⦈) restParams
        restArgs =
       Some newState ⟹
       (case compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈) compiledBody of
        None ⇒ None | Some compiledRest ⇒ Some (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest)) =
       Some compiledFullContract ⟹
       ∃newMWarnings.
          (compiledFullContract,
           ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = FaustusState.boundValues s,
              minSlot = FaustusState.minSlot s⦈,
           e, warnings, payments) →*
          (compiledBody,
           ⦇State.accounts = FaustusState.accounts newState, choices = FaustusState.choices newState, boundValues = FaustusState.boundValues newState,
              minSlot = FaustusState.minSlot newState⦈,
           e, newMWarnings, payments)"
  apply (cases evaluatedObs, auto)
  subgoal premises ps proof-
    have "evaluatedObs = True" using ps by auto
    moreover have "evalObservation e (faustusStateToMarloweState s) compiledObs = True" using ps preservationOfValueEvaluation(2) by fastforce
    moreover have "evalValue e (faustusStateToMarloweState s) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) = 1" using ps calculation by auto
    moreover obtain tyCtx where "wellTypedObservation tyCtx obs ∧ wellTypedArguments tyCtx restParams restArgs ∧ wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover have "wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles by fastforce
    moreover have "s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈ = (s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈)⦇boundPubKeys:=(boundPubKeys (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈))⦈" by auto
    moreover have "s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈ = (s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈)⦇boundPubKeys:=(boundPubKeys (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈))⦈" by auto
    moreover have "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert obsId 1 (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation apply auto
      by (metis calculation(9) compileArgumentsOnlyBoundPubKeysMatter)
    moreover have "(∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(∀warnings payments. ∃newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, (faustusStateToMarloweState s)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState s))⦈, e, newMWarnings, payments))" using calculation(3) apply (cases "lookup obsId (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply metis
      using SmallStep.LetShadow by metis
    moreover have "(faustusStateToMarloweState s)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState s))⦈ = ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈" by auto
    moreover have "(∃newMWarnings. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt ‹∃newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments)› ‹∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 1 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)› faustusStateToMarloweState.simps star.step)
  qed
  subgoal premises ps proof-
    have "evaluatedObs = False" using ps by auto
    moreover have "evalObservation e (faustusStateToMarloweState s) compiledObs = False" using ps preservationOfValueEvaluation(2) by fastforce
    moreover have "evalValue e (faustusStateToMarloweState s) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) = 0" using ps calculation by auto
    moreover obtain tyCtx where "wellTypedObservation tyCtx obs ∧ wellTypedArguments tyCtx restParams restArgs ∧ wellTypedState tyCtx s" using ps by auto
    moreover have "wellTypedState tyCtx (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈)" using ps calculation insertValueWellTypedStateIsWellTyped by auto
    moreover obtain newState where "evalFaustusArguments e (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈) restParams restArgs = Some newState" using ps by auto
    moreover obtain compiledRest where "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation wellTypedArgumentsCompiles by fastforce
    moreover have "s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈ = (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈)⦇boundPubKeys:=(boundPubKeys (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈))⦈" by auto
    moreover have "s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈ = (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈)⦇boundPubKeys:=(boundPubKeys (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈))⦈" by auto
    moreover have "compileArguments restParams restArgs (s⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues s)⦈) compiledBody = Some compiledRest" using ps calculation by auto
    moreover have "(∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments))" using ps calculation by auto
    moreover have "(∀warnings payments. ∃newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, (faustusStateToMarloweState s)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState s))⦈, e, newMWarnings, payments))" using calculation(3) apply (cases "lookup obsId (State.boundValues (faustusStateToMarloweState s))")
      using SmallStep.LetNoShadow apply metis
      using SmallStep.LetShadow by metis
    moreover have "(faustusStateToMarloweState s)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState s))⦈ = ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈" by auto
    moreover have "(∃newMWarnings. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments))" using calculation by auto
    ultimately show ?thesis using ps star.step ps SmallStep.smallStepStarWarningsAreArbitrary apply auto
      by (smt ‹∃newMWarnings. (Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledRest, faustusStateToMarloweState s, e, warnings, payments) → (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, newMWarnings, payments)› ‹∀warnings payments. ∃newMWarnings. (compiledRest, ⦇State.accounts = FaustusState.accounts s, choices = FaustusState.choices s, boundValues = MList.insert obsId 0 (FaustusState.boundValues s), minSlot = FaustusState.minSlot s⦈, e, warnings, payments) →* (compiledBody, faustusStateToMarloweState newState, e, newMWarnings, payments)› faustusStateToMarloweState.simps star.step)
  qed
  done

(* EvalFaustusArguments will give same state as executing compiled Marlowe to body. *)
lemma preservationOfArgumentEvaluation:
"(∀newState compiledBody compiledFullContract warnings payments tyCtx argCtx. wellTypedArguments tyCtx params args ⟶
  wellTypedState tyCtx state ⟶
  evalFaustusArguments env state params args = Some newState ⟶
  compileArguments params args state compiledBody = Some compiledFullContract ⟶
  (∃newMWarnings . (compiledFullContract, faustusStateToMarloweState state, env, warnings, payments) →*
  (compiledBody, faustusStateToMarloweState newState, env, newMWarnings, payments)))"
proof (induction rule: evalFaustusArguments.induct)
  case (1 e s)
  then show ?case by auto
next
  case (2 e s vid restParams val restArgs)
  then show ?case apply (cases "evalFaustusValue e s val" "compileFaustusValue val s" rule: option.exhaust[case_product option.exhaust], auto)
    using preservationOfArgumentEvaluation_ValueParameter_Induct by presburger
next
  case (3 e s obsId restParams obs restArgs)
  then show ?case apply (cases "evalFaustusObservation e s obs" "compileFaustusObservation obs s" rule: option.exhaust[case_product option.exhaust], auto split del: if_split)
    using preservationOfArgumentEvaluation_ObservationParameter_Induct by presburger
next
  case (4 e s pkId restParams pk restArgs)
  then show ?case apply auto
    apply (cases "evalFaustusPubKey s pk", auto)
    apply (cases "compileFaustusPubKey pk s", auto)
    by (simp add: insertPubKeyWellTypedStateIsWellTyped preservationOfPubKeyEvaluation)
next
  case ("5_1" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_2" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_3" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_4" e s v restParams va restArgs)
  then show ?case by auto
next
  case ("5_5" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_6" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_7" e s va restParams v restArgs)
  then show ?case by auto
next
  case ("5_8" e s va restParams v restArgs)
  then show ?case by auto
next
  case (6 e s v va)
  then show ?case by auto
next
  case (7 e s v va)
  then show ?case by auto
qed

(* Zero or more well typed Faustus steps correspond
  to zero or more Marlowe steps. *)
lemma preservationOfSemantics_FaustusStepImpMarloweSteps:
"f →f f' ⟹
  f = (faustusContract, ctx, state, env, warnings, payments) ⟹
  f' = (newFaustusContract, newCtx, newState, newEnv, newWarnings, newPayments) ⟹
  wellTypedContract tyCtx faustusContract ⟹
  wellTypedFaustusContext tyCtx ctx ⟹
  wellTypedState tyCtx state ⟹
  compile faustusContract ctx state = Some marloweContract ⟹
  compile newFaustusContract newCtx newState = Some newMarloweContract ⟹
  (∃newMWarnings . (marloweContract, faustusStateToMarloweState state, env, warnings, payments) →* (newMarloweContract, faustusStateToMarloweState newState, newEnv, newMWarnings, newPayments))"
proof (induction rule: small_step_reduce.induct)
  case (CloseRefund s party token money newAccount ctx env warns payments)
  moreover have "refundOne (State.accounts (faustusStateToMarloweState state)) = Some ((party, token, money), newAccount)" using calculation by auto
  moreover have "⦇State.accounts = newAccount, choices = FaustusState.choices state, boundValues = FaustusState.boundValues state, minSlot = FaustusState.minSlot state⦈ = (faustusStateToMarloweState state)⦇State.accounts := newAccount⦈" using calculation by auto
  moreover have "(Contract.Close, faustusStateToMarloweState state, newEnv, warnings, payments) → (Contract.Close, (faustusStateToMarloweState state)⦇State.accounts := newAccount⦈, newEnv, warnings @ [ReduceNoWarning], payments @ [Payment party token money])" using calculation SmallStep.CloseRefund by blast
  ultimately show ?case by fastforce
next
  case (PayNonPositive env s val res fromAcc accId toPayee payee token cont ctx warns payments)
  moreover obtain compiledAccId where "compileFaustusPubKey accId newState = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee newState = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val newState = Some compiledFaustusValue" using calculation by fastforce
  ultimately show ?case apply auto
    using preservationOfValueEvaluation(1) by fastforce
next
  case (PayPositivePartialWithPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs somePayment cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > 0" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token 0 (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyToPay" using calculation preservationOfPubKeyEvaluation by (auto, force)
  moreover have "toPayee = compiledPayee" using calculation preservationOfPubKeyEvaluation wellTypedPubKeyEvaluates apply auto
    apply (cases payee, auto)
     apply (metis option.inject)
    by (metis option.inject)
  moreover have "giveMoney compiledPayee token moneyToPay newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(15) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) → (newCompiledCont, (faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈, newEnv, warnings @ [ReducePartialPay compiledAccId compiledPayee token moneyToPay (evalValue env (faustusStateToMarloweState state) compiledFaustusValue)], payments @ [somePayment])" using calculation by blast
  moreover have "((faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositivePartialWithoutPayment env s val res fromAcc accId toPayee payee token newAccs moneyToPay payment finalAccs cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > 0" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue > moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token 0 (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyToPay" using calculation preservationOfPubKeyEvaluation by (auto, force)
  moreover have "toPayee = compiledPayee" using calculation preservationOfPubKeyEvaluation wellTypedPubKeyEvaluates apply auto
    apply (cases payee, auto)
     apply (metis option.inject)
    by (metis option.inject)
  moreover have "giveMoney compiledPayee token moneyToPay newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(15) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) → (newCompiledCont, (faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈, newEnv, warnings @ [ReducePartialPay compiledAccId compiledPayee token moneyToPay (evalValue env (faustusStateToMarloweState state) compiledFaustusValue)], payments)" using calculation by blast
  moreover have "((faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositiveFullWithPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs somePayment cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue = res" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue ≤ moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token newBalance (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by (auto, force, force)
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyInAcc" using calculation preservationOfPubKeyEvaluation by (auto, force)
  moreover have "toPayee = compiledPayee" using calculation preservationOfPubKeyEvaluation wellTypedPubKeyEvaluates apply auto
    apply (cases payee, auto)
     apply (metis option.inject)
    by (metis option.inject)
  moreover have "giveMoney compiledPayee token res newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(16) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) → (newCompiledCont, (faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈, newEnv, warnings @ [ReduceNoWarning], payments @ [somePayment])" using calculation by blast
  moreover have "((faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (PayPositiveFullWithoutPayment env s val res fromAcc accId toPayee payee token moneyInAcc newBalance newAccs payment finalAccs cont ctx warns payments)
  moreover obtain newCompiledCont where "compile newFaustusContract newCtx state = Some newCompiledCont" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "compile newFaustusContract newCtx newState = Some newCompiledCont" using calculation wellTypedCompiles typePreservation compileAccountsArbitrary by fastforce
  moreover obtain compiledAccId where "compileFaustusPubKey accId state = Some compiledAccId" using calculation by fastforce
  moreover obtain compiledPayee where "compileFaustusPayee payee state = Some compiledPayee" using calculation by fastforce
  moreover obtain compiledFaustusValue where "compileFaustusValue val state = Some compiledFaustusValue" using calculation by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue = res" using calculation preservationOfValueEvaluation option.sel by fastforce
  moreover have "evalValue env (faustusStateToMarloweState state) compiledFaustusValue ≤ moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state))" using calculation preservationOfValueEvaluation preservationOfPubKeyEvaluation option.sel by (auto, smt)
  moreover have "updateMoneyInAccount compiledAccId token newBalance (State.accounts (faustusStateToMarloweState state)) = newAccs" using calculation preservationOfPubKeyEvaluation option.sel by (auto, force, force)
  moreover have "moneyInAccount compiledAccId token (State.accounts (faustusStateToMarloweState state)) = moneyInAcc" using calculation preservationOfPubKeyEvaluation by (auto, force)
  moreover have "toPayee = compiledPayee" using calculation preservationOfPubKeyEvaluation wellTypedPubKeyEvaluates apply auto
    apply (cases payee, auto)
     apply (metis option.inject)
    by (metis option.inject)
  moreover have "giveMoney compiledPayee token res newAccs = (payment, finalAccs)" using calculation preservationOfPubKeyEvaluation by auto
  moreover have "compile faustusContract ctx state = Some (Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont)" using calculation apply auto
    by (smt calculation(16) compileCorrectness_Pay option.inject)
  moreover have "(Contract.Pay compiledAccId compiledPayee token compiledFaustusValue newCompiledCont, faustusStateToMarloweState state, env, warnings, payments) → (newCompiledCont, (faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈, newEnv, warnings @ [ReduceNoWarning], payments)" using calculation by blast
  moreover have "((faustusStateToMarloweState state)⦇State.accounts := finalAccs⦈) = faustusStateToMarloweState newState" using calculation by fastforce
  ultimately show ?case by (simp, meson star_step1)
next
  case (IfTrue env s obs cont1 cont2 ctx warns payments)
  then show ?case apply (cases "compile cont2 newCtx newState", auto)
    using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles(2) wellTypedCompiles by fastforce
next
  case (IfFalse env s obs cont1 cont2 ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles(2) wellTypedCompiles by fastforce
next
  case (WhenTimeout env startSlot endSlot timeout cases cont ctx s warns payments)
  then show ?case apply auto
    by (cases "compileCases cases newCtx newState", auto)
next
  case (LetShadow valId s oldVal env val res cont ctx warns payments)
  moreover have "MList.lookup valId (State.boundValues (faustusStateToMarloweState state)) = Some oldVal" using calculation by auto
  moreover obtain compiledVal where "compileFaustusValue val state = Some compiledVal" using calculation wellTypedValueObservationCompiles(1) by fastforce
  moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using calculation wellTypedCompiles by fastforce
  moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈)⦈" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(12) calculation(13))
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (boundValues state)⦈" using calculation by auto
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
  moreover have "(evalValue env (faustusStateToMarloweState state) compiledVal) = res" using calculation preservationOfValueEvaluation by fastforce
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) →
    (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ [ReduceShadowing valId oldVal (res)], payments)" using calculation by blast
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) →
    (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceShadowing valId oldVal (res)], payments)" using calculation by auto
  ultimately show ?case apply auto
    by (metis (no_types, lifting) State.update_convs(3) ‹(Contract.Let valId compiledVal compiledCont, faustusStateToMarloweState state, env, warns, payments) → (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceShadowing valId oldVal res], payments)› ‹faustusStateToMarloweState newState = faustusStateToMarloweState state ⦇State.boundValues := MList.insert valId res (State.boundValues (faustusStateToMarloweState state))⦈› faustusStateToMarloweState.simps faustusStateToMarloweStatePreservationOfBoundValues star_step1)
next
  case (LetNoShadow valId s env val res cont ctx warns payments)
  moreover have "MList.lookup valId (State.boundValues (faustusStateToMarloweState state)) = None" using calculation by auto
  moreover obtain compiledVal where "compileFaustusValue val state = Some compiledVal" using calculation wellTypedValueObservationCompiles(1) by fastforce
  moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using calculation wellTypedCompiles by fastforce
  moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert valId 0 (FaustusState.boundValues state)⦈)⦈" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(12) calculation(13))
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (boundValues state)⦈" using calculation by auto
  moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
  moreover have "(evalValue env (faustusStateToMarloweState state) compiledVal) = res" using calculation preservationOfValueEvaluation by fastforce
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) →
    (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert valId (res) (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ [ReduceNoWarning], payments)" using calculation by blast
  moreover have "(Semantics.Let valId compiledVal compiledCont, (faustusStateToMarloweState state), env, warns, payments) →
    (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceNoWarning], payments)" using calculation by auto
  ultimately show ?case apply auto
    by (metis (no_types, lifting) State.update_convs(3) ‹(Contract.Let valId compiledVal compiledCont, faustusStateToMarloweState state, env, warns, payments) → (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceNoWarning], payments)› ‹faustusStateToMarloweState newState = faustusStateToMarloweState state ⦇State.boundValues := MList.insert valId res (State.boundValues (faustusStateToMarloweState state))⦈› faustusStateToMarloweState.simps faustusStateToMarloweStatePreservationOfBoundValues star_step1)
next
  case (LetObservation env s obs res obsId cont ctx warns payments)
  then show ?case apply auto
    subgoal premises ps proof (cases "MList.lookup obsId (State.boundValues (faustusStateToMarloweState state))")
      case None
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)⦈" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(4) calculation(3))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (boundValues state)⦈" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using calculation preservationOfValueEvaluation by fastforce
      moreover have "(Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ [ReduceNoWarning], payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetNoShadow)
      moreover have "(Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ [ReduceNoWarning], payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) ‹(Contract.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, faustusStateToMarloweState state, env, warns, payments) → (compiledCont, faustusStateToMarloweState newState, env, warns @ [ReduceNoWarning], payments)› faustusStateToMarloweState.simps option.inject option.simps(5) ps(4) ps(5) ps(7) ps(9) ps(10) ps(11) ps(12) ps(13) ps(14) ps(15) ps(17) star_step1)
    next
      case (Some x2)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)⦈" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(3) calculation(4))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (boundValues state)⦈" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 1" using calculation preservationOfValueEvaluation by fastforce
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 1 (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetShadow)
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) LetObservation.prems(7) ‹∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)› faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) ps(9) star_step1)
    qed
    subgoal premises ps proof (cases "MList.lookup obsId (State.boundValues (faustusStateToMarloweState state))")
      case (None)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)⦈" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(4) calculation(3))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (boundValues state)⦈" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using calculation preservationOfValueEvaluation by fastforce
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetNoShadow)
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply (auto)
        by (metis (no_types, lifting) LetObservation.prems(7) ‹∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)› faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) star_step1)
    next
      case (Some x2)
      moreover obtain compiledObs where "compileFaustusObservation obs state = Some compiledObs" using calculation ps wellTypedValueObservationCompiles(2) by fastforce
      moreover obtain compiledCont where "compile newFaustusContract newCtx (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈) = Some compiledCont" using ps calculation wellTypedCompiles by fastforce
      moreover have "newState = newState⦇boundPubKeys := boundPubKeys (state⦇FaustusState.boundValues := MList.insert obsId 0 (FaustusState.boundValues state)⦈)⦈" using ps calculation by auto
      moreover have "compile newFaustusContract newCtx newState = Some compiledCont" by (metis compileContractOnlyBoundPubKeysMatter(1) calculation(3) calculation(4))
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (boundValues state)⦈" using ps calculation by auto
      moreover have "faustusStateToMarloweState newState = (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))⦈" using calculation by auto
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using ps calculation preservationOfValueEvaluation by fastforce
      moreover have "(evalValue env (faustusStateToMarloweState state) (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0))) = 0" using calculation preservationOfValueEvaluation by fastforce
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState state)⦇State.boundValues := MList.insert obsId 0 (State.boundValues (faustusStateToMarloweState state))⦈, env, warns @ newMWarning, payments)" by (metis calculation(1) calculation(8) calculation(9) SmallStep.LetShadow)
      moreover have "∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)" using calculation by auto
      ultimately show ?thesis apply auto
        by (metis (no_types, lifting) LetObservation.prems(7) ‹∃newMWarning. (Semantics.Let obsId (Value.Cond compiledObs (Value.Constant 1) (Value.Constant 0)) compiledCont, (faustusStateToMarloweState state), env, warns, payments) → (compiledCont, (faustusStateToMarloweState newState), env, warns @ newMWarning, payments)› faustusStateToMarloweState.simps option.inject option.simps(5) ps(12) ps(14) ps(15) ps(4) star_step1)
    qed
    done
next
  case (LetPubKey s pk res pkId cont ctx env warns payments)
  then show ?case using star.refl preservationOfPubKeyEvaluation by fastforce
next
  case (AssertTrue env s obs cont ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles by fastforce
next
  case (AssertFalse env s obs cont ctx warns payments)
  then show ?case using preservationOfValueEvaluation(2) wellTypedValueObservationCompiles by fastforce
next
  case (LetC cid params boundCon cont ctx s env warns payments)
  then show ?case by fastforce
next
  case (UseCFound ctx cid params bodyCont innerCtx newState env s args warns payments)
  moreover obtain innerTyCtx where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, innerTyCtx)" using calculation lookupContractTypeWellTypedContext by fastforce
  moreover have "wellTypedArguments tyCtx params args" using calculation by auto
  moreover obtain argState where "argumentsToCompilerState params args state = Some argState" using calculation wellTypedArgumentsCompiles by fastforce
  moreover have "boundPubKeys argState = boundPubKeys newState" using calculation argumentsToContextBoundPubKeysSameAsEvaluateArguments by auto
  moreover have "newState⦇boundPubKeys := boundPubKeys argState⦈ = newState" using calculation by auto
  moreover obtain innerTyCtx where "lookupContractIdParamTypeInformation tyCtx cid = Some (params, innerTyCtx)" using calculation lookupContractTypeWellTypedContext by blast
  moreover obtain compiledBody where "compile newFaustusContract newCtx argState = Some compiledBody" using calculation wellTypedCompiles typePreservation by fastforce
  moreover have "newState = newState⦇boundPubKeys := (boundPubKeys argState)⦈" using calculation by auto
  moreover have "compile newFaustusContract newCtx newState = Some compiledBody" using calculation by (metis compileContractOnlyBoundPubKeysMatter(1))
  moreover have "compileArguments params args state compiledBody = Some marloweContract" using calculation by auto
  moreover have "∃newMWarnings . (marloweContract, faustusStateToMarloweState state, env, warnings, payments) →*
    (compiledBody, faustusStateToMarloweState newState, env, newMWarnings, payments)" using calculation preservationOfArgumentEvaluation by auto
  ultimately show ?case by auto
qed

(* If a well typed Faustus contract executes to a certain state in any number of steps,
  then the compiled Marlowe contract will execute to the same state.*)
lemma preservationOfSemantics_FaustusStepsImpMarloweSteps:
"(faustusContract, ctx, state, env, warnings, payments) →f* (newFaustusContract, newCtx, newState, newEnv, newWarnings, newPayments) ⟹
  wellTypedContract tyCtx faustusContract ⟹
  wellTypedFaustusContext tyCtx ctx ⟹
  wellTypedState tyCtx state ⟹
  (∃newMWarnings . (the (compile faustusContract ctx state), faustusStateToMarloweState state, env, warnings, payments) →* (the (compile newFaustusContract newCtx newState), faustusStateToMarloweState newState, newEnv, newMWarnings, newPayments))"
proof (induction arbitrary: tyCtx rule: star.induct[of small_step_reduce, split_format(complete)])
  case (refl fc ctx a a a b)
  then show ?case by auto
next
  case (step fCont ctx s e w p newFCont newCtx newS newE newW newP finalFCont finalCtx finalS finalE finalW finalP)
  moreover obtain newTyCtx where "wellTypedContract newTyCtx newFCont ∧ wellTypedFaustusContext newTyCtx newCtx ∧ wellTypedState newTyCtx newS" using typePreservation step by blast
  moreover obtain firstMarloweContract where "compile fCont ctx s = Some firstMarloweContract" using step wellTypedCompiles by blast
  moreover obtain secondMarloweContract finalMarloweContract fmw where "compile newFCont newCtx newS = Some secondMarloweContract" and
    "(secondMarloweContract, faustusStateToMarloweState newS, newE, newW, newP) →* (finalMarloweContract, faustusStateToMarloweState finalS, finalE, fmw, finalP)" using step calculation wellTypedCompiles by (metis option.sel)
  moreover obtain secondMarloweWarnings where "(firstMarloweContract, faustusStateToMarloweState s, e, w, p) →* (secondMarloweContract, faustusStateToMarloweState newS, newE, secondMarloweWarnings, newP)" using step calculation preservationOfSemantics_FaustusStepImpMarloweSteps by presburger
  moreover obtain finalMarloweWarnings where "(secondMarloweContract, faustusStateToMarloweState newS, newE, secondMarloweWarnings, newP) →* (finalMarloweContract, faustusStateToMarloweState finalS, finalE, finalMarloweWarnings, finalP)" using step calculation SmallStep.smallStepStarWarningsAreArbitrary by blast
  ultimately show ?case using step apply auto
    by (meson SmallStep.smallStepStarWarningsAreArbitrary star_trans)
qed

lemma preservationOfSemantics_HaltedFaustusImpliesHaltedMarlowe:
"final (faustusContract, ctx, state, env, warnings, payments) ⟹
  wellTypedContract tyCtx faustusContract ⟹
  wellTypedFaustusContext tyCtx ctx ⟹
  wellTypedState tyCtx state ⟹
  finalMarlowe (the (compile faustusContract ctx state), faustusStateToMarloweState state, env, warnings, payments)"
  subgoal premises ps proof (cases faustusContract)
    case Close
    then show ?thesis using ps apply (cases "refundOne (accounts state)", auto simp add: finalMarlowe_def final_def)
      by blast
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (If x31 x32 x33)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (When x41 timeout x43)
    then show ?thesis using ps apply (auto simp add: finalMarlowe_def final_def)
      apply (cases "snd (slotInterval env) ≥ timeout" "fst (slotInterval env) ≥ timeout" rule: bool.exhaust[case_product bool.exhaust], auto)
      using surjective_pairing apply blast
        apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
        apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) apply fastforce
       apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
       apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) apply fastforce
      apply (cases "compile x43 ctx state", auto)
      using wellTypedCompiles(1) apply fastforce
      apply (cases "compileCases x41 ctx state", auto)
      using wellTypedCompiles(2) by fastforce
  next
    case (Let x51 x52 x53)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetObservation x61 x62 x63)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetPubKey x71 x72 x73)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (Assert x81 x82)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (LetC x91 x92 x93 x94)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  next
    case (UseC x101 x102)
    then show ?thesis using FaustusSemantics.finalD ps(1) ps(2) ps(3) ps(4) by blast
  qed
  done

datatype FaustusReduceStepResult = Reduced FaustusContext ReduceWarning ReduceEffect FaustusState FaustusContract
                          | NotReduced
                          | AmbiguousSlotIntervalReductionError
                          | UnboundIdentifierError
                          | ArgumentEvaluationError

fun reduceContractStep :: "FaustusContext ⇒ Environment ⇒ FaustusState ⇒ FaustusContract ⇒ FaustusReduceStepResult" where
"reduceContractStep ctx _ state Close =
  (case refundOne (accounts state) of
     Some ((party, token, money), newAccount) ⇒
       let newState = state ⦇ accounts := newAccount ⦈ in
       Reduced ctx ReduceNoWarning (ReduceWithPayment (Payment party token money)) newState Close
   | None ⇒ NotReduced)" |
"reduceContractStep ctx env state (Pay faustusAccId faustusPayee token val cont) =
  (case (evalFaustusPubKey state faustusAccId, evalFaustusPayee state faustusPayee, evalFaustusValue env state val) of 
    (None, _, _) ⇒ UnboundIdentifierError
    | (_, None, _) ⇒ UnboundIdentifierError
    | (_, _, None) ⇒ UnboundIdentifierError
    | (Some accId, Some payee, Some moneyToPay) ⇒
     (if moneyToPay ≤ 0
       then (let warning = ReduceNonPositivePay accId payee token moneyToPay in
             Reduced ctx warning ReduceNoPayment state cont)
       else (let balance = moneyInAccount accId token (accounts state) in
            (let paidMoney = min balance moneyToPay in
             let newBalance = balance - paidMoney in
             let newAccs = updateMoneyInAccount accId token newBalance (accounts state) in
             let warning = (if paidMoney < moneyToPay
                            then ReducePartialPay accId payee token paidMoney moneyToPay
                            else ReduceNoWarning) in
             let (payment, finalAccs) = giveMoney payee token paidMoney newAccs in
             Reduced ctx warning payment (state ⦇ accounts := finalAccs ⦈) cont))))" |
"reduceContractStep ctx env state (If obs cont1 cont2) =
  (case evalFaustusObservation env state obs of
    None ⇒ UnboundIdentifierError
    | Some evaluatedObservation ⇒ (let cont = (if evaluatedObservation then cont1 else cont2) in
       Reduced ctx ReduceNoWarning ReduceNoPayment state cont))" |
"reduceContractStep ctx env state (When _ timeout cont) =
  (let (startSlot, endSlot) = slotInterval env in
   if endSlot < timeout
   then NotReduced
   else (if timeout ≤ startSlot
         then Reduced ctx ReduceNoWarning ReduceNoPayment state cont
         else AmbiguousSlotIntervalReductionError))" |
"reduceContractStep ctx env state (Let valId val cont) =
  (case evalFaustusValue env state val of
    None ⇒ UnboundIdentifierError
    | Some evaluatedValue ⇒ (let boundVals = boundValues state in
      let newState = state ⦇ boundValues := MList.insert valId evaluatedValue boundVals⦈ in
      let warn = case lookup valId boundVals of
                  Some oldVal ⇒ ReduceShadowing valId oldVal evaluatedValue
                | None ⇒ ReduceNoWarning in
      Reduced (ValueAbstraction valId#ctx) warn ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetObservation obsId obs cont) =
  (case evalFaustusObservation env state obs of
    None ⇒ UnboundIdentifierError
    | Some evaluatedObservation ⇒ (let boundVals = boundValues state in
      let newState = state ⦇ boundValues := MList.insert obsId (if evaluatedObservation then 1 else 0) boundVals ⦈ in
      Reduced (ObservationAbstraction obsId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (LetPubKey pkId pk cont) =
  (case evalFaustusPubKey state pk of
    None ⇒ UnboundIdentifierError
    | Some evaluatedPubKey ⇒ (let boundPubKeys = boundPubKeys state in
      let newState = state ⦇ boundPubKeys := MList.insert pkId evaluatedPubKey boundPubKeys ⦈ in
      Reduced (PubKeyAbstraction pkId#ctx) ReduceNoWarning ReduceNoPayment newState cont))" |
"reduceContractStep ctx env state (Assert obs cont) =
  (case evalFaustusObservation env state obs of
    None ⇒ UnboundIdentifierError
    | Some evaluatedObservation ⇒ (let warning = if evaluatedObservation
                 then ReduceNoWarning
                 else ReduceAssertionFailed
   in Reduced ctx warning ReduceNoPayment state cont))" |
"reduceContractStep ctx env state (LetC cid params boundCon cont) = Reduced (ContractAbstraction (cid, params, boundCon)#ctx) ReduceNoWarning ReduceNoPayment state cont" |
"reduceContractStep ctx env state (UseC cid args) = (
  case lookupContractIdAbsInformation ctx cid of
    None ⇒ UnboundIdentifierError
    | Some (params, bodyCont, innerCtx) ⇒ (case evalFaustusArguments env state params args of
      None ⇒ ArgumentEvaluationError
      | Some newState ⇒ Reduced ((paramsToFaustusContext params)@innerCtx) ReduceNoWarning ReduceNoPayment newState bodyCont))"

(* If reduce step function reduces a contract, then a small step reduction is possible. *)
lemma reduceStepIsSmallStepReduction:
assumes "reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract"
shows "∃ appendPayment .(contract, ctx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, newState, env, initialWarnings @ [warning], initialPayments @ appendPayment)"
proof (cases contract paymentReduceResult rule: FaustusContract.exhaust[case_product ReduceEffect.exhaust])
  case Close_ReduceNoPayment
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (Close_ReduceWithPayment reducePayment)
  then show ?thesis using assms apply auto
    by (cases "refundOne (accounts state)", auto)
next
  case (Pay_ReduceNoPayment accId payee token val cont)
  then show ?thesis using assms apply auto
    apply (cases "evalFaustusPubKey state accId" "evalFaustusPayee state payee" "evalFaustusValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
     apply (metis FaustusSemantics.small_step_reduce.PayNonPositive append.right_neutral)
    apply (split option.split_asm, auto simp add: min_def)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments)› append_Nil2)
    qed
    apply (split if_split_asm, auto)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments)› append_Nil2)
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments)› append_Nil2)
    qed
    apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token evaluatedValue (MList.insert (evaluatedPubKey, token) (availableMoney - evaluatedValue) (FaustusState.accounts state)) = (ReduceNoPayment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments)" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments)› append_Nil2)
    qed
    done
next
  case (Pay_ReduceWithPayment accId payee token val cont payment)
  then show ?thesis using assms apply auto
    apply (cases "evalFaustusPubKey state accId" "evalFaustusPayee state payee" "evalFaustusValue env state val" rule: option.exhaust[case_product option.exhaust[case_product option.exhaust]], auto)
    apply (split if_split_asm, auto)
    apply (split option.split_asm, auto simp add: min_def)
     apply (auto split: prod.split_asm)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token 0 (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token 0 evaluatedValue], initialPayments @ [payment])›)
    qed
    apply (split if_split_asm, auto)
     apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReducePartialPay evaluatedPubKey evaluatedPayee token availableMoney evaluatedValue], initialPayments @ [payment])›)
    qed
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token availableMoney (MList.delete (evaluatedPubKey, token) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps PayPositivePartialWithPayment by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])›)
    qed
    apply (split prod.split_asm, auto)
    subgoal premises ps proof -
      obtain evaluatedPubKey where "evalFaustusPubKey state accId = Some evaluatedPubKey" using ps by auto
      moreover obtain evaluatedPayee where "evalFaustusPayee state payee = Some evaluatedPayee" using ps by auto
      moreover obtain evaluatedValue where "evalFaustusValue env state val = Some evaluatedValue" using ps by auto
      moreover obtain availableMoney where "lookup (evaluatedPubKey, token) (FaustusState.accounts state) = Some availableMoney" using ps calculation by auto
      moreover obtain finalAccs where "giveMoney evaluatedPayee token evaluatedValue (MList.insert (evaluatedPubKey, token) (availableMoney - evaluatedValue) (FaustusState.accounts state)) = (ReduceWithPayment payment, finalAccs)" using ps calculation by auto
      moreover have "(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])" using calculation ps by auto
      ultimately show ?thesis using ps apply auto
        by (metis ‹(FaustusContract.Pay accId payee token val continueContract, newCtx, state, env, initialWarnings, initialPayments) →f (continueContract, newCtx, state ⦇FaustusState.accounts := finalAccs⦈, env, initialWarnings @ [ReduceNoWarning], initialPayments @ [payment])›)
    qed
    done
next
  case (If_ReduceNoPayment obs x32 x33)
  then show ?thesis using assms apply (auto split: option.split_asm)
     apply (metis IfTrue append_Nil2)
    by (metis IfFalse append.right_neutral)
next
  case (If_ReduceWithPayment x31 x32 x33 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (When_ReduceNoPayment x41 timeout x43)
  then show ?thesis using assms apply auto
    subgoal premises ps proof (cases "slotInterval env")
      case (Pair ss es)
      then show ?thesis using ps apply auto
        apply (cases "es < timeout", auto)
        apply (cases "timeout ≤ ss", auto)
        by (smt WhenTimeout append_Nil2)
    qed
    done
next
  case (When_ReduceWithPayment x41 x42 x43 x2)
  then show ?thesis using assms by (auto split: if_split_asm prod.split_asm)
next
  case (Let_ReduceNoPayment vid x52 x53)
  then show ?thesis using assms apply (auto split: option.split_asm)
    apply (cases "lookup vid (boundValues state)", auto)
     apply (metis LetNoShadow append_Nil2)
    by (metis (no_types, lifting) LetShadow append_Nil2)
next
  case (Let_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (cases "lookup vid (boundValues state)", auto)
next
  case (LetObservation_ReduceNoPayment obsId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetObservation append_Nil2)
next
  case (LetObservation_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetPubKey_ReduceNoPayment pkId x52 x53)
  then show ?thesis using assms apply (simp split: option.split_asm)
    by (metis (no_types, lifting) LetPubKey append_Nil2)
next
  case (LetPubKey_ReduceWithPayment vid x52 x53 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (Assert_ReduceNoPayment x61 x62)
  then show ?thesis using assms apply (auto split: option.split_asm)
     apply (metis AssertTrue append.right_neutral)
    by (metis AssertFalse append_Nil2)
next
  case (Assert_ReduceWithPayment x61 x62 x2)
  then show ?thesis using assms by (auto split: option.split_asm)
next
  case (LetC_ReduceNoPayment cid x72 x73)
  then show ?thesis using assms apply auto
    by (metis LetC append.right_neutral)
next
  case (LetC_ReduceWithPayment cid x72 x73 x2)
  then show ?thesis using assms by auto
next
  case (UseC_ReduceNoPayment cid)
  then show ?thesis using assms apply (auto split: option.split_asm)
    by (metis UseCFound append_Nil2)
next
  case (UseC_ReduceWithPayment cid x2)
  then show ?thesis using assms by (auto split: option.split_asm)
qed

(* If a small step reduction is possible, then the reduce step function will reduce the contract *)
lemma smallStepReductionImpReduceStep:
assumes "cs = (contract, ctx, state, env, initialWarnings, initialPayments) ∧
    cs' = (continueContract, newCtx, newState, newEnv, newWarnings, newPayments) ∧
    cs →f cs'"
shows "∃warning paymentReduceResult . reduceContractStep ctx env state contract = Reduced newCtx warning paymentReduceResult newState continueContract"
proof (cases contract)
  case Close
  then show ?thesis using assms by auto
next
  case (Pay accId payee token val cont)
  then show ?thesis using assms apply auto
        apply (auto split: option.split)
              apply (auto simp add: min.strict_order_iff min.absorb_iff2)
        apply metis
       apply metis
      apply metis
     apply metis
    by metis
next
  case (If x31 x32 x33)
  then show ?thesis using assms by auto
next
  case (When cases timeout cont)
  then show ?thesis using assms by auto
next
  case (Let x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetObservation x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (LetPubKey x51 x52 x53)
  then show ?thesis using assms by auto
next
  case (Assert x61 x62)
  then show ?thesis using assms by auto
next
  case (LetC x71 x72 x73)
  then show ?thesis using assms by auto
next
  case (UseC x8)
  then show ?thesis using assms by (auto split: option.split)
qed

lemma wellTypedContractNeverGetsUnboundIdentifierError:
"wellTypedContract tyCtx contract ⟹
  wellTypedFaustusContext tyCtx ctx ⟹
  wellTypedState tyCtx state ⟹
  reduceContractStep ctx env state contract ≠ UnboundIdentifierError"
  apply (cases contract, auto)
          apply (cases "refundOne (accounts state)", auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm simp add: wellTypedPubKeyEvaluates wellTypedValueObservationExecutes wellTypedPayeeEvaluates min_def)
   apply (meson FaustusReduceStepResult.distinct(5))
  using lookupContractAbstractionWellTypedContext by fastforce

lemma wellTypedContractNeverGetsArgumentEvaluationError:
"wellTypedContract tyCtx contract ⟹
  wellTypedFaustusContext tyCtx ctx ⟹
  wellTypedState tyCtx state ⟹
  reduceContractStep ctx env state contract ≠ ArgumentEvaluationError"
  apply (cases contract, auto)
          apply (cases "refundOne (accounts state)", auto)
         apply (auto split: option.split_asm if_split_asm prod.split_asm simp add: wellTypedPubKeyEvaluates wellTypedValueObservationExecutes wellTypedPayeeEvaluates min_def)
   apply (meson FaustusReduceStepResult.distinct)
  by (metis lookupContractTypeWellTypedContext old.prod.inject option.inject wellTypedArgumentsExecute)


datatype FaustusReduceResult = ContractQuiescent "ReduceWarning list" "Payment list"
                                          FaustusState FaustusContract
                                          | RRAmbiguousSlotIntervalError
                                          | RRUnboundIdentifierError
                                          | RRArgumentEvaluationError

function (sequential) reductionLoop :: "FaustusContext ⇒ Environment ⇒ FaustusState ⇒ FaustusContract ⇒ ReduceWarning list ⇒
                                        Payment list ⇒ FaustusReduceResult" where
"reductionLoop ctx env state contract warnings payments =
  (case reduceContractStep ctx env state contract of
     Reduced newCtx warning effect newState ncontract ⇒
       let newWarnings = (if warning = ReduceNoWarning
                          then warnings
                          else warning # warnings) in
       let newPayments = (case effect of
                            ReduceWithPayment payment ⇒ payment # payments
                          | ReduceNoPayment ⇒ payments) in
       reductionLoop newCtx env newState ncontract newWarnings newPayments
   | AmbiguousSlotIntervalReductionError ⇒ RRAmbiguousSlotIntervalError
   | NotReduced ⇒ ContractQuiescent (rev warnings) (rev payments) state contract
   | UnboundIdentifierError ⇒ RRUnboundIdentifierError
   | ArgumentEvaluationError ⇒ RRArgumentEvaluationError)"
  by pat_completeness auto

lemma reductionLoop_termination_aux1:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract ⟹
       ¬ faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) ⟹
       faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
       faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 ≤ fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps by (cases "lookup x51 (boundValues state)", auto split: option.split_asm)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux2:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 ⟹
       ¬ faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) ⟹
       ¬ length newCtx < length ctx ⟹ length newCtx = length ctx"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps by (cases "refundOne (accounts state)", auto)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
  case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 ≤ fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

lemma reductionLoop_termination_aux3:
"FaustusSemantics.reduceContractStep ctx env state contract =
       FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 ⟹
       ¬ faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
          < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) ⟹
       ¬ length newCtx < length ctx ⟹
       (⋀ctx env state contract newCtx x12 x13 x14 newContract.
           FaustusSemantics.reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 newContract ⟹
           ¬ faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) ⟹
           faustusContractSize newContract + (fold (+) (map (abstractionInformationSize) newCtx) 0) =
           faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0)) ⟹
       (⋀ctx env state contract newCtx x12 x13 x14 x15.
           FaustusSemantics.reduceContractStep ctx env state contract =
           FaustusReduceStepResult.Reduced newCtx x12 x13 x14 x15 ⟹
           ¬ faustusContractSize x15 + (fold (+) (map (abstractionInformationSize) newCtx) 0)
              < faustusContractSize contract + (fold (+) (map (abstractionInformationSize) ctx) 0) ⟹
           ¬ length newCtx < length ctx ⟹ length newCtx = length ctx) ⟹
       length (accounts x14) < length (accounts state)"
  subgoal premises ps proof (cases contract)
    case Close
    then show ?thesis using ps apply auto
      apply (induction "accounts state", auto)
      by (cases "refundOne (accounts state)", auto simp add: refundOneShortens)
  next
    case (Pay x21 x22 x23 x24 x25)
    then show ?thesis using ps by (auto split: option.split_asm if_split_asm prod.split_asm simp add: min_def)
  next
    case (If x31 x32 x33)
    then show ?thesis using ps by (auto split: option.split_asm simp add: le_imp_less_Suc)
  next
    case (When x41 x42 x43)
    then show ?thesis using ps by (cases "slotInterval env" "snd (slotInterval env) < x42" "x42 ≤ fst (slotInterval env)" rule: prod.exhaust[case_product bool.exhaust[case_product bool.exhaust]], auto)
  next
    case (Let x51 x52 x53)
    then show ?thesis using ps apply (auto split: option.split_asm)
      by (cases "lookup x51 (boundValues state)", auto)
  next
    case (LetObservation obsId obs cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetPubKey pkId pk cont)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (Assert x61 x62)
    then show ?thesis using ps by (auto split: option.split_asm)
  next
    case (LetC x71 x72 x73)
    then show ?thesis using ps by (auto simp add: fold_plus_sum_list_rev)
  next
    case (UseC x8)
    then show ?thesis using ps apply (auto split: option.split_asm)
      using paramsToFaustusContextNoSize lookupContractCompilerContextSmaller by fastforce
  qed
  done

termination reductionLoop
  apply (relation "measures [(λ(ctx, env, state, contract, _) . ((faustusContractSize contract)) + (fold (+) (map (abstractionInformationSize) ctx) 0)), 
    (λ(ctx, _, state, contract, _) . (length ctx)),
    (λ(ctx, _, state, contract, _) . (length (accounts state)))]", auto)
  using reductionLoop_termination_aux1
    reductionLoop_termination_aux2 
    reductionLoop_termination_aux3 by auto

end
-/
end FaustusSemantics
