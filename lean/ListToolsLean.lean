import .lovelib

-- More functions for lists, named ListTools in Isabelle
namespace ListTools

-- Any and All fuctions
universe u
variable {α : Type u}

def any {f : α → Prop} : (α → Prop) → list α → Prop
| f list.nil := false
| f (list.cons h t) := (f h ∨ any f t)
#check any

def all {f : α → Prop} : (α → Prop) → list α → Prop
| f list.nil := true
| f (list.cons h t) := (f h ∧  all f t)
#check all

end ListTools