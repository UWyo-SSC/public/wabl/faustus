import .lovelib
import .SListLean

namespace MList

open SList


universes u v 

variables {α: Type u}{β : Type v}{ha : linear_order α}

#check map_bind
#check map_seq
#check list.map
#check list(α × β)
#check list.map
#check prod_map
#check valid_set
#check list.nil
#check prod.fst

-- valid_map function 

@[simp]def valid_map {α : Type u} [ha : linear_order α] : list(α × β) → Prop :=
λ x, (let y := list.map prod.fst x in valid_set y)
#check valid_map


-- empty definition for a map
def emptyMap (α : Type u) (β : Type v): list(α × β) := list.nil
#check emptyMap

lemma valid_empty [ha : linear_order α] : valid_map (emptyMap α β):=
begin
  simp,
  rw emptyMap,
  simp,
end
#check valid_empty


/-# Insert_map function and associated lemmas-/

def insert_map [ha : linear_order α] :α → β → list (α × β) → list (α × β) 
  |a b list.nil :=  ((a, b) :: list.nil)
  |a b ((x, y) :: z) := (if a < x  then ((a, b) :: (x, y) :: z) else
    (if a > x then ((x, y) :: (insert_map a b z)) else ((x, b) :: z))) 
#check insert_map

#check list.length
lemma insert_length {a : α} {b : β} {c : list (α × β)} [ha : linear_order α]: list.length (insert_map a b c) ≤ (list.length c + 1):=
  begin
    induction c with z c,
    rw insert_map,
    simp,
    simp,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y),
    simp,
    rw hz,
    rw insert_map,
    split_ifs,
    simp,
    simp,
    exact c_ih,
    have he : a = x,
      {
        have ht := decidable.lt_trichotomy a x,
        cases ht with hl ht,
        contradiction,
        cases ht with he hg,
        exact he,
        contradiction,
      },
    simp,
  end
#check insert_length

lemma insert_in_middle_map {a x: α} {b y c: β} {z : list (α × β)} [ha : linear_order α] : 
  x < a  → valid_map ((a, b) :: z) → valid_map ((x, y) :: (a, c) :: z):=
  begin
    intros hx hvm,
    simp,
    split,
    {
      intro b',
      split,
      {
        intro he,
        rw ← he at hx,
        exact hx,
      },
      intros x' hin,
      have ha : a < b',
      {
        simp at hvm,
        cases hvm with hvm1 hvm2,
        apply hvm1,
        exact hin,
      },
      transitivity,
      exact hx,
      exact ha,
    },
    {
      simp at hvm,
      cases hvm with hvm1 hvm2,
      split,
      {
        intros b' x hbxin,
        apply hvm1,
        exact hbxin,
      },
      {
        exact hvm2,
      },
    },
  end
#check insert_in_middle_map

lemma remove_from_middle_map {a x: α} {b y: β} {c : list (α × β)} [ha : linear_order α] :
  valid_map ((x, y) :: (a, b) :: c) → x < a :=
  begin
    intro hvm,
    simp at hvm,
    cases hvm with hvm1 hvm,
    specialize hvm1 a,
    cases hvm1 with hvm11 hvm12,
    apply hvm11,
    refl,
  end
#check remove_from_middle_map

lemma sublist_valid_map {x: α} {y: β} {c : list (α × β)} [ha : linear_order α] :
  valid_map ((x, y) :: c) → valid_map c :=
  begin
    intro hvm,
    simp,
    simp at hvm,
    cases hvm with hvm1 hvm2,
    exact hvm2,
  end
#check sublist_valid_map



lemma insert_valid_map_aux1 {a x: α} {b y: β} {c : list (α × β)} [ha : linear_order α] :
  x < a → valid_map ((x, y) :: c) → valid_map (insert_map a b c) → 
  valid_map ((x, y) :: (insert_map a b c)):=
  begin
    intros hs hvm1 hvm2,
    induction c with z c,
    have h : insert_map a b list.nil = ((a,b) :: list.nil), from rfl,
    rw h,
    rw h at hvm2,
    rw valid_map,
    simp,
    exact hs,

    let u:= z.fst,
    let v:= z.snd,
    have hz : z = (u, v), by simp,
    rw hz,
    have hzvm : valid_map (z :: c),
    {
      apply sublist_valid,
      exact hvm1,
    },
    rw insert_map,
    split_ifs,
    {
      simp,
      simp at hzvm,
      split,
      {
        intro b1,
        split,
        {
          intro he,
          rw he,
          exact hs,
        },
        {
          split,
          {
            intro he,
            rw he,
            transitivity,
            exact hs,
            exact h,
          },
          {
            intros x1 hbin,
            cases hzvm with hzvm1 hzvm2,
            have hxu : x < u,
            {
              transitivity,
              exact hs,
              exact h,
            },
            transitivity,
            exact hxu,
            apply hzvm1,
            exact hbin,
          },
        },
      },
      {
        split,
        {
          intro b1,
          split,
          {
            intro he,
            rw he,
            exact h,
          },
          {
            intros x1 hbin,
            cases hzvm with hzvm1 hzvm2,
            transitivity,
            exact h,
            apply hzvm1,
            exact hbin,
          },
        },
        {
          exact hzvm,
        },
      },
    },
    {
      apply insert_in_middle_map,
      apply remove_from_middle_map,
      rw hz at hvm1,
      exact hvm1,
      simp,
      simp at hvm2,
      rw hz at hvm2,
      rw insert_map at hvm2,
      split_ifs at hvm2,
      simp at hvm2,
      cases hvm2 with hvm21 hvm22,
      split,
      {
        exact hvm21,
      },
      {
        exact hvm22,
      },
      exact v,
    },
    {
      have he : a = u,
      {
        have ht := decidable.lt_trichotomy a u,
        cases ht with hl ht,
        contradiction,
        cases ht with he hg,
        exact he,
        contradiction,
      },
      apply insert_in_middle_map,
      rw he at hs,
      exact hs,
      rw hz at hzvm,
      exact hzvm,
    },
  end
#check insert_valid_map_aux1

lemma insert_valid_map_aux2 {a x: α} {b y: β} {c : list (α × β)} [ha : linear_order α] : 
  valid_map c → valid_map (insert_map a b c) → valid_map ((x, y) :: c) → 
  x < a → valid_map (( x, y) :: insert_map a b c):=
  begin
    intros hvm1 hvm2 hvm3 hx,
    exact insert_valid_map_aux1 hx hvm3 hvm2,
  end
#check insert_valid_map_aux2

lemma insert_valid_map_aux3 (a x: α) (b y: β) (c : list (α × β))(ha : linear_order α) :
  valid_map c → valid_map (insert_map a b c) → valid_map ((x, y) ::c) → 
  valid_map (insert_map a b ((x,y) :: c)):=
  begin
    intros hvm1 hvm2 hvm3,
    rw insert_map,
    split_ifs with hl hg,
    {
      apply insert_in_middle_map,
      exact hl,
      exact hvm3,
    },
    {
      apply insert_valid_map_aux2,
      exact hvm1,
      exact hvm2,
      exact hvm3,
      simp at hg,
      exact hg,
    },
    {
      simp,
      simp at hvm3,
      exact hvm3,
    },
  end
#check insert_valid_map_aux3

lemma insert_valid_map {a: α} {b: β} {c : list (α × β)} [ha : linear_order α] :
  valid_map c → valid_map (insert_map a b c) :=
  begin
    intro hvm,
    induction c with z c,
    have hin : insert_map a b list.nil = ((a, b) ::list.nil), from rfl,
    rw hin,
    simp,

    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    have hvmu : valid_map ((u, v) :: c),
    {
      rw hz at hvm,
      exact hvm,
    },
    have hc := sublist_valid_map hvmu,
    rw hz,
    exact insert_valid_map_aux3 a u b v c ha hc (c_ih hc) hvmu,
  end
#check insert_valid_map

-- lemma made to simplify future lemmas
lemma insert_valid_map_aux {a z : (α × β)} {c : list (α × β)} [ha : linear_order α] :
  valid_map (a :: z :: c) → valid_map (a :: c) :=
  begin
    intro hvm,
    simp at hvm,
    cases hvm with hvm1 hvm2,
    cases hvm2 with hvm2 hvm3,
    simp,
    split,
    {
      intros b1 x1 hbin,
      specialize hvm1 b1,
      cases hvm1 with hvm11 hvm12,
      specialize hvm12 x1,
      apply hvm12,
      exact hbin,
    },
    {
      exact hvm3,
    },
  end
#check insert_valid_map_aux

/-# Delete_map function and lemmas-/

def delete_map [ha : linear_order α] : α → list (α × β) → list (α × β) 
  |a list.nil :=  (list.nil)
  |a ((x, y) :: z) := (if a = x  then (z) else
    (if a > x then ((x, y) :: (delete_map a z)) else ((x, y) :: z))) 
#check delete_map

lemma delete_length {a : α} {b : list (α × β)} [ha : linear_order α] : list.length (delete_map a b) ≤ list.length b :=
  begin
    induction b with z b,
    rw delete_map,

    simp,
    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    rw hz,
    rw delete_map,
    split_ifs,
    simp,
    simp,
    exact b_ih,
    simp,
  end
#check delete_length

-- Delete_map is a subset of the list it is deleting from
lemma delete_map_subset {a : α} {c : list (α × β)} [ha : linear_order α] : delete_map a c ⊆ c :=
  begin
    induction c with z c,
    rw delete_map,
    refl,
    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    rw hz,
    rw delete_map,
    split_ifs,
    {
      simp,
    },
    {
      simp,
      have hc : c ⊆ z :: c, by simp,
      transitivity,
      exact c_ih,
      exact hc,
    },
    {
      refl,
    },
  end
#check delete_map_subset

lemma delete_valid_map_aux1 {a: α} {b : α × β} {c : list (α × β)} [ha : linear_order α] :
  valid_map (b :: c) → valid_map (b :: delete_map a c) :=
  begin
    intro hvm,
    induction c with z c,
    rw delete_map,
    exact hvm,

    let u := z.fst,
    let v := z.snd,
    have hz : z = (u,v), by simp,
    let x := b.fst,
    let y := b.snd,
    have hb : b = (x, y), by simp,
    have hvz : valid_map (z :: c),
    {
      rw hb at hvm,
      apply sublist_valid_map,
      exact hvm,
    },    
    have hvb : valid_map (b :: c),
    {
      rw hb,
      simp,
      rw valid_map at hvz,
      simp at hvz,
      cases hvz with hvz1 hvz2,
      split,
      {
        intros b1 x1 hbin,
        have hl : x < u,
        {
          rw hb at hvm,
          rw hz at hvm,
          apply remove_from_middle_map,
          exact hvm,
        },
        transitivity,
        exact hl,
        apply hvz1,
        exact hbin,
      },
      {
        exact hvz2,
      },
    },
    specialize c_ih hvb,
    rw hz,
    rw hb,
    rw delete_map,
    split_ifs,
    {
      exact hvb,
    },
    {
      simp at h_1,
      have hl := remove_from_middle_map hvm,
      apply insert_in_middle_map,
      exact hl,
      rw valid_map,
      simp,
      rw valid_map at c_ih,
      simp at c_ih,
      cases c_ih with hc1 hc2,
      split,
      {
        intros b1 x1 hbin,
        rw valid_map at hvz,
        simp at hvz,
        cases hvz with hvz1 hvz2,
        apply hvz1,
        apply delete_map_subset,
        exact hbin,
      },
      {
        exact hc2,
      },
      repeat{exact v,},
    },
    {
      rw hz at hvm,
      exact hvm,
    },
  end

#check delete_valid_map_aux1 

lemma delete_valid_map_aux2 {a : α} {b : α × β} {c : list (α × β)} [ha : linear_order α] :
  valid_map c → valid_map (delete_map a c) → valid_map (b :: c) →
  valid_map (delete_map a (b ::c)):=
  begin
    intros hvm1 hvm2 hvm3,
    let x := b.fst,
    let y := b.snd,
    have hb : b = (x, y), by simp,
    rw hb,
    rw delete_map,
    split_ifs,
    exact hvm1,
    apply delete_valid_map_aux1,
    repeat 
    {
      rw ← hb,
      exact hvm3,
    },
  end
#check delete_valid_map_aux2

theorem delete_valid_map {a : α} {c : list (α × β)} [ha : linear_order α] :
  valid_map c → valid_map (delete_map a c):=
  begin
    intro hvm,
    induction c with b c,
    simpa,
    let u := b.fst,
    let v := b.snd,
    have hb : b = (u, v), by simp,
    rw hb at hvm,
    have hvmc := sublist_valid_map hvm,
    exact delete_valid_map_aux2 hvmc (c_ih hvmc) hvm,
  end
#check delete_valid_map

lemma delete_step {k k2: α} {v : β} {t : list (α × β)} [ha : linear_order α] :
  valid_map ((k, v) :: t) → ¬ k2 = k →
  delete_map k2 ((k, v) :: t) = ((k, v)::(delete_map k2 t)):=
  begin
    intros hvm hne,
    induction t with z t,
    have hd : delete_map k2 list.nil = list.nil, from rfl,
    exact β,
    rw hd,
    rw delete_map,
    split_ifs,
    {
      split,
      refl,
      exact hd,
    },
    {
      split,
      repeat {refl,},
    },
    {
      rw delete_map,
      split_ifs,
      split,
      repeat {refl,},
      split,
      {
        refl,
      },
      {
        -- set
        let x := z.fst,
        let y := z.snd,
        have hz : z = (x, y), by simp,
        have hl : k2 < k,
        {
          have ht := decidable.lt_trichotomy k2 k,
          cases ht with hl ht,
          exact hl,
          cases ht with he hg,
          repeat{contradiction,},
        },
        rw hz,
        rw hz at hvm,
        have hkl := remove_from_middle_map hvm,
        have hk2l : k2 < x,
        {
          transitivity,
          exact hl,
          exact hkl,
        },
        have hk2ne := ne_of_lt hk2l,
        have hk2ng := asymm hk2l,
        rw delete_map,
        split_ifs,
        {
          contradiction,
        },
        {
          split,
          repeat {refl,},
        },
      },
    },
  end
#check delete_step


/-# Lookup function and associated lemmas-/

--lookup function
def lookup [ha : linear_order α] : α → list (α × β) → option β 
  |a list.nil :=  none
  |a ((x, y) :: z) := (if a = x  then (some y) else
    (if a > x then (lookup a z) else (none))) 
#check lookup

lemma lookup_empty {y : α} [ha : linear_order α] : lookup y (emptyMap α β) = none :=
  begin
    rw emptyMap,
    refl,
  end
#check lookup_empty

lemma insert_existing_length {k : α} {v a : β} {l : list (α × β)} [ha : linear_order α] : 
  lookup k l = some a → list.length (insert_map k v l) = list.length l:=
  begin
    intro hl,
    induction l with z l,
    rw lookup at hl,
    simp at hl,
    rw insert_map,
    simp,
    exact hl,

    simp,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz at hl,
    --Look here for future
    have hknl : ¬ k < x,
    {
      by_contradiction,
      have hkng := asymm h,
      have hkne := ne_of_lt h,
      simp at hkne,
      rw lookup at hl,
      split_ifs at hl,
      exact hl,
    },
    rw hz,
    rw insert_map,
    split_ifs,
    {
      simp,
      apply l_ih,
      rw ← hl,
      rw lookup,
      have hkne := ne_of_gt h,
      simp at hkne,
      split_ifs,
      refl,
    },
    {
      simp,
    },
  end
#check insert_existing_length

lemma delete_lookup_None_aux1 {c : α} {d : β} {b : list (α × β)} [ha : linear_order α] : 
  valid_map ((c, d) :: b) → lookup c b = none :=
  begin
    intro hvm,
    induction b with z b,
    rw lookup,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz,
    rw hz at hvm,
    have hl := remove_from_middle_map hvm,
    rw lookup,
    have hne := ne_of_lt hl,
    simp at hne,
    have hng := asymm hl,
    split_ifs,
    refl,
  end
#check delete_lookup_None_aux1

lemma delete_lookup_None_aux2 {a c : α} {d : β} {b : list (α × β)} [ha : linear_order α] :
  (valid_map b → lookup a (delete_map a b) = none ) → valid_map ((c, d) :: b) → 
  lookup a (delete_map a ((c, d) :: b)) = none :=
  begin
    intros himp hvm,
    have ht := decidable.lt_trichotomy a c,
    cases ht with hl ht,
    {
      have hne := ne_of_lt hl,
      simp at hne,
      have hng := asymm hl,
      rw delete_map,
      split_ifs,
      rw lookup,
      split_ifs,
      refl,
    },
    {
      cases ht with he hg,
      {
        rw delete_map,
        split_ifs,
        apply delete_lookup_None_aux1,
        rw he,
        exact hvm,
      },
      {
        rw delete_map,
        have hne := ne_of_gt hg,
        simp at hne,
        split_ifs,
        rw lookup,
        split_ifs,
        apply himp,
        apply sublist_valid_map,
        exact hvm,
      },
    },
  end
#check delete_lookup_None_aux2

theorem delete_lookup_None {a : α} {b : list ( α × β)} [ha : linear_order α] : valid_map b → 
  lookup a (delete_map a b) = none:=
  begin
    intro hvm,
    induction b with z b,
    rw delete_map,
    refl,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz,
    apply delete_lookup_None_aux2,
    exact b_ih,
    rw hz at hvm,
    exact hvm,
  end
#check delete_lookup_None

theorem insert_lookup_some {a : α} {b : β} {c :list (α × β)} [ha : linear_order α] :
  lookup a (insert_map a b c) = some b:=
  begin
    
    have ha : a = a, from rfl,
    induction c with z c,
    rw insert_map,
    rw lookup,
    split_ifs,
    refl,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz,
    rw insert_map,
    have ht := decidable.lt_trichotomy a x,
    cases ht with hl ht,
    {
      split_ifs,
      rw lookup,
      split_ifs,
      refl,
      rw lookup,
      split_ifs,
      refl,
    },
    {
      cases ht with he hg,
      {
        have hnlg : ¬ (a < x) ∧ ¬ (x < a), 
        {
          rw incomp_iff_eq,
          exact he,
        },
        cases hnlg with hnl hng,
        split_ifs,
        rw lookup,
        split_ifs,
        refl,
      },
      {
        have hnl := asymm hg,
        split_ifs,
        rw lookup,
        split_ifs,
        refl,
        have hne := ne_of_gt hg,
        simp at hne,
        rw lookup,
        split_ifs,
        exact c_ih,
      },
    },
  end
#check insert_lookup_some

theorem insert_lookup_different {a b : α} {c : β} {d : list (α × β)} [ha : linear_order α] :
  a ≠ b → lookup a (insert_map b c d) = lookup a d:=
  begin
    intro hne,
    simp at hne,
    induction d with z d,
    rw insert_map,
    rw lookup,
    split_ifs,
    repeat{refl,},
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz,
    rw insert_map,
    have ht := decidable.lt_trichotomy b x,
    cases ht with hl ht,
    {
      split_ifs,
      rw lookup,
      split_ifs,
      refl,
      rw lookup,
      have halb : a < b,
      {
        have htr := decidable.lt_trichotomy a b,
        cases htr with hl htr,
        exact hl,
        cases htr with he hg,
        repeat{contradiction,},
      },
      have hal : a < x,
      {
        transitivity,
        exact halb,
        exact hl,
      },
      split_ifs,
      have hnx := ne_of_lt hal,
      contradiction,
      have hnxg := asymm hal,
      contradiction,
      refl,
      contradiction,
    },
    {
      cases ht with he hg,
      {
        have hnlg : ¬ (b < x) ∧ ¬ (x < b), 
        {
          rw incomp_iff_eq,
          exact he,
        },
        cases hnlg with hnl hng,
        split_ifs,
        rw lookup,
        split_ifs,
        rw ← he at h,
        contradiction,
        rw lookup,
        split_ifs,
        refl,
        rw lookup,
        split_ifs,
        refl,
      },
      {
        have hnl := asymm hg,
        split_ifs,
        contradiction,
        rw lookup,
        split_ifs,
        rw lookup,
        split_ifs,
        refl,
        rw lookup,
        split_ifs,
        exact d_ih,
        rw lookup,
        split_ifs,
        refl,
      },
    },
  end
#check insert_lookup_some

lemma different_delete_lookup_aux {a x b : α} {y : β} {c : list (α × β)} [ha : linear_order α] :
  (valid_map c → a ≠ b → lookup a (delete_map b c) = lookup a c) → valid_map ((x, y) :: c) → 
  a ≠ b → lookup a (delete_map b ((x, y) ::c )) = lookup a ((x, y ) ::c):=
  begin
    intros himp hvm1 hne,
    have hvm2 := sublist_valid_map hvm1,
    specialize himp hvm2,
    specialize himp hne,
    rw delete_map,
    split_ifs,
    {
      rw lookup,
      split_ifs,
      {
        rw ← h at h_1,
        contradiction,
      },
      {
        refl,
      },
      {
        have hl : a < x,
        {
          have ht := decidable.lt_trichotomy a x,
          cases ht with hl ht,
          exact hl,
          cases ht with he hg,
          repeat{contradiction,},
        },
        rw ← h at hl,
        rw ← h at hvm1,
        apply delete_lookup_None_aux1,
        rw valid_map,
        simp,
        rw valid_map at hvm1,
        simp at hvm1,
        cases hvm1 with hvm11 hvm12,        
        split,
        intros b1 x1 hbin,
        have hbl : b < b1,
        {
          apply hvm11,
          exact hbin,
        },
        transitivity,
        exact hl,
        exact hbl,
        exact hvm12,
        exact y,
      },
    },
    {
      rw lookup,
      split_ifs,
      rw lookup,
      split_ifs,
      refl,
      rw himp,
      rw lookup,
      split_ifs,
      refl,
      rw lookup,
      split_ifs,
      refl,
    },
    {
      refl,
    },
  end
#check different_delete_lookup_aux

theorem different_delete_lookup {a b : α} {c : list (α × β)} [ha : linear_order α] :
  valid_map c → a ≠ b → lookup a (delete_map b c) = lookup a c:=
  begin
    intros hvm1 hne,
    induction c with z c,
    rw delete_map,
    let x := z.fst,
    let y := z.snd,
    have hz : z = (x, y), by simp,
    rw hz,
    have H := different_delete_lookup_aux _ hvm1 hne,
    exact H,
    intros h1 h2,
    apply c_ih,
    exact h1,
  end
#check different_delete_lookup


/-# unionWith function and associated lemmas-/

def unionWith [ha : linear_order α]: (β → β → β) → list (α × β) → list (α × β) →  list (α × β) 
  | f ((x, y) :: t) ((x2, y2) :: t2) := (if (x < x2) then ((x, y) :: (unionWith f t ((x2, y2) :: t2)))
    else (if (x > x2) then ((x2, y2) :: unionWith f ((x, y) :: t) t2) else ((x, f y y2) :: unionWith f t t2)))
  | f list.nil l := l
  | f l list.nil := l
#check unionWith

@[simp]lemma unionWithNil {t: list (α × β)} {f : β → β → β} [ha : linear_order α] :
unionWith f list.nil t = t :=
begin
  induction t with z t,
  rw unionWith,
  let x := z.fst,
  let y := z.snd,
  have hz : z = (x, y), by simp,
  rw hz,
  rw unionWith,
end
#check unionWithNil

@[simp]lemma nilUnionWith {t: list (α × β)} {f : β → β → β} [ha : linear_order α] :
unionWith f t list.nil = t :=
begin
  induction t with z t,
  rw unionWith,
  let x := z.fst,
  let y := z.snd,
  have hz : z = (x, y), by simp,
  rw hz,
  rw unionWith,
end
#check nilUnionWith

#check list.head
#check prod.fst


--Looking more at this one
lemma unionWithMonotonic1 {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α]:
  x < x2 → ( list.head (list.map prod.fst ((unionWith f ((x, y) :: t) ((x2, y2) :: t2) ) ) ) )= x :=
  begin
    intro hl,
    rw unionWith,
    split_ifs,
    simp,
  end
#check unionWithMonotonic1

--Rather than this one
lemma unionWithMonotonic2 (x x2 : α)(y y2 : β)(t t2 : list (α × β))(ha : linear_order α)(f : β → β → β) (hi : inhabited (α × β)):
  x < x2 → prod.fst( list.head ((unionWith f ((x, y) :: t) ((x2, y2) :: t2) ) ) )= x :=
  begin
    intro hl,
    rw unionWith,
    split_ifs,
    rw list.head,
  end
#check unionWithMonotonic2

-- This one does not need an inhabited list, outputs option not type however
lemma unionWithMonotonic3 (x x2 : α)(y y2 : β)(t t2 : list (α × β))(ha : linear_order α)(f : β → β → β) :
  x < x2 → list.head' (list.map prod.fst ((unionWith f ((x, y) :: t) ((x2, y2) :: t2)))) = x :=
  begin
    intro hl,
    rw unionWith,
    split_ifs,
    simp,
    refl,
  end
#check unionWithMonotonic3

#check list.head

lemma insert_before {x : α} {y : β} {c : list (α × β)} [ha : linear_order α] [hi : inhabited α] :
  valid_map c → x < (list.head (list.map prod.fst (c))) → valid_map ((x, y ) :: c) :=
  begin
    intros hvm hl,
    induction c with z c,
    rw valid_map,
    simp,
    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    have hvc := sublist_valid_map hvm,
    specialize c_ih hvc,
    simp at hl,
    apply insert_in_middle_map,
    exact y,
    exact hl,
    exact hvm,
    repeat{exact v,},
  end
#check insert_before

lemma insert_before_union1 {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x < x2 → valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → 
  x < (list.head (list.map prod.fst (unionWith f t ((x2, y2) :: t2)))):=
  begin
    intros hl hvm1 hvm2,
    induction t with z t,
    rw unionWith,
    simp,
    exact hl,
    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    rw hz,    
    rw unionWith, 
    split_ifs,
    {
      simp,
      rw hz at hvm1,
      exact remove_from_middle_map hvm1,
    },
    {
      simp,
      exact hl,
    },
    {
      simp,
      have he : u = x2,
      {
        have ht := decidable.lt_trichotomy u x2,
        cases ht with hl ht,
        contradiction,
        cases ht with he hg,
        exact he,
        contradiction,
      },
      rw ← he at hl,
      exact hl,
    },
  end
#check insert_before_union1

lemma insert_before_union2 {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x2 < x → valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → 
  x2 < (list.head (list.map prod.fst (unionWith f ((x, y) :: t) t2))):=
  begin
    intros hl hvm1 hvm2,
    induction t2 with z t2,
    rw unionWith,
    simp,
    exact hl,
    let u := z.fst,
    let v := z.snd,
    have hz : z = (u, v), by simp,
    rw hz,
    rw unionWith,
    split_ifs,
    {
      simp,
      exact hl,
    },
    {
      simp,
      rw hz at hvm2,
      exact remove_from_middle_map hvm2,
    },
    {
      exact hl,
    },
  end
#check insert_before_union2
#check inhabited

#check list.ne_nil_of_mem
#check list.reduce_option_nil
#check tactic.interactive.squeeze_simp
#check tactic.interactive.finish
#check auto.finish


lemma insert_before_union3 {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → (t ≠ [] ∨ t2 ≠ []) → 
  x < (list.head (list.map prod.fst (unionWith f t t2))):=
  begin
    intros hvm1 hvm2 hor,
    induction t,
    {
      simp at hor,
      induction t2,
      {
        exfalso,
        apply hor,
        refl,
      },
      {
        cases t2_hd,
        rw unionWith,
        simp,
        specialize t2_ih (insert_valid_map_aux hvm2),
        sorry,
      },
    },
    sorry,
  end
#check insert_before_union3

lemma unionWithValidLT_aux {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x < x2 → valid_map (unionWith f t ((x2, y2) :: t2)) → valid_map ((x, y) :: t) → 
  valid_map ((x2, y2) :: t2) → valid_map ((x, y) :: unionWith f t ((x2, y2) :: t2)):=
  begin
    intros hl hvm1 hvm2 hvm3,
    apply insert_before,
    exact hvm1,
    apply insert_before_union1,
    exact hl,
    exact hvm2,
    exact hvm3,
  end
#check unionWithValidLT_aux

lemma unionWithValidLT {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x < x2 → (valid_map t → valid_map ((x2, y2) :: t2) → valid_map (unionWith f  t ((x2, y2) :: t2))) →
  valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → valid_map (unionWith f ((x, y) :: t) ((x2, y2) ::t2)) :=
  begin
    intros hl himp hvm1 hvm2,
    have hvmt := sublist_valid_map hvm1,
    specialize himp hvmt,
    specialize himp hvm2,
    have hs : valid_map ((x, y) :: unionWith f t ((x2, y2) :: t2)),
    {
      apply unionWithValidLT_aux,
      exact hl,
      exact himp,
      exact hvm1,
      exact hvm2,
    },
    rw unionWith,
    split_ifs,
    exact hs,
  end
#check unionWithValidLT

lemma unionWithValidGT_aux {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x2 < x → valid_map (unionWith f ((x, y) :: t) t2) → valid_map ((x, y) :: t) → 
  valid_map ((x2, y2) :: t2) → valid_map ((x2, y2) :: unionWith f ((x, y) ::t) t2) :=
  begin
    intros hl hvm1 hvm2 hvm3,
    apply insert_before,
    exact hvm1,
    apply insert_before_union2,
    exact hl,
    exact hvm2,
    exact hvm3,
  end
#check unionWithValidGT_aux

lemma unionWithValidGT {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  x2 < x → (valid_map ((x, y) :: t) → valid_map t2 → valid_map (unionWith f ((x, y) :: t) t2)) → 
  valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → valid_map (unionWith f ((x, y) :: t) ((x2, y2) :: t2)) :=
  begin
    intros hl himp hvm1 hvm2,
    specialize himp hvm1,
    have hvmt := sublist_valid_map hvm2,
    specialize himp hvmt,
    have hs : valid_map ((x2, y2) :: unionWith f ((x, y) ::t) t2),
    {
      apply unionWithValidGT_aux,
      exact hl,
      exact himp,
      exact hvm1,
      exact hvm2,
    },
    have hng := asymm hl,
    rw unionWith,
    split_ifs,
    exact hs,
  end
#check unionWithValidGT

#check list.ne_nil_of_mem
#check insert_valid_map

lemma unionWithValidEQ_aux{x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  valid_map (unionWith f t t2) → valid_map ((x, y) :: t) → valid_map ((x2, y2) :: t2) → 
  valid_map ((x, f y y2) :: unionWith f t t2) :=
  begin
    intros hvm1 hvm2 hvm3,
    apply insert_before,
    exact hvm1,
    apply insert_before_union3,
    exact hvm2,
    exact hvm3,
    

    induction t with z t,
    induction t2 with w t2,
    rw unionWith at hvm1,
    simp at hvm3,
    sorry,
    right, 
    simp,
    left,
    simp,
  end
#check unionWithValidEQ_aux

lemma unionWithValidEQ {x x2 : α} {y y2 : β} {t t2 : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  (valid_map t → valid_map t2 → valid_map (unionWith f t t2)) → valid_map ((x, y) :: t) → 
  valid_map ((x, y2) :: t2) → valid_map (unionWith f ((x, y) :: t) ((x, y2) :: t2)):=
  begin
    intros himp hvm1 hvm2,
    
    have hvmt := sublist_valid_map hvm1,
    have hvmt2 := sublist_valid_map hvm2,
    have hs : valid_map ((x, f y y2) :: unionWith f t t2),
    {
      apply unionWithValidEQ_aux,
      apply himp,
      exact hvmt,
      exact hvmt2,
      exact hvm1,
      exact hvm2,
    },
    rw unionWith,
    split_ifs,
    have hn : ¬ x < x, by simp,
    contradiction,
    exact hs,
  end
#check unionWithValidEQ

theorem unionWithValid {a b : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  valid_map a → valid_map b → valid_map (unionWith f a b) :=
  begin
    intros hvma hvmb,
    induction a,
    {
      induction b,
      {
        rw unionWith,
        exact hvma,
      },
      {
        cases b_hd,
        rw unionWith,
        exact hvmb,
      },
    },
    {
      induction b,
      {
        cases a_hd,
        rw unionWith,
        exact hvma,
      },
      {
        cases a_hd,
        cases b_hd,
        have ht := decidable.lt_trichotomy a_hd_fst b_hd_fst,
        cases ht with hl ht,
        {
          apply unionWithValidLT,
          exact hl,
          intros hvmatl hvmbcons,
          exact a_ih hvmatl,
          exact hvma,
          exact hvmb,
        },
        {
          cases ht with he hg,
          {
            rw he,
            apply unionWithValidEQ,
            exact a_hd_fst,
            intros hvmatl hvmbtl,

            sorry,
            rw ← he,
            exact hvma,
            exact hvmb,
          },
          {
            apply unionWithValidGT,
            exact hg,
            intros hvmacons hvmbtl,
            apply b_ih,
            exact hvmbtl,
            intro hvmatl,

            sorry,
            exact hvma,
            exact hvmb,
          },
        },
      },
    },
  end
#check unionWithValid

#check list.remove_nth

theorem unionWithSym {a b : list (α × β)} {f : β → β → β} [ha : linear_order α] [hi : inhabited α] :
  valid_map a → valid_map b → unionWith f a b = unionWith (flip f) b a :=
  begin
    intros hvma hvmb,
    induction a,
    {
      induction b,
      {
        simp,
      },
      {
        simp,
      },
    },
    {
      induction b,
      {
        simp,
      },
      {
        cases a_hd,
        cases b_hd,
        
        sorry,  
      },
    },
  end
#check unionWithSym

/-
def unionWith (ha : linear_order α): (β → β → β) → list (α × β) → list (α × β) →  list (α × β) 
  | f ((x, y) :: t) ((x2, y2) :: t2) := (if (x < x2) then ((x, y) :: (unionWith f t ((x2, y2) :: t2)))
    else (if (x > x2) then ((x2, y2) :: unionWith f ((x, y) :: t) t2) else ((x, f y y2) :: unionWith f t t2)))
  | f list.nil l := l
  | f l list.nil := l
#check unionWith

-/



def findWithDefault [ha : linear_order α] :β → α → list (α × β) → β
  | d k l := (match (lookup k l) with
              | none := d
              | some x := x
              end) 
#check findWithDefault



lemma findWithDefault_step {k k2 : α} {v d : β} {tail : list (α × β)} [ha : linear_order α] :
  valid_map ((k, v) :: tail) → k2 ≠ k → findWithDefault d k2 ((k, v) :: tail) = findWithDefault d k2 tail:=
  begin
    intros hvm hne,
    rw findWithDefault,
    rw lookup,
    split_ifs,
    {
      contradiction,
    },
    {
      rw findWithDefault,
    },
    {
      rw findWithDefault,
      induction tail,
      rw lookup,
      cases tail_hd,
      rw lookup,
      specialize tail_ih (insert_valid_map_aux hvm),
      split_ifs,
      {
        rw findWithDefault._match_1,
        rw findWithDefault._match_1,
        have H : k > k2,
        {
          have ht := decidable.lt_trichotomy k k2,
          cases ht with hl ht,
          {
            contradiction,
          },
          {
            cases ht with he hg,
            {
              have hsymm : k2 = k,
              {
                symmetry,
                exact he,
              },
              contradiction,
            },
            {
              exact hg,
            },
          },
        },
        rw ← h_2 at hvm,
        have hcontra := remove_from_middle_map hvm,
        contradiction,
      },
      {
        exact tail_ih,
      },
      {
        refl,
      },
    },
  end
#check findWithDefault_step

def member [ha : linear_order α] : α → list (α × β) → Prop :=
  λ k d, (lookup k d ≠ none)
#check member

fun unionWith :: "('b ⇒ 'b ⇒ 'b) ⇒ ('a × 'b) list ⇒
                  ('a × 'b) list ⇒ (('a::linorder) × 'b) list" where
  "unionWith f (Cons (x, y) t) (Cons (x2, y2) t2) =
    (if x < x2
    then Cons (x, y) (unionWith f t (Cons (x2, y2) t2))
    else (if x > x2
         then Cons (x2, y2) (unionWith f (Cons (x, y) t) t2)
         else Cons (x, f y y2) (unionWith f t t2)))" |
  "unionWith f Nil l = l" |
  "unionWith f l Nil = l"

lemma insert_before_union3 :
  "valid_map ((x, y) # t) ⟹
   valid_map ((x, y2) # t2) ⟹
   (t ≠ [] ∨ t2 ≠ []) ⟹
   x < fst ( hd ( unionWith f t t2 ) )"
  apply (induction t)
  apply (metis list.collapse prod.collapse remove_from_middle unionWith.simps(2))
  apply (induction t2)
  apply auto[1]
  by auto

lemma unionWithValidEQ_aux :
  "(valid_map (unionWith f t t2)) ⟹
   valid_map ((x, y) # t) ⟹
   valid_map ((x, y2) # t2) ⟹
   valid_map ((x, f y y2) # unionWith f t t2)"
  by (metis insert.simps(1) insert_before insert_before_union3 insert_valid unionWith.simps(2))

lemma unionWithValidEQ :
  "(valid_map t ⟹ valid_map t2 ⟹ valid_map (unionWith f t t2)) ⟹
   valid_map ((x, y) # t) ⟹
   valid_map ((x, y2) # t2) ⟹
   valid_map (unionWith f ((x, y) # t) ((x, y2) # t2))"
  apply (simp only:unionWith.simps sublist_valid)
  by (smt order.asym unionWithValidEQ_aux)

theorem unionWithValid : "valid_map a ⟹ valid_map b ⟹
                          valid_map (unionWith f a b)"
  apply (induction f a b rule:unionWith.induct)
  apply (metis less_linear unionWithValidEQ unionWithValidGT unionWithValidLT)
  by auto

theorem unionWithSym : "valid_map a ⟹ valid_map b ⟹
                        unionWith f a b = unionWith (flip f) b a"
  apply (induction f a b rule:unionWith.induct)
  apply auto[1]
  apply (metis list.exhaust unionWith.simps(2) unionWith.simps(3))
  by simp

end MList
