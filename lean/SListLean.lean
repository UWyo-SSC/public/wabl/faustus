import .lovelib
import data.list.sort
import data.list.basic

namespace SList

--Checking
#check list.sorted
#check list.nil
#check list.cons
#check equiv.list_equiv_of_equiv
#check linear_order
#check true_eq_false_of_false

-- Prop are true or false (similar to booleans)
#check true

-- Showing (<) works as our relation for pairwise
#check (<)
universe u

-- Definition of a Valid Set
@[simp]def valid_set {α : Type u} [ha : linear_order α]: list α → Prop :=
  λ a, list.sorted (<) a 
#check valid_set 

-- Variables
variables {α : Type u}{ha : linear_order α}

-- Making definition of empty list
def emptyListLinear (ha : linear_order α): list α := list.nil
#check emptyListLinear

-- The empty list is valid
lemma valid_empty: valid_set (emptyListLinear ha):=
  begin
    exact list.sorted_nil,
  end
#check valid_empty



/-# Insert function and associated lemmas-/

-- Insert function
def insert [ha : linear_order α] : α → list α → list α 
  |a list.nil :=  (a :: list.nil)
  |a (x :: z) := (if a < x  then (a :: x :: z) else
    (if a > x then (x :: (insert a z)) else (x :: z))) 
#check insert

-- Properties of Insert function
lemma insert_in_middle {x a: α} {z : list α} [ha: linear_order α] :
  x < a → valid_set (a :: z) → valid_set (x :: a :: z) :=
  begin
    intros hx hvs,
    unfold valid_set,
    simp,
    split,
    {
      intro b,
      split,
      {
        intro he,
        rw he,
        exact hx,
      },
      {
        intro hz,
        have h1 := list.rel_of_sorted_cons hvs b hz,
        transitivity,
        exact hx,
        exact h1,
      },
    },
    {
      split,
      {
        apply list.rel_of_sorted_cons,
        exact hvs,
      },
      {
        unfold valid_set at hvs,
        apply list.sorted_of_sorted_cons,
        exact hvs,
      },
    },
  end
#check insert_in_middle


-- Removing an element from a list
lemma remove_from_middle {x a: α} {z : list α} [ha: linear_order α] : 
  valid_set (x :: a :: z) → 
  x < a :=
  begin
    intro hvs,
    unfold valid_set at hvs,
    apply list.rel_of_sorted_cons,
    exact hvs,
    simp,
  end
#check remove_from_middle

-- Determining if a sublist is valid
lemma sublist_valid {x : α} {c : list α} [ha : linear_order α] : 
  valid_set (x :: c) → valid_set c :=
  begin
    intro hvs,
    exact list.sorted_of_sorted_cons hvs,
  end
#check sublist_valid

-- auxiliary lemmas for insert
lemma insert_valid_aux1 {x a: α} {c : list α} [ha : linear_order α] : 
x < a → valid_set (x :: c) → valid_set (insert a c) →
valid_set (x :: insert a c) :=
begin
  intros hx hvs1 hvs2,
  induction c with z c,
  have h1 : insert a list.nil = [a], from rfl,
  rw h1,
  rw h1 at hvs2,
  simp,
  exact hx,
 
  have hzc := sublist_valid hvs1,
  have hxlz := remove_from_middle hvs1, 
  have H := decidable.lt_trichotomy a z,
  cases H with hl H,
  swap,
  cases H with he hg,

  have he1 : insert a (z :: c) = (z :: c),
  {
    rw insert,
    have he2 : ¬ (a < z) ∧ ¬ (z < a), 
    {
      rw incomp_iff_eq,
      exact he,
    },
    cases he2 with he21 he22,
    split_ifs,
    split,
    repeat {refl,},
  },
  rw he1,
  exact hvs1,

  have hg1 : insert a (z :: c) = list.cons z (insert a c),
  {
    rw insert,
    have hg2 := asymm hg,
    split_ifs,
    contradiction,
    split,
    repeat {refl,},
  },
  rw hg1,
  apply insert_in_middle,
  exact hxlz,
  rw hg1 at hvs2,
  exact hvs2,

  have hl1 : insert a (z :: c) = (a :: z :: c),
  {
    rw insert,
    split_ifs,
    repeat {split, refl,},
    repeat {refl,},
  },
  rw hl1,
  apply insert_in_middle,
  exact hx,
  rw hl1 at hvs2,
  exact hvs2,
end
#check insert_valid_aux1

lemma insert_valid_aux2 {x : α} {c : list α} [ha : linear_order α] :
∀ (a : α), valid_set c → valid_set (insert a c) → valid_set ( x :: c) →
x < a → valid_set (x :: insert a c) :=
 begin
    intros a hvs1 hvs2 hvs3 hx,
    exact insert_valid_aux1 hx hvs3 hvs2,
 end
#check insert_valid_aux2

lemma insert_valid_aux3 {x : α} {c : list α} [ha : linear_order α]:
∀ (a : α), valid_set c → valid_set (insert a c) → valid_set ( x :: c) →
valid_set (insert a (x :: c)) :=
  begin
    intros a hvs1 hvs2 hvs3,
    have ht := decidable.lt_trichotomy x a,
    cases ht with hl ht,
    {
      have hl1 : insert a (x :: c) = (x :: (insert a c)),
      {
        rw insert,
        have hl2 := asymm hl,
        split_ifs,
        contradiction,
        split,
        repeat {refl,},
      },
      rw hl1,
      apply insert_valid_aux2,
      exact hvs1,
      exact hvs2,
      exact hvs3,
      exact hl,
    },
    {
      cases ht with he hg,
      {
        have he1 : insert a (x :: c) = (x :: c),
        {
          rw insert,
          have he2 : ¬ (a < x) ∧ ¬ (x < a), 
          {
            rw incomp_iff_eq,
            symmetry,
            exact he,
          },
          cases he2 with he21 he22,
          split_ifs,
          split,
          repeat {refl},
        },
        rw he1,
        exact hvs3,
      },
      {
        have hg1 : insert a (x :: c) = (a :: x :: c),
        {
          rw insert, 
          split_ifs,
          repeat {split, refl,},
          repeat {refl,},
        },
        rw hg1,
        apply insert_in_middle,
        exact hg,
        exact hvs3,
      },
    },
  end 
#check insert_valid_aux3

-- insert is a valid set lemma
lemma insert_valid {a : α} [ha : linear_order α] {c : list α} : 
valid_set c → valid_set (insert a c) :=
begin
  intro hvs,
  induction c with z c,
  have ha : insert a list.nil = [a], from rfl,
  rw ha,
  simp,
  apply insert_valid_aux3,
  exact sublist_valid hvs,
  apply c_ih,
  exact sublist_valid hvs,
  exact hvs,
end

-- lemma made to simplify future lemmas
lemma insert_valid_set_aux {a z: α} {c : list α} [ha : linear_order α] : 
  valid_set (a :: z :: c) → valid_set (a :: c) :=
  begin
    intros hvs,
    simp,
    simp at hvs,
    cases hvs with hvs1 hvs2,
    cases hvs2 with hvs2 hvs3,
    split,
    {
      intro b1,
      specialize hvs1 b1,
      cases hvs1 with hvs11 hvs12,
      exact hvs12,
    },
    {
      exact hvs3,
    },
  end
#check insert_valid_set_aux



/-# Delete function and associated lemmas-/

-- delete function
def delete [ha : linear_order α] : α → list α → list α 
  |a list.nil := list.nil
  |a (x :: z) := (if a = x then z else
    (if a > x then (x :: (delete a z)) else (x :: z))) 
#check delete

-- New definition that delete is a subset of the original set
lemma delete_subset {a : α} {c : list α} [ha : linear_order α] : delete a c ⊆ c :=
  begin
    induction c with z c,
    rw delete,
    refl,
    rw delete,
    split_ifs,
    {
      simp,
    },
    {
      simp,
      have hc : c ⊆ z :: c, by simp,
      transitivity,
      exact c_ih,
      exact hc,
    },
    {
      refl,
    },
  end
#check delete_subset


-- delete lemmas
lemma delete_valid_aux1 {a b : α} {c : list α} [ha : linear_order α] :
  valid_set (a :: c) → valid_set (a :: delete b c) :=
  begin
    intro hvs,
    induction c with z c,
    rw delete,
    exact hvs,
    have hc := insert_valid_set_aux hvs,
    specialize c_ih hc,
    rw delete,
    split_ifs,
    {
      exact hc,
    },
    {
      simp at h_1,
      have hl := remove_from_middle hvs,
      have hz : valid_set (z :: c),
      {
        apply sublist_valid,
        exact hvs,
      },
      apply insert_in_middle,
      exact hl,
      rw valid_set,
      simp,
      rw valid_set at c_ih,
      simp at c_ih,
      cases c_ih with hc1 hc2,      
      split,
      {
        intros b1 hbin,
        rw valid_set at hz,
        simp at hz,
        cases hz with hz1 hz2,
        apply hz1,
        apply delete_subset,
        exact hbin,
      },
      {
        exact hc2,
      },
    },
    {
      exact hvs,
    },

  end
#check delete_valid_aux1


lemma delete_valid_aux2 {b : α} {c : list α} [ha : linear_order α] :
∀ (a : α), valid_set c → valid_set (delete a c) →
valid_set (b :: c) → valid_set (delete a (b :: c)) :=
  begin
    intros a hvs1 hvs2 hvs3,
    have h1 := delete_valid_aux1 hvs3,
    have ht := decidable.lt_trichotomy a b,
    cases ht with hl ht,
    {
      have hl1 : delete a (b :: c) = (b :: c),
      {
        rw delete,
        have hl2 := ne_of_lt hl,
        simp at hl2,
        split_ifs,
        have hl3 := asymm hl,
        contradiction,
        split,
        repeat {refl,},
      },
      rw hl1,
      exact hvs3,
    },
    {
      cases ht with he hg,
      {
        have he1 : delete a (b :: c) = c,
        {
          rw delete,
          split_ifs,
          refl,
        },
        rw he1,
        exact hvs1,
      },
      {
        have hg1 : delete a (b :: c) = b :: (delete a c),
        { 
          rw delete,
          simp,
          have hg2 := ne_of_lt hg,
          have hg3 : a ≠ b, symmetry, exact hg2,
          simp at hg3,
          split_ifs,
          split,
          repeat {refl,},
        },
        rw hg1,
        exact h1,
      },
    },
end
#check ne_of_gt

-- A set after deleting is still valid
lemma delete_valid {a : α} {c : list α} [ha : linear_order α] : 
valid_set c → valid_set (delete a c) :=
begin
  intro hvs,
  induction c with z c,
  {
    rw delete,
    exact hvs,
  },
  {
    have hc1 := sublist_valid hvs,
    have hc2 := c_ih hc1,
    exact delete_valid_aux2 a hc1 hc2 hvs,
  },
end
#check delete_valid


/-# Element function and associated lemmas-/

-- element function
def element [ha : linear_order α] : α → list α → Prop
  | a list.nil := false
  | a (x :: z) := (if (a = x) then (true) else
    (if (a > x) then (element a z) else (false)))
#check element

-- Delete lookup None auxiliary lemmas
lemma delete_lookup_None_aux1 {c : α} {b : list α} [ha : linear_order α] :
valid_set (c :: b) → element c b = false :=
begin
  intro hvs,
  induction b with a b,
  {
    refl,
  },
  {
    rw element,
    have hlt := remove_from_middle hvs,
    have hne := ne_of_lt hlt,
    simp,
    push_neg,
    split,
    exact hne,
    intro halt,
    have halt2 := asymm halt,
    contradiction,
  },
end
#check delete_lookup_None_aux1

lemma delete_lookup_None_aux2 {a c : α} {b : list α} [ha : linear_order α] :
(valid_set b → element a (delete a b) = false) → 
valid_set (c :: b) → element a (delete a (c :: b)) = false :=
begin
  intros hvsfalse hvs,
  have hvsb : valid_set b,
  apply sublist_valid,
  exact hvs,
  specialize hvsfalse hvsb,

  have ht := decidable.lt_trichotomy a c,
  cases ht with hl ht,
  {
    have hne := ne_of_lt hl,
    simp at hne,
    have hng := asymm hl,
    rw delete,
    split_ifs,
    rw element,
    split_ifs,
    refl,
  },
  {
    cases ht with he hg,
    {
      rw delete,
      split_ifs,
      apply delete_lookup_None_aux1,
      rw he,
      exact hvs,
    },
    {
      have hne := ne_of_gt hg,
      simp at hne,
      have hnl := asymm hg,
      rw delete,
      split_ifs,
      rw element,
      split_ifs,
      exact hvsfalse,
    },
  },
end
#check delete_lookup_None_aux2

-- Delete lookup None theorem
theorem delete_lookup_None {a : α} {b : list α} [ha : linear_order α] :
  valid_set b → element a (delete a b) = false :=
  begin
    induction b with c b,
    intro hvs,
    refl,
    intro hvs,
    apply delete_lookup_None_aux2,
    exact b_ih,
    exact hvs,
  end
#check delete_lookup_None

theorem insert_lookup_Some {a : α} {c : list α} [ha : linear_order α] :
  valid_set c → element a (insert a c) = true :=
  begin
    intro hvs,
    induction c with z c,
    {
      rw insert,
      have href : a = a, from rfl,
      rw element,
      split_ifs,
      refl,
    },
    {
      have hc := sublist_valid hvs,
      specialize c_ih hc,
      rw insert,
      split_ifs,
      {
        rw element,
        have href : a = a, from rfl,
        split_ifs,
        refl,
      },
      {
        rw element,
        split_ifs,
        refl,
        exact c_ih,
      },
      {
        have heq : a = z,
        {
          have ht := decidable.lt_trichotomy a z,
          cases ht with hl ht,
          contradiction,
          cases ht with he hg,
          exact he,
          contradiction,
        },
        rw element,
        split_ifs,
        refl,
      },
    },
  end
#check insert_lookup_Some

-- Auxiliary lemma for different delete lookup
lemma  different_delete_lookup_aux {a b x: α} {c : list α} [ha : linear_order α] :
  (valid_set c → a ≠ b → element a (delete b c) = element a c) →
  valid_set (x :: c) → a ≠ b → element a (delete b (x :: c)) = element a (x :: c) :=
  begin
    intros hvsimp hvs hne,
    have hc := sublist_valid hvs,
    specialize hvsimp hc,
    specialize hvsimp hne,
    rw element,
    split_ifs,
    {
      rw delete,
      split_ifs,
      {
        rw ← h_1 at h,
        contradiction,
      },
      repeat
      {
        rw element,
        split_ifs,
        refl,
      },
    },
    {
      rw delete,
      split_ifs,
      {
        refl,
      },
      {
        rw element,
        split_ifs,
        exact hvsimp,
      },
      {
        rw element,
        split_ifs,
        refl,
      },
    },
    {
      rw delete,
      have hal : a < x,
      {
        have ht := decidable.lt_trichotomy a x,
        cases ht with hl ht,
        exact hl,
        cases ht with he hg,
        repeat{contradiction,},
      },
      split_ifs,
      rw valid_set at hvs,
      simp at hvs,
      cases hvs with hvs1 hvs2,      
      apply delete_lookup_None_aux1,
      rw valid_set,
      simp,
      split,
      intro b1,
      intro hb,
      have hxl : x < b1,
      {
        apply hvs1,
        exact hb,
      },
      transitivity,
      exact hal,
      exact hxl,
      exact hvs2,
      repeat
      {
        rw element,
        split_ifs,
        refl,
      },
    },
  end
#check different_delete_lookup_aux

-- Theorem for different delete lookup
theorem different_delete_lookup {a b : α} {c : list α} [ha : linear_order α] :
  valid_set c → a ≠ b → element a (delete b c) = element a c :=
  begin
    intros hvs hne,
    induction c with z c,
    rw delete,
    apply different_delete_lookup_aux,
    have hc : valid_set c,
    apply sublist_valid,
    exact hvs,
    specialize c_ih hc,
    intros hvs2 hne2,
    exact c_ih,
    exact hvs,
    exact hne,
  end
#check different_delete_lookup

end SList