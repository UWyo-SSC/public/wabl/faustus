module Language.Faustus
    ( module Language.Faustus.Semantics
    )
where

import           Language.Faustus.Semantics
import           Language.Faustus.Compiler
