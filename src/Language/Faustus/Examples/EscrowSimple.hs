{-# LANGUAGE OverloadedStrings #-}
module Language.Faustus.Examples.EscrowSimple where

import           Language.Faustus
import           Language.Marlowe (Bound(..), TransactionInput(..), Input(..))
import qualified Language.Marlowe as M

{- What does the vanilla contract look like?
   Using layout for scoping here

When
  bobClaims
    when
      carolAgrees
        Pay "alice" "bob" price
      carolDisagrees
        Close "alice"
  aliceClaims
    when
      carolAgrees
        Close "alice"
      carolDisagrees
        Pay "alice" "bob" price

-}

contract :: Contract
contract = defineProcessClaim $ When [Case (Deposit (ConstantPubKey "alice") (ConstantPubKey "alice") price)
                      (When [ Case bobClaims (UseC (Identifier "processClaim") [PubKeyArgument . ConstantPubKey $ "bob", PubKeyArgument . ConstantPubKey $ "alice"])
                            , Case aliceClaims (UseC (Identifier "processClaim") [PubKeyArgument . ConstantPubKey $ "alice", PubKeyArgument . ConstantPubKey $ "bob"])
                            ]
                            90
                            Close)
                ]
                10
                Close

defineProcessClaim = LetC (Identifier "processClaim") [PubKeyParameter . Identifier $ "claimant", PubKeyParameter . Identifier $ "nonclaimant"]
  (When 
    [Case carolAgrees (Pay (UsePubKey . Identifier $ "nonclaimant") (Party . UsePubKey . Identifier $ "claimant") price Close)
    , Case carolDisagrees (Pay (UsePubKey . Identifier $ "claimant") (Party . UsePubKey . Identifier $ "nonclaimant") price Close)] 100 Close)

aliceClaims, bobClaims, carolAgrees, carolDisagrees :: Action

aliceClaims    = Choice (ChoiceId "claim" (ConstantPubKey "alice")) [Bound 0 0]
bobClaims      = Choice (ChoiceId "claim" (ConstantPubKey "bob"))   [Bound 0 0]
carolAgrees    = Choice (ChoiceId "agree" (ConstantPubKey "carol")) [Bound 0 0]
carolDisagrees = Choice (ChoiceId "agree" (ConstantPubKey "carol")) [Bound 1 1]


-- Value under escrow
price :: Value
price = Constant 450

runBobClaimCarolAgree = playTrace 0 contract [inputs]
  where inputs = TransactionInput {txInterval = M.SlotInterval 0 0, txInputs = [IDeposit "alice" "alice" 450, IChoice (M.ChoiceId "claim" "alice") 0, IChoice (M.ChoiceId "agree" "carol") 1]}
