{-# LANGUAGE OverloadedStrings #-}
module Language.Faustus.Examples.MultiSig where

import           Language.Faustus
import           Language.Marlowe (Bound(..), TransactionInput(..), Input(..))
import qualified Language.Marlowe as M
import qualified Data.Text as T

{- What does the vanilla contract look like?
   Using layout for scoping here

When
  bobClaims
    when
      carolAgrees
        Pay "alice" "bob" price
      carolDisagrees
        Close "alice"
  aliceClaims
    when
      carolAgrees
        Close "alice"
      carolDisagrees
        Pay "alice" "bob" price

-}

partys :: [PubKey]
partys = map (ConstantPubKey . T.pack . show) [0..4]

mPartys :: [M.PubKey]
mPartys = map (M.PubKey . T.pack . show) [0..4]

sendChoiceName :: M.ChoiceName
sendChoiceName = "sendMoney"

addAllChoices :: Value
addAllChoices = AddValue (AddValue (AddValue (AddValue (ChoiceValue (ChoiceId sendChoiceName (UsePubKey "claimant1"))) (ChoiceValue (ChoiceId sendChoiceName (UsePubKey "claimant2")))) (ChoiceValue (ChoiceId sendChoiceName (UsePubKey "claimant3")))) (ChoiceValue (ChoiceId sendChoiceName (UsePubKey "claimant4")))) (ChoiceValue (ChoiceId sendChoiceName (UsePubKey "claimant5")))

defineProcessFifthChoice :: Contract -> Contract
defineProcessFifthChoice = LetC (Identifier "processChoiceMade5") [PubKeyParameter "claimant1", PubKeyParameter "claimant2", PubKeyParameter "claimant3", PubKeyParameter "claimant4", PubKeyParameter "claimant5"]
  (If (ValueGE addAllChoices (Constant 3)) (Pay (ConstantPubKey "alice") (Party . ConstantPubKey $ "bob") price Close) Close)

defineProcessFourthChoice :: Contract -> Contract
defineProcessFourthChoice = LetC (Identifier "processChoiceMade4") [PubKeyParameter "claimant1", PubKeyParameter "claimant2", PubKeyParameter "claimant3", PubKeyParameter "claimant4", PubKeyParameter "nonclaimant1"]
  (When 
    [Case (makesClaim (UsePubKey "nonclaimant1")) (UseC "processChoiceMade5" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "claimant3", PubKeyArgument . UsePubKey $ "claimant4", PubKeyArgument . UsePubKey $ "nonclaimant1"])] 100 Close)

defineProcessThirdChoice :: Contract -> Contract
defineProcessThirdChoice = LetC (Identifier "processChoiceMade3") [PubKeyParameter "claimant1", PubKeyParameter "claimant2", PubKeyParameter "claimant3", PubKeyParameter "nonclaimant1", PubKeyParameter "nonclaimant2"]
  (When 
    [Case (makesClaim (UsePubKey "nonclaimant1")) (UseC "processChoiceMade4" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "claimant3", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2"])
    , Case (makesClaim (UsePubKey "nonclaimant2")) (UseC "processChoiceMade4" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "claimant3", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant1"])] 100 Close)

defineProcessSecondChoice :: Contract -> Contract
defineProcessSecondChoice = LetC (Identifier "processChoiceMade2") [PubKeyParameter "claimant1", PubKeyParameter "claimant2", PubKeyParameter "nonclaimant1", PubKeyParameter "nonclaimant2", PubKeyParameter "nonclaimant3"]
  (When 
    [Case (makesClaim (UsePubKey "nonclaimant1")) (UseC "processChoiceMade3" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant3"])
    , Case (makesClaim (UsePubKey "nonclaimant2")) (UseC "processChoiceMade3" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant3"])
    , Case (makesClaim (UsePubKey "nonclaimant3")) (UseC "processChoiceMade3" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "claimant2", PubKeyArgument . UsePubKey $ "nonclaimant3", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2"])] 100 Close)

defineProcessFirstChoice :: Contract -> Contract
defineProcessFirstChoice = LetC (Identifier "processChoiceMade1") [PubKeyParameter "claimant1", PubKeyParameter "nonclaimant1", PubKeyParameter "nonclaimant2", PubKeyParameter "nonclaimant3", PubKeyParameter "nonclaimant4"]
  (When 
    [Case (makesClaim (UsePubKey "nonclaimant1")) (UseC "processChoiceMade2" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant3", PubKeyArgument . UsePubKey $ "nonclaimant4"])
    , Case (makesClaim (UsePubKey "nonclaimant2")) (UseC "processChoiceMade2" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant3", PubKeyArgument . UsePubKey $ "nonclaimant4"])
    , Case (makesClaim (UsePubKey "nonclaimant3")) (UseC "processChoiceMade2" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "nonclaimant3", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant4"])
    , Case (makesClaim (UsePubKey "nonclaimant4")) (UseC "processChoiceMade2" [PubKeyArgument . UsePubKey $ "claimant1", PubKeyArgument . UsePubKey $ "nonclaimant4", PubKeyArgument . UsePubKey $ "nonclaimant1", PubKeyArgument . UsePubKey $ "nonclaimant2", PubKeyArgument . UsePubKey $ "nonclaimant3"])] 100 Close)

contract :: Contract
contract = defineProcessFifthChoice . defineProcessFourthChoice . defineProcessThirdChoice . defineProcessSecondChoice . defineProcessFirstChoice $ When [Case (Deposit (ConstantPubKey "alice") (ConstantPubKey "alice") price) 
                      (When 
                        [Case (makesClaim $ partys !! 0) (UseC "processChoiceMade1" [PubKeyArgument $ partys !! 0, PubKeyArgument $ partys !! 1, PubKeyArgument $ partys !! 2, PubKeyArgument $ partys !! 3, PubKeyArgument $ partys !! 4])
                        , Case (makesClaim $ partys !! 1) (UseC "processChoiceMade1" [PubKeyArgument $ partys !! 1, PubKeyArgument $ partys !! 0, PubKeyArgument $ partys !! 2, PubKeyArgument $ partys !! 3, PubKeyArgument $ partys !! 4])
                        , Case (makesClaim $ partys !! 2) (UseC "processChoiceMade1" [PubKeyArgument $ partys !! 2, PubKeyArgument $ partys !! 0, PubKeyArgument $ partys !! 1, PubKeyArgument $ partys !! 3, PubKeyArgument $ partys !! 4])
                        , Case (makesClaim $ partys !! 3) (UseC "processChoiceMade1" [PubKeyArgument $ partys !! 3, PubKeyArgument $ partys !! 0, PubKeyArgument $ partys !! 1, PubKeyArgument $ partys !! 2, PubKeyArgument $ partys !! 4])
                        , Case (makesClaim $ partys !! 4) (UseC "processChoiceMade1" [PubKeyArgument $ partys !! 4, PubKeyArgument $ partys !! 0, PubKeyArgument $ partys !! 1, PubKeyArgument $ partys !! 2, PubKeyArgument $ partys !! 3])] 100 Close)
                ]
                10
                Close

makesClaim :: PubKey -> Action
makesClaim p = Choice (ChoiceId sendChoiceName p) [Bound 0 1]


-- Value under escrow
price :: Value
price = Constant 450

runSendToBobClaims = playTrace 0 contract [inputs]
  where inputs = TransactionInput {txInterval = M.SlotInterval 0 0, txInputs = [IDeposit "alice" "alice" 450,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 0)) 1,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 1)) 0,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 2)) 1,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 3)) 0,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 4)) 1]}
                                                                                
runSendToAliceClaims = playTrace 0 contract [inputs]
  where inputs = TransactionInput {txInterval = M.SlotInterval 0 0, txInputs = [IDeposit "alice" "alice" 450,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 0)) 0,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 1)) 1,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 2)) 0,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 3)) 1,
                                                                                IChoice (M.ChoiceId sendChoiceName (mPartys !! 4)) 0]}

{-
LetC "processChoiceMade5" [(PubKeyParameter "claimant1"), (PubKeyParameter "claimant2"), (PubKeyParameter "claimant3"), (PubKeyParameter "claimant4"), (PubKeyParameter "claimant5")]
  (If
     (ValueGE
        (AddValue
           (AddValue
              (AddValue
                 (AddValue
                    (ChoiceValue
                       (ChoiceId "sendMoney" (UsePubKey "claimant1")))
                    (ChoiceValue
                       (ChoiceId "sendMoney" (UsePubKey "claimant2"))))
                 (ChoiceValue
                    (ChoiceId "sendMoney" (UsePubKey "claimant3"))))
              (ChoiceValue
                 (ChoiceId "sendMoney" (UsePubKey "claimant4"))))
           (ChoiceValue
              (ChoiceId "sendMoney" (UsePubKey "claimant5"))))
        (Constant 3))
     (Pay (ConstantPubKey "alice")
        (Party (ConstantPubKey "bob"))
        (Constant 450) Close) Close)
  (LetC "processChoiceMade4" [(PubKeyParameter "claimant1"), (PubKeyParameter "claimant2"), (PubKeyParameter "claimant3"), (PubKeyParameter "claimant4"), (PubKeyParameter "nonclaimant1")]
     (When [
        (Case
           (Choice
              (ChoiceId "sendMoney" (UsePubKey "nonclaimant1")) [
              (Bound 0 1)])
           (UseC "processChoiceMade5" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "claimant3")),(PubKeyArgument(UsePubKey "claimant4")),(PubKeyArgument(UsePubKey "nonclaimant1"))]))] 100 Close)
     (LetC "processChoiceMade3" [(PubKeyParameter "claimant1"),(PubKeyParameter "claimant2"),(PubKeyParameter "claimant3"),(PubKeyParameter "nonclaimant1"),(PubKeyParameter "nonclaimant2")]
        (When [
              (Case
                 (Choice
                    (ChoiceId "sendMoney" (UsePubKey "nonclaimant1")) [
                    (Bound 0 1)])
                 (UseC "processChoiceMade4" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "claimant3")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2"))]))
              ,
              (Case
                 (Choice
                    (ChoiceId "sendMoney" (UsePubKey "nonclaimant2")) [
                    (Bound 0 1)])
                 (UseC "processChoiceMade4" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "claimant3")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant1"))]))] 100 Close)
        (LetC "processChoiceMade2" [(PubKeyParameter "claimant1"),(PubKeyParameter "claimant2"),(PubKeyParameter "nonclaimant1"),(PubKeyParameter "nonclaimant2"),(PubKeyParameter "nonclaimant3")]
           (When [
                 (Case
                    (Choice
                       (ChoiceId "sendMoney" (UsePubKey "nonclaimant1")) [
                       (Bound 0 1)])
                    (UseC "processChoiceMade3" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant3"))]))
                 ,
                 (Case
                    (Choice
                       (ChoiceId "sendMoney"(UsePubKey "nonclaimant2")) [
                       (Bound 0 1)])
                    (UseC "processChoiceMade3" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant3"))]))
                 ,
                 (Case
                    (Choice
                       (ChoiceId "sendMoney"(UsePubKey "nonclaimant3")) [
                       (Bound 0 1)])
                    (UseC "processChoiceMade3" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "claimant2")),(PubKeyArgument(UsePubKey "nonclaimant3")),
                                               (PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2"))]))] 100 Close)
           (LetC "processChoiceMade1" [(PubKeyParameter "claimant1"),(PubKeyParameter "nonclaimant1"),(PubKeyParameter "nonclaimant2"),(PubKeyParameter "nonclaimant3"),(PubKeyParameter "nonclaimant4")]
              (When [
                    (Case
                       (Choice
                          (ChoiceId "sendMoney"(UsePubKey "nonclaimant1")) [
                          (Bound 0 1)])
                       (UseC "processChoiceMade2" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant3")),(PubKeyArgument(UsePubKey "nonclaimant4"))]))
                    ,
                    (Case
                       (Choice
                          (ChoiceId "sendMoney"(UsePubKey "nonclaimant2")) [
                          (Bound 0 1)])
                       (UseC "processChoiceMade2" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant3")),(PubKeyArgument(UsePubKey "nonclaimant4"))]))
                    ,
                    (Case
                       (Choice
                          (ChoiceId "sendMoney"(UsePubKey "nonclaimant3")) [
                          (Bound 0 1)])
                       (UseC "processChoiceMade2" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "nonclaimant3")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant4"))]))
                    ,
                    (Case
                       (Choice
                          (ChoiceId "sendMoney"(UsePubKey "nonclaimant4")) [
                          (Bound 0 1)])
                       (UseC "processChoiceMade2" [(PubKeyArgument(UsePubKey "claimant1")),(PubKeyArgument(UsePubKey "nonclaimant4")),(PubKeyArgument(UsePubKey "nonclaimant1")),(PubKeyArgument(UsePubKey "nonclaimant2")),(PubKeyArgument(UsePubKey "nonclaimant3"))]))] 100 Close)
              (When [
                 (Case
                    (Deposit(ConstantPubKey "alice")(ConstantPubKey "alice")(Constant 450))
                    (When [
                          (Case
                             (Choice
                                (ChoiceId "sendMoney"(ConstantPubKey "0")) [
                                (Bound 0 1)])
                             (UseC "processChoiceMade1" [(PubKeyArgument(ConstantPubKey "0")),(PubKeyArgument(ConstantPubKey "1")),(PubKeyArgument(ConstantPubKey "2")),(PubKeyArgument(ConstantPubKey "3")),(PubKeyArgument(ConstantPubKey "4"))]))
                          ,
                          (Case
                             (Choice
                                (ChoiceId "sendMoney"(ConstantPubKey "1")) [
                                (Bound 0 1)])
                             (UseC "processChoiceMade1" [(PubKeyArgument(ConstantPubKey "1")),(PubKeyArgument(ConstantPubKey "0")),(PubKeyArgument(ConstantPubKey "2")),(PubKeyArgument(ConstantPubKey "3")),(PubKeyArgument(ConstantPubKey "4"))]))
                          ,
                          (Case
                             (Choice
                                (ChoiceId "sendMoney"(ConstantPubKey "2")) [
                                (Bound 0 1)])
                             (UseC "processChoiceMade1" [(PubKeyArgument(ConstantPubKey "2")),(PubKeyArgument(ConstantPubKey "0")),(PubKeyArgument(ConstantPubKey "1")),(PubKeyArgument(ConstantPubKey "3")),(PubKeyArgument(ConstantPubKey "4"))]))
                          ,
                          (Case
                             (Choice
                                (ChoiceId "sendMoney"(ConstantPubKey "3")) [
                                (Bound 0 1)])
                             (UseC "processChoiceMade1" [(PubKeyArgument(ConstantPubKey "3")),(PubKeyArgument(ConstantPubKey "0")),(PubKeyArgument(ConstantPubKey "1")),(PubKeyArgument(ConstantPubKey "2")),(PubKeyArgument(ConstantPubKey "4"))]))
                          ,
                          (Case
                             (Choice
                                (ChoiceId "sendMoney"(ConstantPubKey "4")) [
                                (Bound 0 1)])
                             (UseC "processChoiceMade1" [(PubKeyArgument(ConstantPubKey "4")),(PubKeyArgument(ConstantPubKey "0")),(PubKeyArgument(ConstantPubKey "1")),(PubKeyArgument(ConstantPubKey "2")),(PubKeyArgument(ConstantPubKey "3"))]))] 100 Close))] 10 Close)))))
 -}