{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -Wno-name-shadowing -Wno-orphans #-}
module Language.Faustus.Compiler where

import           Language.Faustus.Semantics
import           Language.Marlowe (Bound(..), TransactionInput(..), Input(..))
import qualified Language.Marlowe as M
import qualified Data.Text as T
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

compilePubKey :: PubKey -> State -> Maybe M.PubKey
compilePubKey (UsePubKey p) s = Map.lookup p (boundPubKeys s)
compilePubKey (ConstantPubKey p) s = return $ M.PubKey p

compilePayee :: Payee -> State -> Maybe M.Payee
compilePayee (Account p) s = M.Account <$> compilePubKey p s
compilePayee (Party p) s = M.Party <$> compilePubKey p s

compileChoiceId :: ChoiceId -> State -> Maybe M.ChoiceId
compileChoiceId (ChoiceId choiceName p) s = M.ChoiceId choiceName <$> compilePubKey p s

compileValue :: Value -> State -> Maybe M.Value
compileValue (AvailableMoney p) s = M.AvailableMoney <$> compilePubKey p s
compileValue (Constant i) s = return $ M.Constant i
compileValue (NegValue v) s = M.NegValue <$> compileValue v s
compileValue (AddValue v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.AddValue compiledV1 compiledV2
compileValue (SubValue v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.SubValue compiledV1 compiledV2
compileValue (MulValue v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.MulValue compiledV1 compiledV2
compileValue (Scale r v) s = M.Scale r <$> compileValue v s
compileValue (ChoiceValue c) s = M.ChoiceValue <$> compileChoiceId c s
compileValue SlotIntervalStart s = return $ M.SlotIntervalStart
compileValue SlotIntervalEnd s = return $ M.SlotIntervalEnd
compileValue (UseValue (Identifier valId)) s = return $ M.UseValue . M.ValueId $ valId
compileValue (Cond obs v1 v2) s = do
    compiledObs <- compileObservation obs s
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.Cond compiledObs compiledV1 compiledV2

compileObservation :: Observation -> State -> Maybe M.Observation
compileObservation (AndObs o1 o2) s = do
    compiledO1 <- compileObservation o1 s
    compiledO2 <- compileObservation o2 s
    return $ M.AndObs compiledO1 compiledO2
compileObservation (OrObs o1 o2) s = do
    compiledO1 <- compileObservation o1 s
    compiledO2 <- compileObservation o2 s
    return $ M.OrObs compiledO1 compiledO2
compileObservation (NotObs o) s = M.NotObs <$> compileObservation o s
compileObservation (ValueGE v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.ValueGE compiledV1 compiledV2
compileObservation (ValueGT v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.ValueGT compiledV1 compiledV2
compileObservation (ValueLT v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.ValueLT compiledV1 compiledV2
compileObservation (ValueLE v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.ValueLE compiledV1 compiledV2
compileObservation (ValueEQ v1 v2) s = do
    compiledV1 <- compileValue v1 s
    compiledV2 <- compileValue v2 s
    return $ M.ValueEQ compiledV1 compiledV2
compileObservation (UseObservation (Identifier obsId)) s = return $ M.NotObs (M.ValueEQ (M.UseValue . M.ValueId $ obsId) (M.Constant 0))
compileObservation (ChoseSomething c) s = M.ChoseSomething <$> compileChoiceId c s
compileObservation TrueObs s = return M.TrueObs
compileObservation FalseObs s = return M.FalseObs

compileAction :: Action -> State -> Maybe M.Action
compileAction (Deposit fromPk toPk v) s = do
    compiledFromPk <- compilePubKey fromPk s
    compiledToPk <- compilePubKey toPk s
    compiledV <- compileValue v s
    return $ M.Deposit compiledFromPk compiledToPk compiledV
compileAction (Choice choiceId bounds) s = do
    compiledChoiceId <- compileChoiceId choiceId s
    return $ M.Choice compiledChoiceId bounds
compileAction (Notify obs) s = M.Notify <$> compileObservation obs s

argumentsToState :: [Parameter] -> [Argument] -> State -> Maybe State
argumentsToState [] [] s = return s
argumentsToState (p:ps) [] s = fail "Not enough arguments."
argumentsToState [] (a:as) s = fail "Too many arguments."
argumentsToState (PubKeyParameter pkId:ps) (PubKeyArgument pk:as) s = do
    compiledPk <- compilePubKey pk s
    argumentsToState ps as (s{boundPubKeys = Map.insert pkId compiledPk (boundPubKeys s)})
argumentsToState (ValueParameter vid:ps) (ValueArgument v:as) s = argumentsToState ps as s
argumentsToState (ObservationParameter vid:ps) (ObservationArgument v:as) s = argumentsToState ps as s
argumentsToState ps as s = fail "Mismatch parameters and arguments."

compileArguments :: [Parameter] -> [Argument] -> State -> M.Contract -> Maybe M.Contract
compileArguments [] [] s c = return c
compileArguments (p:ps) [] s c = fail "Not enough arguments."
compileArguments [] (a:as) s c = fail "Too many arguments."
compileArguments (ValueParameter (Identifier valId):ps) (ValueArgument v:as) s c = do
    compiledV <- compileValue v s
    compiledRest <- compileArguments ps as s c
    return (M.Let (M.ValueId valId) compiledV compiledRest)
compileArguments (ObservationParameter (Identifier obsId):ps) (ObservationArgument o:as) s c = do
    compiledO <- compileObservation o s
    compiledRest <- compileArguments ps as s c
    return (M.Let (M.ValueId obsId) (M.Cond compiledO (M.Constant 1) (M.Constant 0)) compiledRest)
compileArguments (PubKeyParameter pkId:ps) (PubKeyArgument pk:as) s c = do
    compiledPk <- compilePubKey pk s
    compiledRest <- compileArguments ps as (s{boundPubKeys = Map.insert pkId compiledPk (boundPubKeys s)}) c
    return compiledRest
compileArguments ps as s c = fail "Mismatched parameters and arguments."

compile :: Contract -> [AbstractionInformation] -> State -> Maybe M.Contract
compile Close ctx s = return M.Close
compile (Pay accId payee val cont) ctx s = do
    compiledAccId <- compilePubKey accId s
    compiledPayee <- compilePayee payee s
    compiledVal <- compileValue val s
    compiledCont <- compile cont ctx s
    return $ M.Pay compiledAccId compiledPayee compiledVal compiledCont
compile (If obs trueCont falseCont) ctx s = do
    compiledObs <- compileObservation obs s
    compiledTrueCont <- compile trueCont ctx s
    compiledFalseCont <- compile falseCont ctx s
    return $ M.If compiledObs compiledTrueCont compiledFalseCont
compile (When cases t tCont) ctx s = do
    compiledCases <- compileCases cases ctx s
    compiledTCont <- compile tCont ctx s
    return $ M.When compiledCases t compiledTCont
compile (Let (Identifier valId) val cont) ctx s = do
    compiledVal <- compileValue val s
    compiledCont <- compile cont ctx s
    return $ M.Let (M.ValueId valId) compiledVal compiledCont
compile (LetObservation (Identifier obsId) obs cont) ctx s = do
    compiledObs <- compileObservation obs s
    compiledCont <- compile cont ctx s
    return $ M.Let (M.ValueId obsId) (M.Cond compiledObs (M.Constant 1) (M.Constant 0)) compiledCont
compile (LetPubKey pkId pk cont) ctx s = do
    compiledPk <- compilePubKey pk s
    compile cont ctx (s{boundPubKeys = Map.insert pkId compiledPk (boundPubKeys s)}) -- Only pubkeys have enough information to be determined at compile time.
compile (Assert obs cont) ctx s = do
    compiledObs <- compileObservation obs s
    compiledCont <- compile cont ctx s
    return $ M.Assert compiledObs compiledCont
compile (LetC cid params boundCon cont) ctx s = compile cont (ContractAbstraction cid params boundCon:ctx) s
compile (UseC cid args) ctx s = do
    (ContractAbstraction cid2 params boundCon, newCtx) <- lookupContractIdInformation ctx cid
    newS <- argumentsToState params args s
    compiledBoundCon <- compile boundCon newCtx newS
    compileArguments params args s compiledBoundCon

compileCases :: [Case] -> [AbstractionInformation] -> State -> Maybe [M.Case]
compileCases (Case a c:rest) ctx s = do
        compiledC <- compile c ctx s
        compiledA <- compileAction a s
        compiledRest <- compileCases rest ctx s
        return $ (M.Case compiledA compiledC:compiledRest)
compileCases [] ctx s = return []
